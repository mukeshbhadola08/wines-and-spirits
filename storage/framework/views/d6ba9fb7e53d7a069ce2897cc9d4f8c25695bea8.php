<?php $__env->startSection('header'); ?>
    <section class="content-header">
      <h1>
         News
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(backpack_url()); ?>"><?php echo e(config('backpack.base.project_name')); ?></a></li>
        <li class="active">News</li>
      </ol>
    </section>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<div class="body">
	<?php if(\Session::has('success')): ?>
        <div class="alert alert-success">
            <?php echo e(\Session::get('success')); ?>

        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-sm-12"><a href="<?php echo e(url('/admin/blogs/create')); ?>" class="btn btn-primary">Add News</a></div>
    </div><hr>

	<div class="row">
		<div class="col-sm-12">
			<?php if(isset($blogs) && count($blogs)>0): ?>
				<table class="table table-striped" id="blogs-table">
					<thead>
						<tr>
						  <th width="10%">Image</th>
						  <th width="10%">Slug</th>
						  <th width="15%">Title</th>
						  <th width="10%">Video</th>
						  <th width="15%">Summary</th>
						  <th width="7%" class="hidden">Visible</th>
						  <th width="8%" class="hidden">Dutch</th>
						  <th width="25%">Action</th>
						</tr>
					</thead>
					<tbody>
						
					<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blogDetails): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

						<tr>
							<td>
								<?php if($blogDetails->slider_id): ?>
									<img class="media-object" src="<?php echo e(App\Http\Controllers\SliderController::getImage($blogDetails->slider_id)); ?>" alt="no-img" height="100" width="100">
								<?php else: ?>
									<img class="media-object" src="<?php echo e(action('BlogController@getImage',$blogDetails->id)); ?>" alt="no-img" height="100" width="100">
								<?php endif; ?>
							</td>
							<td><?php echo e($blogDetails->clean_url); ?></td>
							<td><?php echo e($blogDetails->blog_title); ?></td>
							<td><?php echo e($blogDetails->video_url); ?></td>
							<td><?php echo e($blogDetails->short_description); ?></td>
							<td class="hidden"><?php if($blogDetails->is_visible==1): ?> YES <?php else: ?> No <?php endif; ?></td>
							<td class="hidden">
								<?php
								 $parentDetails = App\Blog::where('parent_lang_id',$blogDetails->id)->first();
								?>
								<?php if(empty($parentDetails)): ?>
									<a href="<?php echo e(action('BlogController@addInSpanish',$blogDetails->id)); ?>" class="btn btn-info" ><i class="fa fa-language" ></i>&nbsp;Add Dutch</a>
								<?php else: ?>
									<a href="<?php echo e(action('BlogController@edit',$parentDetails->id)); ?>" class="btn btn-info"><i class="fa fa-language" ></i>&nbsp;Edit Dutch</a>
								<?php endif; ?>
							</td>
							<td>
								<a href="<?php echo e(action('BlogController@view',$blogDetails->clean_url)); ?>" class="btn btn-success" target="_blank"><i class="fa fa-eye"></i>&nbsp;View</a>&nbsp;
								<a href="<?php echo e(action('BlogController@edit',$blogDetails->id)); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>&nbsp;								
								<a href="<?php echo e(action('BlogController@delete',$blogDetails->id)); ?>" class="btn btn-danger delete_blog"><i class="fa fa-trash-o"></i>&nbsp;DELETE</a>
								
							</td>
						</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
					</tbody>
				</table>
			<?php else: ?>
			<div class="alert alert-info">
				There are no blogs added yet.
			</div>
			<?php endif; ?>
		</div>
	</div>

</div >
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
<script src="<?php echo e(asset('vendor/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('vendor/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<script>
$(document).ready(function() {
    $('#blogs-table').DataTable( {
        "order": [[ 2, "asc" ]]
    });
	$('#blogs-table_paginate').addClass('pull-right');
});	
jQuery(document).on('click','.delete_blog',function(e){
	if(confirm("Are you sure want to delete this blog?")){
		return true;
	} else{
		return false;
	}
});		
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
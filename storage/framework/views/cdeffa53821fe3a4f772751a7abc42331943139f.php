<?php $__env->startSection('header'); ?>
<section class="content-header">
    <h1>Add Product</h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo e(backpack_url()); ?>"><?php echo e(config('backpack.base.project_name')); ?></a></li>
        <li><a href="<?php echo e(backpack_url('pages')); ?>">Products</a></li>
        <li>Add Product</li>
    </ol>
</section>
<?php $__env->stopSection(); ?>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

<?php $__env->startSection('content'); ?>

<div class="body">
    
    <div class="row">
        <div class="col-sm-12">
            <form method="post" action='<?php echo e(backpack_url("product/$cat_id/create")); ?>' enctype="multipart/form-data">
            <div class="form-group">
                <input type="hidden" value="<?php echo e(csrf_token()); ?>" name="_token" />
                <label for="title">Name:</label>
                <input type="text" class="form-control" name="product_title" value ="<?php echo e(old('product_title')); ?>" />
                <?php if($errors->has('product_title')): ?>
                    <span class="invalid-feedback text-danger">
                        <strong><?php echo e($errors->first('product_title')); ?></strong>
                    </span>
                <?php endif; ?>
            </div>
            <div class="form-group">
                <label for="description">Description:</label>
                <textarea name="description" class="form-control" id="page-ckeditor"><?php echo e(old('description')); ?></textarea>
                <?php if($errors->has('description')): ?>
                    <span class="invalid-feedback text-danger">
                        <strong><?php echo e($errors->first('description')); ?></strong>
                    </span>
                <?php endif; ?>
            </div>
            <div class="form-group images"> 
                <label for="default_image">Photo:</label>
                <input type="file" onchange="$('.added-img').remove();" name="image" id="upload" />
                <?php if($errors->has('image')): ?>
                    <span class="invalid-feedback text-danger">
                        <strong><?php echo e($errors->first('image')); ?></strong>
                    </span>
                <?php endif; ?>
            </div>


            <!-- <div class="form-group">   
                <label for="image">Default Image:</label>
                
                <img class="media-object" src="<?php echo asset("img/intro-img-top.png"); ?>" alt="no-img" height="150" width="150">
            </div>
             
            <div class="form-group images"> 
                <label for="image">Change Default Image:</label>
                <input type="file" class="form-control" onchange="$('.added-img').remove();" name="image" id="upload" />
                <?php if($errors->has('image')): ?>
                    <span class="invalid-feedback text-danger">
                        <strong><?php echo e($errors->first('image')); ?></strong>
                    </span>
                <?php endif; ?>
            </div>
            <div class="form-group">    
                <label>OR</label>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-primary addimage" data-toggle="modal" data-target="#portfolioModal">Add Media</button>
            </div> -->
            <div class="form-group">
                <label for="region">Region:</label>&nbsp;
                <input type="text" class="form-control" name="region" value ="<?php echo e(old('region')); ?>" />
                <?php if($errors->has('region')): ?>
                    <span class="invalid-feedback text-danger">
                        <strong><?php echo e($errors->first('region')); ?></strong>
                    </span>
                <?php endif; ?>
            </div>
            <div class="form-group winery">
                <label for="varietal">Varietal:</label>&nbsp;
                 <input type="text" class="form-control" name="varietal" value ="<?php echo e(old('varietal')); ?>" />
                <?php if($errors->has('varietal')): ?>
                    <span class="invalid-feedback text-danger">
                        <strong><?php echo e($errors->first('varietal')); ?></strong>
                    </span>
                <?php endif; ?>
            </div>
            <div class="form-group">
                <label for="country">Country:</label>&nbsp;
                <select name="country" id="country">
                    <option>SELECT</option>
                    <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option><?php echo e($country->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <?php if($errors->has('country')): ?>
                    <span class="invalid-feedback text-danger">
                        <strong><?php echo e($errors->first('country')); ?></strong>
                    </span>
                <?php endif; ?>
            </div>
            <input type ="hidden" name="added_by" value="<?php echo e(Auth::user()->id); ?>" / >
            <input type ="hidden" name="cat_id" value="<?php echo e($cat_id); ?>" / >
            <button type="submit" class="btn btn-primary">Create</button>
            </form>
        </div>
    </div>

</div>


<script>
    window.onload = function() {
        CKEDITOR.replace( 'page-ckeditor', {
            filebrowserUploadUrl: '<?php echo e(route("upload",["_token" => csrf_token()])); ?>'
        });
    };
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
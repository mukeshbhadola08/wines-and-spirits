<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>Edit Routine</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(backpack_url()); ?>"><?php echo e(config('backpack.base.project_name')); ?></a></li>
		<li><a href="<?php echo e(backpack_url('pages')); ?>">Routines</a></li>
		<li>Edit Routine</li>
	</ol>
</section>
<?php $__env->stopSection(); ?>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

<?php $__env->startSection('content'); ?>
<div class="body">
	<?php if(\Session::has('success')): ?>
        <div class="alert alert-success">
            <?php echo e(\Session::get('success')); ?>

        </div>
    <?php endif; ?>



	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="<?php echo e(action('RoutinesController@update',$id)); ?>" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="<?php echo e(csrf_token()); ?>" name="_token" />
				<label for="title">Routine Name:</label>
				<input type="text" class="form-control" name="title" value="<?php echo e($routines->title); ?>"/>
				<?php if($errors->has('title')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('title')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group">
				<label for="description">Description:</label>
				<textarea name="description" class="form-control"><?php echo e($routines->description); ?></textarea>
				<?php if($errors->has('description')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('description')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			
			<div class="form-group">
			
				<label for="working_on">Working On:</label>
				<textarea name="working_on" class="form-control"><?php echo e($routines->working_on); ?></textarea>
				<?php if($errors->has('working_on')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('working_on')); ?></strong>
					</span>
				<?php endif; ?>
			</div>

			<div class="form-group">
				<label for="additional_skills">Additional Skills:</label>
				<input type="text" class="form-control" name="additional_skills" value ="<?php echo e($routines->additional_skills); ?>" />
			</div>
			<div class="form-group">
				<label for="level">Level:</label>
				<input type="text" class="form-control" name="level" value ="<?php echo e($routines->level); ?>" />
			</div>
			<div class="form-group">	
				<label for="video_url">Youtube Video Url:</label>
				<input type="url" class="form-control" name="video_url" id="video_url" value="<?php echo e($routines->video_url); ?>" />
			</div>
			<div class="form-group">	
				<label for="video_time">Video Time:</label>
				<input type="text" class="form-control" name="video_time" id="video_time" value="<?php echo e($routines->video_time); ?>" />
			</div>
			<div class="form-group">	
				<label for="image">Image:</label>
				<?php if($routines->slider_id): ?>
					<img class="media-object" alt="" height="150" width="150" src="<?php echo e(App\Http\Controllers\SliderController::getImage($routines->slider_id)); ?>">
				<?php else: ?>
					<img class="media-object" alt="" height="150" width="150" src="<?php echo e(action('RoutinesController@getImage',$routines->id)); ?>">
				<?php endif; ?>
			</div>
			<div class="form-group images">	
				<label for="image">Change Image:</label>
				<input type="file" class="form-control" onchange="$('.added-img').remove();" name="image" id="upload" />
				<?php if($errors->has('image')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('image')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group">	
				<label>OR</label>
			</div>
			<div class="form-group">
				<button type="button" class="btn btn-primary addimage" data-toggle="modal" data-target="#portfolioModal">Add Media</button>
			</div>


			<div class="form-group hidden">
				
				<label for="visible">Visible:</label>
				<?php if($routines->is_visible==1): ?>
					<input type="checkbox"  name="is_visible" value="1" checked="checked"  />
				<?php else: ?>
					<input type="checkbox"  name="is_visible" value="1"  />
				<?php endif; ?>
			</div>
		
			<button type="submit" class="btn btn-primary">Update</button>
			</form>
		</div>
	</div>

</div>

<script>
	CKEDITOR.replace( 'page-ckeditor' );
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
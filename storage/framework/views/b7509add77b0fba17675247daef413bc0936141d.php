<?php $__env->startSection('header'); ?>

    <section class="content-header">

      <h1>

		Edit Navigation Menu

      </h1>

      <ol class="breadcrumb">

        <li><a href="<?php echo e(backpack_url()); ?>"><?php echo e(config('backpack.base.project_name')); ?></a></li>

        <li class="active">Navigations  </li>

      </ol>

    </section>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>

	<?php if($errors->any()): ?>

	<div class="alert alert-danger">

        <ul>

		<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

			<li><?php echo e($error); ?></li>

		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

		</ul>

	</div><br />

	<?php endif; ?>

    <div class="row">

		<div class="col-sm-12">

			<form method="post" class="form-horizontal" action="<?php echo e(action('NavigationController@updateNavigation',$navigation->id)); ?>" enctype="multipart/form-data">

			<div class="form-group">

				<input type="hidden" value="<?php echo e(csrf_token()); ?>" name="_token" />

				<div class="col-sm-2">

					<label for="title">Navigation Title:</label>

				</div>

				<div class="col-sm-8">

					<input type="text" class="form-control" name="title" value="<?php echo e($navigation->title); ?>"/>

				</div>

			</div>

			<div class="form-group hidden">

				<div class="col-sm-2">

					<label for="title">Do you want to show:</label>

				</div>

				<div class="col-sm-8">

					 Page 

					<!-- <input type="radio" name="type" value="2" class="nav_type" <?php if($navigation->type == 2): ?> checked <?php endif; ?>> Blog  -->

				</div>

			</div>

			<input type="hidden" name="type" value="1" class="nav_type" <?php if($navigation->type == 1): ?> checked <?php endif; ?>  >
			
			<div id="page_form_fields" <?php if($navigation->type == 2): ?> style="display:none;" <?php endif; ?> >
				<div class="form-group">
				<div class="col-sm-2 ">
				<label for="Page">Choose Page Link: </label>
				</div>
				<div class="col-sm-8">
				<select class="form-control" name="page" >
					 <option value="homepage" <?php if($navigation->static_page_name == 'homepage'): ?> selected <?php endif; ?> >Home Page</option>
					 <option value="eventslisting" <?php if($navigation->static_page_name == 'eventslisting'): ?> selected <?php endif; ?>>Events Listing</option>
					 <option value="resultslisting" <?php if($navigation->static_page_name == 'resultslisting'): ?> selected <?php endif; ?>>Results Listing</option>
					 <option value="portfoliolisting" <?php if($navigation->static_page_name == 'portfoliolisting'): ?> selected <?php endif; ?>>Portfolio Listing</option>
					 <option value="newslisting" <?php if($navigation->static_page_name == 'newslisting'): ?> selected <?php endif; ?>>News Listing</option>
					 <?php if(isset($pages) && count($pages)>0): ?>
						<optgroup label="Pages">
						<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="-page-<?php echo e($p->id); ?>"><?php echo e($p->page_title); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</optgroup>
					 <?php endif; ?>
					 
					  <?php if(isset($blogs) && count($blogs)>0): ?>
						<optgroup label="News">
						<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="-blog-<?php echo e($p->id); ?>"><?php echo e($p->blog_title); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</optgroup>
					 <?php endif; ?>
					 
				</select>
				</div>
			</div>
			
				<!-- <div class="col-sm-2">

				<label for="Page">Page:</label>

				</div>

				<div class="col-sm-8">

				<select class="form-control" name="page" >

					<option value="">Select Page</option>

					 <?php if(isset($pages) && count($pages)>0): ?>

						<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

							<?php

								$selected = '';

								if($navigation->type == 1 && $navigation->reference_type_id == $p->id){

									$selected = 'selected="selected"';

								}

							?>

							<option value="<?php echo e($p->id); ?>" <?php echo e($selected); ?> ><?php echo e($p->page_title); ?></option>

						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

					 <?php endif; ?>

				</select>

				</div> 

			</div>-->



			<!-- <div class="form-group" id="blog_form_fields" <?php if($navigation->type == 1): ?> style="display:none;" <?php endif; ?> >

				<div class="col-sm-2">

				<label for="Page">Blog:</label>

				</div>

				<div class="col-sm-8">

				<select class="form-control" name="blog" >

					<option value="">Select Blog</option>

					 <?php if(isset($blogs) && count($blogs)>0): ?>

						<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

							<?php

								$selected = '';

								if($navigation->type == 2 && $navigation->reference_type_id == $p->id){

									$selected = 'selected="selected"';

								}

							?>

							<option value="<?php echo e($p->id); ?>" <?php echo e($selected); ?> ><?php echo e($p->blog_title); ?></option>

						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

					 <?php endif; ?>

				</select>

				</div>

			</div> -->

			

			<div class="form-group hidden">

				<div class="col-sm-2">

				<label for="visible">Visible:</label>

				</div>

				<div class="col-sm-8">

					<?php if($navigation->is_visible == 1): ?>

						<input type="checkbox"  name="visible" value="1" checked="checked" />

					<?php else: ?>

						<input type="checkbox"  name="visible" value="1" />

					<?php endif; ?>

				</div>

			</div>

			<?php if($navigation->parent_id == 0): ?>

				<div class="form-group hidden">

					<div class="col-sm-2">

					<label for="visible">Show In Header:</label>

					</div>

					<div class="col-sm-8">

						<?php if($navigation->show_header == 1): ?>

							<input type="checkbox"  name="show_header" value="1" checked="checked" />

						<?php else: ?>

							<input type="checkbox"  name="show_header" value="1" />

						<?php endif; ?>

					

					</div>

				</div>

				<!-- <div class="form-group hidden">

					<div class="col-sm-2">

					<label for="visible">Show In Footer:</label>

					</div>

					<div class="col-sm-8">

						<?php if($navigation->show_footer == 1): ?>

							<input type="checkbox"  name="show_footer" value="1" checked="checked" />

						<?php else: ?>

							<input type="checkbox"  name="show_footer" value="1" />

						<?php endif; ?>

					</div>

				</div> -->

			<?php endif; ?>



			<input type="hidden" name="parent" value="<?php echo e($navigation->parent_id); ?>" />

			<button type="submit" class="btn btn-primary">Update</button>

			</form>

		</div>

	</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>

<script>



		 jQuery(document).on('change','.nav_type',function(){

			

			if (this.value == 1) {

				$('#page_form_fields').show();

				$('#blog_form_fields').hide();

			}

			else {

				$('#page_form_fields').hide();

				$('#blog_form_fields').show();

			}

		}); 

		

</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
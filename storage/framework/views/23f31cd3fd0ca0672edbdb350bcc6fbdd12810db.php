<?php echo $__env->make('site.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	
	<!-- page title -->
		<!-- <div class="heading-img">
			<div class="heading heading-bg-img">
				<h2><?php echo e($page->page_title); ?></h2>
			</div>
		</div> -->
		<!-- /page title -->
	<!-- intro -->

	<section id="intro" class="clearfix">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 intro-img order-md-first order-last">
					<?php if($page->slider_id): ?>
						<img  alt="" class="img-fluid wow fadeInUp" src="<?php echo e(App\Http\Controllers\SliderController::getImage($page->slider_id)); ?>">
					<?php else: ?>
						<img  alt="" class="img-fluid wow fadeInUp" src="<?php echo e(action('PageController@getImage',$page->id)); ?>">
					<?php endif; ?>
				</div>
				<div class="col-sm-12 intro-info  wow fadeInDown order-md-last order-first ">
					<h2><?php echo e($page->page_title); ?> <?php if($page->clean_url == "about"){ ?><?php } ?> </h2>
				</div>
			</div>

		</div>
	</section>
	<!-- /intro -->
	<!-- main content -->
	<section class="inner-page-section">
		<div class="container">
			<?php if($page->clean_url == "contact-us"){ ?>
				
		
				<div class="row justify-content-between">
					<div class="col-12 col-sm-5 contact-form">

						<?php if($errors->any()): ?>
							<div class="alert alert-danger">
								<ul>
									<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<li><?php echo e($error); ?></li>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</ul>
							</div>
						<?php endif; ?>

						<form method="post" action="<?php echo e(action('PageController@view','contact-us')); ?>" enctype="multipart/form-data">
							<input type="hidden" value="<?php echo e(csrf_token()); ?>" name="_token" />
							<div class="input-group">
								<div class="input-group-addon"><i aria-hidden="true" class="fa fa-user"></i></div>
								<input class="form-control" placeholder="Name" name="contact_name" type="text">
							</div>
							<div class="input-group">
								<div class="input-group-addon"><i aria-hidden="true" class="fa fa-envelope-o"></i></div>
								<input class="form-control" placeholder="Email" name="contact_email" type="text">
							</div>
							<div class="input-group">
								<div class="input-group-addon"><i aria-hidden="true" class="fa fa-phone"></i></div>
								<input class="form-control" placeholder="Phone Number" name="contact_phone" type="text">
							</div>
							<div class="input-group">
								<div class="input-group-addon"><i aria-hidden="true" class="fa fa-comment"></i></div>
								<textarea class="form-control" placeholder="Message" name="contact_message"></textarea>
							</div>
							<div class="" style="margin-left:50px;">
								<input class="btn submit-contact-home btn-lg" value="SUBMIT" type="submit"> 
							</div>
						</form>
					</div>

					<div class="col-12 col-sm-6 info-rental">
						<div>
							
							<strong>Address</strong> 
							<p><?php echo nl2br(config('constants.ADDRESS')); ?></p>
						</div>
						
							<strong>Hours of Operation</strong>
							<p>Monday - Sunday, 8:00 a.m. - 8:00 p.m.<br>
							Out-Of-Hours Consultations Available On Evenings
						</p>
						
						<p>
							<strong>Email:- </strong> <?php echo e(config('constants.ADMIN_EMAIL')); ?>

						</p>
						
						<p>
							<strong>Call:- </strong> <?php echo e(config('constants.PHONE_NO')); ?> 
						</p>
					</div>
				</div>
	
				<?php } else if($page->clean_url == 'about'){ ?>

		<!-- about sid-->			
			<div class="row row-flex ">
			<div class="col-sm-4 col-12 wow fadeInDown ">
				<!-- Name -->
				<?php if(config('constants.NAME')): ?>
					<div class="features-about">
						<div class="feature-img">

							<img src="<?php echo asset("dist/images/user.svg"); ?>">							
						</div>
						<div class="feature-content">
							<p>Name:</p>
							<h4><?php echo e(config('constants.NAME')); ?></h4>
						</div>
					</div>
				<?php endif; ?>
				<?php if(config('constants.CONTACT')): ?>
				<!-- Contact -->
					<div class="features-about">
						<div class="feature-img">
							<img src="<?php echo asset("dist/images/contact-icon.svg"); ?>">
						</div>
						<div class="feature-content">
							<p style="width: 92%;"><span style="color:#bcbcbc;">Email:</span>
								<a class="pull-right" style="font-size: 14px; margin-top: 2px;" href="mailto:'<?php echo e(config('constants.CONTACT')); ?>'">Send Mail</a></p>
							<h4><a style="color: #ee8e6c;" href="mailto:'<?php echo e(config('constants.CONTACT')); ?>'"><?php echo e(config('constants.CONTACT')); ?></a></h4>
						</div>

					</div>
				
				<?php endif; ?>
				<!-- /Name -->
				<?php if(config('constants.DOB')): ?>
				<!-- Date of birth -->
					<div class="features-about">
						<div class="feature-img">
							<img src="<?php echo asset("dist/images/calendar-icon.svg"); ?>">
						</div>
						<div class="feature-content">
							<p>Date of Birth:</h4>
							<h4><?php echo e(config('constants.DOB')); ?></h4>
						</div>
					</div>
				<?php endif; ?>
				<?php if(config('constants.GYM')): ?>
				<!-- School -->
					<div class="features-about">
						<div class="feature-img">
							<img src="<?php echo asset("dist/images/pulling-up-grey.svg"); ?>">
						</div>
						<div class="feature-content">
							<p style="width: 92%;"><span style="color:#bcbcbc;">Gym:</span>
								<a class="pull-right" style="font-size: 14px; margin-top:2px;" href="<?php echo e(config('constants.GYM_PAGE')); ?>" target="_blank">Read more</a>
							</p>
							<h4><?php echo e(config('constants.GYM')); ?></h4>
						</div>
					</div>
				<?php endif; ?>
				<?php if(config('constants.SCHOOL')): ?>
				<!-- School -->
					<div class="features-about">
						<div class="feature-img">
							<img src="<?php echo asset("dist/images/school-icon.svg"); ?>">
						</div>
						<div class="feature-content">
							<p style="width: 92%;"><span style="color:#bcbcbc;">School:</span>
								<a class="pull-right" style="font-size: 14px; margin-top: 2px;" href="<?php echo e(action('CoachesGradesController@gradeslist')); ?>" target="_blank">Read more</a>
							</p>
							<h4><?php echo e(config('constants.SCHOOL')); ?></h4>
						</div>
					</div>
				<?php endif; ?>
				<?php if(config('constants.GRADE')): ?>
				<!-- Grade -->
					<div class="features-about">
						<div class="feature-img">
							<img src="<?php echo asset("dist/images/grade-icon.svg"); ?>">
						</div>
						<div class="feature-content">
							<p>Grade:</p>
							<h4><?php echo e(config('constants.GRADE')); ?></h4>
						</div>
					</div>
				<?php endif; ?>
				<!-- /Grade -->
				<?php if(config('constants.AWARDS')): ?>
				<!-- Awards -->
					<div class="features-about">
						<div class="feature-img">
							<img src="<?php echo asset("dist/images/awards-icon.svg"); ?>">
						</div>
						<div class="feature-content">
							<p>Awards:</p>
							<h4><?php echo e(config('constants.AWARDS')); ?></h4>
						</div>
					</div>
				<?php endif; ?>
				<!-- /Awards -->
				<?php if(config('constants.ADDRESS')): ?>
				<!-- Address -->
					<div class="features-about">
						<div class="feature-img">
							<img src="<?php echo asset("dist/images/location-icon.svg"); ?>">
						</div>
						<div class="feature-content">
							<p>Address:</p>
							<h4><?php echo e(config('constants.ADDRESS')); ?></h4>
						</div>
					</div>
				<?php endif; ?>
				<!-- /Address -->
				
				<!-- /Date of birth -->	
				<?php if(config('constants.PARENTS')): ?>			
				<!-- Parents -->
					<div class="features-about">
						<div class="feature-img">
							<img src="<?php echo asset("dist/images/parents-icon.svg"); ?>">
						</div>
						<div class="feature-content">
							<p>Parents:</p>
							<h4><?php echo e(config('constants.PARENTS')); ?></h4>
						</div>
					</div>
				<?php endif; ?>
				<!-- /Parents -->
				<?php if(config('constants.SIBLINGS')): ?>
				<!-- Siblings: -->
					<div class="features-about">
						<div class="feature-img">
							<img src="<?php echo asset("dist/images/sibling-icon.svg"); ?>">
						</div>
						<div class="feature-content">
							<p>Siblings:</p>
							<h4><?php echo e(config('constants.SIBLINGS')); ?></h4>
						</div>
					</div>
				<?php endif; ?>
				<!-- /Siblings: -->	
				<!-- /School -->
				
				<?php if(config('constants.GPA')): ?>
				<!-- GPA -->
					<div class="features-about">
						<div class="feature-img">
							<img src="<?php echo asset("dist/images/GPA-icon.svg"); ?>">
						</div>
						<div class="feature-content">
							<p>GPA:</p>
							<h4><?php echo e(config('constants.GPA')); ?></h4>
						</div>
					</div>
				<?php endif; ?>
				<!-- /GPA -->
				<!-- /Awards -->
			</div>
			<!-- /col-sm-4 col-12 -->
			<div class="col-sm-8 col-12">
				<div class="about-bottom-area">
					<?php if($page->slider_id): ?>
								<img  alt="" class="about-img-area wow fadeInLeftBig" src="<?php echo e(App\Http\Controllers\SliderController::getImage($page->slider_id)); ?>">
							<?php else: ?>
								<img  alt="" class="about-img-area wow fadeInLeftBig" src="<?php echo e(action('PageController@getImage',$page->id)); ?>">
							<?php endif; ?>
					<!-- <img src="<?php echo asset("dist/images/about-img.jpg"); ?>" > -->
						<div class="wow fadeInRightBig">
							<?php echo $page->page_description; ?>
						</div>
				</div>
			</div>

		</div>


		<!-- about sid finish -->		
		<?php } else { ?>
			<?php echo $page->page_description; ?>
		<?php } ?>
			
		</div>
	</section>
	<!-- main content -->
<?php echo $__env->make('site.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
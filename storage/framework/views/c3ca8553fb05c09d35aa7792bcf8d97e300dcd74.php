<?php $__env->startSection('header'); ?>
<section class="content-header">
	<!-- <h1>Add Product</h1> -->
	<ol class="breadcrumb">
		<li><a href="<?php echo e(backpack_url()); ?>"><?php echo e(config('backpack.base.project_name')); ?></a></li>
		<li><a href="<?php echo e(backpack_url('pages')); ?>">Products</a></li>
		<li>Add Product</li>
	</ol>
</section>
<?php $__env->stopSection(); ?>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

<?php $__env->startSection('content'); ?>
<div class="body">
	<?php if(\Session::has('success')): ?>
        <div class="alert alert-success">
            <?php echo e(\Session::get('success')); ?>

        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-sm-12">
			<!-- <a href='<?php echo e(url("/admin/product/$id/add-product")); ?>' class="btn btn-primary">Add Product</a> -->
        </div>
    </div>

	<br />
		<div class="row">
		<div class="col-sm-12">
			
        <table class="table table-striped">
          <thead>
            <tr>
              <th width="5%">Title</th>
              <th width="10%">Descripton</th>
              <th width="5%">Photo</th>
              <th width="10%">Country</th>
              <th width="10%">Region</th>
              <th width="10%">Varietal</th>
            </tr>
          </thead>
          <tbody>
            
            <tr>
              <td><a href='<?php echo e(url("/admin/showProduct/".$edit->id)); ?>'><?php echo e($edit->product_title); ?></a></td> 
              <td><?php echo html_entity_decode($edit->description); ?></td>
              <td><img src="<?php echo e(URL::to('/')); ?>/uploads/<?php echo e($edit->image); ?>" height="30" width="50"></td>
              <td><?php echo e($edit->country); ?></td>
              <td><?php echo e($edit->region); ?></td>
              <td><?php echo e($edit->varietal); ?></td>
            </tr>
          </tbody>
        </table>
      <!-- <div class="alert alert-info">
        There are no Product added yet.
      </div> -->
		</div>
	</div>

</div >
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
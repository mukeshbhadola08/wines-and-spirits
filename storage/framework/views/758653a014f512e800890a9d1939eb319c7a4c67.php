<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->

<link rel="stylesheet" href="<?php echo URL::to('dist/css/admin.css');?>">

<li><a href="<?php echo e(backpack_url('dashboard')); ?>"><i class="fa fa-dashboard"></i> <span><?php echo e(trans('backpack::base.dashboard')); ?></span></a></li>

<li><a href="<?php echo e(backpack_url('winesandspirits')); ?>"><i class="fa fa-home"></i><span>Wines And Spirits</span></a></li>
<li><a href="<?php echo e(backpack_url('settings')); ?>"><i class="fa fa-cogs"></i><span>Settings</span></a></li>

<!-- <li><a href="<?php echo e(backpack_url('users')); ?>"><i class="fa fa-users"></i><span>User Management</span></a></li> -->
<li><a href="<?php echo e(backpack_url('homepagesection')); ?>"><i class="fa fa-list"></i><span>Home Page Management</span></a></li>  

<li><a href="<?php echo e(backpack_url('pages')); ?>"><i class="fa fa-list"></i><span>Pages</span></a></li>

<li><a href="<?php echo e(backpack_url('blogs')); ?>"><i class="fa fa-bold"></i><span>Blog</span></a></li>
<!-- <li><a href="<?php echo e(backpack_url('blogs')); ?>"><i class="fa fa-bold"></i><span>News</span></a></li> -->

<li><a href="<?php echo e(backpack_url('navigations')); ?>"><i class="fa fa-list"></i><span>Navigations</span></a></li>
<!-- <li class="treeview">
<a href="<?php echo e(backpack_url('slider')); ?>"><i class="fa fa-home"></i><span>Home Page Management</span><span class="pull-right-container">     
<i class="fa fa-angle-left pull-right"></i></span></a>
<ul class="treeview-menu">
<li><a href="<?php echo e(backpack_url('slider')); ?>"><i class="fa fa-sliders"></i><span>Slider Management</span></a></li> 
<li><a href="<?php echo e(backpack_url('homepagesection/en')); ?>"><i class="fa fa-list"></i><span>Benefits Yoga & Mindfulness</span></a></li>      
<li><a href="<?php echo e(backpack_url('footersection/en')); ?>"><i class="fa fa-list"></i><span>Footer Section</span></a></li>     
</ul>
</li> -->
<!--  -->

<li><a href="<?php echo e(backpack_url('photo-gallery')); ?>"><i class="fa fa-picture-o"></i><span>Media</span></a></li>
<li><a href="<?php echo e(backpack_url('history')); ?>"><i class="fa fa-history"></i><span>History</span></a></li>

<!-- <li><a href="<?php echo e(backpack_url('news')); ?>"><i class="fa fa-newspaper-o"></i><span>Events</span></a></li>
<li><a href="<?php echo e(backpack_url('routines')); ?>"><i class="fa fa-calendar"></i><span>Routines</span></a></li> -->

<!-- <li><a href="<?php echo e(backpack_url('coaches-grades')); ?>"><i class="fa fa-calendar"></i><span>Coaches/Grades</span></a></li> -->


<!-- <li><a href="<?php echo e(backpack_url('pendingcomments')); ?>"><i class="fa fa-comment-o"></i><span>Pending Comments</span></a></li>

<li><a href="<?php echo e(backpack_url('localisations')); ?>"><i class="fa fa-language"></i><span>Manage Localisation</span></a></li>
 -->
 <?php $__env->startSection('after_scripts'); ?>
 <!-- The Modal -->
<div class="modal" id="portfolioModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Photo</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" style="display: block;
    float: left;">
        <ul style="list-style: none;"></ul>
      </div>

      <!-- Modal footer -->

      <div class="modal-footer">
        <button id="save" disabled onclick="save()" class="btn btn-width bkgrnd-cyan save-details" type="button" name="save-details" >Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style type="text/css">
	.custom-border{
    border: 1px solid #57549C !important;
	}
	.border{
    border: 1px solid transparent;
	}
	.content-header{padding-top: 15px !important;}
	.content-header > .breadcrumb{top: 15px !important;}
</style>

<script>
  
  function save(){
        var imageDIV = $('.images');
        imageDIV.find('.added-img').remove();

        var imagurl = $( "#portfolioModal" ).find('span.custom-border img').attr('src');
        var imagid = $( "#portfolioModal" ).find('span.custom-border img').data('id');

        var image = "<div class='added-img' style='margin-top:10px;'><img class=\"img-responsive\" style=\"height: 150px; \" src='"+imagurl+"'><input type='hidden' name='image' value='"+imagid+"'/></div>";
        imageDIV.html(image);

        $("#upload").val(''); 
        $( "#portfolioModal" ).modal('hide');
    
        $( "#portfolioModal" ).find('.modal-body ul').empty();  	     
  }


  jQuery(function() {

  	$( "#portfolioModal" ).on('hidden.bs.modal', function(){

      $( "#portfolioModal .modal-footer #save").attr("disabled", 1);
      $( "#portfolioModal .modal-footer #save").removeClass('btn-primary').addClass('bkgrnd-cyan');

    });

    $( "#portfolioModal" ).on('shown.bs.modal', function(){
      $( "#portfolioModal" ).find('.modal-body ul').empty();
	     jQuery.ajax({
         type:'POST',
         url:'/admin/getportfolios',
         data:'_token = <?php echo csrf_token(); ?>',
         success:function(res) {
         	 $.each(res.data, function(i){
	            $( "#portfolioModal" ).find('.modal-body ul').append("<li class='inline-block pull-left'><span style='display:block;margin: 4px;' class='border'><img onclick=\"$( this ).closest('ul').find('span').removeClass('custom-border'); $(this).parent().addClass('custom-border'); $(this).closest('.modal-content').find('.modal-footer #save').removeAttr('disabled').removeClass('bkgrnd-cyan').addClass('btn-primary'); \" data-id='"+res.data[i].id+"'class='img-responsive' style='width: 150px;height: 150px;     margin: 8px; cursor:pointer;' src='"+res.data[i].url+"'></span></li>");
	        });    
          
         }
      
      });
	});
});

</script>
<?php $__env->stopSection(); ?>
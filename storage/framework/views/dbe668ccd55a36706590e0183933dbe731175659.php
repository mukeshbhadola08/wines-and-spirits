<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>Edit Media</h1>
	<div class="alert alert-info" style="margin-bottom: 0;">
	    <strong>Note: </strong>
	    Please note that recommended dimension of image is 1200 by 1200 pixels and Size of the uploaded image should be less than 1.5mb
	</div>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(backpack_url()); ?>"><?php echo e(config('backpack.base.project_name')); ?></a></li>
		<li><a href="<?php echo e(backpack_url('slider')); ?>">Media</a></li>
		<li>Edit Media</li>
	</ol>
</section>
<?php $__env->stopSection(); ?>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

<?php $__env->startSection('content'); ?>
<div class="body">
	<?php if($errors->any()): ?>
		<div class="alert alert-danger">
			<ul>
				<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<li><?php echo e($error); ?></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</ul>
		</div>
	<?php endif; ?>

	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="<?php echo e(action('SliderController@update',$id)); ?>" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="<?php echo e(csrf_token()); ?>" name="_token" />
				<label for="blog_title">Title:</label>
				<input type="text" class="form-control" name="title" value="<?php echo e($slide->title); ?>"/>
			</div>
			<div class="form-group">
			
				<label for="blog_description">Description:</label>
				<textarea name="description" class="form-control"  id="blog-ckeditor"><?php echo e($slide->description); ?></textarea>
			</div>
			<?php
				$video = App\Slider::where('id', $slide->id)->pluck('video');
			?>
			<?php if(!$video[0]): ?>
			<div class="form-group">	
				<label for="image">Old Image:</label>
				<img class="media-object" src="<?php echo e(URL::to('/')); ?>/uploads/slider_images/<?php echo e($slide->image); ?>" alt="no-img" height="150" >
			</div>
			<?php endif; ?>
			
			<div class="form-group">	
				<label for="image">New Image:</label><!--  <i>Image should be minimum width 1000 and height 686  </i> -->
				<input type="file" class="form-control" name="image" id="upload" />
			</div>
			<div class="form-group hidden">	
				<label for="video">Video:</label><!--  <i>Image should be minimum width 1000 and height 686  </i> -->
				
				<input type="text" class="form-control" name="video" value="<?php echo e($video[0]); ?>" />
				<?php if($errors->has('video')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('video')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group hidden">
				<input type="checkbox" name = "is_portfolio" id = "is_portfolio" value="1" <?php if($slide->is_portfolio == 1): ?> checked <?php endif; ?> />&nbsp;
				<label for="is_portfolio">Is Portfolio</label>
			</div>
			
			<?php if($slide->lang_code == 'en'): ?>	
			<div class="form-group hidden">
				<input type="checkbox" name = "is_visible" value="1" <?php if($slide->is_visible == 1): ?> checked <?php endif; ?> />&nbsp;
				<label for="is_visible">Is Visible</label>
			</div>
			<?php else: ?> 
				<input type="hidden" name="is_visible" value="1">
			<?php endif; ?>

			<input type ="hidden" name="added_by" value="<?php echo e(Auth::user()->id); ?>" / >
			<button type="submit" class="btn btn-primary" id="add_slide" >Update</button>
			</form>
		</div>
	</div>

</div >

<script>
	CKEDITOR.replace( 'blog-ckeditor' );
</script>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
<script>
$(function () {
	var _URL = window.URL || window.webkitURL;
	$("#upload").on("change", function () {
		 var file, img;
		if ((file = this.files[0])) {
			if ( (/\.(png|jpeg|jpg|gif)$/i).test(file.name) ) {
				img = new Image();
				img.onload = function () {
					if(this.width<1000 && this.height<686){
					   new PNotify({'text':'Image dimension is not correct.','type':'error'}); 
					    $('#add_slide').attr('disabled','disabled');
					} else {
						$('#add_slide').removeAttr('disabled');
					}
				};
				 img.src = _URL.createObjectURL(file); 
		  } else {
			errors = file.name +" Unsupported Image extension.";  
			new PNotify({'text':errors,'type':'error'});
				 $('#add_slide').attr('disabled','disabled');
		  }
			
		}
	});
});
		 
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
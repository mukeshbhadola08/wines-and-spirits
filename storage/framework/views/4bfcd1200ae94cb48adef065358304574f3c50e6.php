<?php $__env->startSection('header'); ?>
<section class="content-header">
	<ol class="breadcrumb">
		<li><a href="<?php echo e(backpack_url()); ?>"><?php echo e(config('backpack.base.project_name')); ?></a></li>
		<li>Settings</li>
	</ol>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="body">
	<?php if(\Session::has('success')): ?>
        <div class="alert alert-success">
            <?php echo e(\Session::get('success')); ?>

        </div>
    <?php endif; ?>
	<?php if($errors->any()): ?>
		<div class="alert alert-danger">
			<ul>
				<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<li><?php echo e($error); ?></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</ul>
		</div>
	<?php endif; ?>
	
	<?php
	foreach ($settings as $setting){
		$variable = $setting->option_name;
		$$variable = $setting->option_value;
	}
	?>
	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="<?php echo e(backpack_url('homepagesection')); ?>" enctype="multipart/form-data" class="form-horizontal">
			<input type="hidden" value="<?php echo e(csrf_token()); ?>" name="_token" />

			<div class="sitesettings">
				<h3>Homepage Settings</h3>
				<div class="alert alert-info">
				    <strong>Note: </strong>
				    Please upload Top & right Images with correct dimensions as mentioned. 
				</div>
				<div class="form-group">
					<label for="achievements_title" class="col-sm-2 control-label">Achievements Title</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[achievements_title]" placeholder="Achievements Title" value="<?php echo e(config('constants.ACHIEVEMENTS_TITLE')); ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="gym_title" class="col-sm-2 control-label">Gym information Title</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[gym_title]" placeholder="Gym information title" value="<?php echo e(config('constants.GYM_TITLE')); ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="portfolio_title" class="col-sm-2 control-label">Portfolio Section Title</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[portfolio_title]" placeholder="Portfolio Title" value="<?php echo e(config('constants.PORTFOLIO_TITLE')); ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="latest_events_title" class="col-sm-2 control-label">Latest Events Title</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[latest_events_title]" placeholder="Latest Events Title" value="<?php echo e(config('constants.LATEST_EVENTS_TITLE')); ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="latest_news_title" class="col-sm-2 control-label">Latest News Title</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[latest_news_title]" placeholder="Latest News Title" value="<?php echo e(config('constants.LATEST_NEWS_TITLE')); ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="latest_news_title" class="col-sm-2 control-label">Porfolio Autoplay Timing</label>
					<div class="col-sm-10">
						<input type="number" min="1" max="10" class="form-control" name="settings[portfolio_autoplay_timing]" placeholder="Porfolio Autoplay Timing" value="<?php echo e(config('constants.PORTFOLIO_AUTOPLAY_TIMING')); ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="home_textarea" class="col-sm-2 control-label">Home textarea on top </label>
					<div class="col-sm-10">
						<textarea class="form-control" name="settings[home_textarea]" placeholder="Home textarea on top"><?php echo e(config('constants.HOME_TEXTAREA')); ?></textarea>
					</div>
				</div>
			</div>
			<div class="form-group">	
				<label for="image" class="col-sm-2 control-label">Old Top Image:</label>
				<img class="media-object" src="<?php echo e(url('/public/storage/slider_images/' . config('constants.TOP_IMAGE'))); ?>" alt="no-img" height="150" >
			</div>
			<div class="form-group">	
				<label for="image" class="col-sm-2 control-label">Change Top Image for homepage:</label>
				<div class="col-sm-10">
					<input type="file" class="form-control" onchange="$('.added-img').remove();" name="settings[top_image]" id="upload" value="<?php echo e(config('constants.TOP_IMAGE')); ?>"/>
					<p class="text-danger" style="color:red;" ><strong>Please note that Dimension of image must be 582 by 493 pixels</strong></p>
				</div>
			</div>
			
			<div class="form-group">	
				<label for="image" class="col-sm-2 control-label" >Old Right Image:</label>
				<img class="media-object" src="<?php echo e(url('/public/storage/slider_images/' . config('constants.RIGHT_IMAGE'))); ?>" alt="no-img" height="150" >
			</div>
			<div class="form-group">	
				<label for="image" class="col-sm-2 control-label">Change Right Image for homepage:</label>
				<div class="col-sm-10">
					<input type="file" class="form-control" onchange="$('.added-img').remove();" name="settings[right_image]" value="<?php echo e(config('constants.RIGHT_IMAGE')); ?>" id="upload" />
					<p class="text-info" style="color:red;"><b>Please note that Dimension of image must be 320 by 699 pixels</b></p>
				</div>
			</div>
			
			<button type="submit" class="btn btn-primary">Update Settings</button>
			</form>
		</div>
	</div>

</div >
<?php $__env->stopSection(); ?>
 <?php $__env->startSection('after_scripts'); ?>
<div class="modal" id="portfolioModal1">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Photo</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" style="display: block;
    float: left;">
        <ul style="list-style: none;"></ul>
      </div>

      <!-- Modal footer -->

      <div class="modal-footer">
        <button id="save" disabled onclick="save2()" class="btn btn-width bkgrnd-cyan save-details" type="button" name="save-details" >Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style type="text/css">
	.custom-border{
    border: 1px solid #57549C !important;
	}
	.border{
    border: 1px solid transparent;
	}
	.content-header{padding-top: 15px !important;}
	.content-header > .breadcrumb{top: 15px !important;}
</style>
<script>
  
  function save2(){
        var imageDIV = $('.images');
        imageDIV.find('.added-img').remove();
        var image_no = "slider_image"+imageDIV.data('image');
        var imagurl = $( "#portfolioModal1" ).find('span.custom-border img').attr('src');
        var imagid = $( "#portfolioModal1" ).find('span.custom-border img').data('id');

        var image = "<div class='added-img' style='margin-top:10px;'><img class=\"img-responsive\" style=\"height: 150px; margin-left:180px; margin-top:20px;  \" src='"+imagurl+"'><input type='hidden' name='settings["+image_no+"]' value='"+imagid+"'/></div>";

        imageDIV.append(image);

        $("#upload").val(''); 
        $( "#portfolioModal1" ).modal('hide');
    
        $( "#portfolioModal1" ).find('.modal-body ul').empty();  	     
  }


  jQuery(function() {

  	$( "#portfolioModal1" ).on('hidden.bs.modal', function(){
		$("button.btn-primary").parent().removeClass('images');
		$( "#portfolioModal1 .modal-footer #save").attr("disabled", 1);
		$( "#portfolioModal1 .modal-footer #save").removeClass('btn-primary').addClass('bkgrnd-cyan');
    });

    $( "#portfolioModal1" ).on('shown.bs.modal', function(){
      $( "#portfolioModal1" ).find('.modal-body ul').empty();
	     jQuery.ajax({
         type:'POST',
         url:'/admin/getportfolios',
         data:'_token = <?php echo csrf_token(); ?>',
         success:function(res) {
         	 $.each(res.data, function(i){
	            $( "#portfolioModal1" ).find('.modal-body ul').append("<li class='inline-block pull-left'><span style='display:block;margin: 4px;' class='border'><img onclick=\"$( this ).closest('ul').find('span').removeClass('custom-border'); $(this).parent().addClass('custom-border'); $(this).closest('.modal-content').find('.modal-footer #save').removeAttr('disabled').removeClass('bkgrnd-cyan').addClass('btn-primary'); \" data-id='"+res.data[i].id+"'class='img-responsive' style='width: 150px;height: 150px;     margin: 8px; cursor:pointer;' src='"+res.data[i].url+"'></span></li>");
	        });    
          
         }
      
      });
	});
});

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('site.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<!-- <style>
	
  	@media (max-width: 991px) {
  	.logo-left {
	    position: absolute;
	    left: 15px;
	    transform: skew(0deg, 0deg);
	    top: 14px;
	}
	#header {
	    height: 70px;
	    padding: 15px 0;
	  	border-bottom: 1px solid #ccc;
	  	background: rgba(0, 0, 0, 0.5);
	  	-webkit-transform: skew(0deg, 0deg);
	    transform: skew(0deg, 0deg);
	    margin-top: 0px;
	 }
  	#header.header-scrolled, #header.header-pages {
	    height: 70px !important;
	    padding: 15px 0 !important;
	    background-color: rgba(0, 0, 0, 0.7) !important;
	    box-shadow: 0px 0px 30px rgba(127, 137, 161, 0.3) !important;
	}
	#header {
		height: 70px !important;
		padding: 15px 0 !important;
		border-bottom: 1px solid #ccc !important;
		background: rgba(0, 0, 0, 0.5) !important;
		-webkit-transform: skew(0deg, 0deg) !important;
		transform: skew(0deg, 0deg) !important;
		margin-top: 0px !important;
	}

 	}
 	.card-body a {
 		display: none;
 	}
 	.car-body a.coaches{
 		display: block !important;
 	}
 	.carousel-3d-slide{
 		border-color:transparent;
 		background-color:transparent;
 	}
	</style> -->

	<!-- Pre load -->
	<!-- <div id="preloader">
		<h1><?php echo nl2br(config('constants.PRELOADER')); ?></h1>
	</div> -->
 	<!-- /Pre load -->

<!-- intro -->
<!-- full-image-slider-section-->
  <section class="full-image-slider">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		<div class="container">
			<ol class="carousel-indicators">
				<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				<li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
				<li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
			</ol>
		</div>
     
		<div class="carousel-inner">
			<?php $__currentLoopData = $slides; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $slideDetails): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<div class="carousel-item <?php if($key == 0): ?> active <?php endif; ?>">
			  	<img class="d-block w-100" src="<?php echo e(URL::to('/')); ?>/uploads/slider_images/<?php echo e($slideDetails->image); ?>" alt="First slide">
			  <div class="container">
				<div class="carousel-caption d-none d-md-block">
				  <h5><?php echo e($slideDetails->title); ?></h5>
				  <p><?php echo html_entity_decode($slideDetails->description); ?></p>
				</div>
			  </div>
			</div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div>
		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<img src="<?php echo asset('dist/images/left-arrow.svg'); ?>" alt="arrow-prev" class="arrow-img" /> 
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			<img src="<?php echo asset('dist/images/right-arrow.svg'); ?>" class="arrow-img" /> 
			<span class="sr-only">Next </span>
		</a>
	</div>
</section>
<!-- / Home Slider -->

 <!-- Our mission -->
<section class="about-section">
	<div class="about-container">
		<div class="row">
			<div class="col-sm-6 about-bg-img about-bg-responsive" 
			style="background-image:url(<?php echo asset("dist/images/about-us-img.png"); ?>);"></div>
			<div class="col-sm-6">
				<div class="about-us-content">
				   <div class="heading-section">
					<h2>Our Mission</h2>
				   </div>
				   <div class="outer-content">
					 <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. 
					 it is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
				  </div>
				  <div class="mission-list">
					<ul>
						<li>
							<img src="<?php echo asset("dist/images/grape.svg"); ?>" alt="wines-battle">
							<p> it is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
						</li>
						<li>
							<img src="<?php echo asset("dist/images/grape.svg"); ?>" alt="wines-battle">
							<p> it is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
						</li>
						<li>
							<img src="<?php echo asset("dist/images/grape.svg"); ?>" alt="wines-battle">
							<p> it is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
						</li>
						<li>
							<img src="<?php echo asset("dist/images/grape.svg"); ?>" alt="wines-battle">
							<p> it is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
						</li>
						<li>
							<img src="<?php echo asset("dist/images/grape.svg"); ?>" alt="wines-battle">
							<p> it is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
						</li>
					</ul>
				  </div>
				</div> 
			</div>
	   </div>
	</div>
</section>
<!-- / Our misssion -->

<!-- Our history -->
<section class="history-section" style="background-image:url(<?php echo asset("dist/images/bg-history-2.png"); ?>);">
	<div class="container">
		<div class="heading-section white-heading">
			<h2>Our History</h2>
		</div>

		<!-- slider -->
		<div class="owl-carousel owl-theme site-owl">
			<div class="item">
				<div class="row">
					<div class="col-sm-6 col-md-6 col-12">
						<img src='<?php echo asset('dist/images/history-img.png'); ?>' alt="history-img" class="img-fluid about-img"/> 
					</div>
					<div class="col-sm-6 col-md-6 col-12">
						<div class="history-content">
							<div class="heading-history">
								<h3>1957-1960 </h3>
							</div>
							<div class="history-content-inner">
								<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
								<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
								<p>Contrary to popular belief, Lorem Ipsum is not simply random text.It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old .</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="row">
					<div class="col-sm-6 col-md-6 col-12">
						<img src="<?php echo asset('dist/images/history-img.png'); ?>" alt="history-img" class="img-fluid about-img"/> 
					</div>
					<div class="col-sm-6 col-md-6 col-12">
						<div class="history-content">
							<div class="heading-history">
								<h3>1960-1970 </h3>
							</div>
							<div class="history-content-inner">
								<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
								<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
								<p>Contrary to popular belief, Lorem Ipsum is not simply random text.It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old .</p>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
		<!-- slider -->
	</div>
</section>
<!-- /Our history -->

<!-- Wines -->
<section class="wines-section">
	<div class="container">
		<div class="heading-section">
			<h2>Wines & Spirits</h2>
		</div>

		<!-- slider -->
		<div class="owl-carousel owl-theme site-owl">
			<?php $__currentLoopData = $wineslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $wines): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<div class="item">
				<div class="row">
					<div class="col-sm-6 col-md-6 col-12">
						<div class="wines-content">
							<h2><?php echo e($wines->cat_title); ?></h2>
							<h4><?php echo e($wines->country); ?></h4>
							<div class="content-area-wine"> 
								<?php echo html_entity_decode($wines->description); ?>

							</div>
							<buttton class="btn btn-outline-wine">Read More</buttton>
						</div>
					</div>
					<div class="col-sm-6 col-md-6 col-12">
						<div class="icon-circle-outer">
							<div class="icon-circle">
								<img src="<?php echo e(asset('/')); ?>/dist/images/white-icon-grape.svg"  alt="wine-logo">
							</div>
						</div>
						<div class="wines-image">
							<img src="<?php echo e(URL::to('/')); ?>/uploads/<?php echo e($wines->logo); ?>" alt="wine-logo">
							<!-- <img src="<?php echo e(asset('/')); ?>/dist/images/wine-logo-img.png" alt="wine-logo"> -->
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div>
		<!-- /slider -->
	</div>
</section>
<!-- /Wines -->

<!-- Blogs -->
<section class="history-section" style="background-image:url(<?php echo asset("dist/images/bg-blogs-2.png"); ?>);">
	<div class="container">
		<div class="heading-section white-heading">
			<h2>Lastest Blogs</h2>
		</div>
		<!-- row -->
		<div class="row">
			<?php $__currentLoopData = $allrecentblogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blogs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<!-- blog 1 -->
            <div class="col-sm-6 mb-3">
				<div class="latestBlog">
					<div class="row no-gutters">
						<div class="col-md-5 img-blog-home" style="background-image: url(<?php echo e(action('BlogController@getImage',$blogs->id)); ?>)">
						<!-- <div class="col-md-5 img-blog-home" style="background-image: url(<?php echo e(URL::to('/')); ?>/blogs/image/<?php echo e($blogs->id); ?>)"> -->
							<!-- <img src="" class="" alt=""> -->
						</div>
						<div class="col-md-7">
							<div class="blog-inner">
								<div class="date">
									<h4><?php echo date('d');?></h4>
									<div class="month">
										<span><?php echo date('M');?></span>
									</div>
								</div>
								<h3><?php echo e($blogs->blog_title); ?></h3>
								<div class="blog-desc">
									<?php echo $blogs->blog_description; ?>

								</div>
								<buttton class="btn btn-outline-wine mt-2">Read More</buttton>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div>
		<!-- /row -->
	</div>
</section>
<!-- /Blogs -->
<?php echo $__env->make('site.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
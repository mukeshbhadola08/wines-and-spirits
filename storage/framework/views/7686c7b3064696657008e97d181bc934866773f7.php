

<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>Add Partner</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(backpack_url()); ?>"><?php echo e(config('backpack.base.project_name')); ?></a></li>
		<li><a href="<?php echo e(backpack_url('pages')); ?>">Wines & Spirits</a></li>
		<li>Add Partner</li>
	</ol>
</section>
<?php $__env->stopSection(); ?>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

<?php $__env->startSection('content'); ?>
<div class="body">
	
	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="<?php echo e(backpack_url('winesandspirits/create')); ?>" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="<?php echo e(csrf_token()); ?>" name="_token" />
				<label for="type">Partner Type:</label>&nbsp;
				<select name="type" id="type" onchange="showDetails(this.value);">
					<option value=''>--SELECT--</option>
					<option value="winery">Winery</option>
					<option value="distillery">Distillery</option>
				</select>
				<?php if($errors->has('partner_type')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('partner_type')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="hidden" id="details">
				<div class="form-group">
					<label for="cat_title">Name:</label>
					<input type="text" class="form-control" name="cat_title" value ="<?php echo e(old('cat_title')); ?>" />
					<?php if($errors->has('cat_title')): ?>
						<span class="invalid-feedback text-danger">
							<strong><?php echo e($errors->first('cat_title')); ?></strong>
						</span>
					<?php endif; ?>
				</div>
				
				<div class="form-group">
					<label for="description">Description:</label>
					<textarea name="description" class="form-control" id="page-ckeditor"><?php echo e(old('page_description')); ?></textarea>
					<?php if($errors->has('page_description')): ?>
						<span class="invalid-feedback text-danger">
							<strong><?php echo e($errors->first('page_description')); ?></strong>
						</span>
					<?php endif; ?>
				</div>
				<div class="form-group images">	
					<label for="image">Logo:</label>
					<input type="file" onchange="$('.added-img').remove();" name="logo" id="upload" />
					<?php if($errors->has('image')): ?>
						<span class="invalid-feedback text-danger">
							<strong><?php echo e($errors->first('image')); ?></strong>
						</span>
					<?php endif; ?>
				</div>
				<div class="form-group">
					<label for="region">Region:</label>&nbsp;
					<input type="text" class="form-control" name="region" value ="<?php echo e(old('region')); ?>" />
					<?php if($errors->has('region')): ?>
						<span class="invalid-feedback text-danger">
							<strong><?php echo e($errors->first('region')); ?></strong>
						</span>
					<?php endif; ?>
				</div>
				<div class="form-group">
					<label for="country">Country:</label>&nbsp;
					<select name="country" id="country">
						<option>SELECT</option>
						<?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option><?php echo e($country->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>
					<?php if($errors->has('country')): ?>
						<span class="invalid-feedback text-danger">
							<strong><?php echo e($errors->first('country')); ?></strong>
						</span>
					<?php endif; ?>
				</div>
				<input type ="hidden" name="added_by" value="<?php echo e(Auth::user()->id); ?>" / >
				<button type="submit" class="btn btn-primary">Create</button>	
			</div>
			</form>
		</div>
	</div>

</div>


<script>
	window.onload = function() {
		CKEDITOR.replace( 'page-ckeditor', {
			filebrowserUploadUrl: '<?php echo e(route("upload",["_token" => csrf_token()])); ?>'
		});
	};

	function showDetails(val) {
		
		if(val == ""){
			jQuery("#details").addClass("hidden");

		}else{
			if(val == "distillery") 
				jQuery(".winery").addClass("hidden");
			else
				jQuery(".winery").removeClass("hidden");

			jQuery("#details").removeClass("hidden");	
		}
	}

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('header'); ?>
    <section class="content-header">
      <h1>
         Routines
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(backpack_url()); ?>"><?php echo e(config('backpack.base.project_name')); ?></a></li>
        <li class="active">Routines</li>
      </ol>
    </section>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<div class="body">
	<?php if(\Session::has('success')): ?>
        <div class="alert alert-success">
            <?php echo e(\Session::get('success')); ?>

        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-sm-12">
            
				<a href="<?php echo e(url('/admin/routines/create')); ?>" class="btn btn-primary">Add Routine</a>
			
        </div>
    </div>
	<br />
	<div class="row">
		<div class="col-sm-12">
			<?php if(isset($routines) && count($routines)>0): ?>
				<table class="table table-striped" id="routines_table">
					<thead>
						<tr>
							<th width="5%">Image</th>
						  <th width="20%">Title</th>
						  <th width="10%">Descripton</th>
						  <th width="15%">Working on</th>
						  <th width="15%">Level</th>
						  <th width="15%">Video Url</th>
						  <th width="15%">Video Time</th>
						  <th width="20%">Additional Skills</th>
						</tr>
					</thead>
					<tbody>
						
					<?php $__currentLoopData = $routines; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $routinesDetails): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						
						<tr>
							<td>
								<?php if($routinesDetails->slider_id): ?>
									<img class="media-object" src="<?php echo e(App\Http\Controllers\SliderController::getImage($routinesDetails->slider_id)); ?>" alt="no-img" height="100" width="100">
								<?php else: ?>
									<img class="media-object" src="<?php echo e(action('RoutinesController@getImage',$routinesDetails->id)); ?>" alt="no-img" height="100" width="100">
								<?php endif; ?>
							</td>
							<td><?php echo e($routinesDetails->title); ?></td>
							<td><?php echo e($routinesDetails->description); ?></td>
							<td><?php echo e($routinesDetails->working_on); ?></td>
							<td><?php echo e($routinesDetails->level); ?></td>
							<td><?php echo e($routinesDetails->video_url); ?></td>
							<td><?php echo e($routinesDetails->video_time); ?></td>
							<td><?php echo e($routinesDetails->additional_skills); ?></td>
							
							<td>
								<a href="<?php echo e(action('RoutinesController@edit',$routinesDetails->id)); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>&nbsp;

								
								<a href="<?php echo e(action('RoutinesController@delete',$routinesDetails->id)); ?>" class="btn btn-danger delete_page"><i class="fa fa-trash-o"></i>&nbsp;DELETE</a>
								
							</td>
						</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
					</tbody>
				</table>
			<?php else: ?>
			<div class="alert alert-info">
				There are no routines added yet.
			</div>
			<?php endif; ?>
		</div>
	</div>

</div >
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
<script src="<?php echo e(asset('vendor/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('vendor/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<script>
$(document).ready(function() {
    $('#routines_table').DataTable( {
        "order": [[ 1, "asc" ]]
    });
	$('#routines_table_paginate').addClass('pull-right');
});	
jQuery(document).on('click','.delete_page',function(e){
	if(confirm("Are you sure all related information also be deleted?")){
		return true;
	} else{
		return false;
	}
});		
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>Add Media</h1>
	<div class="alert alert-info" style="margin-bottom: 0;">
	    <strong>Note: </strong>
	    Please note that recommended dimension of image is 1200 by 1200 pixels and Size of the uploaded image should be less than 1.5mb
	</div>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(backpack_url()); ?>"><?php echo e(config('backpack.base.project_name')); ?></a></li>
		<li><a href="<?php echo e(backpack_url('slider')); ?>">Media</a></li>
		<li>Add Media</li>
	</ol>
</section>
<?php $__env->stopSection(); ?>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

<?php $__env->startSection('content'); ?>
<div class="body">

	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="<?php echo e(backpack_url('photo/create')); ?>" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="<?php echo e(csrf_token()); ?>" name="_token" />
				<label for="title">Title:</label>
				<input type="text" class="form-control" name="title" value ="<?php echo e(old('title')); ?>" />
				<?php if($errors->has('title')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('title')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group">
				<label for="description">Description:</label>
				<textarea name="description" class="form-control" id="blog-ckeditor"><?php echo e(old('description')); ?></textarea>
				<?php if($errors->has('description')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('description')); ?></strong>
					</span>
				<?php endif; ?>
			</div>


			<div class="form-group">	
				<label for="image">Image:</label><!--  <i>Image should be minimum width 1000 and height 686  </i> -->
				<input type="file" class="form-control" name="image" id="upload" />
				<?php if($errors->has('image')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('image')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group"></div>

			<div class="form-group hidden">	
				<label for="video">Video:</label><!--  <i>Image should be minimum width 1000 and height 686  </i> -->
				<input type="text" class="form-control" name="video" />
				<?php if($errors->has('video')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('video')); ?></strong>
					</span>
				<?php endif; ?>
			</div>

			<div class="form-group hidden">
				<input type="checkbox" name = "is_portfolio" id = "is_portfolio" value="1" />&nbsp;
				<label for="is_portfolio">Is portfolio</label>
			</div>
			<div class="form-group hidden">
				
				<input type="checkbox" name = "is_visible" value="1" <?php echo e(old('is_visible') ? 'checked' : ''); ?> />&nbsp;
				<label for="is_visible">Is Visible</label>
			</div>						<input type ="hidden" name="lang_code" value="en" / >			<input type ="hidden" name="parent" value="0" / >

			<button type="submit" class="btn btn-primary" id="add_slide" >Create</button>
			</form>
		</div>
	</div>

</div >
<script>
	CKEDITOR.replace( 'blog-ckeditor' );
</script>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
<script>
/*$(function () {
	var _URL = window.URL || window.webkitURL;
	$("#upload").on("change", function () {
		 var file, img;
		if ((file = this.files[0])) {
			if ( (/\.(png|jpeg|jpg|gif)$/i).test(file.name) ) {
				img = new Image();
				img.onload = function () {
					if(this.width<1000 && this.height<686){
					   new PNotify({'text':'Image dimension is not correct.','type':'error'}); 
					    $('#add_slide').attr('disabled','disabled');
					} else {
						$('#add_slide').removeAttr('disabled');
					}
				};
				 img.src = _URL.createObjectURL(file); 
		  } else {
			errors = file.name +" Unsupported Image extension.";  
			new PNotify({'text':errors,'type':'error'});
				 $('#add_slide').attr('disabled','disabled');
		  }
			
		}
	});
});*/
		 
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
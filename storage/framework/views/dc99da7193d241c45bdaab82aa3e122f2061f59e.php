<?php echo $__env->make('site.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- Single page -->
	<div class="wrapper-page" style="background-image: url(<?php echo e(asset('/')); ?>/dist/images/bg-blogs.png);"> 
		<div class="container">
			<!-- heading -->
			<div class="heading-section white-heading">
				<h2>Blogs</h2>
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item"><a href="#">Blogs</a></li>
					</ol>
				</nav>
			</div>
			<!-- /heading -->
			<!-- inner wrapper -->
			<div class="inner-wrapper blogs-listing">
				<div class="row">
					<!-- blog list -->
					<div class="col-sm-4">
						<div class="card-content">
							<div class="card-img">
								<img src="<?php echo e(asset('/')); ?>/dist/images/blogs-img-1.png" alt="">
							</div>
							<div class="card-desc">
								<h3>New Wine</h3>
								<div class="blogs-info">
									<span class="comment-view"><i class="fa fa-commenting" aria-hidden="true"></i>&nbsp;20 Comments</span>
									<span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;May 20, 2019</span>
								</div>
								<div class="desc-blog">
									<p>Some quick example text to build on the card title and make up the bulk of the card's content.Some quick example text to build on the card title and make up the bulk of the card's content.</p>
								</div>
								<div class="text-right">
									<a href="#" class="btn btn-outline-wine">Read More</a>
								</div>  
							</div>
						</div>
					</div>
					<!-- blog list -->
					<!-- blog list -->
					<div class="col-sm-4">
						<div class="card-content">
							<div class="card-img">
								<img src="<?php echo e(asset('/')); ?>/dist/images/blogs-img-1.png" alt="">
							</div>
							<div class="card-desc">
								<h3>New Wine</h3>
								<div class="blogs-info">
									<span class="comment-view"><i class="fa fa-commenting" aria-hidden="true"></i>&nbsp;20 Comments</span>
									<span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;May 20, 2019</span>
								</div>
								<div class="desc-blog">
									<p>Some quick example text to build on the card title and make up the bulk of the card's content.Some quick example text to build on the card title and make up the bulk of the card's content.</p>
								</div>
								<div class="text-right">
									<a href="#" class="btn btn-outline-wine">Read More</a>
								</div>  
							</div>
						</div>
					</div>
					<!-- blog list -->
					<!-- blog list -->
					<div class="col-sm-4">
						<div class="card-content">
							<div class="card-img">
								<img src="<?php echo e(asset('/')); ?>/dist/images/blogs-img-1.png" alt="">
							</div>
							<div class="card-desc">
								<h3>New Wine</h3>
								<div class="blogs-info">
									<span class="comment-view"><i class="fa fa-commenting" aria-hidden="true"></i>&nbsp;20 Comments</span>
									<span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;May 20, 2019</span>
								</div>
								<div class="desc-blog">
									<p>Some quick example text to build on the card title and make up the bulk of the card's content.Some quick example text to build on the card title and make up the bulk of the card's content.</p>
								</div>
								<div class="text-right">
									<a href="#" class="btn btn-outline-wine">Read More</a>
								</div>  
							</div>
						</div>
					</div>
					<!-- blog list -->
				</div>
				
			</div>
		</div>
	</div>
<?php echo $__env->make('site.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
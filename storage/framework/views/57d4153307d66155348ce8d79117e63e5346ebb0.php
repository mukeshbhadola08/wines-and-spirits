<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>Edit Page</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(backpack_url()); ?>">
		    <?php echo e(config('backpack.base.project_name')); ?>

		 </a></li>
		<li><a href="<?php echo e(backpack_url('pages')); ?>">Pages</a></li>
		<li>Edit Page</li>
	</ol>
</section>
<?php $__env->stopSection(); ?>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

<?php $__env->startSection('content'); ?>
<div class="body">
	<?php if(\Session::has('success')): ?>
        <div class="alert alert-success">
            <?php echo e(\Session::get('success')); ?>

        </div>
    <?php endif; ?>



	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="<?php echo e(action('PageController@update',$id)); ?>" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="<?php echo e(csrf_token()); ?>" name="_token" />
				<label for="title">Page Title:</label>
				<input type="text" class="form-control" name="page_title" value="<?php echo e($page->page_title); ?>"/>
			</div>
			<div class="form-group">
			
				<label for="description">Description:</label>
				<textarea name="page_description" class="form-control" id="page-ckeditor" ><?php echo e($page->page_description); ?></textarea>
			</div>

			<div class="form-group">
			
				<label for="description">Keywords</label>
				<textarea name="page_keywords" class="form-control" ><?php echo e($page->page_keywords); ?></textarea>
			</div>
			<div class="form-group">	
				<label for="image">Image:</label>
				<?php if($page->slider_id): ?>
					<img class="media-object" src="<?php echo e(App\Http\Controllers\SliderController::getImage($page->slider_id)); ?>" alt="no-img" height="150" width="150">
				<?php else: ?>
					<img class="media-object" src="<?php echo e(action('PageController@getImage',$page->id)); ?>" alt="no-img" height="150" width="150">
				<?php endif; ?>
			</div>
			<div class="form-group images">	
				<label for="image">Change Image:</label>
				<input type="file" class="form-control" onchange="$('.added-img').remove();" name="image" id="upload" />
				<?php if($errors->has('image')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('image')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group">	
				<label>OR</label>
			</div>
			<div class="form-group">
				<button type="button" class="btn btn-primary addimage" data-toggle="modal" data-target="#portfolioModal">Add Media</button>
			</div>
			<?php if($page->lang_code == 'en'): ?>
			<div class="form-group hidden">
				
				<label for="visible">Visible:</label>
				<input type="checkbox"  name="is_visible" value="1" checked="checked"  />
			<!-- 	<?php if($page->is_visible==1): ?>
					<input type="checkbox"  name="is_visible" value="1" checked="checked"  />
				<?php else: ?>
					<input type="checkbox"  name="is_visible" value="1"  />
				<?php endif; ?> -->
			</div>
			<?php else: ?>
			<input type="hidden" name="is_visible" value="1">
			<?php endif; ?>
			<input type ="hidden" name="added_by" value="<?php echo e(Auth::user()->id); ?>" / >
			<button type="submit" class="btn btn-primary">Update</button>
			</form>
		</div>
	</div>

</div>

<script>
	window.onload = function() {
		CKEDITOR.replace( 'page-ckeditor', {
			filebrowserUploadUrl: '<?php echo e(route("upload",["_token" => csrf_token()])); ?>'
		});
	};
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
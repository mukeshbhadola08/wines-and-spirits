<?php $__env->startSection('header'); ?>
<section class="content-header">
	<ol class="breadcrumb">
		<li><a href="<?php echo e(backpack_url()); ?>"><?php echo e(config('backpack.base.project_name')); ?></a></li>
		<li>Settings</li>
	</ol>
</section>
<?php $__env->stopSection(); ?>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<?php $__env->startSection('content'); ?>
<div class="body">
	<?php if(\Session::has('success')): ?>
        <div class="alert alert-success">
            <?php echo e(\Session::get('success')); ?>

        </div>
    <?php endif; ?>
	<?php if($errors->any()): ?>
		<div class="alert alert-danger">
			<ul>
				<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<li><?php echo e($error); ?></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</ul>
		</div>
	<?php endif; ?>
	
	<?php
	foreach ($settings as $setting){
		$variable = $setting->option_name;
		$$variable = $setting->option_value;
	}
	?>
	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="<?php echo e(backpack_url('settings')); ?>" enctype="multipart/form-data" class="form-horizontal">
			
			<input type="hidden" value="<?php echo e(csrf_token()); ?>" name="_token" />

			<div class="sitesettings">
				<h3>Site Settings</h3><br/>
				<div class="form-group">
					<!-- <label for="site_title" class="col-sm-2 control-label">Site Title</label> -->
					<label for="site_title" class="col-sm-2 control-label">Company Name</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[site_title]" placeholder="Site Title" value="<?php echo e(config('constants.SITE_TITLE')); ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="admin_email" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[admin_email]" placeholder="Admin Email" value="<?php echo e(config('constants.ADMIN_EMAIL')); ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="phone_no" class="col-sm-2 control-label">Contact</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[phone_no]" placeholder="Phone no." value="<?php echo e(config('constants.PHONE_NO')); ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="phone_no" class="col-sm-2 control-label">Fax</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[phone_no]" placeholder="Phone no." value="<?php echo e(config('constants.PHONE_NO')); ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="address" class="col-sm-2 control-label">Address</label>
					<div class="col-sm-10">
						<textarea class="form-control" value="<?php echo e(config('constants.ADDRESS')); ?>" name="settings[address]"><?php echo e(config('constants.ADDRESS')); ?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="address" class="col-sm-2 control-label">Google API</label>
					<div class="col-sm-10">
						<textarea class="form-control" value="<?php echo e(config('constants.ADDRESS')); ?>" name="settings[address]"><?php echo e(config('constants.ADDRESS')); ?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="address" class="col-sm-2 control-label">Meta Keywords</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="metasetting" placeholder="Phone no." value="<?php echo e(config('constants.metasetting')); ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="tag_line" class="col-sm-2 control-label">Meta Description</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[tag_line]" placeholder="Tag Line" value="<?php echo e(config('constants.TAG_LINE')); ?>">
					</div>
				</div>
				<!-- <div class="form-group">
					<label for="tag_line" class="col-sm-2 control-label">Tag Line</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[tag_line]" placeholder="Tag Line" value="<?php echo e(config('constants.TAG_LINE')); ?>">
					</div>
				</div> -->
				<!-- <div class="form-group">
					<label for="language" class="col-sm-2 control-label">Language</label>
					<div class="col-sm-10">
						<select class="form-control" name="settings[language]" id="langauage_select">
							<?php
								$allLanguages = App\Language::where('disabled',0)->get();
								$sel_lang = 'es';
								if(config('constants.LANGUAGE') !== null){
									$sel_lang = config('constants.LANGUAGE');
								}
							?>
							<?php if(isset($allLanguages) && count($allLanguages)>0): ?>
								<?php $__currentLoopData = $allLanguages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<?php
										$selected ='';
										if($lang->code==$sel_lang){ 
											$selected = 'selected="selected"';
										}
									?>
									<option value="<?php echo e(trim($lang->code)); ?>" <?php echo e($selected); ?> ><?php echo e($lang->language); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<?php endif; ?>
						</select>
					</div>
				</div> -->

				<!-- <div class="form-group">
					<label for="address" class="col-sm-2 control-label">Address</label>
					<div class="col-sm-10">
						<textarea class="form-control" value="<?php echo e(config('constants.ADDRESS')); ?>" name="settings[address]"><?php echo e(config('constants.ADDRESS')); ?></textarea>
					</div>
				</div> -->

				<!-- <div class="form-group">
					<label for="city" class="col-sm-2 control-label">City</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[city]" placeholder="City" value="<?php echo e(config('constants.CITY')); ?>">
					</div>
				</div> -->
				<!-- <div class="form-group">
					<label for="state" class="col-sm-2 control-label">State</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[state]" placeholder="State" value="<?php echo e(config('constants.STATE')); ?>">
					</div>
				</div> -->
				<!-- <div class="form-group">
					<label for="zipcode" class="col-sm-2 control-label">Zip Code</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[zipcode]" placeholder="Zip Code" value="<?php echo e(config('constants.ZIPCODE')); ?>">
					</div>
				</div> -->

				<!-- <div class="form-group">
					<label for="admin_name" class="col-sm-2 control-label">Admin Name</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[admin_name]" placeholder="Admin Name" value="<?php echo e(config('constants.ADMIN_NAME')); ?>">
					</div>
				</div> -->
				<!-- <div class="form-group">
					<label for="preloader" class="col-sm-2 control-label">Preloader</label>
					<div class="col-sm-10">
						<textarea class="form-control" name="settings[preloader]"><?php echo e(config('constants.PRELOADER')); ?></textarea>
					</div>
				</div> 
			</div>-->
			<?php 
				// $pages = App\Page::where([['is_visible','=', 1],['lang_code','=','en']])->get();
				// $blogs = App\Blog::where([['is_visible','=', 1],['lang_code','=','en']])->get();
			?>
			<!-- <div class="sitesettings">
				<h3>Gym Information</h3><br/>
				<div class="form-group">
					<label for="gym" class="col-sm-2 control-label">Current Gym</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[gym]" placeholder="Current Gym" value="<?php echo e(config('constants.GYM')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">

								<select class="form-control" name="settings[gym_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.GYM_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.GYM_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.GYM_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.GYM_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.GYM_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.GYM_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.GYM_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.GYM_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.GYM_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.GYM_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="gym" class="col-sm-2 control-label">Coaches</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[coaches]" placeholder="Current Coaches" value="<?php echo e(config('constants.COACHES')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[coaches_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.COACHES_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.COACHES_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.COACHES_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.COACHES_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.COACHES_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.COACHES_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.COACHES_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.COACHES_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.COACHES_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.COACHES_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="club_membership" class="col-sm-2 control-label">Club Membership</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[club_membership]" placeholder="Club Membership" value="<?php echo e(config('constants.CLUB_MEMBERSHIP')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[club_membership_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.CLUB_MEMBERSHIP_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.CLUB_MEMBERSHIP_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.CLUB_MEMBERSHIP_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.CLUB_MEMBERSHIP_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.CLUB_MEMBERSHIP_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.CLUB_MEMBERSHIP_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.CLUB_MEMBERSHIP_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.CLUB_MEMBERSHIP_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.CLUB_MEMBERSHIP_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.CLUB_MEMBERSHIP_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="previous_gyms" class="col-sm-2 control-label">Previous Gyms</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[previous_gyms]" placeholder="Previous Gyms" value="<?php echo e(config('constants.PREVIOUS_GYMS')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[previous_gyms_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.PREVIOUS_GYMS_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.PREVIOUS_GYMS_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.PREVIOUS_GYMS_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.PREVIOUS_GYMS_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.PREVIOUS_GYMS_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.PREVIOUS_GYMS_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.PREVIOUS_GYMS_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.PREVIOUS_GYMS_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.PREVIOUS_GYMS_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.PREVIOUS_GYMS_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="coach" class="col-sm-2 control-label">Coach</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[coach]" placeholder="Coach" value="<?php echo e(config('constants.COACH')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[coach_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.COACH_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.COACH_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.COACH_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.COACH_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.COACH_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.COACH_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.COACH_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.COACH_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.COACH_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.COACH_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="level" class="col-sm-2 control-label">Level</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[level]" placeholder="Level" value="<?php echo e(config('constants.LEVEL')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[level_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.LEVEL_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.LEVEL_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.LEVEL_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.LEVEL_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.LEVEL_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.LEVEL_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.LEVEL_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.LEVEL_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.LEVEL_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.LEVEL_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="choreographer" class="col-sm-2 control-label">Choreographer</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[choreographer]" placeholder="Choreographer" value="<?php echo e(config('constants.CHOREOGRAPHER')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[choreographer_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.CHOREOGRAPHER_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.CHOREOGRAPHER_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.CHOREOGRAPHER_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.CHOREOGRAPHER_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.CHOREOGRAPHER_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.CHOREOGRAPHER_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.CHOREOGRAPHER_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.CHOREOGRAPHER_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.CHOREOGRAPHER_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.CHOREOGRAPHER_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="floormusic" class="col-sm-2 control-label">Floor Music</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[floormusic]" placeholder="Floor Music" value="<?php echo e(config('constants.FLOORMUSIC')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[floormusic_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.FLOORMUSIC_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.FLOORMUSIC_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.FLOORMUSIC_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.FLOORMUSIC_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.FLOORMUSIC_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.FLOORMUSIC_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.FLOORMUSIC_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.FLOORMUSIC_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.FLOORMUSIC_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.FLOORMUSIC_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="hourstrained" class="col-sm-2 control-label">Hours Trained</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[hourstrained]" placeholder="Hours Trained" value="<?php echo e(config('constants.HOURSTRAINED')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[hourstrained_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.HOURSTRAINED_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.HOURSTRAINED_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.HOURSTRAINED_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.HOURSTRAINED_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.HOURSTRAINED_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.HOURSTRAINED_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.HOURSTRAINED_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.HOURSTRAINED_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.HOURSTRAINED_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.HOURSTRAINED_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>

			</div> -->
		
			<!-- <div class="sitesettings">
				<h3>General Information</h3><br/>
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label">Name</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[name]" placeholder="Name" value="<?php echo e(config('constants.NAME')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[name_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.NAME_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.NAME_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.NAME_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.NAME_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.NAME_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.NAME_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.NAME_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.NAME_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.NAME_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.NAME_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="dob" class="col-sm-2 control-label">Date of Birth</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[dob]" placeholder="Date of Birth" value="<?php echo e(config('constants.DOB')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[dob_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.DOB_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.DOB_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.DOB_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.DOB_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.DOB_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.DOB_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.DOB_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.DOB_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.DOB_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.DOB_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="parents" class="col-sm-2 control-label">Parents</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[parents]" placeholder="Parents" value="<?php //echo e(config('constants.PARENTS')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[parents_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.PARENTS_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.PARENTS_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.PARENTS_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.PARENTS_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.PARENTS_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.PARENTS_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.PARENTS_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.PARENTS_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.PARENTS_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.PARENTS_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="siblings" class="col-sm-2 control-label">Siblings</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[siblings]" placeholder="Siblings" value="<?php //echo e(config('constants.SIBLINGS')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[siblings_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.SIBLINGS_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.SIBLINGS_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.SIBLINGS_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.SIBLINGS_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.SIBLINGS_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.SIBLINGS_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.SIBLINGS_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.SIBLINGS_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.SIBLINGS_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.SIBLINGS_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="address" class="col-sm-2 control-label">Address</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[address]" placeholder="Address" value="<?php //echo e(config('constants.ADDRESS')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[address_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.ADDRESS_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.ADDRESS_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.ADDRESS_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.ADDRESS_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.ADDRESS_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.ADDRESS_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.ADDRESS_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.ADDRESS_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.ADDRESS_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.ADDRESS_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="school" class="col-sm-2 control-label">School</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[school]" placeholder="Hours Trained" value="<?php //echo e(config('constants.SCHOOL')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[school_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.SCHOOL_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.SCHOOL_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.SCHOOL_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.SCHOOL_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.SCHOOL_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.SCHOOL_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.SCHOOL_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.SCHOOL_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.SCHOOL_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.SCHOOL_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="grade" class="col-sm-2 control-label">Grade</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[grade]" placeholder="Grade" value="<?php //echo e(config('constants.GRADE')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[grade_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.GRADE_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.GRADE_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.GRADE_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.GRADE_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.GRADE_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.GRADE_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.GRADE_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.GRADE_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.GRADE_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.GRADE_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="gpa" class="col-sm-2 control-label">GPA</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[gpa]" placeholder="GPA" value="<?php //echo e(config('constants.GPA')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[gpa_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.GPA_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.GPA_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.GPA_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.GPA_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.GPA_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.GPA_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.GPA_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.GPA_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.GPA_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.GPA_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="awards" class="col-sm-2 control-label">Awards</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[awards]" placeholder="Awards" value="<?php //echo e(config('constants.AWARDS')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[awards_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.AWARDS_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.AWARDS_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.AWARDS_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.AWARDS_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.AWARDS_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.AWARDS_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.AWARDS_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.AWARDS_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.AWARDS_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.AWARDS_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="contact" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="settings[contact]" placeholder="Email" value="<?php //echo e(config('constants.CONTACT')); ?>">
					</div>
					<div class="col-sm-4">
						<div id="page_form_fields" >
							<div class="form-group">
								<select class="form-control" name="settings[contact_page]" >
										<option value="">Select Page</option>
										 <option <?php if(config('constants.CONTACT_PAGE') == '/'): ?> selected <?php else: ?> <?php endif; ?> value="/">Home Page</option>
										 <option <?php if(config('constants.CONTACT_PAGE') == '/grades/list'): ?> selected <?php else: ?> <?php endif; ?> value="/grades/list">Grades Listing</option>
										 <option <?php if(config('constants.CONTACT_PAGE') == '/coaches/list'): ?> selected <?php else: ?> <?php endif; ?> value="/coaches/list">Coaches Listing</option>
										 <option <?php if(config('constants.CONTACT_PAGE') == '/events/list'): ?> selected <?php else: ?> <?php endif; ?> value="/events/list">Events Listing</option>
										 <option <?php if(config('constants.CONTACT_PAGE') == '/results/list'): ?> selected <?php else: ?> <?php endif; ?> value="/results/list">Results Listing</option>
										 <option <?php if(config('constants.CONTACT_PAGE') == '/portfolio/list?type=Photos'): ?> selected <?php else: ?> <?php endif; ?> value="/portfolio/list?type=Photos">Portfolio Listing</option>
										 <option <?php if(config('constants.CONTACT_PAGE') == '/news/list'): ?> selected <?php else: ?> <?php endif; ?> value="/news/list">News Listing</option>
										 <option <?php if(config('constants.CONTACT_PAGE') == '/routine/list'): ?> selected <?php else: ?> <?php endif; ?> value="/routine/list">Routines Listing</option>
										 <?php if(isset($pages) && count($pages)>0): ?>
											<optgroup label="Pages">
											<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$page = App\Page::where('id', $p->id)->first();
												$page_url = '/' . $page->clean_url;
											?>
												<option <?php if(config('constants.CONTACT_PAGE') == $page_url): ?> selected <?php else: ?> <?php endif; ?> value="/<?php echo e($page->clean_url); ?>"><?php echo e($p->page_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>
										 
										  <?php if(isset($blogs) && count($blogs)>0): ?>
											<optgroup label="News">
											<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$blog = App\Blog::where('id', $p->id)->first();
												$blog_url = '/news/' . $blog->clean_url;
											?>
												<option <?php if(config('constants.CONTACT_PAGE') == $blog_url): ?> selected <?php else: ?> <?php endif; ?> value="/news/<?php echo e($blog->clean_url); ?>"><?php echo e($p->blog_title); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</optgroup>
										 <?php endif; ?>	 
								</select>
							</div>
						</div>
					</div>
				</div>

			</div> -->
			<!-- <div class="sitesettings">
				<h3>Social Media Links</h3><br/>
				<div class="form-group">
					<label for="youtube_url" class="col-sm-2 control-label">Youtube URL</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[youtube_url]" placeholder="Youtube URL" value="<?php //echo e(config('constants.YOUTUBE_URL')); ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="facebook_url" class="col-sm-2 control-label">Facebook URL</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[facebook_url]" placeholder="Facebook URL" value="<?php echo e(config('constants.FACEBOOK_URL')); ?>">
					</div>
				</div>

				<div class="form-group">
					<label for="twitter_url" class="col-sm-2 control-label">Twitter URL</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[twitter_url]" placeholder="Twitter URL" value="<?php echo e(config('constants.TWITTER_URL')); ?>">
					</div>
				</div>

				<div class="form-group">
					<label for="instagram_url" class="col-sm-2 control-label">Instagram URL</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[instagram_url]" placeholder="Instagram URL" value="<?php echo e(config('constants.INSTAGRAM_URL')); ?>">
					</div>
				</div>
 -->
				<!-- <div class="form-group">
					<label for="linkedin_url" class="col-sm-2 control-label">LinkedIn URL</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[linkedin_url]" placeholder="LinkedIn URL" value="<?php echo e(config('constants.LINKEDIN_URL')); ?>">
					</div>
				</div> -->


				<!-- <div class="form-group">
					<label for="instagram_url" class="col-sm-2 control-label">Google Plus URL</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[googleplus_url]" placeholder="Instagram URL" value="<?php echo e(config('constants.GOOGLEPLUS_URL')); ?>">
					</div>
				</div>

			</div> -->
			
			<!-- <div class="sitesettings">
				<h3>Textareas</h3><br/>
				<div class="form-group">
					<label for="routinestextarea" class="col-sm-2 control-label">Routines textarea on top</label>
					<div class="col-sm-10">
						<textarea class="form-control" name="settings[routinestextarea]" id="routinestextarea-ckeditor" ><?php //echo e(config('constants.ROUTINESTEXTAREA')); ?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="coachestextarea" class="col-sm-2 control-label">Coaches textarea on top</label>
					<div class="col-sm-10">
						<textarea class="form-control" name="settings[coachestextarea]" id="coachestextarea-ckeditor"><?php //echo e(config('constants.COACHESTEXTAREA')); ?></textarea>
					</div>
				</div>
			</div> -->
			
			<!-- <div class="sitesettings">
				<h3>Admin Password</h3><br/>
				<div class="form-group">
					<label for="admin_pwd" class="col-sm-2 control-label">Admin Password For Smtp</label>
					<div class="col-sm-10">
						<input type="password" class="form-control"  name="settings[admin_pwd]" value="<?php //echo config('constants.ADMIN_PWD'); ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="coachestextarea" class="col-sm-2 control-label">Coaches textarea on top</label>
					<div class="col-sm-10">
						<textarea class="form-control" name="settings[coachestextarea]" id="coachestextarea-ckeditor"><?php //echo e(config('constants.COACHESTEXTAREA')); ?></textarea>
					</div>
				</div>
			</div>
			<div class="sitesettings">
				<h3>No. of Events/News</h3><br/>
				<div class="form-group">
					<label for="noofeventsnews" class="col-sm-2 control-label">No. of Events/News</label>
					<div class="col-sm-10">
						<input type="number" min="2" max="10" class="form-control" name="settings[noofeventsnews]" placeholder="No. of Events/News" value="<?php //echo e(config('constants.NOOFEVENTSNEWS')); ?>">
					</div>
				</div>
			</div> -->
			<button type="submit" class="btn btn-primary">Update Settings</button>
			</form>
		</div>
	</div>

</div>

<script>
	CKEDITOR.replace( 'routinestextarea-ckeditor' , {
			filebrowserUploadUrl: '<?php echo e(route("upload",["_token" => csrf_token()])); ?>'
		});
	CKEDITOR.replace( 'coachestextarea-ckeditor' , {
			filebrowserUploadUrl: '<?php echo e(route("upload",["_token" => csrf_token()])); ?>'
		});
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
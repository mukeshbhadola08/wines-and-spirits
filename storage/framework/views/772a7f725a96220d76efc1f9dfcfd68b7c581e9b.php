<?php $__env->startSection('header'); ?>
    <section class="content-header">
      <h1>
         Pages
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(backpack_url()); ?>"><?php echo e(config('backpack.base.project_name')); ?></a></li>
        <li class="active">Pages</li>
      </ol>
    </section>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<div class="body">
	<?php if(\Session::has('success')): ?>
        <div class="alert alert-success">
            <?php echo e(\Session::get('success')); ?>

        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-sm-12">
            
				<a href="<?php echo e(url('/admin/pages/create')); ?>" class="btn btn-primary">Add Page</a>
			
        </div>
    </div>
	<br />
	<div class="row">
		<div class="col-sm-12">
			<?php if(isset($pages) && count($pages)>0): ?>
				<table class="table table-striped" id="pages_table">
					<thead>
						<tr>
						  <th width="5%">Image</th>
						  <th width="5%">Slug</th>
						  <th width="15%">Title</th>
						  <th class="hidden" width="7%">Visible</th>
						  <th width="15%">Keywords</th>
						  <th class="hidden" width="8%">Dutch</th>
						  <th width="25%">Action</th>
						</tr>
					</thead>
					<tbody>
						
					<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pageDetails): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						
						<tr>
							<td>
								<?php if($pageDetails->slider_id): ?>
									<img class="media-object" src="<?php echo e(App\Http\Controllers\SliderController::getImage($pageDetails->slider_id)); ?>" alt="no-img" height="100" width="100">
								<?php else: ?>
									<img class="media-object" src="<?php echo e(action('PageController@getImage',$pageDetails->id)); ?>" alt="no-img" height="100" width="100">
								<?php endif; ?>
							</td>
							<td><?php echo e($pageDetails->clean_url); ?></td>
							<td><?php echo e($pageDetails->page_title); ?></td>
							<td class="hidden"><?php if($pageDetails->is_visible==1): ?> YES <?php else: ?> No <?php endif; ?></td>
							<td><?php echo e($pageDetails->page_keywords); ?></td>
							<td class="hidden">
								<?php
								 $parentDetails = App\Page::where('parent_lang_id',$pageDetails->id)->first();
								?>
								<?php if(empty($parentDetails)): ?>
									<a href="<?php echo e(action('PageController@addInSpanish',$pageDetails->id)); ?>" class="btn btn-info" ><i class="fa fa-language" ></i>&nbsp;Add Dutch</a>
								<?php else: ?>
									<a href="<?php echo e(action('PageController@edit',$parentDetails->id)); ?>" class="btn btn-info"><i class="fa fa-language" ></i>&nbsp;Edit Dutch</a>
								<?php endif; ?>
							</td>
							<td>
								<a href="<?php echo e(action('PageController@view',$pageDetails->clean_url)); ?>" class="btn btn-success" target="_blank"><i class="fa fa-eye"></i>&nbsp;View</a>&nbsp;
								<a href="<?php echo e(action('PageController@edit',$pageDetails->id)); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>&nbsp;

								<?php if(isset($pageDetails->clean_url) && ($pageDetails->clean_url != "about-us" && $pageDetails->clean_url != "privacy-policy" && $pageDetails->clean_url != "contact-us")): ?>
								<a href="<?php echo e(action('PageController@delete',$pageDetails->id)); ?>" class="btn btn-danger delete_page"><i class="fa fa-trash-o"></i>&nbsp;DELETE</a>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
					</tbody>
				</table>
			<?php else: ?>
			<div class="alert alert-info">
				There are no pages added yet.
			</div>
			<?php endif; ?>
		</div>
	</div>

</div >
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
<script src="<?php echo e(asset('vendor/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('vendor/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<script>
$(document).ready(function() {
    $('#pages_table').DataTable( {
        "order": [[ 1, "asc" ]]
    });
	$('#pages_table_paginate').addClass('pull-right');
});	
jQuery(document).on('click','.delete_page',function(e){
	if(confirm("Are you sure all related information also be deleted?")){
		return true;
	} else{
		return false;
	}
});		
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
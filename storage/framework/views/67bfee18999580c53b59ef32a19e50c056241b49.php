<!DOCTYPE html>
<html>
<head>

	<meta charset="UTF-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="description" content="">

    <meta name="author" content="">

    <link rel="icon" href="favicon.ico">

	<title><?php echo e(config('constants.SITE_TITLE')); ?> <?php if(isset($title)){ echo " - ". $title; } ?></title>

	
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="<?php echo e(asset('/')); ?>dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo e(asset('/')); ?>dist/css/style.css" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo e(asset('/')); ?>dist/fontawesome/css/font-awesome.css">	
	<link rel="stylesheet" href="<?php echo e(asset('/')); ?>dist/owlcarousel/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="<?php echo e(asset('/')); ?>dist/owlcarousel/assets/owl.carousel.min.css">	


	<script charset="utf-8" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	<!-- <script charset="utf-8" src="<?php echo e(asset('/')); ?>dist/lib/js/bootstrap.min.js"></script> -->
	<script charset="utf-8" src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

<style type="text/css">
/*.media:hover ul {
	display: block !important;
	
}
.media:hover a{
	 background: #ffffff;
}
.media ul {
	
	/*position: absolute;
	top: 44px;
	right: -1px;
	position: absolute;
	top: 44px;
	right: -30px;
	 padding: 10px 0;
	box-shadow: 0px 0px 30px rgba(127, 137, 161, 0.25);
	transition: ease all 0.3s;
}
.media ul a{
	border-radius: 0;
	background: #ffffff;
	color: #000;
	margin-left: 0;
	text-align: center;
	padding-left: 0;
	padding-right: 0;
}
@media (min-width: 992px){
		.media ul a{
		border-radius: 0;
		background: #ffffff;
		color: #000;
		margin-left: 0 !important;

	}
}
.media li{
	border: 1px solid #c1c1c1;*/
	   /*width: 66px;  width: 90px;
	text-align: center;
	  
}
@media (max-width: 991px){
	.media:hover a{
		 background: transparent;
	}
	.media{

		position: relative;
		height: 140px;
	}
	.media ul a{
		padding-left: 35px;
		 background: transparent !important;
		color: white;
	}
	
	.media ul li{
		border: none;
	}
	.media ul{
		display: block !important;
		width: 100%;
		margin-left: -84px;
		top: 44px;
		right: 0;
		 box-shadow: 0px 0px 30px rgba(127, 137, 161, 0.0);
	
	}
}*/
</style>	

</head>
<?php 
global $lang_val;

// active class for language
$activeClasssp = "";
$activeClassen = "";
if($lang_val=="es") {
	$activeClasssp = "active";
} else {
	$activeClassen = "active";
}
// active class for language
?>
 <body>
 
	<!--==========================
	Header
	============================-->
	<header id="header" style="display: none;">   
		<div class="container">
			<div class="logo float-left logo-left">
				<!-- Uncomment below if you prefer to use an image logo -->
				<h1 class="text-light"><a href="<?php echo url('/'); ?>" class="scrollto"><?php echo e(config('constants.SITE_TITLE')); ?><span><?php echo e(config('constants.TAG_LINE')); ?></span></a></h1>
				<!-- <a href="#header" class="scrollto"><img src="images/logo.png" alt="" class="img-fluid"></a> -->
			</div>
			<nav class="main-nav float-right d-none d-lg-block">
				
				<ul>
					
					<?php

								$navigations = App\Navigation::where([['is_visible','=',1],['show_header','=',1],['parent_id','=',0],['lang_code','=','en']])->orderBy('nav_order', 'asc')->get();

							?>
							<?php if(isset($navigations) && count($navigations)>0): ?>
								<?php $__currentLoopData = $navigations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<?php

										$nav_title = $nav->title;
										if($nav->type == 1){
											$pageDetails = $nav->page($nav->type)->first();
											$slug = $pageDetails->clean_url;
											$pageurl = '/'.$slug;
											
										} else if($nav->type == 2){
											$blogDetails = $nav->page($nav->type)->first();
											$slug = $blogDetails->clean_url;
											$pageurl = '/blogs/'.$slug;
										} else {
											if($nav->static_page_name == "eventslisting"){
												$pageurl = "/events/list";
											} else if($nav->static_page_name == "resultslisting"){
												$pageurl = "/results/list";
											} else if($nav->static_page_name == "portfoliolisting"){
												$pageurl = "/portfolio/list";
												
											} else if($nav->static_page_name == "newslisting"){
												$pageurl = "/news/list";
											} else if($nav->static_page_name == "routinelisting"){
												$pageurl = "/routine/list";
											} else {
												$pageurl = "/";
											}
										}

										if($lang_val !='en'){
											$navDetails = App\Navigation::where([['parent_lang_id','=',$nav->id],['lang_code','=',$lang_val]])->first();
											
											if(!empty($navDetails)){
												$nav_title = $navDetails->title;
												if($navDetails->type == 1){
													$pageDetails = $navDetails->page($navDetails->type)->first();
													$slug = $pageDetails->clean_url;
													$pageurl = '/'.$slug;
													
												} else {
													$blogDetails = $navDetails->page($navDetails->type)->first();
													$slug = $blogDetails->clean_url;
													$pageurl = '/blogs/'.$slug;
												}
											}
										}
										

										$subnavigations = App\Navigation::where([['is_visible','=',1],['parent_id','=',$nav->id],['lang_code','=','en']])->orderBy('nav_order', 'asc')->get();
									?>
									<?php if(count($subnavigations)>0): ?>
										<li class="dropdown dropdown-nav">
											<a href="<?php echo e(url($pageurl)); ?>" class="dropdown-href"> <?php echo e($nav_title); ?></a> 
											<a class="dropdown-toggle dropdown-icon " data-toggle="dropdown"  data-hover="dropdown" data-close-others="true"><span class="caret"></span> </a>
										</li>
									<?php else: ?>
										<?php
										$page_url = $pageurl;
										$pageurl = strtok($pageurl,'?');
										$page_slug = trim($pageurl, '/');
										$active = Request::path() == $page_slug ? 'active': '';

										if(Request::path() == $pageurl){
											$active = 'active';
											
										}
										?>
										
										
										<li class="<?php echo e($active); ?> <?php if($pageurl == '/portfolio/list'): ?> drop-down media <?php endif; ?>" >
											<?php if($pageurl == "/portfolio/list"): ?>
											
											<?php else: ?>
											<a href="<?php echo e(url($page_url)); ?>"> <?php echo e($nav_title); ?>  </a>
											<?php endif; ?>
											<?php if($pageurl == "/portfolio/list"): ?>
								            <li class="drop-down"><a href="javascript:;"> <?php echo e($nav_title); ?></a>
								                <ul>
								                  <li><a href="<?php echo e(url($page_url)); ?>?type=Videos"> Videos </a></li>
													<li>
													<a href="<?php echo e(url($page_url)); ?>?type=Photos"> Photos </a>
													</li>
								                </ul>
								              </li>
								            <?php endif; ?>
								          </li>
											
										
									<?php endif; ?>
									
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<?php endif; ?>
				</ul>
			</nav><!-- .main-nav -->
		  
		</div>
	</header>
	<!-- header-->
  <div class='thetop'></div>
  <header class="header">
    <nav class="navbar navbar-expand-lg custom-navbar">
      <div class="container">
        <a href="index.html"> <img class="navbar-brand" src="<?php echo asset('dist/images/logo.png'); ?>" id="logo_custom"></a>
        <button class="navbar-toggler navbar-toggler-right custom-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse navbar-expand-lg" id="collapsibleNavbar">
          <ul class="navbar-nav ml-auto">
            <!-- <li class="nav-item active">
              <a class="nav-link">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link">Wines</a>
            </li>
            <li class="nav-item">
              <a class="nav-link">Spirits</a>
            </li>  
            <li class="nav-item">
              <a class="nav-link">Our History</a>
            </li> 
             <li class="nav-item">
              <a class="nav-link">Blogs</a>
            </li> 
            <li class="nav-item ">
              <a class="nav-link ">Contact Us</a>
            </li> -->
            <?php
				$navigations = App\Navigation::where([['is_visible','=',1],['show_header','=',1],['parent_id','=',0],['lang_code','=','en']])->orderBy('nav_order', 'asc')->get();
			?>
			<?php if(isset($navigations) && count($navigations)>0): ?>
				<?php $__currentLoopData = $navigations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php

						$nav_title = $nav->title;
						if($nav->type == 1){
							$pageDetails = $nav->page($nav->type)->first();
							$slug = $pageDetails->clean_url;
							$pageurl = '/'.$slug;
							
						} else if($nav->type == 2){
							$blogDetails = $nav->page($nav->type)->first();
							$slug = $blogDetails->clean_url;
							$pageurl = '/blogs/'.$slug;
						} else {
							if($nav->static_page_name == "wineslisting"){
								$pageurl = "/wines";
							} else if($nav->static_page_name == "resultslisting"){
								$pageurl = "/results/list";
							} else if($nav->static_page_name == "portfoliolisting"){
								$pageurl = "/portfolio/list";
								
							} else if($nav->static_page_name == "newslisting"){
								$pageurl = "/news/list";
							} else if($nav->static_page_name == "routinelisting"){
								$pageurl = "/routine/list";
							} else {
								$pageurl = "/";
							}
						}

						if($lang_val !='en'){
							$navDetails = App\Navigation::where([['parent_lang_id','=',$nav->id],['lang_code','=',$lang_val]])->first();
							
							if(!empty($navDetails)){
								$nav_title = $navDetails->title;
								if($navDetails->type == 1){
									$pageDetails = $navDetails->page($navDetails->type)->first();
									$slug = $pageDetails->clean_url;
									$pageurl = '/'.$slug;
									
								} else {
									$blogDetails = $navDetails->page($navDetails->type)->first();
									$slug = $blogDetails->clean_url;
									$pageurl = '/blogs/'.$slug;
								}
							}
						}
						

						
					?>
					
						<?php
						$page_url = $pageurl;
						$pageurl = strtok($pageurl,'?');
						$page_slug = trim($pageurl, '/');
						$active = Request::path() == $page_slug ? 'active': '';

						if(Request::path() == $pageurl){
							$active = 'active';
							
						}
						?>
						<li class="<?php echo e($active); ?> nav-item">
							<a href="<?php echo e(url($page_url)); ?>" class="nav-link"> <?php echo e($nav_title); ?>  </a>
				        </li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php endif; ?>
          </ul>
           <div class="navbar-right">
            <form class="search-form" role="search">
              <div class="form-group pull-right" id="search">
                <input type="text" class="form-control" placeholder="Search">
                <button type="submit" class="form-control form-control-submit">Submit</button>
                <span class="search-label"><i class="fa fa-search" aria-hidden="true"></i></span>
              </div>
            </form>
          </div>
           <div class="language-outer">
            <ul class="navbar-nav language-btn ">
             <li class="dropdown ">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <img src="<?php echo asset('dist/images/english-flag.png'); ?>" alt="English-flag"/> English
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#"><img src="<?php echo asset('dist/images/english-flag.png'); ?>" alt="English-flag"/> English</a>
                <a class="dropdown-item" href="#"><img src="<?php echo asset('dist/images/spanish-flag.png'); ?>" alt="spanish-flag.png"/> Spanish</a>
               </div>
            </li>
          </ul>
        </div>
        </div>
      </div>  
    </nav>
  </header>
  <!-- / header -->
<!--header-->
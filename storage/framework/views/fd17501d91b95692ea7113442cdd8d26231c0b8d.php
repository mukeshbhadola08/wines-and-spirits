<!--==========================
    Footer
  ============================-->
  <!-- <footer id="footer" class="section-bg">
    <div class="container">
    <div class="social-links"> 
      <a href="<?php echo e(config('constants.YOUTUBE_URL')); ?>"><i class="fa fa fa-youtube"></i></a>
      <a href="<?php echo e(config('constants.FACEBOOK_URL')); ?>"><i class="fa fa-facebook"></i></a>
      <a href="<?php echo e(config('constants.TWITTER_URL')); ?>"><i class="fa fa-twitter"></i></a>
      <a href="<?php echo e(config('constants.GOOGLEPLUS_URL')); ?>"><i class="fa fa-google-plus"></i></a>      
      <a href="<?php echo e(config('constants.INSTAGRAM_URL')); ?>"><i class="fa fa-instagram"></i></a>     
    </div>
    <div class="copyright">
      Copyright &copy; <strong><?php echo e(config('constants.SITE_TITLE')); ?></strong>  2019. All Rights Reserved.
    </div>      
    </div>
  <div id="sheru"></div>
  <div id="imageurl"></div>
  </footer> #footer  -->



  <!--footer-->
  <footer >
  <div class="footer-bg">
    <div class="container">
      <div class="row">
        <div class="col-sm-4 col-md-4 col-12">
          <h3>Essential Details</h3>
          <div class="footer-nav">
            <ul>
              <li><a href="#">Home</a></li>
              <li><a href="#">Spirits</a> </li>
              <li><a href="#"> Wines</a></li>
              <li><a href="#">Our History</a></li>
              <li><a href="#">Blog</a> </li>
              <li><a href="#"> Contact Us </a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-4 col-md-4 col-12">
          <div class="footer-logo mr-auto text-center">
            <img src="<?php echo asset("dist/images/footer-logo.png"); ?>" alt="footer-logo">
          </div>
        </div>
        <div class="col-sm-4 col-md-4 col-12">
          <h3> Stay In Touch</h3>
          
          <div class="footer-social-links">
            <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
            <a href="#" title="Twitter" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            <a href="#" title="Google+" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            <a href="#" title="LinkedIn+" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
            <a href="#" title="Dribbble" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="copyright text-center">
    <p>Design By IDEA Foundation</p>
  </div>
</footer>
<!-- / footer -->
  <div class='scrolltop'>
      <div class='scroll icon'><i class="fa fa-4x fa-angle-up"></i></div>
  </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo e(asset('/')); ?>dist/js/jquery.js"></script>
    <script src="<?php echo e(asset('/')); ?>dist/js/bootstrap.min.js" ></script>
    <script src="<?php echo e(asset('/')); ?>dist/js/main.js"></script>
    <script src="<?php echo e(asset('/')); ?>dist/owlcarousel/owl.carousel.min.js"></script>
    <script>
      $(document).ready(function(){
        $('#search').on("click",(function(e){
        $(".form-group").addClass("sb-search-open");
          e.stopPropagation()
        }));
         $(document).on("click", function(e) {
          if ($(e.target).is("#search") === false && $(".form-control").val().length == 0) {
            $(".form-group").removeClass("sb-search-open");
          }
        });
          $(".form-control-submit").click(function(e){
            $(".form-control").each(function(){
              if($(".form-control").val().length == 0){
                e.preventDefault();
                $(this).css('border', '2px solid #39b54a');
              }
          })
        })
      })

       $(document).ready(function() {
              $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                navText: ['<img src="<?php echo e(asset("/")); ?>/dist/images/left-arrow-slider.svg" style="width:25px;"/>','<img src="<?php echo e(asset("/")); ?>/dist/images/right-arrow-slider.svg" style="width:25px;"/>'] ,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
                    nav: true
                  },
                  600: {
                    items: 1,
                    nav: false
                  },
                  1000: {
                    items: 1,
                    nav: true,
                    loop: false,
                    margin: 20
                  }
                }
              })
            })

            $(window).scroll(function() {
      if ($(this).scrollTop() > 50 ) {
          $('.scrolltop:hidden').stop(true, true).fadeIn();
      } else {
          $('.scrolltop').stop(true, true).fadeOut();
      }
  });
  $(function()
    {$(".scroll").click(function(){$("html,body").animate({scrollTop:$(".thetop").offset().top},"1000");return false})})
     


    </script>
</body>
</html>

<?php $__env->startSection('after_styles'); ?>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
    <section class="content-header">
      <h1>
         History
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(backpack_url()); ?>"><?php echo e(config('backpack.base.project_name')); ?></a></li>
        <li class="active">History</li>
      </ol>
    </section>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<div class="body">
	<?php if(\Session::has('success')): ?>
        <div class="alert alert-success">
            <?php echo e(\Session::get('success')); ?>

        </div>
    <?php endif; ?>
    <?php if(\Session::has('danger')): ?>
        <div class="alert alert-danger">
            <?php echo e(\Session::get('danger')); ?>

        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-sm-12"><a href="<?php echo e(url('/admin/history/create')); ?>" class="btn btn-primary">Add History</a></div>
    </div><hr>

	<div class="row">
		<div class="col-sm-12">
			<?php if(isset($slides) && count($slides)>0): ?>
				<table class="table table-striped" id="example1">
					<thead>
						<tr>
						  <th></th>
						  <th width="25%">Image/Video</th>
						  <th width="20%">Title</th>
						  <th>Description</th>
						  <th class="hidden" width="15%">Is Visible</th>
						  <th width="" class="hidden">Dutch</th>
						  <th width="35%">Action</th>
						</tr>
					</thead>
					<tbody id="nav-menu-body-slider">
						
					<?php $__currentLoopData = $slides; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slideDetails): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr class="dd-item-slider-tr" data-id="<?php echo e($slideDetails->id); ?>">
							<td width="5%" class="dd-handle-slider-field"><div style="width:40px; text-align:center;"><i class="fa fa-arrows"></i></div></td>
							<td>
								<?php
									$video_key  = App\Http\Controllers\SliderController::YoutubeID($slideDetails->id);
								?>
								<?php if($video_key): ?>
									<iframe width="100" height="100" src="http://www.youtube.com/embed/<?php echo e($video_key); ?>" frameborder="0" allowfullscreen></iframe>
								<?php else: ?>
									<img class="media-object" src="<?php echo e(url('/public/uploads/slider_images/' . $slideDetails->image)); ?>" alt="no-img" height="100">
								<?php endif; ?>
							</td>
							<td><?php echo e($slideDetails->title); ?></td>
							<td><?php echo html_entity_decode($slideDetails->description) ?></td>
							<td class="hidden"><?php if($slideDetails->is_visible): ?> YES <?php else: ?> No <?php endif; ?></td>
							
							<td class="hidden">
							<?php

							 $parentDetails = App\Slider::where('parent_lang_id',$slideDetails->id)->first();

							?>

							<?php if(empty($parentDetails)): ?>

								<a href="<?php echo e(action('SliderController@addInSpanish',$slideDetails->id)); ?>" class="btn btn-info" ><i class="fa fa-language" ></i>&nbsp;Add Dutch</a>

							<?php else: ?>

								<a href="<?php echo e(action('SliderController@edit',$parentDetails->id)); ?>" class="btn btn-info"><i class="fa fa-language" ></i>&nbsp;Edit Dutch</a>

							<?php endif; ?>
							</td>
							
							<td>
								<a href="<?php echo e(action('SliderController@edit',$slideDetails->id)); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>&nbsp;								
								<a href="<?php echo e(action('SliderController@delete',$slideDetails->id)); ?>" class="btn btn-danger delete_slide"><i class="fa fa-trash-o"></i>&nbsp;DELETE</a>
								
							</td>
						</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
					</tbody>
				</table>
			<?php else: ?>
			<div class="alert alert-info">
				There are no Portfolio added yet.
			</div>
			<?php endif; ?>
		</div>
	</div>

</div >
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo e(asset('vendor/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('vendor/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<script>		 
jQuery(document).on('click','.delete_slide',function(e){
	if(confirm("Are you sure want to delete this slide?")){
		return true;
	} else{
		return false;
	}
});		

$(function() {
	var fieldOrder = [];
	$( "#nav-menu-body-slider" ).sortable({			
		handle: ".dd-handle-slider-field",
		update: function(event, ui) {
			$('.dd-item-slider-tr').each(function(){					
				fieldOrder.push($(this).attr('data-id'));					
			});
			$.ajax({
			   type:'POST',
			   url:'/admin/media_order',
			   data:{'_token': '<?php echo csrf_token() ?>','order':fieldOrder},
			   success:function(cost){
				 new PNotify({'text':'Media has been re-orders.','type':'success'});
				 fieldOrder.length = 0;
			   }
			});
		}
	});
});

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
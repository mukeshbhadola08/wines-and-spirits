<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>Add Page</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(backpack_url()); ?>"><?php echo e(config('backpack.base.project_name')); ?></a></li>
		<li><a href="<?php echo e(backpack_url('pages')); ?>">Pages</a></li>
		<li>Add Page</li>
	</ol>
</section>
<?php $__env->stopSection(); ?>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

<?php $__env->startSection('content'); ?>
<div class="body">
	
	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="<?php echo e(backpack_url('pages/create')); ?>" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="<?php echo e(csrf_token()); ?>" name="_token" />
				<label for="title">Page Title:</label>
				<input type="text" class="form-control" name="page_title" value ="<?php echo e(old('page_title')); ?>" />
				<?php if($errors->has('page_title')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('page_title')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group">
			
				<label for="permalink">Permalinks:</label>
				<input type="text" class="form-control" name="permalink" value ="<?php echo e(old('permalink')); ?>" />
				<?php if($errors->has('permalink')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('permalink')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group">
			
				<label for="description">Description:</label>
				<textarea name="page_description" class="form-control" id="page-ckeditor"><?php echo e(old('page_description')); ?></textarea>
				<?php if($errors->has('page_description')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('page_description')); ?></strong>
					</span>
				<?php endif; ?>
			</div>

			<div class="form-group">
			
				<label for="description">Keywords</label>
				<textarea name="page_keywords" class="form-control" ><?php echo e(old('page_keywords')); ?></textarea>
			</div>
			<div class="form-group">	
				<label for="image">Default Image:</label>
				
				<img class="media-object" src="<?php echo asset("img/intro-img-top.png"); ?>" alt="no-img" height="150" width="150">
			</div>
			 
			<div class="form-group images">	
				<label for="image">Change Default Image:</label>
				<input type="file" class="form-control" onchange="$('.added-img').remove();" name="image" id="upload" />
				<?php if($errors->has('image')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('image')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group">	
				<label>OR</label>
			</div>
			<div class="form-group">
				<button type="button" class="btn btn-primary addimage" data-toggle="modal" data-target="#portfolioModal">Add Media</button>
			</div>
			<div class="form-group hidden">
				
				<label for="visible">Visible:</label>
				<input type="checkbox"  name="is_visible" value="1" checked />
			</div>
			<input type ="hidden" name="added_by" value="<?php echo e(Auth::user()->id); ?>" / >
			<input type ="hidden" name="lang_code" value="en" / >
			<input type ="hidden" name="parent" value="0" / >
			<button type="submit" class="btn btn-primary">Create</button>
			</form>
		</div>
	</div>

</div>


<script>
	window.onload = function() {
		CKEDITOR.replace( 'page-ckeditor', {
			filebrowserUploadUrl: '<?php echo e(route("upload",["_token" => csrf_token()])); ?>'
		});
	};
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('site.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <!-- / header -->
  <!-- Our history -->
  <div class="wrapper-page" style='background-image: url(<?php echo e(asset("/")); ?>/dist/images/bg-wine.png);'> 
  	<div class="container">
		<!-- heading -->
		<div class="heading-section white-heading">
			<h2>Wines</h2>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Wines</li>
				</ol>
			</nav>
		</div>
		<!-- /heading -->
		<!-- inner wrapper -->
		<div class="inner-wrapper">
			<div class="row">
				<div class="col-sm-3">
					<div class="cat-shorting-left">
						<div class="heading-shorting-top">
							<h3>Search here</h3>
							<form method="post">
								<input type="text" name="search">
								<button type="submit">Submit</button>
							</form>
							
						</div>
						<div class="list-group">
							<div class="list-heading">Wines</div>
							<div class="list-area">
								<ul>
									<li><label><input type="checkbox" checked>Malbec</label></li>
									<li><label><input type="checkbox">Cabernet Sauvignon</label></li>
									<li><label><input type="checkbox">Tannat</label></li>
									<li><label><input type="checkbox">Pinot noir</label></li>
									<li><label><input type="checkbox">Syrah</label></li>
								</ul>
							</div>
						</div>

						<div class="list-group">
							<div class="list-heading">Country</div>
							<div class="list-area">
								<ul>
									<li><label><input type="checkbox" checked>Spain</label></li>
									<li><label><input type="checkbox">Argentina</label></li>
									<li><label><input type="checkbox">USA</label></li>
									<li><label><input type="checkbox">Canada</label></li>
									<li><label><input type="checkbox">Syrah</label></li>
								</ul>
							</div>
						</div>

						<div class="list-group">
							<div class="list-heading">Country</div>
							<div class="list-area">
								<ul>
									<li><label><input type="checkbox" checked>Spain</label></li>
									<li><label><input type="checkbox">Argentina</label></li>
									<li><label><input type="checkbox">USA</label></li>
									<li><label><input type="checkbox">Canada</label></li>
									<li><label><input type="checkbox">Syrah</label></li>
									<li><label><input type="checkbox">India</label></li>
								</ul>
							</div>
						</div>

						<div class="list-group">
							<div class="list-heading">Varietal</div>
							<div class="list-area">
								<ul>
									<li><label><input type="checkbox">Riesling</label></li>
									<li><label><input type="checkbox">Syrah</label></li>
									<li><label><input type="checkbox">Pinot noir</label></li>
									<li><label><input type="checkbox">Merlot</label></li>
									<li><label><input type="checkbox">Syrah</label></li>
									<li><label><input type="checkbox">Cabernet</label></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-sm-9">
					<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $wines): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<!-- cat item 1 -->
					<div class="cat-item-wrapper">
						
						<div class="row">
							
							<div class="col-sm-4">
								<div class="wine-image">
									<!-- <img src="<?php echo e(asset('/')); ?>/dist/images/wine-single-img.png" class="img-fluid"> -->
									<img src="<?php echo e(URL::to('/')); ?>/uploads/<?php echo e($wines->logo); ?>" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-8">
								<div class="cat-item-content">
									<div class="heading-item">
										<!-- <h3>Viña Zorzal Wines</h3> -->
										<h3><?php echo e($wines->cat_title); ?></h3>
										<div class="info-item">
											<span><img src="<?php echo e(asset('/')); ?>/dist/images/spn-flag.png" alt="" width="24"> &nbsp; Spain</span> 
											<!-- <span class="city">Navarra </span> -->
											<span class="city"><?php echo e($wines->country); ?> </span>
										</div>
									</div>								
									<div class="cat-item-text">
										<?php echo html_entity_decode($wines->description); ?>

										<!-- Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  -->
									</div>
									<a href class="btn btn-outline-wine">Our Portfolio</a>
								</div>
							</div>
						</div>
					</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $__env->make('site.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
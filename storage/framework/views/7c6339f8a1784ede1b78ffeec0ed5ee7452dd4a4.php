

<?php $__env->startSection('header'); ?>
    <section class="content-header">
      <h1>
         Wines & Spirits
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(backpack_url()); ?>"><?php echo e(config('backpack.base.project_name')); ?></a></li>
        <li class="active">Wines & Spirits</li>
      </ol>
    </section>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<div class="body">
	<?php if(\Session::has('success')): ?>
        <div class="alert alert-success">
            <?php echo e(\Session::get('success')); ?>

        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-sm-12">
            
				  <a href="<?php echo e(url('/admin/winesandspirits/create')); ?>" class="btn btn-primary">Add Partner</a>
			 
        </div>
    </div>

	<br />
		<div class="row">
		<div class="col-sm-12">
			<?php if(isset($data) && count($data)>0): ?>
        <table class="table table-striped" id="example1">
          <thead>
            <tr>
              <th width="5%">Title</th>
              <th width="5%">Type</th>
              <th width="10%">Descripton</th>
              <th width="5%">Region</th>
              <th width="5%"># of Products</th>
              <th width="5%">Country</th>
              <th width="5%">Logo</th>
              <th width="25%">Action</th>
            </tr>
          </thead>
          <tbody>
          
          <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dataDetails): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <td><a href='<?php echo e(url("/admin/products/".$dataDetails->id)); ?>'><?php echo e($dataDetails->cat_title); ?></a></td>
              <td><?php echo e($dataDetails->type); ?></td>
              <td><?php echo str_limit($dataDetails->description, 20); ?></td>
              <td><?php echo e($dataDetails->region); ?></td>
              <td><?php echo e($dataDetails->products->count()); ?></td>
              <td><?php echo e($dataDetails->country); ?></td>
              <td><img src="<?php echo e(URL::to('/')); ?>/uploads/<?php echo e($dataDetails->logo); ?>" height="30" width="50"></td>
              <td>
                <a href="<?php echo e(action('ProductsController@addProduct',$dataDetails->id)); ?>" class="btn btn-success"><i class="fa fa-product-hunt"></i>&nbsp;Add Product</a>

                <a href="<?php echo e(action('WinesSpiritsController@edit',$dataDetails->id)); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>&nbsp;
                
                <a href="<?php echo e(action('WinesSpiritsController@delete',$dataDetails->id)); ?>" onclick="return confirm('Are you sure want to delete?')" class="btn btn-danger delete_page"><i class="fa fa-trash-o"></i>&nbsp;DELETE</a>
                
              </td>
            </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
          </tbody>
        </table>
      <?php else: ?>
      <div class="alert alert-info">
        There are no routines added yet.
      </div>
      <?php endif; ?>
		</div>
	</div>

</div >
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
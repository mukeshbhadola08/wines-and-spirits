<?php $__env->startSection('header'); ?>
    <section class="content-header">
      <h1>
		Create <?php if(isset($id)): ?> Sub <?php endif; ?> Navigation Menu
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(backpack_url()); ?>"><?php echo e(config('backpack.base.project_name')); ?></a></li>
        <li class="active">Navigations  </li>
      </ol>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<?php if($errors->any()): ?>
	<div class="alert alert-danger">
        <ul>
		<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<li><?php echo e($error); ?></li>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</ul>
	</div><br />
	<?php endif; ?>
    <div class="row">
		<div class="col-sm-12">
			<form method="post" class="form-horizontal" action="<?php echo e(backpack_url('save_menu')); ?>" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="<?php echo e(csrf_token()); ?>" name="_token" />
				<div class="col-sm-2">
					<label for="title">Navigation Title:</label>
				</div>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="title"/>
				</div>
			</div>	
			
			<div class="form-group">
				<div class="col-sm-2">
				<label for="Page">Choose Page Link: </label>
				</div>
				<div class="col-sm-8">
				<select class="form-control" name="page">
					 <option value="homepage">Home Page</option>
					 <option value="eventslisting">Events Listing</option>
					 <option value="resultslisting">Results Listing</option>
					 <option value="portfoliolisting">Portfolio Listing</option>
					 <option value="newslisting">News Listing</option>
					 <?php if(isset($pages) && count($pages)>0): ?>
						<optgroup label="Pages">
						<?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="-page-<?php echo e($p->id); ?>"><?php echo e($p->page_title); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</optgroup>
					 <?php endif; ?>
					 
					  <?php if(isset($blogs) && count($blogs)>0): ?>
						<optgroup label="News">
						<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="-blog-<?php echo e($p->id); ?>"><?php echo e($p->blog_title); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</optgroup>
					 <?php endif; ?>
					 
				</select>
				</div>
			</div>
			
			<div class="form-group hidden">
				<div class="col-sm-2">
				<label for="visible">Visible:</label>
				</div>
				<div class="col-sm-8">
				<input type="hidden"  name="visible" value="1" />
				</div>
			</div>
			<?php if(!isset($id)): ?>
				<div class="form-group">
					<div class="col-sm-2">
					<label for="visible">Show In Header:</label>
					</div>
					<div class="col-sm-8">
					<input type="checkbox"  name="show_header" value="1" />
					</div>
				</div>
				<!-- <div class="form-group">
					<div class="col-sm-2">
					<label for="visible">Show In Footer:</label>
					</div>
					<div class="col-sm-8">
					<input type="checkbox"  name="show_footer" value="1" />
					</div>
				</div> -->
			<?php endif; ?>

			<input type="hidden" name="parent" value="<?php echo e($parent = isset($id)?$id:0); ?>" />
			<input type ="hidden" name="lang_code" value="en" / >
			<input type ="hidden" name="parent_lang_id" value="0" / >
			<button type="submit" class="btn btn-primary">Create</button>
			</form>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('after_scripts'); ?>
<script>

		 jQuery(document).on('change','.nav_type',function(){
			
			if (this.value == 1) {
				$('#page_form_fields').show();
				$('#blog_form_fields').hide();
			}
			else {
				$('#page_form_fields').hide();
				$('#blog_form_fields').show();
			}
		}); 
		
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
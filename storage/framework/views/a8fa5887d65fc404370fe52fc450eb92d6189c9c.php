<style>
	 i.fa-play:hover .routine-video-watch{
	  	 display: block !important;
	  }
</style>

<?php echo $__env->make('site.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php
	global $lang_val;
	global $languageVariables;

	$title = App\Navigation::where('static_page_name','routinelisting')->pluck('title')[0];
?>

	<!-- intro -->
	<section id="intro" class="clearfix">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 intro-img order-md-first order-last">
					<img src="<?php echo e(asset('/')); ?>dist/images/intro-img-top.png" alt="" class="img-fluid wow fadeInUp">
				</div>
				<!--<?php if(config('constants.ROUTINESTEXTAREA')): ?>
					<div class="col-sm-6 dark-box">
						
					</div>
				<?php endif; ?>-->
				<div class="col-sm-12 intro-info  wow fadeInDown order-md-last order-first" style="visibility: visible; animation-name: fadeInDown;">
						<h2><?php echo e($title); ?>  </h2>
			</div>

		</div>
	</section>
	<!-- /intro -->

	<section class="single-page-container section-padding">
		<div class="container routine">
			<!-- <p>Myra&#39;s videos are on her&nbsp;<a href="https://www.youtube.com/channel/UCmjR3rVbqpmVeGIKIAKR2Tg" target="_blank">OFFICIAL YOUTUBE PAGE</a>. To watch her routine videos, please click on the &quot;Watch Video&quot; link. We hope you enjoy watching Myra&nbsp;in action!&nbsp;
						&nbsp;</p> -->
						<br>
				<div class="row">
					
				<?php if(isset($routines) && count($routines)>0): ?>
					<?php $__currentLoopData = $routines; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $routine): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="col-sm-6">
				      <div class="card">
				        <div class="card-header" style="text-align: center;">
					         <p class="text-left pull-left" style="width:45%; margin:0;padding-top:10px;"> <?php echo e($routine->title); ?> </p>

							<?php if($routine->slider_id): ?>
								<img alt="" class="pull-left" style="max-width: 50px;padding-top:8px;" src="<?php echo e(App\Http\Controllers\SliderController::getImage($routine->slider_id)); ?>">
							<?php else: ?>
								<img alt="" class="pull-left" style="max-width: 45px;padding-top:8px;" src="<?php echo e(action('RoutinesController@getImage',$routine->id)); ?>">
							<?php endif; ?>
							<div style="position: relative;">
								<?php if($routine->video_url && $routine->video_time): ?>	
										
									<?php endif; ?>
									<?php
										$video_key  = App\News::youtube_id_from_url($routine->video_url);
									?>
									<?php if($video_key): ?>	
									<a class="fancybox-media2 playIcon-routine"  href="http://www.youtube.com/embed/<?php echo e($video_key); ?>?enablejsapi=1">
										<i class="fa fa-youtube-play"></i>
									</a>
									<?php endif; ?>
									<!-- <div class="routine-video-watch" style="display: none; position: absolute;top: -15px;right: -16px;">
										<?php if($routine->video_url && $routine->video_time): ?>	
											<span href="<?php echo e($routine->video_time); ?>" style="margin-right: 10px;    font-size: 14px;"><?php echo e($routine->video_time); ?></span>
										<?php endif; ?>
										<?php
											$video_key  = App\News::youtube_id_from_url($routine->video_url);
										?>
										<?php if($video_key): ?>	
											<a style="font-size: 14px;" href="http://www.youtube.com/embed/<?php echo e($video_key); ?>" target="_blank">Watch Video</a>
										<?php endif; ?>
									</div> -->
								
							</div>
						</div>
				        <div class="card-body">
				          <blockquote class="blockquote mb-0">
						  <div class="body-routine" style="padding: 0;">

				            <p><?php echo e($routine->description); ?> </p>
							<?php if($routine->additional_skills): ?>
							 <p><strong>Additional Skills:</strong> <?php echo e($routine->additional_skills); ?></p>
							<?php endif; ?>

				            <footer class="blockquote-footer">
							
							<strong>Working On:</strong> <?php echo e($routine->working_on); ?>

							
							
							
							</footer>
							
							</div>
				          </blockquote>
				          
				        </div>
				      </div>        
				      </div>        
				      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>				
				<?php endif; ?>
			</div>
				<div class="row">
					<div class="col-sm-12 routine-text">
						 <?php  echo config('constants.ROUTINESTEXTAREA'); ?>
						<!-- Myra's videos are on her<a href="#"> OFFICIAL YOUTUBE PAGE.</a> To watch her routine videos, please click on the "Watch Video" link. We hope you enjoy watching Myra in action! -->
					</div>
				</div>
			</div>
	</section>

<?php echo $__env->make('site.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- <script  src="http://fancyapps.com/fancybox/source/helpers/jquery.fancybox-media.js"></script>
<script  src="http://fancyapps.com/fancybox/source/helpers/jquery.fancybox-buttons.js"></script>

<link rel="stylesheet" type="text/css" href="http://fancyapps.com/fancybox/source/helpers/jquery.fancybox-buttons.css">
<link rel="stylesheet" type="text/css" href="http://fancyapps.com/fancybox/source/jquery.fancybox.css"> -->

<script src="https://www.youtube.com/player_api"></script>
<script type="text/javascript">
function onYouTubePlayerAPIReady() {
	    $(document).ready(function () {
	        $('.fancybox-media2').fancybox({
				helpers: {
	                media: {
	                    youtube: {
	                        params: {
	                            autoplay: 1,
	                            rel: 0,
	                            // controls: 0,
	                            showinfo: 0,
	                            autohide: 1,
	                            mute:1
	                        }
	                    }
	                },
	            },
				
				hash : false,  
				slideShow: {
					autoStart: false,
					speed: 3000
				},
				afterShow: function () {
				var id = jQuery(document).find(".fancybox-slide--current").find('.fancybox-iframe').attr('id');
				var player = new YT.Player(id, {
				    events: {
				        onReady: onPlayerReady,
				        onStateChange: onPlayerStateChange
				    }
				});
				},
				
			  	share: {
				  url: function(instance, item) {
					return (
					  (!instance.currentHash && !(item.type === "inline" || item.type === "html") ? item.origSrc || item.src : false) || window.location
					);
				  },				  
				}
					

			});
	    });
	}
		
	function onPlayerReady(event) {
	    event.target.playVideo();
	}

	function onPlayerStateChange(event) {
	    if (event.data === 0) {
	        jQuery.fancybox.close();
	    }
	}
	
</script>
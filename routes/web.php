<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/sendbasicemail','MailController@basic_email');
Route::get('/home', 'SiteController@index')->name('home');
Route::get('/wines', 'SiteController@wine');

Route::get('/blog', 'SiteController@blog');
Route::get('/history', 'SiteController@history');
Route::get('/spirits', 'SiteController@spirits');
Route::get('/search', 'SiteController@search');
Route::get('/singleWines/{id}', 'SiteController@singleWines');
Route::get('/product/{product}', 'SiteController@product');


Route::get('/wine/{wine}', 'SiteController@winepage');
Route::get('/singleproduct/{id}', 'SiteController@winepage');

//Events Page
Route::get('/events/list/','NewsController@list')->name('eventslist');
Route::get('/blogs','BlogController@list');
Route::get('/news/list','BlogController@list');
Route::get('/results/list','NewsController@resultlist');

//conatct us
Route::get('/admin/contact','ContactusController@index')->middleware('adminuser')->name('contact');
Route::get('/admin/contact/add','ContactusController@create');
Route::post('/admin/contact/store','ContactusController@store');
Route::get('/admin/contact/{id}','ContactusController@edit');
Route::post('/admin/contact/{id}','ContactusController@update');
Route::get('/admin/contact/delete/{id}','ContactusController@delete');

// *** Front End Site routes //
Route::get('/', 'SiteController@index');
Route::get('/contactus', 'SiteController@index_contactus');
Route::post('/contactus/send', 'SiteController@contactussend');
Route::get('refresh_captcha', 'SiteController@refreshCaptcha')->name('refresh_captcha');
Route::get('/mytransactions','UserController@mytransactions');
//User Profile related routes
Route::get('/profile', 'UserController@myprofile');
//Route::post('/profile_pic', 'UserController@update_avatar');
Route::post('/update_profile', 'UserController@edit_profile');
Route::post('/change_password', 'UserController@edit_password');
// *** Front End Site routes //

Route::get('/redirect', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');
// *** Facebook auth routes //

//for Service agent registration
Route::get('/register/{agent?}', 'Auth\RegisterController@showRegistrationForm')->name('register');

//Admin End-Users Listing
Route::get('/admin/users', 'UserController@get_all_users')->middleware('adminuser')->name('User Management');
//Block/Unblock users
Route::get('/admin/users/change_status/{userId}', 'UserController@block_unblock_user')->middleware('adminuser');

// CMS Pages Routes
Route::get('/admin/pages/create','PageController@create')->name('pages');
Route::post('/admin/pages/create','PageController@store')->middleware('adminuser');
Route::get('/admin/pages','PageController@index')->middleware('adminuser')->name('pages');
Route::get('/page/image/{pageid}', 'PageController@getImage');
Route::get('/admin/pages/{id}','PageController@edit')->middleware('adminuser');
Route::post('/admin/pages/{id}','PageController@update')->middleware('adminuser');
Route::get('/admin/pages/delete/{id}','PageController@delete')->middleware('adminuser');
Route::get('/admin/pages/createinspan/{id}','PageController@addInSpanish')->middleware('adminuser')->name('pages');
Route::post('/admin/pages/createinlang/{id}','PageController@storeInlang')->middleware('adminuser');

// CMS Page Settings Routes
Route::get('/admin/page_settings','PageSettingsController@index')->middleware('adminuser')->name('pages');
Route::post('/admin/page_settings/store','PageSettingsController@store')->middleware('adminuser')->name('pages');
Route::post('/admin/page_settings/remove_background_image','PageSettingsController@remove_background_image')->middleware('adminuser')->name('pages');


//Blog Pages Routes
Route::get('/admin/blogs/create','BlogController@create')->middleware('adminuser')->name('blogs');
Route::post('/admin/blogs/create','BlogController@store')->middleware('adminuser');
Route::get('/admin/blogs','BlogController@index')->middleware('adminuser')->name('pages');
Route::get('/admin/blogs/{id}','BlogController@edit')->middleware('adminuser');
Route::post('/admin/blogs/{id}','BlogController@update')->middleware('adminuser');
Route::get('/admin/blogs/delete/{id}','BlogController@delete')->middleware('adminuser');

Route::get('/admin/comments','BlogController@allComments')->middleware('adminuser')->name('All Comments');
Route::get('/admin/comments_settings','ConfigurationController@commentOptionSettings')->middleware('adminuser')->name('Setting Comments');
Route::get('admin/commentListings','BlogController@allCommentsListings')->middleware('adminuser')->name('All Comments');
Route::get('admin/missionListings','MissionController@allMissionListings')->middleware('adminuser')->name('All Mission Listing');
Route::post('/blogs/search','BlogController@searchList');
Route::get('/blogs/{cleanurl}','BlogController@view');
Route::get('/blogs/image/{blogid}', 'BlogController@getImage');
Route::post('/blogs/addcomment/adduser', 'BlogController@addcomment');
Route::post('/blogs/addcomment/addguest', 'BlogController@addGuestComment');
Route::post('/commentactions', 'BlogController@blogCommentActions');
Route::post('/commentValUpdate', 'BlogController@blogCommentValUpdate');
Route::get('/user/image/{user_id}', 'UserController@getImage');
Route::get('/admin/blogs/createinspan/{id}','BlogController@addInSpanish')->middleware('adminuser')->name('blogs');
Route::post('/admin/blogs/createinlang/{id}','BlogController@storeInlang')->middleware('adminuser');


//Slider Pages Routes
Route::get('/admin/photo/create','SliderController@create')->middleware('adminuser')->name('slider');
Route::post('/admin/photo/create','SliderController@store')->middleware('adminuser');
Route::get('/admin/{name}','SliderController@index')->middleware('adminuser')->name('pages')->where('name', '(slider|photo-gallery)');
Route::get('/admin/photo/{id}','SliderController@edit')->middleware('adminuser');
Route::post('/admin/photo/{id}','SliderController@update')->middleware('adminuser');
Route::get('/admin/photo/delete/{id}','SliderController@delete')->middleware('adminuser');
Route::get('/slider/image/{slideid}', 'SliderController@getImage');
Route::post('/admin/navigation_order','NavigationController@setNavigationOrder')->middleware('adminuser');
//Pending Comments Routes
Route::get('/admin/pendingcomments','BlogController@pendingcomments')->middleware('adminuser')->name('pendingcomments');
Route::get('/admin/approvecomment/{comment_id}','BlogController@approvecomment')->middleware('adminuser')->name('approvecomment');

//HISTORY
Route::get('/admin/history','HistoryController@index')->middleware('adminuser')->name('Navigation Management');
Route::get('/admin/history/create','HistoryController@create')->middleware('adminuser')->name('history');
Route::post('/admin/history/create','HistoryController@store')->middleware('adminuser');
Route::get('/admin/history/{id}','HistoryController@edit')->middleware('adminuser');
Route::post('/admin/history/{id}','HistoryController@update')->middleware('adminuser');
Route::get('/admin/history/delete/{id}','HistoryController@delete')->middleware('adminuser');

Route::group(['prefix' => 'admin',  'middleware' => 'adminuser'], function () {
    //Admin Site Settings
    Route::get('/settings','ConfigurationController@settings')->name('settings');
    Route::post('/settings','ConfigurationController@update');

    Route::get('/setting_blog','ConfigurationController@HomePageSettings')->name('setting_blog');
    Route::post('/setting_blog','ConfigurationController@HomePageSettingsUpdate');
    Route::post('/setting_blog_comment','ConfigurationController@blogCommentSettingsUpdate');

     Route::get('/setting_history','ConfigurationController@HomePageSettings')->name('setting_history');
    Route::get('/setting_wines','ConfigurationController@HomePageSettings')->name('setting_wines');
    Route::post('/settings/optionRemove','ConfigurationController@resetSettingsDefault');

    Route::get('/setting_mission','ConfigurationController@HomePageSettings')->name('setting_wines');
    //Admin Site Wines and spirits
    Route::get('/winesandspirits','WinesSpiritsController@winesandspirits')->name('winesandspirits');

    Route::get('/spirits','WinesSpiritsController@spirits')->name('spirits');

    Route::get('/winesandspirits/create','WinesSpiritsController@create')->name('create_winesandspirits');
    Route::post('/winesandspirits/create','WinesSpiritsController@store');

    Route::post('/winesandspirits/{id}','WinesSpiritsController@update');
    Route::get('/winesandspirits/{id}','WinesSpiritsController@edit');
    Route::get('/winesandspirits/delete/{id}','WinesSpiritsController@delete');
    

});
Route::post('/admin/wineries_order','WinesSpiritsController@setWineriesOrder');
Route::post('/admin/distillery_order','WinesSpiritsController@setDistelleryOrder');
Route::post('/admin/media_order','SliderController@setMediaOrder');
Route::post('/admin/product','ProductsController@setProductOrder');
//OUR MISSION
Route::post('/mission/addlist','MissionController@addListing')->middleware('adminuser');
Route::post('/mission/missionRemoveItem','MissionController@missionRemoveItem')->middleware('adminuser');


// admin Products
 Route::get('/admin/product/{id}/add-product/{type}','ProductsController@addProduct')->middleware('adminuser')->name('add_product');
Route::post('/admin/product/{id}/create/{type}','ProductsController@store')->middleware('adminuser')->name('store_product');
Route::get('/admin/products/{winesandspirits}','ProductsController@Products')->middleware('adminuser')->name('products');
Route::get('/admin/product/{id}/edit-product/{type}','ProductsController@editProduct')->middleware('adminuser')->name('edit_product');
Route::post('/admin/product/{id}/{type}','ProductsController@update')->middleware('adminuser');
Route::get('/admin/product/delete/{id}','ProductsController@delete')->middleware('adminuser');
Route::get('/admin/showProduct/{id}','ProductsController@showProduct')->middleware('adminuser');


//footer content
Route::get('/admin/footersection/{lang}','HomeController@footersection')->middleware('adminuser')->name('footersection');
Route::post('/admin/updatefootersection','HomeController@updatefootersection')->middleware('adminuser');


//Site Home Page 4 boxes
Route::get('/admin/homepagesection','ConfigurationController@homesettings')->middleware('adminuser')->name('homesettings');
Route::get('/blogs/sectionimage/{imageurl}', 'HomeController@getImage');

// This needs to be at bottom always
Route::post('/{cleanurl}','PageController@contact');
Route::get('/{cleanurl}','PageController@view');

// navigation management
Route::get('/admin/navigations','NavigationController@allNavigation')->middleware('adminuser')->name('Navigation Management');
Route::get('/admin/create_menu','NavigationController@createNav')->middleware('adminuser');
Route::get('/admin/edit_menu/{id}','NavigationController@editNav')->middleware('adminuser');
Route::post('/admin/save_menu','NavigationController@saveNavigation')->middleware('adminuser');
Route::get('/admin/delete_nav/{id}','NavigationController@deleteNav')->middleware('adminuser');
Route::post('/admin/update_menu/{id}','NavigationController@updateNavigation')->middleware('adminuser');
Route::post('/admin/navigation_order','NavigationController@setNavigationOrder')->middleware('adminuser');
Route::get('/admin/create_sub_menu/{id}','NavigationController@createSubNav')->middleware('adminuser');
Route::get('/admin/sub_navigations/{id}','NavigationController@getSubNav')->middleware('adminuser')->name('Navigation Management');
Route::get('/admin/navigation/createinspan/{id}','NavigationController@addInSpanish')->middleware('adminuser')->name('Navigation Management');
Route::post('/admin/navigation/createinlang','NavigationController@storeInlang')->middleware('adminuser');
Route::get('/admin/navigation/editinspan/{id}','NavigationController@editNavInLang')->middleware('adminuser')->name('Navigation Management');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/admin/getportfolios','AjaxController@index');

Route::post('/admin/upload_image','CkeditorController@uploadImage')->name('upload');

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentService extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'service_agents';

	/**/

	public function saveAgentService($data){
		$this->agent_id = $data['userId'];
		$this->service_id = $data['service_id'];
		$this->save();
		return $this->id;
	}

	public function updateAgentService($data){
        $service = $this->where('agent_id','=',$data['userId'])->first();
        //dd($user);
        $service->agent_id = $data['userId'];
        $service->service_id = $data['service_id'];
        $service->save();
        return 1;
	}

	public function user(){
        return $this->belongsTo('App\User','agent_id');
    }
    
}

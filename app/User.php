<?php

namespace App;

use Cmgmyr\Messenger\Traits\Messagable;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
use App\Notifications\ResetPassword;

class User extends Authenticatable
{
    use Notifiable, Messagable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'user_type','block_status','social_provider',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function comments()
    {
        return $this->hasMany('App\BlogComments');
    }

	function scopemytransactions_site(){
		/* $whereData = [
			['j.status', '<>', '0']
		]; */
		$returndata = DB::table('transactions as t')->select('t.*')->orderBy('t.id','desc')->get();
		if($returndata){
				return $returndata;
		} else {
				return false;
		}
	}
    // function to overrride default function with the same name used for sending password reset email//
    public function sendPasswordResetNotification($token) {
        $this->notify(new ResetPassword($token));
    }
    //---function to overrride default function with the same name used for sending password reset email--//

    public function userprofile(){
         //$this->hasOne(UserProfile::class);
        return $this->hasOne('App\UserProfile');
    }


	public function services()
    {
        return $this->hasMany('App\Models\AgentService');
    }
}

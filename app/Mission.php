<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mission extends Model
{
    protected $table = 'mission';

    public $timestamps = false;

    protected $fillable = ['description', 'image'];


    public function saveSlider($data)
    {
        $this->description = $data['description'];
        $this->save();
        return $this->id;
    }

    public function updateSlider($data){
        $history = $this->find($data['id']);
        $history->title = $data['title'];
        $history->description = $data['description'];
        if(isset($data['image'])) $history->image = $data['image'];
        $history->save();
        return $history->id;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'blogs';

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
	/**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    //protected $dateFormat = 'U';
	
    //protected $connection = 'connection-name' //By default, all Eloquent models will use the default database connection configured for your application. If you would like to specify a different connection for the model, use the $connection property:

	/**
     * The attributes that aren mass assignable.
     *
     * @var array
     */
	protected $fillable = ['added_by', 'blog_title', 'blog_description','video_url','lang_code','parent_lang_id'];

	/**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['clean_url', 'blog_image', 'short_description'];

    public function comments()
    {
        return $this->hasMany('App\BlogComments');
    }


	public function saveBlog($data)
	{
		$this->added_by = auth()->user()->id;
		$this->blog_title = $data['blog_title'];
		$this->blog_description = $data['blog_description'];
		$this->short_description = $data['short_description'];
		$this->clean_url = $data['clean_url'];
        $this->is_visible = $data['is_visible'];
		if(!empty($data['image'])){
            $this->default_image = $data['image'];
        }
        if(isset($data['slider_id']))
            $this->slider_id = $data['slider_id'];
        
		$this->lang_code = $data['lang_code'];
		$this->parent_lang_id = $data['parent'];
		$this->save();
		return $this->id;
	}

	public function updateBlog($data){
        $blog = $this->find($data['id']);
        $blog->blog_title = $data['blog_title'];
        $blog->blog_description = $data['blog_description'];
        $blog->short_description = $data['short_description'];
		if(!empty($data['image']) && isset($data['image'])){
            $blog->default_image = $data['image'];
            $blog->slider_id = null;
        }elseif(isset($data['slider_id'])){
            $blog->slider_id = $data['slider_id'];
        }
		$blog->is_visible = $data['is_visible'];
        $blog->save();
        return 1;
	}
	
	public function getAllBlogs(){
		return Blog::where('is_visible', 1)->get();
    }

    public function getAllComments(){
		return Blog::whereIn('status', [0, 1])->get();
    }
}

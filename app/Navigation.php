<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Navigation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'navigation_menu';

	/**/
    /**
    *By default, Eloquent expects created_at and updated_at columns to exist on your tables. If you do not *wish to have these columns automatically managed by Eloquent, set the $timestamps property on your *model to false
    **/
    public $timestamps = false;
    /**/

	protected $fillable = ['title','type','reference_type_id','is_visible','parent_id','added_date','nav_order','show_header','show_footer','lang_code','parent_lang_id','static_page_name'];

	public function saveNavigation($data){
		$this->title = $data['title'];
		$this->type = $data['type'];
		$this->reference_type_id= $data['reference_id'];
		$this->is_visible = $data['visible'];
		$this->parent_id  = $data['parent'];
		$this->added_date = date('Y-m-d H:i:s');
		$this->nav_order = 0;
		$this->show_header = $data['show_header'];
		$this->show_footer = $data['show_footer'];
		$this->lang_code = $data['lang_code'];
		$this->static_page_name = $data['static_page_name'];
		$this->parent_lang_id = $data['parent_lang_id'];
		$this->save();
		return $this->id;
	}

	public function updateNavigation($data,$id)
	{
		$menu = $this->find($id);
		$menu->title = $data['title'];
		$menu->type = $data['type'];
		$menu->reference_type_id= $data['reference_id'];
		$menu->is_visible = $data['visible'];
		$menu->parent_id  = $data['parent'];
		$menu->show_header = $data['show_header'];
		$menu->show_footer = $data['show_footer'];
		$menu->static_page_name = $data['static_page_name'];
		$menu->save();
	}
    public function page($type)
    {
		
		if($type == 1){
			return $this->belongsTo('App\Page','reference_type_id');
		} else {
			return $this->belongsTo('App\Blog','reference_type_id');
		}
    }
}

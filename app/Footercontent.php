<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Footercontent extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'footer_content';

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
	/**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    //protected $dateFormat = 'U';
	
    //protected $connection = 'connection-name' //By default, all Eloquent models will use the default database connection configured for your application. If you would like to specify a different connection for the model, use the $connection property:

	/**
     * The attributes that aren mass assignable.
     *
     * @var array
     */
	protected $fillable = ['meta_key', 'meta_value'];

	/**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

	public function insertRecord($data){
		$content = new Footercontent();
		$content->meta_key = $data['meta_key'];
		$content->meta_value = $data['meta_value'];
		$content->save();
		return $content->id;
	}

	public function updateRecord($data){
		$content = $this->find($data['id']);
        $content->meta_key = $data['meta_key'];
        $content->meta_value = $data['meta_value'];
        $content->save();
        return 1;
	}
}

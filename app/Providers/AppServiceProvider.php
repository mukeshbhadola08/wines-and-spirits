<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App\Configuration;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    const DATABASE_STR_LENGTH = 191;
    public function boot()
    {
		Schema::defaultStringLength(static::DATABASE_STR_LENGTH);

		$settings = Configuration::all();
		
		global $lang_val;

        foreach ($settings as $setting) {
            $variable = strtoupper(trim($setting->option_name));
            config(['constants.' . $variable => trim($setting->option_value)]);
        }

        $config = [
            'mail.driver'     => 'sendmail',
            'mail.host'       => 'smtp.gmail.com',
            'mail.port'       => 547,
            'mail.from.name'  => config('constants.SITE_TITLE'),
            'mail.from.address'  => config('constants.ADMIN_EMAIL'),
            'mail.password'  => config('constants.ADMIN_PWD'),
            'encryption' => 'ssl'
        ];

        config()->set($config);

        if (isset($_COOKIE['lang_val'])) {
            $lang_val = $_COOKIE['lang_val'];
        } else {
            $lang_val = config('constants.LANGUAGE');
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'history';

    public $timestamps = false;

    protected $fillable = ['title','description', 'image'];


    public function saveSlider($data)
    {
        $this->title = $data['title'];
        $this->description = $data['description'];
        $this->save();
        return $this->id;
    }

    public function updateSlider($data){
        $history = $this->find($data['id']);
        $history->title = $data['title'];
        $history->description = $data['description'];
        if(isset($data['image'])) $history->image = $data['image'];
        $history->save();
        return $history->id;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sliders';

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
	/**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    //protected $dateFormat = 'U';
	
    //protected $connection = 'connection-name' //By default, all Eloquent models will use the default database connection configured for your application. If you would like to specify a different connection for the model, use the $connection property:

	/**
     * The attributes that aren mass assignable.
     *
     * @var array
     */
	protected $fillable = ['title', 'is_visible', 'nav_order'];

	/**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */


	public function saveSlider($data)
	{
		$this->title = $data['title'];
        $this->description = $data['description'];
		$this->is_visible = $data['is_visible'];
        $this->image = $data['image'];
        $this->lang_code = $data['lang_code'];
		$this->added_date  = date('Y-m-d H:i:s');
		$this->slide_order = 0;
		$this->save();
		return $this->id;
	}

	public function updateSlider($data){
        $slider = $this->find($data['id']);
        $slider->title = $data['title'];
        $slider->description = $data['description'];
		$slider->is_visible = $data['is_visible'];
        if(isset($data['image'])) $slider->image = $data['image'];
        $slider->save();
        return 1;
	}

    public function updateSliderPageField($sliderid, $pageid){
        $slider = $this->find($sliderid);
        $slider->page_id = $pageid;
        $slider->save();
        return 1;
    }
	
	public function getAllSliders(){
		return Slider::where('is_visible', 1)->get();
    }
	
	 public function children()
    {
        return $this->hasMany('App\CourseModule','parent_id');
    }
}

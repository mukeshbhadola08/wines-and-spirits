<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    //protected $dateFormat = 'U';

    //protected $connection = 'connection-name' //By default, all Eloquent models will use the default database connection configured for your application. If you would like to specify a different connection for the model, use the $connection property:

    /**
     * The attributes that aren mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cat_id', 'product_title', 'description', 'region', 'country', 'varietal','slider_id','product_order','product_background_img'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */

    public function country() {
        return $this->belongsTo('App\Country', 'country_id');
    }

    public function wine() {
    return $this->belongsTo('App\WinesSpirits', 'cat_id');
  }

    public function savePage($data, $type)
    {
        $this->cat_id = $data['cat_id'];
        $this->product_title = $data['product_title'];
        $this->description = $data['description'];
        $this->region = $data['region'];
        $this->country_id = $data['country_id'];
        if($type == 'winery'){
            $this->varietal = $data['varietal'];
        }
        $this->product_order = 0;
        $this->save();
        return $this->id;
    }

    public function updatePage($data, $type){
        $products = $this->find($data['id']);
        $products->cat_id = $data['cat_id'];
        $products->product_title = $data['product_title'];
        $products->description = $data['description'];
        $products->region = $data['region'];
        $products->country_id = $data['country_id'];
        if($type == 'winery'){
            $products->varietal = $data['varietal'];
        }
        if (isset($data['image'])) $products->image = $data['image'];
        if (isset($data['product_background_img'])) $products->product_background_img = $data['product_background_img'];
        $products->save();
        return $products->id;
    }

    public function updatePageDefaultImage($data){
        $products = $this->find($data['id']);
        $products->default_image = $data['image'];
        $products->save();
        return 1;
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class WinesSpirits extends Model
{
	const LIMIT = 10;

	protected $table = 'wines_spirits';

	public $timestamps = true;

	protected $fillable = ['added_by', 'cat_title', 'description', 'logo', 'pdf_file', 'region', 'country', 'type', 'varietal', 'age','wine_spirit_order','background_image'];

	public function products() {
    return $this->hasMany('App\Products', 'cat_id');
  }

	public function country() {
    return $this->belongsTo('App\Country', 'country_id');
  }



	public function savePage($data)
	{
		$this->added_by = auth()->user()->id;
		$this->cat_title = $data['cat_title'];
		$this->description = $data['description'];
		$this->region = $data['region'];
		$this->country_id = $data['country_id'];
		$this->type = $data['type'];
		$this->wine_spirit_order = 0;
		$this->save();

		return $this->id;
	}

	public function updatePage($data)
	{
        $winesandspirits = $this->find($data['id']);
        $winesandspirits->cat_title = $data['cat_title'];
        $winesandspirits->description = $data['description'];
        $winesandspirits->region = $data['region'];
		$winesandspirits->country_id = $data['country_id'];
		if (isset($data['logo'])) $winesandspirits->logo = $data['logo'];
		if (isset($data['pdf_file'])) $winesandspirits->pdf_file = $data['pdf_file'];
		if (isset($data['background_image'])) $winesandspirits->background_image = $data['background_image'];
        $winesandspirits->save();
        return $winesandspirits->id;
	}

}

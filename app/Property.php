<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'properties';

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
	/**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    //protected $dateFormat = 'U';
	
    //protected $connection = 'connection-name' //By default, all Eloquent models will use the default database connection configured for your application. If you would like to specify a different connection for the model, use the $connection property:

	/**
     * The attributes that aren mass assignable.
     *
     * @var array
     */
	protected $fillable = ['title', 'user_id', 'title', 'description', 'residential_type', 'sale_type', 'is_active'];

	/**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['image', 'slug'];

	public function posts()
    {
        return $this->hasManyThrough(
            'App\PropertyMetaData',
            'property_id', // Foreign key on PropertyMetadata table...
            'id' // Local key on property table.
        );
    }

	public function saveProperty($data)
	{
		$this->user_id = auth()->user()->id;
		$this->title = $data['title'];
		$this->description = $data['description'];
		$this->residential_type = $data['residential_type'];
		$this->sale_type = $data['sale_type'];
		$this->is_active = $data['is_active'];
		$this->slug = $data['slug'];
		$this->image = $data['image'];
		$this->save();
		return $this->id;
	}

	public function updateProperty($data){
        $property = $this->find($data['id']);
        $property->title = $data['title'];
        $property->description = $data['description'];
		$property->residential_type = $data['residential_type'];
		$property->sale_type = $data['sale_type'];
		$property->is_active = $data['is_active'];
		$property->image = $data['image'];
        $property->save();
        return 1;
	}
	
	
	public function getAllBlogs(){
		return Blog::where('is_visible', 1)->get();
    }
	
	public function metadatas(){
		return $this->hasMany('App\PropertyMetaData', 'property_id');	
	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageSetting extends Model
{
    
    public function savePageSetting($data) {
        $this->title = $data['title'];
        $this->description = $data['description'];
        $this->save();
        return $this->id;
    }

}

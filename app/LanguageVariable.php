<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LanguageVariable extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'language_variables';

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
	/**
     * The attributes that aren mass assignable.
     *
     * @var array
     */
	protected $fillable = ['lang_code', 'type', 'var_name','value','added_on'];

	public function saveVariable($data){
		$this->lang_code = $data['lang_code'];
		$this->type = $data['var_type'];
		$this->var_name = $data['var_name'];
		$this->value = $data['var_text'];
		$this->added_on = date('Y-m-d H:i:s');
		$this->save();
		return $this->id;
	}
	public function updateVariable($data){
		$variable = $this->find($data['id']);
		$variable->lang_code = $data['lang_code'];
		$variable->var_name = $data['var_name'];
		$variable->value = $data['var_text'];
		$variable->save();
		return $this->id;
	}

	public static function get_all_lang_variables($lang){

		$result = self::where('lang_code',$lang)->get()->toArray();
		
		$x = array();
		if(count($result)>0)
		{
			foreach ($result as $vl) {			
				$x[$vl['type']][$vl['var_name']]= html_entity_decode($vl['value']);			
			}
		}
		return $x; 
	}
    

}
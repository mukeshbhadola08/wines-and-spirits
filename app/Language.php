<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'language_codes';

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
	/**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    //protected $dateFormat = 'U';
	
    //protected $connection = 'connection-name' //By default, all Eloquent models will use the default database connection configured for your application. If you would like to specify a different connection for the model, use the $connection property:

	/**
     * The attributes that aren mass assignable.
     *
     * @var array
     */

	/**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    

}
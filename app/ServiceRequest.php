<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequest extends Model
{
	protected $table = 'service_requests';
	protected $fillable = ['service_id','user_id','address','agent_id','total_cost'];

	public function user()
    {
        return $this->belongsTo('App\User', 'user_id')->select(array('id','name','email'));
    }

	public function service()
    {
        return $this->belongsTo('App\Services', 'service_id');
    }
	
	public function saveRequest($data)
	{
		$this->service_id = $data[0];
		$this->user_id = $data[1];
		$this->agent_id = 0;
		$this->address = $data[2];
		$this->total_cost=$data[3];
		$this->status='0';
		$this->save();	
		return $this->id;
	}
	public function service_request_data()
    {
        return $this->hasMany('App\ServiceRequestMetaData','service_rquest_id','id');
    }

}
?>
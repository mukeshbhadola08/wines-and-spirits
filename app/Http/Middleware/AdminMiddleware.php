<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    const USER_TYPE_ADMIN = 'S';
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() != null && Auth::user()->user_type == static::USER_TYPE_ADMIN)
        {
            return $next($request); // pass the admin
        }
        return redirect('/'); //move to home
    }
}

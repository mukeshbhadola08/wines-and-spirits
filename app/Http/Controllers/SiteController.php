<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Site;
use App\History;
use App\Services;
use App\News;
use App\Blog;
use App\Slider;
use App\WinesSpirits;
use App\Products;
use App\Configuration;
use App\Mission;
use App\PageSetting;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactUsMail;
use App\Contactus;

class SiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

		parent::__construct();
    }

    function contactussend(Request $request)
    {
        $this->validate($request, [
            'name'     =>  'required',
            'email'  =>  'required|email',
            'message' =>  'required',
            'captcha' => 'required|captcha'
        ],['captcha.captcha'=>'Invalid captcha code.']);

        $data = [
            'name'  => $request->name,
            'email' => $request->email,
            'subject' => $request->subject,
            'body' => $request->message
        ];
        $email = $data['email'];
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        //$to = $request->email;//config("constants.ADMIN_EMAIL") ?? 'info@wnsimports.com';
        $to = config("constants.ADMIN_EMAIL") ?? 'info@wnsimports.com';
        $subject = $data['subject'];
        $message = "
        <html>
        <head>
        <title>Contact email</title>
        </head>
        <body>
        <p>You have a mail from wnsimports.com</p>
        <table>
        <tr>
        <th>Name : </th>
        <td>{$request->name}</td>
        </tr>
        <tr>
        <th>Email : </th>
        <td>{$request->email}</td>
        </tr>
        <tr>
        <th>Message : </th>
        <td>{$request->message}</td>
        </tr>
        </table>
        </body>
        </html>
        ";
        $headers .= 'From: <info@wnsimports.com >' . "\r\n";
        mail($to,$subject,$message,$headers);
       // // Mail::send('email.mail' , $data, function($message) use ($email, $subject) {
       //      $message->to($email)->subject($subject);
       //  });
        return back()->with('success', 'Thanks for contacting us!');
    }

    public function index() {
        $this->data['title'] = 'Home'; // set the page title
        if(config('constants.NOOFEVENTSNEWS')){
            $noofeventtoshow = config('constants.NOOFEVENTSNEWS');
        }
        else{
            $noofeventtoshow = 2;
        }

        $config = Configuration::get()->where('option_name', '=', 'total_no_of_blogs')->first();
        $total  = '';
        if(!empty($config->option_value)){
            $total = $config->option_value;
        }else{
            $total = 10;
        }
        $confighis = Configuration::get()->where('option_name', '=', 'total_no_of_history')->first();
        $totalhis ='';
        if(!empty($confighis->option_value)){
            $totalhis = $confighis->option_value;
        }else{
            $totalhis = 10;
        }

        $configwine = Configuration::get()->where('option_name', '=', 'total_no_of_wines')->first();
        $totalwines ='';
        if(!empty($configwine->option_value)){
            $totalwines = $configwine->option_value;
        }else{
            $totalwines = 10;
        }
        

        $configwine_read_more = Configuration::get()->where('option_name', '=', 'read_more')->first();
        $configblog_read_more = Configuration::get()->where('option_name', '=', 'read_more_blog')->first();
	      $alleventsupcoming = News::where([['event_date', '<', date("Y-m-d")]])->orderBy('event_date','desc')->paginate($noofeventtoshow);

	      $alleventspast = News::where([['event_date', '<', date("Y-m-d")]])->orderBy('event_date','desc')->paginate(5);

	      $allrecentblogs = Blog::orderBy('created_at','desc')->paginate($total);

	      $alleventswithresults = News::where([['results','!=', ""]])->orderBy('event_date','desc')->paginate(9);

        $slides = Slider::where('is_visible','1')->orderBy('nav_order', 'asc')->get();

        $config_mission_title = Configuration::get()->where('option_name', '=', 'homepage_mission_title')->first();

        $config_mission_content = Configuration::get()->where('option_name', '=', 'mission_content')->first();

        $config_mission_list = Configuration::get()->where('option_name', '=', 'total_no_of_list')->first(); ;
        $total_mission ='';
        if(!empty($config_mission_list->option_value)){
            $total_mission = $config_mission_list->option_value;
        }else{
            $total_mission = 10;
        }
        $config_img = Configuration::get()->where('option_name', '=', 'background_image')->first();
        $blog_color = Configuration::get()->where('option_name', '=', 'HOMEPAGE_BLOG_BG_COLOR')->first();
        $blog_color_image = '';
        if(!empty($config_img)){
            $blog_color_image .= ';background-image:url('.$config_img->option_value.')';
        }
        if(!empty($blog_color)){
            $blog_color_image .= ';background-color:'.$blog_color->option_value;
        }

        $history_backgroundimage = Configuration::get()->where('option_name', '=', 'history_background_image')->first();
        $history_backgroundcolor = Configuration::get()->where('option_name', '=', 'HISTORY_BACKGROUND_COLOR')->first();
        $history_color_img ='';
        if(!empty($history_backgroundimage)){
            $history_color_img .= ';background-image:url('.$history_backgroundimage->option_value.')';
        }
        if(!empty($history_backgroundcolor)){
            $history_color_img .= ';background-color:'.$history_backgroundcolor->option_value;
        }

        $wines_backgroundimage = Configuration::get()->where('option_name', '=', 'wine_background_image')->first();
        $wines_backgroundcolor = Configuration::get()->where('option_name', '=', 'WINE_BACKGROUND_COLOR')->first();
        $wines_color_img ='';
        if(!empty($wines_backgroundimage)){
            $wines_color_img .= ';background-image:url('.$wines_backgroundimage->option_value.')';
        }
        if(!empty($wines_backgroundcolor)){
            $wines_color_img .= ';background-color:'.$wines_backgroundcolor->option_value;
        }

        $config_img_mission = Configuration::get()->where('option_name', '=', 'mission_background_image')->first();
        $config_color_mission = Configuration::get()->where('option_name', '=', 'MISSION_BACKGROUND_COLOR')->first();
        $mission_side_image = Configuration::get()->where('option_name', '=', 'mission_side_image')->first();
        $mission_color ='';
        if(!empty($config_img_mission)){
            $mission_color .= ';background-image:url('.$config_img_mission->option_value.')';
        }
        if(!empty($config_color_mission)){
            $mission_color .= ';background-color:'.$config_color_mission->option_value;
        }

        $config_readtext_wines = Configuration::get()->where('option_name', '=', 'read_more_text')->first();
        $blog_readtext = Configuration::get()->where('option_name', '=', 'read_more_blog_text')->first();

        $mission = Mission::orderBy('id','desc')->paginate($total_mission);
        $productlist =  Products::with('wine')->orderBy('created_at','desc')->paginate(5);
        $history = History::orderBy('id','desc')->paginate($totalhis);
        $this->data['upcomingevents']		= $alleventsupcoming;
        $this->data['pastevents']			= $alleventspast;
        $this->data['allrecentblogs']		= $allrecentblogs;
        $this->data['alleventswithresults']	= $alleventswithresults;
        $this->data['slides'] = $slides;
        $this->data['productlist'] = $productlist;
        $this->data['history'] = $history;
        $this->data['mission'] = $mission;
        $this->data['blog_color_image'] = $blog_color_image;
        $this->data['history_color_img'] = $history_color_img;
        $this->data['mission_color'] = $mission_color;
        $this->data['config_mission_title'] =  $config_mission_title;
        $this->data['config_mission_content'] =  $config_mission_content;
        $this->data['configwine_read_more'] =  $configwine_read_more;
        $this->data['configblog_read_more'] =  $configblog_read_more;
        $this->data['config_readtext_wines'] =  $config_readtext_wines;
        $this->data['blog_readtext'] =  $blog_readtext;
        $this->data['mission_side_image'] =  $mission_side_image;
        $this->data['wines_color_img'] =  $wines_color_img;

        return view('site.index', $this->data);
    }

    public function index_contactus()
    {
        $get_contactus = Contactus::where('show_contact',1)->get();
        $configSettings = config('constants');
        $contactPageSettings = PageSetting::where('id', 1)->first();
        return view('site.contactus', compact('configSettings', 'contactPageSettings', 'get_contactus'));
    }


    public static function wineLogo() {
        $configwine_logo = Configuration::get()->where('option_name', '=', 'wines_logo')->first();
        $logo = $configwine_logo->option_value;
        return [$logo];
    }

    public static function wineFooter(){
        $configwine_footer_logo = Configuration::get()->where('option_name', '=', 'wines_footer_logo')->first();
        $configwine_fb = Configuration::get()->where('option_name', '=', 'wines_fb_link')->first();
        $configwine_insta = Configuration::get()->where('option_name', '=', 'wines_insta_link')->first();
        $configwine_twitter = Configuration::get()->where('option_name', '=', 'wines_twitter_link')->first();
        $configwine_youtube = Configuration::get()->where('option_name', '=', 'wines_youtube_link')->first();
        $configwine_linkedin = Configuration::get()->where('option_name', '=', 'wines_linkedin_link')->first();
        $footerlogo = $configwine_footer_logo->option_value;
        $configwinefb = $configwine_fb->option_value;
        $configinsta  = $configwine_insta->option_value;
        $configtwiiter = $configwine_twitter->option_value;
        $configyoutube = $configwine_youtube->option_value;
        $configlinkedin = $configwine_linkedin->option_value;
        return [$footerlogo, $configwinefb, $configinsta, $configtwiiter, $configyoutube, $configlinkedin];

    }

    public function services() {
  		$services = Services::where([['parent','=',0],['is_visible','=',1]])->get();
        return view('site.services.list', compact('services'));
    }

    public function wine(Request $request) {
        $search = $request->search;
        $country = $request->country;
        $varietal = $request->varietal;
        
        $data = WinesSpirits::where('type' ,'winery')->orderBy('wine_spirit_order', 'asc');
        if (!empty($country)) {
            $country = explode(',', $country);
            $data = $data->whereIn('wines_spirits.country_id', $country);
        }
        if (!empty($varietal)) {
            $varietal = explode(',', $varietal);
            $data = $data->join('products', 'products.cat_id', '=', 'wines_spirits.id');
            $data = $data->whereIn('products.varietal', $varietal);
        }
        $data = $data->with('country')->where(function($query) use ($search) {
            $query->where('wines_spirits.cat_title', 'LIKE', "%$search%");
            $query->orWhere('wines_spirits.description', 'LIKE', "%$search%");
            $query->orWhere('wines_spirits.region', 'LIKE', "%$search%");
        })->groupBy('wines_spirits.id')->select('wines_spirits.*')->latest()->paginate(WinesSpirits::LIMIT);
        
        $countries = DB::table('country')
                    ->where('wines_spirits.type' ,'winery')
                    ->join('wines_spirits', 'country.id', '=', 'wines_spirits.country_id')
                    ->groupBy('wines_spirits.country_id')
                    ->orderBy('country.nicename')
                    ->select('country.*')
                    ->get();

        $products = DB::table('products')
                    ->where('wines_spirits.type' ,'winery')
                    ->join('wines_spirits', 'products.cat_id', '=', 'wines_spirits.id')
                    ->groupBy('products.varietal')
                    ->orderBy('products.varietal')
                    ->select('products.*')
                    ->get();

        $winePageSettings = PageSetting::where('id', 2)->first();
        return view('site.wineListing',compact('data', 'search', 'countries', 'products', 'country', 'varietal', 'winePageSettings'));
    }

    public function process(Request $request) {
        $search = $request->input('search');
        return view('site.wineListing', compact('search'));
    }

    public function blog() {
        $blogs = BLOG::get();
        $blogPageSettings = PageSetting::where('id', 4)->first();
        return view('site.blog', compact('blogs', 'blogPageSettings'));
    }

    public function history() {
        $history = History::get();
        return view('site.our_history',  compact('history'));
    }

    public function spirits(Request $request) {
            $search = $request->search;
            $country = $request->country;
            $varietal = $request->varietal;

            $data = WinesSpirits::where('type' ,'distillery')->orderBy('wine_spirit_order', 'asc');
            if (!empty($country)) {
                $country = explode(',', $country);
                $data = $data->whereIn('wines_spirits.country_id', $country);
            }
            if (!empty($varietal)) {
                $varietal = explode(',', $varietal);
                $data = $data->join('products', 'products.cat_id', '=', 'wines_spirits.id');
                $data = $data->whereIn('products.varietal', $varietal);
            }
            $data = $data->with('country')->where(function($query) use ($search) {
                $query->where('wines_spirits.cat_title', 'LIKE', "%$search%");
                $query->orWhere('wines_spirits.description', 'LIKE', "%$search%");
                $query->orWhere('wines_spirits.region', 'LIKE', "%$search%");
            })->groupBy('wines_spirits.id')->select('wines_spirits.*')->latest()->paginate(WinesSpirits::LIMIT);

            $countries = DB::table('country')
                        ->where('wines_spirits.type' ,'distillery')
                        ->join('wines_spirits', 'country.id', '=', 'wines_spirits.country_id')
                        ->groupBy('wines_spirits.country_id')
                        ->orderBy('country.nicename')
                        ->select('country.*')
                        ->get();

            $products = DB::table('products')
                        ->where('wines_spirits.type' ,'distillery')
                        ->join('wines_spirits', 'products.cat_id', '=', 'wines_spirits.id')
                        ->groupBy('products.varietal')
                        ->orderBy('products.varietal')
                        ->select('products.*')
                        ->get();

            $spiritsPageSettings = PageSetting::where('id', 3)->first();
            return view('site.spirits',compact('data', 'search', 'countries', 'products', 'country', 'varietal', 'spiritsPageSettings'));
    }

    public function search(Request $request) {
          $search = $request->search;
          $country = $request->country;
          $varietal = $request->varietal;
          $data = new WinesSpirits;
          if (!empty($country)) {
              $country = explode(',', $country);
              $data = $data->whereIn('wines_spirits.country_id', $country);
          }
          if (!empty($varietal)) {
              $varietal = explode(',', $varietal);
              $data = $data->join('products', 'products.cat_id', '=', 'wines_spirits.id');
              $data = $data->whereIn('products.varietal', $varietal);
          }
          $data = $data->with('country')->where(function($query) use ($search) {
              $query->where('wines_spirits.cat_title', 'LIKE', "%$search%");
          })->select('wines_spirits.*')->get();

          $countries = DB::table('country')
                        ->join('wines_spirits', 'country.id', '=', 'wines_spirits.country_id')
                        ->groupBy('wines_spirits.country_id')
                        ->orderBy('country.nicename')
                        ->select('country.*')
                        ->get();

          $products = DB::table('products')
                        ->join('wines_spirits', 'products.cat_id', '=', 'wines_spirits.id')
                        ->groupBy('products.varietal')
                        ->orderBy('products.varietal')
                        ->select('products.*')
                        ->get();

          return view('site.search',compact('data', 'search', 'countries', 'products', 'country', 'varietal'));
    }

    public function singleWines($id) {
        $showProduct = Products::where('cat_id', $id)->orderBy('product_order', 'asc')->latest()->paginate(WinesSpirits::LIMIT);
        $wineOrSpirit = WinesSpirits::where('id', $id)->first();
        $productPageSettings = PageSetting::where('id', 6)->first();
        return view('site.wines.productlisting', compact('showProduct', 'wineOrSpirit','productPageSettings'));
    }

    public function product(Products $product){
      return view('site.product', compact('product'));
    }

    public function winepage(WinesSpirits $wine) {
        return view('site.singlewines', compact('wine'));
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }
    

}

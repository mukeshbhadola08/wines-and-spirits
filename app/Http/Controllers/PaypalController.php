<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\News;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Srmklive\PayPal\Services\ExpressCheckout;
use Srmklive\PayPal\Services\AdaptivePayments;

class PaypalController extends Controller
{
    //

	public function __construct(){
        $this->middleware('auth');
		$this->provider = new ExpressCheckout();
    }

	public function payment(Request $request, $service_request_id){
		
		$recurring = ($request->get('mode') === 'recurring') ? true : false;

		$cart = $this->getCheckoutData($service_request_id);

        try {

			$useremail = auth()->user()->email;
			$userid = auth()->user()->id;

			$options = [
				'BRANDNAME' => 'Transform Your Energy',
				'LOGOIMG' => 'https://example.com/mylogo.png',
				'CHANNELTYPE' => 'Merchant',
				'LANDINGPAGE'	=> 'Billing',
				'EMAIL'			=> $useremail,
				'HDRBACKCOLOR'	=> "EEEEEE",
				'CUSTOM'		=>  $cart['order_id']
			];

			$this->provider->addOptions($options);

            $response = $this->provider->setExpressCheckout($cart, $recurring);

			if(isset($response['TOKEN']) && $response['TOKEN'] != ""){

				$token = $response['TOKEN']; 

				$invoice = new Invoice();

				$invoice->updateInvoiceToken($cart['order_id'], $token);

				return redirect($response['paypal_link']);
			} else {
				//redirect to service request listing page
			}
            
        } catch (Exception $e) {
          
            session()->put(['code' => 'danger', 'message' => "Error processing PayPal payment for Order $invoice->id!"]);
        }
	}

	/**
     * Set cart data for processing payment on PayPal.
     *
     * @param bool $recurring
     *
     * @return array
     */
    protected function getCheckoutData($service_request_id)
    {
        $data = [];

        $order_id = Invoice::all()->count() + 1;

		$servicerequest = News::find($service_request_id);
		
		if(isset($servicerequest->id)){
			$data['items'] = [
				[
					'name'  => 'Service request payment',
					'price' => $servicerequest->amount,
					'qty'   => 1
				]
			];

			$data['return_url'] = url('/paypal/success');

			$data['cancel_url'] = url('/paypal/cancel');

			$data['service_request_id'] = $service_request_id;

			$data['invoice_description'] = "Payment for Invoice";

			$total = 0;
			foreach ($data['items'] as $item) {
				$total += $item['price'] * $item['qty'];
			}

			$data['total'] = $total;
			
			$invoice = $this->createInvoice($data, 'Invalid');

			$data['order_id'] = $invoice->id;

			$order_id = $data['order_id'];

			$data['invoice_description'] = "Payment for Invoice #".str_pad($order_id, 6,"0",STR_PAD_LEFT);

			$data['invoice_id'] = config('paypal.invoice_prefix').'_'.$invoice->id;

			return $data;
		} else {
			return redirect('/')->with('error', 'Please contact administrator.');
		}
    }

	/**
     * Create invoice.
     *
     * @param array  $cart
     * @param string $status
     *
     * @return \App\Invoice
     */
    protected function createInvoice($cart, $status)
    {
        $invoice = new Invoice();
        $invoice->title = $cart['invoice_description'];
        $invoice->price = $cart['total'];
		 $invoice->news_id = $cart['service_request_id'];
        if (!strcasecmp($status, 'Completed') || !strcasecmp($status, 'Processed')) {
            $invoice->is_paid = 1;
        } else {
            $invoice->is_paid = 0;
        }
		$invoice->user_id = auth()->user()->id;
        $invoice->save();

        return $invoice;
    }

	public function cancel(Request $request){

		$token = ($request->get('token') === 'recurring') ? $request->get('token') : 0;

		$invoiceDetails = Invoice::where('token', $token)->first();
		
		if(isset($invoiceDetails->id)){
			$invoiceDetails->delete();
		}

		return view('site.paypal.cancel');

	}

	/**
     * Process payment on PayPal.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function success(Request $request)
    {
        
        $token = $request->get('token');
        $PayerID = $request->get('PayerID');


        // Verify Express Checkout Token
        $response = $this->provider->getExpressCheckoutDetails($token);
		
        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
			
			
			$invoice = new Invoice();
			$invoiceid = str_replace(config('paypal.invoice_prefix')."_", "", $response['INVNUM']);
			
			$invoiceDetails = $invoice->find($invoiceid);

			$servicerequest = News::find($invoiceDetails->news_id);

			$cart = [];

			$data['items'] = [
				[
					'name'  => 'Service request payment',
					'price' => $servicerequest->amount,
					'qty'   => 1
				]
			];
			
			$order_id = $invoiceDetails->id;
			$invoice_description =  "Payment for Invoice #".str_pad($order_id, 6,"0",STR_PAD_LEFT);

			$data['return_url'] = url('/paypal/success');
       
			$data['invoice_description'] = $invoiceDetails->title;

			$data['invoice_description'] = $invoice_description;

			$data['cancel_url'] = url('/paypal/cancel');

			$data['service_request_id'] = $invoiceDetails->news_id;

			$total = 0;
			foreach ($data['items'] as $item) {
				$total += $item['price'] * $item['qty'];
			}

			$data['total'] = $total;

			$data['invoice_id'] = $order_id;

			// Perform transaction on PayPal
			$payment_status = $this->provider->doExpressCheckoutPayment($data, $token, $PayerID);
			//echo '<pre>';print_r($payment_status);die;
			
			$invoiceDetails->title = $invoice_description;	

			if(isset($payment_status['ACK']) && (trim(strtolower($payment_status['ACK'])) == "success" || trim(strtolower($payment_status['ACK'])) == "successwithwarning")){
				
				$status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'];
				$txn_id = $payment_status['PAYMENTINFO_0_TRANSACTIONID'];

				$invoiceDetails->paypal_txn_id = $txn_id;
				$invoiceDetails->is_paid = 1;
				$invoiceDetails->save();


				return redirect('/news/'. $servicerequest->permalink)->with('success', "Order $invoice->id has been paid successfully!");
			} else {
				//echo '<pre>';print_r($payment_status);die;
				$invoiceDetails->save();
				return redirect('/')->with('error', "Order $invoice->id has not approved by paypal. Please contact administrator.");
			}
        }
    }


	

}

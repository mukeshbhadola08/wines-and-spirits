<?php

namespace App\Http\Controllers;

use App\Contactus;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use File;

class ContactusController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Contactus::all();
        return view('admin.contact.index',compact('data'));
    }

    public function create(){
        return view('admin.contact.create');
    }

    public function store(Request $request)
    {
        $contact = new Contactus();
        $data = $this->validate($request, [
            'address'=> 'required',
            'contact' => 'required',
            'email' =>'required',
            'show_contact'=> '',
            'latitude' =>'required',
            'longitude' =>'required'
        ]);
        if (!isset($data['show_contact'])) {
            $data['show_contact'] = 0;
        }
        $lastinsetid = $contact->savecontact($data);
        return redirect('/admin/contact')->with('success', 'Contact has been created');
    }

    public function edit($id)
    {
        $contact = Contactus::where('id', $id)->first();
        return view('admin.contact.edit', compact('contact', 'id'));
    }

    public function update(Request $request, $id)
    {
        $contact = new Contactus();
        $data = $this->validate($request, [
            'address'=> 'required',
            'contact' => 'required',
            'email' =>'required',
            'show_contact'=> '',
            'latitude' =>'required',
            'longitude' =>'required'
        ]);
        if (!isset($data['show_contact'])) {
            $data['show_contact'] = 0;
        }
        $data['id'] = $id;
        $contact->updateContact($data);
        return redirect('/admin/contact')->with('success', 'Contact has been Updated!');
    }

    public function delete($id)
    {
        $delete = Contactus::find($id);
        $delete->delete();
        return redirect('/admin/contact')->with('success', 'Contact has been deleted!!');
    }
}

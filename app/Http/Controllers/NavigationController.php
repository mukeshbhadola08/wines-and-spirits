<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;
use App\Blog;
use App\Navigation;
use App\Helpers\StaticPages;

class NavigationController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function getSql()
	{
		$builder = $this->getBuilder();
		$sql = $builder->toSql();
		foreach ($builder->getBindings() as $binding) {
			$value = is_numeric($binding) ? $binding : "'" . $binding . "'";
			$sql = preg_replace('/\?/', $value, $sql, 1);
		}
		return $sql;
	}

	public function allNavigation()
	{
		$navigations = Navigation::where([['parent_id', '=', 0], ['lang_code', '=', 'en']])->orderBy('nav_order', 'asc')->get();

		return view('admin.navigations.navigation_menu', compact('navigations'));
	}

	public function createNav()
	{
		$pages = Page::where([['is_visible', '=', 1], ['lang_code', '=', 'en']])->get();

		$blogs = Blog::where([['is_visible', '=', 1], ['lang_code', '=', 'en']])->get();

		return view('admin.navigations.create_menu', compact('pages', 'blogs'));
	}

	public function saveNavigation(Request $request){

		$navigation = new Navigation();
		$data = $this->validate($request, [
			'title' => 'required|unique:navigation_menu',
			'show_header' => '',
			'show_footer' => '',
			'lang_code' => '',
			'parent_lang_id' => '',
			'visible' => ''
		]);
		if (!isset($data['visible'])) {
			$data['visible'] = 0;
		}
		if (!isset($data['show_header'])) {
			$data['show_header'] = 0;
		}
		if (!isset($data['show_footer'])) {
			$data['show_footer'] = 0;
		}

		$page = $request->input('page');
		if (in_array($page, StaticPages::$pages)) {
			$data['static_page_name'] = $page;
			$data['type'] = StaticPages::STATIC_PAGES_TYPE;
		} else {
			$pos = strpos($page, "page-");
			$data['static_page_name'] = "Not Applicable";
			if ($pos !== false) {
				// CMS Page
				$pageid = str_replace("-page-", "", $page);
				$data['type'] = StaticPages::STATIC_CMS_PAGES_TYPE;
			} else {
				// blogs Page
				$pageid = str_replace("-blog-", "", $page);
				$data['type'] = StaticPages::STATIC_BLOG_PAGES_TYPE;
			}
		}

		if ($data['type'] == 1 || $data['type'] == 2) {
			if ($pageid <= 0) {
				return redirect()
					->back()->withErrors('Page Or News is not selected');
			} else {
				$data['reference_id'] = $pageid;
			}
		} else {
			$data['reference_id'] = 0;
		}
		$data['parent'] = 0;

		$navigation->saveNavigation($data);

		if ($data['parent'] > 0) {
			return redirect('/admin/sub_navigations/' . $data['parent'])->with('success', 'New Sub navigation has been created');
		} else {
			return redirect('/admin/navigations')->with('success', 'New navigation has been created');
		}
	}

	public function editNav($id)
	{
		$navigation = Navigation::where('id', $id)->first();
		$blogs = Blog::where([['is_visible', '=', 1], ['lang_code', '=', 'en']])->get();
		$pages = Page::where([['is_visible', '=', 1], ['lang_code', '=', 'en']])->get();
		return view('admin.navigations.edit_menu', compact('pages', 'navigation', 'blogs'));
	}

	public function getSubNav($id)
	{
		$navigations = Navigation::where([['parent_id', '=', $id], ['lang_code', '=', 'en']])->orderBy('nav_order', 'asc')->get();

		return view('admin.navigations.navigation_sub_menu', compact('navigations', 'id'));
	}

	public function createSubNav($id)
	{
		$blogs = Blog::where([['is_visible', '=', 1], ['lang_code', '=', 'en']])->get();
		$pages = Page::where([['is_visible', '=', 1], ['lang_code', '=', 'en']])->get();
		return view('admin.navigations.create_menu', compact('pages', 'id', 'blogs'));
	}

	public function editSubNav($id)
	{
		$navigation = Navigation::where('id', $id)->first();
		$blogs = Blog::where([['is_visible', '=', 1], ['lang_code', '=', 'en']])->get();
		$pages = Page::where([['is_visible', '=', 1], ['lang_code', '=', 'en']])->get();
		return view('admin.navigations.create_menu', compact('pages', 'navigation', 'blogs'));
	}
	
	public function updateNavigation(Request $request,$id){

		$navigation = new Navigation();

		$data = $this->validate($request, [
			'title' => 'required',
			'type' => '',
			'show_header' => '',
			'show_footer' => '',
			'visible' => ''
		]);
		if (!isset($data['visible'])) {
			$data['visible'] = 0;
		}
		if (!isset($data['show_header'])) {
			$data['show_header'] = 0;
		}
		if (!isset($data['show_footer'])) {
			$data['show_footer'] = 0;
		}
		
		$page = $request->input('page');
		if (in_array($page, StaticPages::$pages)) {
			$data['static_page_name'] = $page;
			$data['type'] = StaticPages::STATIC_PAGES_TYPE;
		} else {
			$pos = strpos($page, "page-");
			$data['static_page_name'] = "Not Applicable";

			if ($pos !== false) {
				// CMS Page
				$pageid = str_replace("-page-", "", $page);
				$data['type'] = StaticPages::STATIC_CMS_PAGES_TYPE;
			} else {
				// blog Page
				$pageid = str_replace("-blog-", "", $page);
				$data['type'] = StaticPages::STATIC_BLOG_PAGES_TYPE;
			}
		}

		if ($data['type'] == 1 || $data['type'] == 2) {
			if ($pageid <= 0) {
				return redirect()
					->back()->withErrors('Page Or News is not selected');
			} else {
				$data['reference_id'] = $pageid;
			}
		} else {
			$data['reference_id'] = 0;
		}

		$data['parent'] = 0;
		
		$navigation->updateNavigation($data,$id);

		if ($data['parent'] > 0) {
			return redirect('/admin/sub_navigations/' . $data['parent'])->with('success', 'Sub navigation has been updated');
		} else {
			return redirect('/admin/navigations')->with('success', 'Navigation has been updated');
		}
		
	}
	public function deleteNav($id)
	{
		//delete sub navigation
		Navigation::where('parent_id', $id)->delete();

		$navigation = Navigation::find($id);
		$parent =  $navigation->parent_id;
		$navigation->delete();

		if ($parent > 0) {
			return redirect('/admin/sub_navigations/' . $parent)->with('success', 'Sub navigation has been deleted');
		} else {
			return redirect('/admin/navigations')->with('success', 'Navigation has been deleted');
		}
	}

	public function setNavigationOrder(Request $request)
	{
		$orders = $request->input('order');
		if (count($orders) > 0) {
			foreach ($orders as $k => $o) {
				$navigation = Navigation::find($o);
				$navigation->update(['nav_order' => $k + 1]);
			}
		}
	}

	public function addInSpanish($id)
	{
		$navigation = Navigation::where('id', $id)->first();
		$blogs = Blog::where([['is_visible', '=', 1], ['lang_code', '=', 'es']])->get();
		$pages = Page::where([['is_visible', '=', 1], ['lang_code', '=', 'es']])->get();
		return view('admin.navigations.add_spanish', compact('pages', 'navigation', 'blogs', 'id'));
	}

	public function editNavInLang($id)
	{
		$navigation = Navigation::where('id', $id)->first();
		$blogs = Blog::where([['is_visible', '=', 1], ['lang_code', '=', 'es']])->get();
		$pages = Page::where([['is_visible', '=', 1], ['lang_code', '=', 'es']])->get();
		return view('admin.navigations.edit_spanish', compact('pages', 'navigation', 'blogs'));
	}

	public function storeInlang(Request $request)
	{
		$navigation = new Navigation();
		$data = $this->validate($request, [
			'title' => 'required',
			'type' => '',
			'show_header' => '',
			'show_footer' => '',
			'lang_code' => '',
			'parent_lang_id' => '',
			'visible' => ''
		]);
		if (!isset($data['visible'])) {
			$data['visible'] = 0;
		}
		if (!isset($data['show_header'])) {
			$data['show_header'] = 0;
		}
		if (!isset($data['show_footer'])) {
			$data['show_footer'] = 0;
		}

		if ($data['type'] == 1) {
			$page = $request->input('page');
			if ($page == '') {
				return redirect()
					->back()->withErrors('Page Or News is not selected');
			} else {
				$data['reference_id'] = $page;
			}
		} else {
			$blog = $request->input('blog');
			if ($blog == '') {
				return redirect()
					->back()->withErrors('Page Or News is not selected');
			} else {
				$data['reference_id'] = $blog;
			}
		}

		$data['parent'] = $request->input('parent');
		$navigation->saveNavigation($data);

		if ($data['parent'] > 0) {
			return redirect('/admin/sub_navigations/' . $data['parent'])->with('success', 'Sub navigation has been saved');
		} else {
			return redirect('/admin/navigations')->with('success', 'Navigation has been saved');
		}
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServicesCustomForms;
use App\ServicesCustomFormOptions;

class ServiceCustomFormController extends ServiceController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	public function saveCustomForm(Request $request){
		
		$data = $this->validate($request, [
            'field_label'=>'required',
            'field_type'=> 'required'
			
        ]);
		$id = $request->input('service');
		$formObj = new ServicesCustomForms();
		$form_id =$formObj->saveForm($request->all());

		// to save form options
		$type = $request->input('field_type');
		if($type == 'select' || $type == 'radio' || $type == 'checkbox'){
			$formOptions = $request->input('option_label');
			$optionVisible = $request->input('option_visible');
			if(count($formOptions)>0){
				
				foreach($formOptions as $key=>$field){
					if($field !=''){
						$optionsData = array();
						$optionsData['name'] = $field;
						$optionsData['form'] = $form_id;
						$optionsData['check'] = $optionVisible[$key];
						$formOptionsObj = new ServicesCustomFormOptions();
						$formOptionsObj->saveFormOptions($optionsData);
					}
				}
				
			}
		}

		return redirect('/admin/services/customform/'.$id)->with('success', 'Custom form has been saved successfully.');

	}
	
	public function deleteform($id){
		//first delete it options
		$this->deleteOptions($id);

		//delete form
		$cutomform = ServicesCustomForms::find($id);
		$service = $cutomform->service_id;
        $cutomform->delete();
		return redirect('/admin/services/customform/'.$service)->with('success', 'Custom form has been deleted successfully.');
	}
	public function edit_form($id){
		$this->data['title'] ='Custom Form'; // set the page title
		$customform = ServicesCustomForms::find($id);
		$formOptions= ServicesCustomFormOptions::where('form_id', $customform->id)->get();
        return view('admin.services.edit_form', $this->data)->with(compact('id','customform','formOptions'));

	}
	public function updateCustomForm(Request $request,$id){
		$data = $this->validate($request, [
            'field_label'=>'required',
            'field_type'=> 'required'			
        ]);
		$allRequest = $request->all();
		$allRequest['id'] = $id;
		$formObj = new ServicesCustomForms();
		$formObj->updateForm($allRequest);
		$service = $request->input('service');
		$type = $request->input('field_type');

		//user deleted options
		$delete_options = $request->input('detete_option');
		if($delete_options !=''){
			$delOptions = ServicesCustomFormOptions::whereIn('id', explode(',',$delete_options));
			$delOptions->delete();
		}


		//if user change input where option is not required type  then removed previous option set on respective type

		if($type == 'text' || $type == 'file' || $type == 'textarea'){

			$checkOptions = ServicesCustomFormOptions::where('form_id',$id)->get();

			if(Count($checkOptions)>0){
				$this->deleteOptions($id);
			}
		}

		// to save form options
		
		if($type == 'select' || $type == 'radio' || $type == 'checkbox'){
			$formOptions = $request->input('option_label');
			$optionVisible = $request->input('option_visible');
			if(count($formOptions)>0){
				
				foreach($formOptions as $key=>$field){
					if($key>0){
						$optionsData = array();
						$optionsData['name'] = $field;
						$optionsData['form'] = $id;
						$optionsData['check'] = $optionVisible[$key];
						$formOptionsObj = new ServicesCustomFormOptions();
						$formOptionsObj->updateFormOptions($optionsData,$key);

					} else {
						if(isset($formOptions[0]) && is_array($formOptions[0])){
							foreach($formOptions[0] as $nk=>$nf){
								if($nf !=''){
									$optionsData = array();
									$optionsData['name'] = $nf;
									$optionsData['form'] = $id;
									$optionsData['check'] = $optionVisible[0][$nk];
									$formOptionsObj = new ServicesCustomFormOptions();
									$formOptionsObj->saveFormOptions($optionsData);
								}
							}
						}
					}
				}
				
			}
		}

		return redirect('/admin/services/customform/'.$service)->with('success', 'Custom form has been updated successfully.');
	}
	public function deleteOptions($id){
		$cutomformOption = ServicesCustomFormOptions::where('form_id',$id);
		if(!empty($cutomformOption)){
			$cutomformOption->delete();
		}
	}
}

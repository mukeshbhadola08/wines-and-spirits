<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Response;
use App\Services;
use App\ServicesCustomForms;
use App\ServiceRequest;
use App\ServiceRequestMetaData;
use App\ServicesCustomFormOptions;


class ServiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['getServiceImage']]);
    }
	
	
	public function services()
    {
        $this->data['title'] = 'Services'; // set the page title

		$services = Services::where('parent',0)->get();

        return view('admin.services.services',  $this->data)->with(compact('services'));
    }
	public function add_service()
    {
        $this->data['title'] ='Services'; // set the page title

        return view('admin.services.create_service', $this->data);
    } 
	public function saveService(Request $request)
    {
     
        $service = new Services();
        $data = $this->validate($request, [
            'description'=>'required',
            'title'=> 'required',
			'summery'=> 'required',
			'cost'=> 'nullable|numeric'
        ]);
		$parent=$request->input('parent');
        $id =$service->saveServices($request->all());
		if($parent>0){
			$success = 'Sub-service has been added successfully.';
			$services = Services::where('parent', $parent)->get();
			return view('admin.services.sub_services',['id'=> $parent,'services'=>$services,'success'=>$success]);
		} else {

			return redirect('/admin/services')->with('success', 'Services has been added successfully.');
		}
   
    }
	public function editService($id)
    {
        $service = Services::where('id', $id)->first();
        return view('admin.services.edit_service', compact('service', 'id'));
    }
	public function updateService(Request $request, $id)
    {
        $service = new Services();
        $data = $this->validate($request, [
            'description'=>'required',
            'title'=> 'required',
			'summery'=> 'required',
			'cost'=> 'nullable|numeric'
        ]);
		$parent=$request->input('parent');
		
		$service->updateServices($request->all(),$id);
		if($parent>0){
			$success = 'sub-service has been updated successfully.';
			$services = Services::where('parent', $parent)->get();
			return view('admin.services.sub_services',['id'=> $parent,'services'=>$services,'success'=>$success]);
		} else {
			return redirect('/admin/services')->with('success', 'Service has been updated successfully.');
		}
    }
	public function deleteService($id)
    {
		// check it sub service and deletes its contains

		$subserviceDetails = Services::where('parent',$id)->get();
		if(count($subserviceDetails)>0){
			foreach($subserviceDetails as $sub){

				//first delete all sub service request reference
				$requestDetails = ServiceRequest::where('service_id',$sub['id'])->get();
				if(count($requestDetails)>0){
					foreach($requestDetails as $request){
						$metaDetails = ServiceRequestMetaData::where('service_rquest_id',$request['id'])->get();
						if(!empty($metaDetails) && count($metaDetails)>0){
							ServiceRequestMetaData::where('service_rquest_id',$request['id'])->delete();
						}
						$current = ServiceRequest::find($request['id']);
						$current->delete();
					}
				}

				//delete all created custom form on it
				$form_details = ServicesCustomForms::where('service_id',$sub['id'])->get();

				if(count($form_details)>0){
					foreach($form_details as $form){
						$formOptiondetails = ServicesCustomFormOptions::where('form_id',$form['id'])->get();
						if(!empty($formOptiondetails) && count($formOptiondetails)>0){
							ServicesCustomFormOptions::where('form_id',$form['id'])->delete();
						}
						$currentForm = ServicesCustomForms::find($form['id']);
						$currentForm->delete();
					}
				}

				//finally delete the current service
				$serviceObj = new Services();
				$subservice = Services::find($sub['id']);
				if($subservice->image !=''){
					$serviceObj->deleteImage($sub['id']);
				}
				$subservice->delete();
			}

		}

		//first delete all request reference
		$requestDetails = ServiceRequest::where('service_id',$id)->get();
		if(count($requestDetails)>0){
			foreach($requestDetails as $request){
				$metaDetails = ServiceRequestMetaData::where('service_rquest_id',$request['id'])->get();
				if(!empty($metaDetails) && count($metaDetails)>0){
					ServiceRequestMetaData::where('service_rquest_id',$request['id'])->delete();
				}
				$current = ServiceRequest::find($request['id']);
				$current->delete();
			}
		}

		//delete all created custom form on it
		$form_details = ServicesCustomForms::where('service_id',$id)->get();

		if(count($form_details)>0){
			foreach($form_details as $form){
				$formOptiondetails = ServicesCustomFormOptions::where('form_id',$form['id'])->get();
				if(!empty($formOptiondetails) && count($formOptiondetails)>0){
					ServicesCustomFormOptions::where('form_id',$form['id'])->delete();
				}
				$currentForm = ServicesCustomForms::find($form['id']);
				$currentForm->delete();
			}
		}

		//finally delete the current service
		$serviceObj = new Services();
        $service = Services::find($id);
		if($service->image !=''){
			$serviceObj->deleteImage($id);
		}
        $service->delete();

        return redirect('/admin/services')->with('success', 'Service has been deleted!!');
    }
	public function subServices($id){
		$this->data['title'] ='Services'; // set the page title

		$services = Services::where('parent', $id)->get();

        return view('admin.services.sub_services', $this->data)->with(compact('services','id'));
	}
	public function addSubService($id){
		$this->data['title'] ='Services'; // set the page title

        return view('admin.services.create_service', $this->data)->with(compact('id'));
	}
	public function getServiceImage($id){
		$service = Services::find($id);
		if($service->image !=''){
			$path = storage_path('app/public/services/') . $service->image;
		} else {
			$path = storage_path('app/public/') . 'no-img.jpg';
		}
        if(!File::exists($path)){ 
            $path = storage_path('app/public/') . 'no-img.jpg';
		}

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
	}
	public function serviceCustomForms($id){
		$this->data['title'] ='Services'; // set the page title

		$customForms = ServicesCustomForms::where('service_id', $id)->get();

        return view('admin.services.customForms', $this->data)->with(compact('customForms','id'));

	}
	public function create_form($id){
		$this->data['title'] ='Services'; // set the page title

        return view('admin.services.create_form', $this->data)->with(compact('id'));

	}
    
}

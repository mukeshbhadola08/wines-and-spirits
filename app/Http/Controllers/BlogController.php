<?php

namespace App\Http\Controllers;

use App\BlogComments;
use App\Blog;
use App\Slider;
use App\Http\Controllers\SliderController;
use App\Navigation;
use File;
use Response;
use Storage;
use Carbon\Carbon;
use App\Helpers\CommonFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\PageSetting;
use App\Http\Controllers\Controller;
class BlogController extends Controller
{
	private $uploadTo;

	public function __construct()
    {
		$this->middleware('auth', ['except' => ['view', 'getImage', 'list', 'addGuestComment']]);
		$this->uploadTo = CommonFunctions::getUploadDir();
    }
    /**
     * Display a listing of the pages.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$blogs = Blog::get();
        return view('admin.blogs.index',compact('blogs'));
    }

	public function create(){
		return view('admin.blogs.create');
	}

	private function toAscii($vp_string) {
		$vp_string = trim($vp_string);

		$vp_string = html_entity_decode($vp_string);

		$vp_string = strip_tags($vp_string);

		$vp_string = strtolower($vp_string);

		$vp_string = preg_replace('~[^ a-z0-9_.]~', ' ', $vp_string);

		$vp_string = preg_replace('~ ~', '-', $vp_string);

		$vp_string = preg_replace('~-+~', '-', $vp_string);

		return $vp_string;
	}

    //
	public function store(Request $request)
	{
        $blog = new Blog();
        $data = $this->validate($request, [
            'blog_description'=>'required',
			'short_description'=>'required|max:300',
            'blog_title'=> 'required',
			'permalink'=> 'required',
			'is_visible'=> '',
			'video_url'=> '',
			'lang_code'=> '',
			'parent'=> '',
			'image' => 'required',
			'blog_image' => 'mimes:jpeg,bmp,png,gif,jpg'
        ]);

		$path = '';

		if(is_numeric($data['image'])){
        	$image = Slider::where('id',$data['image'])->pluck('id')->all();
        	$data['image'] = null;
        	$data['slider_id'] = $image[0];
        }else{
        	$original_name = $request->image->getClientOriginalName();

			$ext =  pathinfo($original_name, PATHINFO_EXTENSION);

			$image_name = 'default_'.uniqid().'.'.$ext;

			$path = $request->image->storeAs($this->uploadTo, $image_name);
			$SliderControllerobj = new SliderController();
			 $ext = pathinfo($path, PATHINFO_EXTENSION);
                $SliderControllerobj->correctImageOrientation(
					550,
					storage_path('app/public/'.$path),
					$ext
				);
			$data['slider_id'] = null;
			$data['image'] = $path;
        }

		$cleanurl = $this->toAscii($data['permalink']);

		$cleanurl = $this->getCleanUrl($cleanurl);

		$data['clean_url'] = $cleanurl;

		if(!isset($data['is_visible'])){
			$data['is_visible'] = 0;
		}

        $lastinsetid = $blog->saveBlog($data);
        return redirect('/admin/blogs')->with('success', 'New blog has been created');
    }

	public function edit($id)
    {
        $blog = Blog::where('id', $id)->first();
        return view('admin.blogs.edit', compact('blog', 'id'));
    }

	public function update(Request $request, $id)
    {

		$blogDetails = Blog::where('id', $id)
                        ->first();

        $blog = new Blog();
        $data = $this->validate($request, [
            'blog_description'=>'required',
			'short_description'=>'required|max:300',
            'blog_title'=> 'required',
			'is_visible'=> '',
			'video_url'=> '',
			'image' => '',
			'blog_image' => 'mimes:jpeg,bmp,png,gif,jpg'
        ]);

		if(isset($data['image']) && is_numeric($data['image'])){
        	$image = Slider::where('id',$data['image'])->pluck('id')->all();
        	$data['image'] = null;
        	$data['slider_id'] = $image[0];
        }else{
        	if(!isset($data['image'])){
        		$data['image'] = null;
        		$data['slider_id'] = null;
        	}else{
        		$original_name = $request->image->getClientOriginalName();

				$ext =  pathinfo($original_name, PATHINFO_EXTENSION);

				$image_name = 'default_'.uniqid().'.'.$ext;

				$path = $request->image->storeAs($this->uploadTo, $image_name);

				$SliderControllerobj = new SliderController();
				 $ext = pathinfo(base_path() . '/storage/app/public/'. $path, PATHINFO_EXTENSION);
                $SliderControllerobj->correctImageOrientation(550, base_path() . '/storage/app/public/'. $path, $ext, base_path() . '/storage/app/public/default_images');
				$data['slider_id'] = null;
				$data['image'] = $path;
        	}
        }

		if(!isset($data['is_visible'])){
			$data['is_visible'] = 0;
		}

		$data['id'] = $id;

        $blog->updateBlog($data);

        return redirect('/admin/blogs')->with('success', 'Blog has been updated!!');
    }

	public function delete($id){

		$childBlogs =  Blog::where('parent_lang_id',$id)->get(); // other language references

		//deleting from navigation
		$delNav = Navigation::where([['type','=',2],['reference_type_id','=',$id]])->delete();  // english
		// remove other language data
		if(count($childBlogs)>0){ // other language
			foreach($childBlogs as $cdbg){
				$childOP = Navigation::where([['type','=',2],['reference_type_id','=',$cdbg['id']]]);
				$childOP->delete();
			}
		}
		$childBlog = Blog::where('parent_lang_id',$id)->delete();

        $page = Blog::find($id);

		if($page->blog_image !=''){
			$path = storage_path('app/public/') . $page->blog_image;
			if(File::exists($path)){
				Storage::delete($page->blog_image);
			}
		}

        $page->delete();
        return redirect('/admin/blogs')->with('success', 'Blog has been deleted!!');
    }

	public function getCleanUrl($cleanurl, $i = 0){
		global $db, $connMCP;
		if($i>0){
			$cleanurl = $cleanurl."".$i;
		}

		$page = Blog::where('clean_url', $cleanurl)->first();

		if(isset($page->id)){
			$i = $i + 1;
			$cleanurl = $this->getCleanUrl($cleanurl, $i);
		}

		return $cleanurl;
	}

	 /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($cleanurl){
		global $lang_val;
        $blog = Blog::where(
			'clean_url',
			$cleanurl
		)->first();
		
        if (empty($blog)) {
            return abort(404);
        }

		if ($lang_val != $blog->lang_code) {
			if (($blog->parent_lang_id) > 0) {
				$blogdetails = Blog::find($blog->parent_lang_id);
				if (!empty($blogdetails)) {
					$blog = $blogdetails;
				}
			} else {
				$blogdetails = Blog::where('parent_lang_id', $blog->id)->first();
				if (!empty($blogdetails)) {
					$blog = $blogdetails;
				}
			}
		}

		// all blogs
		$allBlogs = Blog::where([['is_visible','=',1],['lang_code','=','en']])->orderBy('id','DESC')->limit(12)->get();
		$singleBlogPageSettings = PageSetting::where('id', 5)->first();
        return view('site.blogs.view', compact('blog', 'cleanurl','allBlogs', 'singleBlogPageSettings'));
    }

	public function list(){
		$blogs = Blog::where([['is_visible','=',1],['lang_code','=','en']])->orderBy('id','DESC')->paginate(9);
;
		return view('site.blogs.list', compact('blogs'));
	}
	public function searchList(Request $request){
		$blogs = Blog::where([['is_visible','=',1],['lang_code','=','en']])->orderBy('id','DESC')->paginate(9);
;
		return view('site.blogs.list', compact('blogs'));
	}

	public static function getImage($id){
		$blog = Blog::where('id', $id)->first();

        if(isset($blog->default_image) && $blog->default_image !=''){
			$path = storage_path('app/public/') . $blog->default_image;
            if(!File::exists($path)){
                $path = storage_path('app/public/') . 'no-img.jpg';
            }

            $file = File::get($path);
            $type = File::mimeType($path);

            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);

            return $response;

		}elseif($blog->slider_id !=''){
			$slider = Slider::where('id', $blog->slider_id)->first();
			$url = url('/public/storage/slider_images/' . $slider->image);
		} else {
			$url = url('/public/storage/intro-img-top.png');
		}

        return $url;

	}

	public function addcomment(Request $request)
	{
		$blog = new BlogComments();
		$defaultStatus = config('constants.DEFAULT_APPROVE_COMMENTS')??0;
		$defaultStatus = (int) ($defaultStatus == 1 ? $defaultStatus : 0);
		$comment = $request->comment;
		$blog_id = Crypt::decrypt($request->blog_id??0);
		$blog->user_id = auth()->user()->id;
        $parentId = $request->parent_blog_id??'';
        $parentId = $parentId != '' ? Crypt::decrypt($parentId) : 0;
		//$blog->save();
		$insertId = $blog->addcomment($comment, $blog_id, $parentId, $defaultStatus);
		$success = false;
		$message = 'Failed to add your comment, Please try after some time.';
		if ($insertId) {
			$success = true;
			$message = 'your comment added successfully and publish after approval, Thank you.';
		}
		return response()->json(
			[
				'success' => $success,
				'message' => $message
			]
		);
	}

	public function addGuestComment(Request $request)
	{
		$defaultStatus = config('constants.DEFAULT_APPROVE_COMMENTS')??0;
		$defaultStatus = (int) ($defaultStatus == 1 ? $defaultStatus : 0);
		$blog = new BlogComments();
		$validator = \Validator::make($request->all(), [
            'email' => 'required|email',
            'name' => 'required',
		]);
		if ($validator->fails())
		{
			$message = '';
			foreach ($validator->errors()->all() as $error) {
				$message .= $error;
			}
			return Response::json([
				'Invalid comment post! '.$message,
			], 400);
		}
		$comment = $request->comment;
		$guestName = $request->name;
        $guestEmail = $request->email;
        $parentId = $request->parent_blog_id??'';
        $parentId = $parentId != '' ? Crypt::decrypt($parentId) : 0;
		$blog_id = (int) Crypt::decrypt($request->blog_id ?? 0);
		$blog->save();
		$status = $defaultStatus;
		$insertId = $blog->addGuestComment(
			$blog_id,
			$status,
			$comment,
			$guestName,
            $guestEmail,
            $parentId
		);
		$success = false;
		$message = 'Failed to add your comment, Please try after some time.';
		if ($insertId) {
			$success = true;
			$message = 'your comment added successfully and publish after approval, Thank you.';
		}
		return response()->json(
			[
				'success' => $success,
				'message' => $message
			]
		);
	}

	public function allComments()
	{
		return view('admin.blogs.allcomments');
    }

    public function optionsComments()
    {
        return view('admin.blogs.allcomments');
    }

	public function allCommentsListings(Request $request)
	{
		$totalComments = BlogComments::has('blog')->count();
		$all_request_page_row = [];
		$all_request_page_table = [];
		$limit = $request->iDisplayLength??10;
		$offset = $request->iDisplayStart??0;
		$encryptedApproved = Crypt::encryptString(1);
		$encryptedReject = Crypt::encryptString(2);
		$html = '<select class="commentActions" data-refrance="%s">
		<option value="">Select Action</option>
		<option value="'.$encryptedApproved.'">Approve</option>
		<option value="'.$encryptedReject.'">Reject and Remove</option>
		</select>';
		$blogLink = '<a href="'.url('/blogs/%s').'">%s</a>';
		$blogsComments = BlogComments::has('blog')->orderBy('id', 'DESC')->skip($offset)->take($limit)->get();
		if (!empty($blogsComments)) {
			 foreach ($blogsComments as $comment) {
                $encryptedCommentId = Crypt::encryptString($comment->id);
                $all_request_page_row['blog_title'] = sprintf($blogLink, $comment->blog->clean_url, $comment->blog->blog_title);
                $editableComment = '<a href="javascript:void(0)" data-type="text" data-modify_comment="'.$encryptedCommentId.'" class="commentEditable" data-title="'.$comment->comment.'">'.$comment->comment.'</a>';
				$all_request_page_row['comment'] = $editableComment;
				$all_request_page_row['name'] = $comment->user_id > 0 ? $comment->user->name : $comment->guest_name;
				$all_request_page_row['email'] = $comment->user_id > 0? $comment->user->email : $comment->guest_email;
				$all_request_page_row['user_type'] = $comment->user_id > 0 ? 'user' : 'guest';
				$all_request_page_row['status'] = $comment->status > 0 ? 'Approved' : 'Not Approved';
				$all_request_page_row['created_at'] = Carbon::parse($comment->created_at)->format('d/m/Y');
				$all_request_page_row['action'] = $comment->status > 0 ? '' : sprintf($html ,$encryptedCommentId);
				array_push($all_request_page_table, $all_request_page_row);
			}
		}
		return response()->json([
			'sEcho' => intval($request->sEcho),
			'iTotalRecords' => count($blogsComments),
			'iTotalDisplayRecords' => $totalComments,
			'aaData' => $all_request_page_table
		]);
	}

	public function blogCommentActions(Request $request)
	{
        $blog = new BlogComments();
		$action = $request->commentAction??'';
		$commentId = $request->commentToBeActioned??'';
		$action = Crypt::decryptString($action);
        $commentId = Crypt::decryptString($commentId);
        $message = 'selected comment removed successfully!';
		if ($action == 1) {
            $message = 'selected comment approved successfully!';
            $blog->approvecomment($commentId);
		} else {
            $rsltDelRec = BlogComments::find($commentId);
            $rsltDelRec->delete();
        }
		return response()->json(
            [
				'success' => true,
				'message' => $message
			]
        );
    }

    public function blogCommentValUpdate(Request $request)
    {
        $blog = new BlogComments();
		$commentVal = $request->commentVal??'';
		$commentId = $request->comment??'';
        $commentId = (int) Crypt::decryptString($commentId);
        $message = 'failed to update selected comment!';
        $success = false;
        if ($commentVal != '') {
            $message = 'selected comment updated successfully!';
            $success = true;
            $blog->updateCommentMessage(
                $commentId,
                $commentVal
            );
        }
        return response()->json(
            [
				'success' => $success,
				'message' => $message
			]
        );
    }

	public function pendingcomments() {
        $pendingcomments = BlogComments::where('status', '0')->orderBy('id', 'desc')->get();
        return view('admin.blogs.pendingcomments',compact('pendingcomments'));
    }

	public function approvecomment($comment_id) {
		$pendingcomments = new BlogComments();
		$data['status'] = 1;

		$lastinsetid = $pendingcomments->approvecomment($comment_id);
        return redirect('/admin/pendingcomments')->with('success', 'Comment has been approved');
    }
	public function addInSpanish($id)
    {
        $blog = Blog::where('id', $id)
                        ->first();
        return view('admin.blogs.add_spanish', compact('blog', 'id'));
    }
	public function storeInlang(Request $request, $id)
	{
        $blogDetails = Blog::where('id', $id)
                        ->first();

		$blog = new Blog();

        $data = $this->validate($request, [
            'blog_description'=>'required',
			'short_description'=>'required|max:300',
            'blog_title'=> 'required',
			'permalink'=> 'required',
			'is_visible'=> '',
			'lang_code'=> '',
			'parent'=> '',
			'blog_image' => 'mimes:jpeg,bmp,png,gif,jpg'
        ]);

		if(!isset($request->blog_image)){
			$path = $blogDetails->blog_image;
		} else {
			$original_name = $request->blog_image->getClientOriginalName();
			$ext =  pathinfo($original_name, PATHINFO_EXTENSION);
			$image_name = 'blog_'.uniqid().'.'.$ext;
			$path = $request->blog_image->storeAs('blog_images', $image_name);
		}

		$data['blog_image'] = $path;

		$cleanurl = $this->toAscii($data['permalink']);

		$cleanurl = $this->getCleanUrl($cleanurl);

		$data['clean_url'] = $cleanurl;

		if(!isset($data['is_visible'])){
			$data['is_visible'] = 0;
		}

        $lastinsetid = $blog->saveBlog($data);
        return redirect('/admin/blogs')->with('success', 'New blog has been created');
    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\ServiceRequest;
use App\Services;

use Illuminate\Support\Facades\DB;

class MessagesController extends Controller
{
    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function index($keythreadactive = 0){

        // All threads that user is participating in
        $threads = Thread::forUser(Auth::id())->latest('updated_at')->get();
        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages(Auth::id())->latest('updated_at')->get();

		$userid = Auth::id();
        return view('site.messenger.index', compact('threads', 'userid', 'keythreadactive'));
    }

	public function adminindex($keythreadactive=0){

        // All threads that user is participating in
        $threads = Thread::forUser(Auth::id())->latest('updated_at')->get();
        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages(Auth::id())->latest('updated_at')->get();
        $userid = Auth::id();
        return view('admin.messages.index', compact('threads', 'userid', 'keythreadactive'));
    }

	public function chatsbyservice($servicerequestid=0){

        // All threads that user is participating in
        $threads = Thread::forUser(Auth::id())->latest('updated_at')->where("service_request_id", $servicerequestid)->get();
        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages(Auth::id())->latest('updated_at')->get();
        $userid = Auth::id();

		if(!is_array($threads) || count($threads) <=0){
			return redirect()->route('admin.messages.composemessage', $servicerequestid);
		} else {
			return view('admin.messages.servicechat', compact('threads', 'userid'));
		}
    }

    /**
     * Shows a message thread.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

            return redirect()->route('messages');
        }

        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();

        // don't show the current user in list
        $userId = Auth::id();
        $users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get();

        $thread->markAsRead($userId);

        return view('site.messenger.show', compact('thread', 'users'));
    }

    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function create()
    {
        $users = User::where('id', '!=', Auth::id())->get();

        return view('site.messenger.create', compact('users'));
    }

    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store()
    {
        $input = Input::all();

        $thread = Thread::create([
            'subject' => $input['subject'],
			'service_request_id' => 1
        ]);

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => $input['message'],
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'last_read' => new Carbon,
        ]);

        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant($input['recipients']);
        }

        return redirect()->route('messages');
    }

    /**
     * Adds a new message to a current thread.
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

            return redirect()->route('messages');
        }

        $thread->activateAllParticipants();

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => Input::get('message'),
        ]);

        // Add replier as a participant
        $participant = Participant::firstOrCreate([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
        ]);
        $participant->last_read = new Carbon;
        $participant->save();

        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant(Input::get('recipients'));
        }

        return redirect()->route('messages.show', $id);
    }


	 /**
     * Compose a new message thread.
     *
     * @return mixed
     */
    public function composemessage($requestid){

		$servicerequestdetails = ServiceRequest::find($requestid);
		
		if(isset($servicerequestdetails->service_id) && $servicerequestdetails->service_id>0){
			$service = Services::where('id', $servicerequestdetails->service_id)->first();
		
			$users = User::where('id', '!=', Auth::id())->get();

			$users = User::join('service_agents', 'users.id', '=', 'service_agents.agent_id')
			->where('service_agents.service_id','=',$service->id)
			->select('users.*')
			->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
			->get();


			return view('admin.messages.create', compact('users', 'service', 'servicerequestdetails', 'requestid'));
		} else {
			Session::flash('error_message', 'Sorry this request doesnot exist anymore.');
            return redirect()->route('admin/dashboard');
		}
    }

	public function storemessage(){
		$input = Input::all();
		
		foreach($input['recipients'] as $key => $agentid){
			$thread = Thread::create([
				'subject' => $input['subject'],
				'service_request_id' => $input['service_request_id']
			]);

			// Message
			Message::create([
				'thread_id' => $thread->id,
				'user_id' => Auth::id(),
				'body' => $input['message'],
			]);

			// Sender
			Participant::create([
				'thread_id' => $thread->id,
				'user_id' => Auth::id(),
				'last_read' => new Carbon,
			]);

			$participants = array($agentid);

			$thread->addParticipant($participants);
			
		}
		
		return redirect('/admin/messages/list')->with('success', 'Message has been sent to agents.');
    }

	public function updateadmin($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

            return redirect()->route('messages');
        }

        $thread->activateAllParticipants();

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => Input::get('message'),
        ]);

        // Add replier as a participant
        $participant = Participant::firstOrCreate([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
        ]);
        $participant->last_read = new Carbon;
        $participant->save();

        return redirect()->route('admin.messages.list', $id);
    }

	
}

<?php

namespace App\Http\Controllers;

use App\CoachesGrades;
use App\Configuration;
use App\Slider;
use App\News;
use App\Navigation;
use File;
use Response;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CoachesGradesController extends Controller
{
	
	public function __construct()
    {
        $this->middleware('auth', ['except' => ['view', 'getImage', 'coacheslist', 'gradeslist', 'majorachievementslist']]);
    }
	/**
     * Display a listing of the pages.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coaches = CoachesGrades::where('page_type','0')->get();
        $grades = CoachesGrades::where('page_type','1')->get();
        $grades_textarea = Configuration::where('option_name', 'grades_textarea')->pluck("option_value");
        
        return view('admin.coaches-grades.index',compact('coaches', 'grades', 'grades_textarea'));
    }
    
	 public function update(Request $request){
	     
	 	 $data = $this->validate($request, [
            'coach_name'=>'',
            'coach_email'=> '',
			'coach_status'=> '',
			'page_type' => '',
			'created_at' => '',
			'updated_at' => '',
			'school_name' => '',
			'school_year' => '',
			'school_grade' => '',
			'current_coach' => '',
			'coach' => '',
			'from_month' => '',
			'to_month' =>'',
			'from_year' =>'',
			'to_year' =>'',
			'grade' => ''
        ]);
        
       
        
	 	$add_coaches = array();
	 	
	 	$CoachesGrades = new CoachesGrades();
	 	if(isset($data['coach'])){
	 	    $data['coach_status'] = "";
                foreach ($data as $k => $val) {
		   		    if($k == "current_coach"){
		   		        if( $val == "on" ){
		   		            $val = 1;    
		   		        }elseif( $val == "off" ){
		   		            $val = 0;    
		   		        }
		   		        
		   		    }
		   		    
		   		    if($k == "coach_status"){
	   		          $val = $data['from_month'] ." ".$data['from_year'] ." to ".$data['to_month']." ". $data['to_year'];    
		   		    }
		   		  
		   			$data['data'][$k] = $val;
		   		}
		   		if(!isset($data['data']['current_coach'])){
		   		    $data['data']['current_coach'] = 0;
		   		}
		   		$id =  $data['data']['coach'];
		   		
		   		unset( $data['data']['coach']);
		   		unset($data['data']['from_month']);
		   		unset($data['data']['from_year']);
		   		unset($data['data']['to_month']);
		   		unset($data['data']['to_year']);
		   		
		   	$CoachesGrades->updateCoachesGrades($data,$id);
        }

        if(isset($data['grade'])){
	 	    $data['school_year'] = "";
                foreach ($data as $k => $val) {

		   		    
		   		    if($k == "school_year"){
	   		          $val = $data['from_month'] ." ".$data['from_year'] ." to ".$data['to_month']." ". $data['to_year'];    
		   		    }
		   		  
		   			$data['data'][$k] = $val;
		   		}
		   	
		   		$id =  $data['data']['grade'];
		   		
		   		unset($data['data']['grade']);
		   		unset($data['data']['from_month']);
		   		unset($data['data']['from_year']);
		   		unset($data['data']['to_month']);
		   		unset($data['data']['to_year']);
		   	$CoachesGrades->updateCoachesGrades($data,$id);
        }

        
	 	//if(isset($data['coach']))
	   	if(isset($data['page_type'])){
   			
		   	if($data['page_type'][0] == 0){
		   		foreach ( $data as $key => $value) {
		   	    
			   		foreach ($value as $key2 => $value2) {
			   		    if($key == "current_coach"){
			   		        if( $value2 == "on" ){
			   		            $value2 = 1;    
			   		        }elseif( $value2 == "off" ){
			   		            $value2 = 0;    
			   		        }
			   		        
			   		    }
			   			$add_coaches['data'][$key2][$key] = $value2;
			   		}
			   	}
			   
			   foreach($add_coaches['data'] as $key3 => $new_val){
			       	foreach ($new_val as $key4 => $new_val2) {
			       	    if(!isset($add_coaches['data'][$key3]['current_coach'])){
			       	        $add_coaches['data'][$key3]['current_coach'] = 0;
			       	    }
			   		   $add_coaches['data'][$key3]['coach_status'] = $add_coaches['data'][$key3]['from_month'] ." ".$add_coaches['data'][$key3]['from_year'] ." to ".$add_coaches['data'][$key3]['to_month']." ". $add_coaches['data'][$key3]['to_year'];
			   		   
			   		 
			   		}
			   		 unset($add_coaches['data'][$key3]['from_month']);
			   		  unset($add_coaches['data'][$key3]['from_year']);
			   		  unset($add_coaches['data'][$key3]['to_month']);
			   		  unset($add_coaches['data'][$key3]['to_year']);
			    }
				$CoachesGrades->saveCoachesGrades($add_coaches);

		   	}else{
		   	
		   		foreach ( $data as $key1 => $value) {
		   	    
			   		foreach ($value as $key2 => $value2) {
			   		    
			   			$add_coaches['data'][$key2][$key1] = $value2;
			   		}
			   	}
		   		foreach($add_coaches['data'] as $outerkey => $new_val){
		   			
			       	foreach ($new_val as $innerkey => $new_val2) {
			       	    
			   		   $add_coaches['data'][$outerkey]['school_year'] =  $add_coaches['data'][$outerkey]['from_month'] ." ". $add_coaches['data'][$outerkey]['from_year'] ." to ". $add_coaches['data'][$outerkey]['to_month']." ".  $add_coaches['data'][$outerkey]['to_year'];
			   		}

					unset($add_coaches['data'][$outerkey]['from_month']);
					unset($add_coaches['data'][$outerkey]['from_year']);
					unset($add_coaches['data'][$outerkey]['to_month']);
					unset($add_coaches['data'][$outerkey]['to_year']);
			    }
			    $CoachesGrades->saveCoachesGrades($add_coaches);
		   	}
		   	
	   	}

	   	$input = $request->all();
		
		$configuration = new Configuration();
		
		if(isset($input['settings'])){
			foreach($input['settings'] as $option_name => $option_value){
				$conDetails = Configuration::where('option_name', $option_name)->first();
				
				$option_value = (trim($option_value) == "")?"":trim($option_value);

				$data = array("option_name" => $option_name, "option_value" => $option_value);
				$data['type'] = '1';
				if(isset($conDetails->id)){
					$data['id'] = $conDetails->id;
					$configuration->updateRecord($data);
				} else {
					$idgenerated = $configuration->insertRecord($data);
					$data['id'] = $idgenerated;
				}
			}
		}

	   	return redirect('/admin/coaches-grades')->with('success', 'Coaches/Grades has been added!!');
	}

	public function delete($id){
	 	$CoachesGrades = CoachesGrades::find($id);
        $CoachesGrades->delete();
        return redirect('/admin/coaches-grades')->with('success', 'Coach/Grade has been deleted!!');
    }
    
    public function edit($id){
        $page = CoachesGrades::where('id', $id)->first();
        return view('admin.coaches-grades.edit-coaches', compact('page', 'id'));
    }

    public function gradesEdit($id){
    	$page = CoachesGrades::where('id', $id)->first();
        return view('admin.coaches-grades.edit-grades', compact('page', 'id'));
    }

    public function coacheslist(){
	 	$coaches = CoachesGrades::where('page_type', '0')->orderBy('current_coach', 'DESC')->get();
	 	$coach_title = Configuration::where('option_name', 'coaches')->pluck("option_value");
        return view('site.coaches',compact('coaches', 'coach_title'));
    }

    public function gradeslist(){
	 	$grades = CoachesGrades::where('page_type','1')->orderBy('school_year', 'DESC')->get();
	 	$grades_textarea = Configuration::where('option_name', 'grades_textarea')->pluck("option_value");
        return view('site.grades',compact('grades', 'grades_textarea'));
    }

    public function majorachievementslist(){
	 	$alleventswithresults = News::where([['results','!=', ""]])->orderBy('event_date','desc')->paginate(9);
        return view('site.majorachievments',compact('alleventswithresults'));
    }


}

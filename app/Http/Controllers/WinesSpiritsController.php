<?php

namespace App\Http\Controllers;

use App\WinesSpirits;
use App\Products;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use File;

class WinesSpiritsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function winesandspirits()
    {
        $data = WinesSpirits::where('type' ,'winery')->with('products')->orderBy('wine_spirit_order', 'asc')->get();
        return view('admin.winesandspirits.index',compact('data'));
    }

    public function spirits()
    {
        $data = WinesSpirits::where('type' ,'distillery')->with('products')->orderBy('wine_spirit_order', 'asc')->get();
        return view('admin.winesandspirits.spiritsindex',compact('data'));
    }

    public function create()
    {
        $type = request()->type;
        $countries = DB::table('country')->get();
        return view('admin.winesandspirits.create',compact('countries','type'));
    }

     public function Products($id)
    {
        $products = Products::where("cat_id", $id)->count();
        return view('admin.winesandspirits.index', compact('products', 'id'));
    }

    public function store(Request $request)
    {
        $type = request()->type;
    	$WinesSpirits = new WinesSpirits();
        $data = $this->validate($request, [
            'description'=>'required',
            'cat_title'=> 'required|unique:wines_spirits',
            'region'=> 'required',
            'country_id'=> 'required',
            'type'=> '',
            'logo'=>'required',
            'pdf_file' =>'mimes:pdf|max:20000',
            'background_image'=>'mimes:jpg,jpeg,png|max:1024'
        ]);
        if( $request->hasFile('logo')) {
            $image = $request->file('logo');
            $path = public_path(). '/uploads/';
            $filename = time() . '.' . $image->getClientOriginalName();
            $filename_removespace = str_replace(' ', '_', $filename);
            $file = $image->move($path, $filename_removespace);
            $WinesSpirits->logo = $filename_removespace;
        }
        if( $request->hasFile('pdf_file')) {
            $image = $request->file('pdf_file');
            $path = public_path(). '/uploads/';
            $filename = time() . '.' . $image->getClientOriginalName();
            $filename_removespace = str_replace(' ', '_', $filename);
            $file = $image->move($path, $filename_removespace);
            $WinesSpirits->pdf_file = $filename_removespace;
        }
        if( $request->hasFile('background_image')) {
            $image = $request->file('background_image');
            $path = public_path(). '/uploads/background_img/';
            $filename = time() . '.' . $image->getClientOriginalName();
            $filename_removespace = str_replace(' ', '_', $filename);
            $file = $image->move($path, $filename_removespace);
            $WinesSpirits->background_image = $filename_removespace;
        }

        $lastinsetid = $WinesSpirits->savePage($data);
        if($type == 'winery'){
            return redirect('/admin/winesandspirits')->with('success', 'New Winery has been created!');
        }else{
            return redirect('/admin/spirits')->with('success', 'New Distellery has been created!');
        }
    }

	public function update(Request $request, $id)
    {
        $type = request()->type;
        $WinesSpirits = new WinesSpirits();
        $data = $this->validate($request, [
                'description'=>'required',
                'cat_title'=> 'required|unique:wines_spirits,cat_title,'.$id,
                'region'=> '',
                'country_id'=> 'required',
                'logo' =>'',
                'pdf_file' =>'mimes:pdf|max:20000',
                'background_image'=>'mimes:jpg,jpeg,png|max:1024'
        ]);
        if( $request->hasFile('logo') && $id > 0) {
            $image = $request->file('logo');
            $path = public_path(). '/uploads/';
            $filename =time() .'.' .$image->getClientOriginalName();
            $filename_removespace = str_replace(' ', '_', $filename);
            $file = $image->move($path, $filename_removespace);
            $data['logo'] = $filename_removespace;
        }
        if( $request->hasFile('pdf_file')) {
            $image = $request->file('pdf_file');
            $path = public_path(). '/uploads/';
            $filename = time() . '.' . $image->getClientOriginalName();
            $filename_removespace = str_replace(' ', '_', $filename);
            $file = $image->move($path, $filename_removespace);
            $data['pdf_file'] = $filename_removespace;
        }
        if( $request->hasFile('background_image')) {
            $image = $request->file('background_image');
            $path = public_path(). '/uploads/background_img/';
            $filename = time() . '.' . $image->getClientOriginalName();
            $filename_removespace = str_replace(' ', '_', $filename);
            $file = $image->move($path, $filename_removespace);
            $data['background_image'] = $filename_removespace;
        }
        $data['id'] = $id;
        $WinesSpirits->updatePage($data);
        if($type == 'winery'){
            return redirect('/admin/winesandspirits')->with('success', 'Winery has been Updated!');
        }else{
            return redirect('/admin/spirits')->with('success', 'Distellery has been Updated!');
        }
	}

    public function edit($id)
    {
        $type = request()->type;
        $edit = WinesSpirits::where('id', $id)->first();
        $countries = DB::table('country')->get();
        return view('admin.winesandspirits.edit', compact('edit', 'id' ,'countries','type'));

    }

    public function delete($id)
    {
        $type = request()->type;
        $delete = WinesSpirits::with('products')->find($id);
        $delete->delete();
        if($type == 'winery'){
            return redirect('/admin/winesandspirits')->with('success', 'Winery has been deleted!!');
        }else{
            return redirect('/admin/spirits')->with('success', 'Distellery has been deleted!!');
        }
    }

    public function single($id){
        $wines = WinesSpirits::find($id);
        return view('site.wines.singlewine', compact('wines'));
    }

    public function setWineriesOrder(Request $request)
    {
        $orders = $request->input('order');
        if (!empty($orders)) {
            foreach ($orders as $k => $ord) {
                $wineries = WinesSpirits::find($ord);
                $wineries->update(['wine_spirit_order' => $k + 1]);
            }
        }
    }

    public function setDistelleryOrder(Request $request)
    {
        $orders = $request->input('order');
        if (!empty($orders)) {
            foreach ($orders as $k => $ord) {
                $wineries = WinesSpirits::find($ord);
                $wineries->update(['wine_spirit_order' => $k + 1]);
            }
        }
    }
}

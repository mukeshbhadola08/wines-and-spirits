<?php

namespace App\Http\Controllers;

use App\History;
use App\Page;
use File;
use Response;
use Storage;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class HistoryController extends Controller
{
	public function __construct()
    {
      $this->middleware('auth');
    }
    /**
     * Display a listing of the pages.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {		
	 	$data = History::get();
        return view('admin.history.index',compact('data'));
    }
    
	public function create(){
		return view('admin.history.create');
	} 

	public function store(Request $request)
	{
        $history = new History();
        $data = $this->validate($request, [
            'title'=> 'required',
            'image' => 'required',
			'description' =>''
        ]);
        if( $request->hasFile('image')) {
            $image = $request->file('image');
            $path = public_path(). '/uploads/';
            $filename = time() . '.' . $image->getClientOriginalName();
            $file = $image->move($path, $filename);
            $history->image = $filename;
        }
        $lastinsetid = $history->saveSlider($data);
        return redirect('/admin/history')->with('success', 'History has been created');
    }

	public function edit($id)
    {
        $slide = History::where('id', $id)->first();
        return view('admin.history.edit', compact('slide', 'id'));
    }
    public function update(Request $request, $id)
    {
        $history = new History();
        $data = $this->validate($request, [
                'description'=>'required',
                'title'=> 'required',
                'image' =>'required',
        ]);
        if( $request->hasFile('image') && $id > 0) {
            $image = $request->file('image');
            $path = public_path(). '/uploads/';
            $filename =time() .'.' .$image->getClientOriginalName();
            $file = $image->move($path, $filename);
            $data['image'] = $filename;
        }

        $data['id'] = $id;
        $history->updateSlider($data);
        return redirect('/admin/history')->with('success', 'History has been Updated!');
	}

	public function delete($id)
    {
        $delete = History::find($id);
        $delete->delete();
        return redirect('/admin/history')->with('success', 'History has been deleted!!');
    }
}


<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;

use File;
use Response;
use Auth;
use App\User;

use App\UserProfile;
use App\Services;
use App\AgentService;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except'=>['getImage','showLogin']]);
    }
	public function mytransactions()
    {
        $this->data['title'] = 'My Transactions'; // set the page title

		$mytransactions = User::mytransactions_site();
		//return view('admin.jobs', compact('mytransactions'));		
        return view('site.mytransactions', compact('mytransactions'));
    }

    public function myprofile(){
		$id = auth()->user()->id;
        $user_profile = UserProfile::where('user_id', auth()->user()->id)->get();
        //dd($user_profile);
		// set defaullt values
		$image_path = 'no-img.png'; 
		$mobile_no = '';
		$phone_no = '';
		$address  = '';

        if($user_profile->count() > 0){
            foreach($user_profile as $profile){
                $my_id = $profile->user_id;
                //echo $profile;die;
				
                if($profile->key == 'profile_pic'){ // for user's profile pic
                    if($profile->value !=''){
                        $image_path = $profile->value;
                    } else {
                        $image_path = 'no-img.png';
                    }
                }else if($profile->key == 'mobile_no' || $profile->key == 'phone_no' || $profile->key == 'address'){
                        if($profile->key == 'mobile_no'){
                            $mobile_no = $profile->value;
                        }else if($profile->key == 'phone_no'){
                            $phone_no = $profile->value;
                        }else if($profile->key == 'address'){
                            $address = $profile->value;
							// echo $address; 
                        } 
                }

            }   
        }else{
            $my_id = 0;
        }
        //dd($image_path);
        $user = User::where('id', auth()->user()->id)->get();
        foreach($user as $myuser){
            $user_name = $myuser->name;
            $user_email = $myuser->email;
            $user_password = $myuser->password;
            $user_type = $myuser->user_type;
            //Agent services related code STARTS//
            /* $services = array();
            $agent_service_ids = array();
            if($user_type == 'A'){// Adding for agents with services
                $service = new Services();
                $services = $service->getAllServices();//all visible services

                $agent_service_ids = array();
                $agent_services = AgentService::where('agent_id', auth()->user()->id)->get();//all agent's services
                //dd($agent_services);
                foreach($agent_services as $agent_service){
                    $agent_service_ids[] = $agent_service->service_id;

                }

                //dd($services);
            } */
            //----------Agent services related code ENDS-------//

        }

        if($my_id != 0){
            return view('site.myprofile',compact('my_id','image_path','user_name','user_email','user_password','user_type','mobile_no','phone_no','address','services','agent_service_ids','id'));
        }else{
            //echo "User profile data doesn't exist";
            //return redirect('/home')->with('error', 'Sorry!User Profile does not exist');
            return view('site.myprofile',compact('my_id','user_name','user_email','id'));
        }
    }

    public function edit_profile(Request $request){
        
        $request->validate([
        'user_name' => 'required',
        'user_email' => 'required',
        'user_password' => 'required',
        'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'mobile_no' => 'min:10',
        //'phone_no' => 'min:10|regex:/^\d{3}-\d{3}-\d{4}$/',
		'phone_no' => 'nullable | min:10',
        ]);
        //Update user's name,email,password code STARTS//
        $user = Auth::user();
        $user->name = $request->user_name ;
        //$user->email = $request->user_email ;//Not allowing to change email for now
        $user->password = $request->user_password ;
        //dd($user);
        $user->save();
        //---Update user's name,email,password code ENDS---//

		$my_id = auth()->user()->id;


		$user_key_fields = array('profile_pic','mobile_no','phone_no','address');

		$avatarName ='';
        // Storing Profile Pic on Filesystem related code STARTS//
        if($request->avatar != null){
            $avatarName = $my_id.'_avatar'.time().'.'.request()->avatar->getClientOriginalExtension();
            $request->avatar->storeAs('avatars',$avatarName);   
        }
        //--- Storing Profile Pic on Filesystem related code ENDS---//
        
        foreach($user_key_fields as $user_field){
          if($user_field == 'profile_pic'){
                if($request->avatar != null){
                    $userprofile = new UserProfile();
                    $userprofile->updateProfile([
                        'userId' => $my_id,
                        'key'=>'profile_pic',
                        'value'=>$avatarName,
                    ]);
                }
                
          }else{
                $userprofile = new UserProfile();
				$value = (isset($request->$user_field) &&  $request->$user_field!='')?$request->$user_field:'';
                $userprofile->updateProfile([
                    'userId' => $my_id,
                    'key'=> $user_field,
                    'value'=> $value,
                ]);

          }  
        }
        //----Update user's profile pic,phone no.,mobile no.,address i.e users metadata ENDS----//

        //Update agent's services code STARTS//
        /* $user_type = $user->user_type;
        if($user_type == 'A'){// i.e. the case of agent's profile
            $agent_id=$user->id;
            //get already existing agent-service records
            $get_agent_services = AgentService::where('agent_id',$agent_id)->get();
            //dd(count($get_agent_services));
            if(count($get_agent_services) > 0){
                //delete already existing agent-service records
                AgentService::where('agent_id','=',$agent_id)->delete();

            }
            //insert agent services if any//
            if($request->agent_services != null){//the services that are checked by the agent 
                $offered_services = explode(',',$request->agent_services);
                // Save each service id for the agent //
                foreach($offered_services as $service_id){
                    $agent_service = new AgentService();
                    $service = $agent_service->saveAgentService([
                        'userId' => $agent_id,
                        'service_id' => $service_id,
                    ]);
                }

            } 
            //-----insert agent services if any---------//
        } */

        //---Update agent's services code ENDS----//
        //return back()
		return redirect('/profile')->with('success','You have successfully updated your profile.');
    }

    public function edit_password(Request $request){
        //dd($request);
        //$validator = $request->validate([
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6|same:password',

        ]);
        //$user = Auth::user();
        //$user->name = request('user_name');
        //$user->email = request('email');
        //$user->password = bcrypt(request('new_password'));
        //var_dump($validator);
        if ($validator->passes()) {
            $new_password = Hash::make($request->password);
			User::where('id', auth()->user()->id)->update(['password' => $new_password]);
            return Response::json(['success' => '1','new_password' => $new_password]);
        }

        return Response::json(['errors' => $validator->errors()]);
        //return Response::json(['errors' => $validator->errors()->add($additional_errors)]);

    }

	public function get_all_users(){
        $users = User::all();
        
        return view('admin.users.user',compact('users'));

    }


/* admin panel-Agent/Customer Listing related functions */
    public function get_users_listing(Request $request, $userType){
        //$users = User::all();
        //dd($users);
        if($userType=='agent'){
            $user_type = 'A';
        }else if($userType=='customer'){
            $user_type = 'C';

        }
        $users = User::all()->where('user_type',$user_type);
        //dd($users);
        return view('admin.users.userlisting',compact('users','user_type'));

    }

    public function block_unblock_user(Request $request,$userId){
        $user = User::find($userId);
        //Toggle user status
        if($user->block_status==1){
            $new_status = 0;
        }else{
            $new_status = 1;
        }
        $user->block_status = $new_status;
        //dd($user);
        $user->save();
        if($new_status == 0)
            $return_msg = "User has been unblocked!!";
        else
            $return_msg = "User has been blocked!!";
        //echo $return_msg ; die;
        return redirect()->back()->with('success', $return_msg);


    }
	
	public function getImage($user_id){
		$user_profile = UserProfile::where(array('user_id'=>$user_id, "key" => "profile_pic"))->first();
		if(isset($user_profile) && ($user_profile->value !='')){
			$path = storage_path('app/public/avatars/') . $user_profile->value;
		} else {
			$path = storage_path('app/public/avatars/') . 'no-img.png';
		}

        if(!File::exists($path)){ 
            $path = storage_path('app/public/avatars/') . 'no-img.png';
		}

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
	}
	public function showLogin($redirect){
		session(['paypal_request_id' => $redirect]);
		return redirect('/admin/login');
	}

    
}

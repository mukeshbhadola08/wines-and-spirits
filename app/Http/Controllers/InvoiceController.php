<?php
namespace App\Http\Controllers;
use App\Invoice;
use App\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{
	
	public function __construct()
    {
        $this->middleware('auth', ['except'=>['viewinvoice']]);
    }

	public function list(){
		$userId = auth()->user()->id;
		$user_type = auth()->user()->user_type;
		$invoices = Invoice::where('user_id', auth()->user()->id)->get();
		return view('site.invoices.list', compact('invoices', 'user_type'));
	}

     public function viewinvoice($invoiceid){
        $invoice = Invoice::find($invoiceid);
		
		if(isset($invoice->id) && $invoice->id >0){
			$servicerequestdetails	= $invoice->ServiceRequest()->first();

			$servicedetails			= $servicerequestdetails->service()->first();

			$userdetails			= $servicerequestdetails->user()->first();

			$servicedetails = Services::find($invoice->service_id);
			return view('site.invoices.view', compact('invoice', 'invoiceid', 'servicerequestdetails', 'servicedetails', 'userdetails'));
		} else {
			return redirect('/')->with('success', 'This url doesnot exists.');
		}
    }
	public function allTransaction(){
		$invoices = Invoice::all();
		return view('admin.invoices.transactions', compact('invoices'));
	}
	public function salesChart(){
		$currentYear = date('Y');
		$data = array();
		for($i=1; $i<=12;$i++){
			if($i<10){
				$i = '0'.$i;
			}
			$sales = Invoice::selectRaw('sum(price) as total_sale')->where('created_at','like',$currentYear.'-'.$i.'%')->first();
			$data[] = (isset($sales['total_sale']) && $sales['total_sale'] !='')?$sales['total_sale']:0;
		}
		$chart1 = \Chart::title([
			'text' => 'Monthly sales chart',
		])
		->chart([
			'type'     => 'column', // pie , columnt ect
			'renderTo' => 'chart1', // render the chart into your div with id
		])
		->subtitle([
			'text' => '',
		])
		->colors([
			'#0c2959'
		])
		->xaxis([
			'categories' => ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
			'labels'     => [
				'rotation'  => 15,
				'align'     => 'top',
				//'formatter' => 'startJs:function(){return this.value + " (Footbal Player)"}:endJs', 
				// use 'startJs:yourjavasscripthere:endJs'
			],
		])
		->yaxis([
			'text' => 'This Y Axis',
		])
		->legend([
			'layout'        => 'vertikal',
			'align'         => 'right',
			'verticalAlign' => 'middle',
		])
		->series(
			[
				[
					'name'  => 'sale',
					'data'  => $data,
				],
			]
		)
		->display();

		return view('admin.invoices.saleschart', [
			'chart1' => $chart1,
		]);
	}
	public function salesChartByYear(Request $request){
		$year = $request->input('year');
		$data = array();
		for($i=1; $i<=12;$i++){
			if($i<10){
				$i = '0'.$i;
			}
			$sales = Invoice::selectRaw('sum(price) as total_sale')->where('created_at','like',$year.'-'.$i.'%')->first();
			$data[] = (isset($sales['total_sale']) && $sales['total_sale'] !='')?$sales['total_sale']:0;
		}
		$chart1 = \Chart::title([
			'text' => 'Monthly sales chart',
		])
		->chart([
			'type'     => 'column', // pie , columnt ect
			'renderTo' => 'chart1', // render the chart into your div with id
		])
		->subtitle([
			'text' => '',
		])
		->colors([
			'#0c2959'
		])
		->xaxis([
			'categories' => ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
			'labels'     => [
				'rotation'  => 15,
				'align'     => 'top',
				//'formatter' => 'startJs:function(){return this.value + " (Footbal Player)"}:endJs', 
				// use 'startJs:yourjavasscripthere:endJs'
			],
		])
		->yaxis([
			'text' => 'This Y Axis',
		])
		->legend([
			'layout'        => 'vertikal',
			'align'         => 'right',
			'verticalAlign' => 'middle',
		])
		->series(
			[
				[
					'name'  => 'sale',
					'data'  => $data,
				],
			]
		)
		->display();

		return view('admin.invoices.saleschart', [
			'chart1' => $chart1,'year'=>$year
		]);
	}

}

<?php

namespace App\Http\Controllers;
use App\Products;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SliderController;
use App\Slider;
use Validator;
use Response;
use File;
use App\WinesSpirits;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Products(WinesSpirits $winesandspirits) 
    {
        $data['winesandspirits'] = $winesandspirits;
        $data['products'] = Products::where("cat_id", $winesandspirits->id)->orderBy('product_order', 'asc')->get();
        return view('admin.products.index', compact('data'));
    }

    public function showProduct($id) 
    {
        $edit = Products::where('id', $id)->first();
        $cat_id = $edit['cat_id'];
        return view('admin.products.list', compact('edit', 'id', 'cat_id'));
    }

    public function addProduct($cat_id, $type) 
    {
        $countries = DB::table('country')->get();
        return view('admin.products.create', compact('cat_id', 'countries' ,'type'));
    }
   
    public function store(Request $request, $id, $type) 
    {
     $Products = new Products();
     $data = $this->validate($request, [
        'description'=>'required',
        'product_title'=> 'required|unique:products',
        'image'=> 'required',
        'cat_id'=> '',
        'region'=> '',
        'country_id'=> 'required',
        'varietal'=> '',
        'product_background_img'=>''
    ]);

    $cat_id = $data['cat_id'];
    if( $request->hasFile('image')) {
        $image = $request->file('image');
        $path = public_path(). '/uploads/';
        $filename = time() . '.' . $image->getClientOriginalName();
        $filename_removespace = str_replace(' ', '_', $filename);
        $file = $image->move($path, $filename_removespace);
        $Products->image = $filename_removespace;
    }
    if( $request->hasFile('product_background_img')) {
        $image = $request->file('product_background_img');
        $path = public_path(). '/uploads/background_img/';
        $filename = time() . '.' . $image->getClientOriginalName();
        $filename_removespace = str_replace(' ', '_', $filename);
        $file = $image->move($path, $filename_removespace);
        $Products->product_background_img = $filename_removespace;
    }
    $lastinsetid = $Products->savePage($data ,$type);
    return redirect("/admin/products/".$cat_id)->with('success', 'New product has been added!');
    }
    
    public function editProduct($id, $type)
    {
        $edit = Products::where('id', $id)->first();
        $cat_id = $edit['cat_id'];
        $countries = DB::table('country')->get();
        return view('admin.products.edit', compact('edit', 'id', 'cat_id', 'countries','type'));
    }
    public function update(Request $request, $id, $type)
    {
        $Products = new Products();
        $data = $this->validate($request, [
                'description'=>'required',
                'product_title'=> 'required|unique:products,product_title,'.$request->id,
                'image'=> '',
                'region'=> '',
                'country_id'=> '',
                'varietal'=> '',
                'cat_id'=>'',
                'id'=>'',
                'product_background_img'=>''
        ]);
        $cat_id = $data['cat_id'];
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $path = public_path(). '/uploads/';
            $filename =time() .'.' .$image->getClientOriginalName();
            $filename_removespace = str_replace(' ', '_', $filename);
            $file = $image->move($path, $filename_removespace);
            $data['image'] = $filename_removespace;
        }
        if( $request->hasFile('product_background_img')) {
        $image = $request->file('product_background_img');
        $path = public_path(). '/uploads/background_img/';
        $filename = time() . '.' . $image->getClientOriginalName();
        $filename_removespace = str_replace(' ', '_', $filename);
        $file = $image->move($path, $filename_removespace);
        $data['product_background_img'] = $filename_removespace;
    }
        $Products->updatePage($data, $type);
        return redirect('/admin/products/'.$cat_id)->with('success', 'Product has been Updated!');
    }
    public function delete($id)
    {
        $delete = Products::find($id);
        $cat_id = $delete['cat_id'];
        $delete->delete();
        return redirect('/admin/products/'.$cat_id)->with('success', 'Product has been deleted!!');
    }

    public function setProductOrder(Request $request)
    {
        $orders = $request->input('order');
        if (!empty($orders)) {
            foreach ($orders as $k => $ord) {
                $products = Products::find($ord);
                $products->update(['product_order' => $k + 1]);
            }
        }
    }


}
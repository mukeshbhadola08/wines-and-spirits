<?php

namespace App\Http\Controllers;

use App\Page;
use App\Slider;
use App\Navigation;
use App\Http\Controllers\SliderController;
use File;
use Response;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
	
	public function __construct()
    {
        $this->middleware('auth', ['except' => ['view', 'getImage']]);
    }
	/**
     * Display a listing of the pages.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::where('lang_code','en')->get();
        return view('admin.pages.index',compact('pages'));
    }

	public function create(){
		return view('admin.pages.create');
	}
	

	private function toAscii($vp_string) {
		$vp_string = trim($vp_string);
    
		$vp_string = html_entity_decode($vp_string);
		
		$vp_string = strip_tags($vp_string);
		
		$vp_string = strtolower($vp_string);
		
		$vp_string = preg_replace('~[^ a-z0-9_.]~', ' ', $vp_string);
		
		$vp_string = preg_replace('~ ~', '-', $vp_string);
		
		$vp_string = preg_replace('~-+~', '-', $vp_string);
			
		return $vp_string;
	}

    //
	public function store(Request $request)
	{
        $page = new Page();
        $data = $this->validate($request, [
            'page_description'=>'required',
            'page_title'=> 'required',
			'permalink'=> 'required',
			'page_keywords'=> '',
			'lang_code'=>'',
			'parent'=>'',
			'is_visible'=> '',
			'image' => '',
			'background_color'=>'',
			'background_image'=>'mimes:jpg,jpeg,png'
        ]);
        
        if(isset($data['image']) && is_numeric($data['image'])){
        	$image = Slider::where('id',$data['image'])->pluck('id')->all();
        	$data['image'] = null;	
        	$data['slider_id'] = $image[0];
        }else{
        	if(!isset($data['image'])){
        		$data['image'] = null;
        		$data['slider_id'] = null;
        	}else{
        		$original_name = $request->image->getClientOriginalName();
	       	
				$ext =  pathinfo($original_name, PATHINFO_EXTENSION);

				$image_name = 'default_'.uniqid().'.'.$ext;

				$path = $request->image->storeAs('default_images', $image_name);
					$SliderControllerobj = new SliderController();

				 $ext = pathinfo(base_path() . '/storage/app/public/'. $path, PATHINFO_EXTENSION);
                $SliderControllerobj->correctImageOrientation(550, base_path() . '/storage/app/public/'. $path, $ext, base_path() . '/storage/app/public/default_images');
				$data['slider_id'] = null;
				$data['image'] = $path;	
        	}
		}
		
		if(!empty($request->background_image)) {
			$background_image = $request->background_image;
			$path = public_path(). '/uploads/page_background_image';
			$filename = time() . '.' . $background_image->getClientOriginalName();
			$file = $background_image->move($path, $filename);
			$data['background_image'] = $filename;	
		}
         
		$cleanurl = $this->toAscii($data['permalink']);

		$cleanurl = $this->getCleanUrl($cleanurl);

		$data['clean_url'] = $cleanurl;

		if(!isset($data['is_visible'])){
			$data['is_visible'] = 0;
		}
       
        $lastinsetid = $page->savePage($data);
        return redirect('/admin/pages')->with('success', 'New page has been created');
    }

	public function edit($id)
    {
        $page = Page::where('id', $id)->first();
        return view('admin.pages.edit', compact('page', 'id'));
    }

	public function update(Request $request, $id)
    {
        $page = new Page();
        $data = $this->validate($request, [
            'page_description'=>'required',
            'page_title'=> 'required',
			'page_keywords'=> '',
			'is_visible'=> '',
			'image' => '',
			'background_color'=>'',
			'background_image'=>'mimes:jpg,jpeg,png'
        ]);

		if(!isset($request->image)){
			$path = $page->image;
		} else {

			if(isset($data['image']) && is_numeric($data['image'])){
	        	$image = Slider::where('id',$data['image'])->pluck('id')->all();
	        	$data['image'] = null;	
	        	$data['slider_id'] = $image[0];
	        }else{
	        	if(!isset($data['image'])){
	        		$data['image'] = null;
	        		$data['slider_id'] = null;
	        	}else{
	        		$original_name = $request->image->getClientOriginalName();
		       	
					$ext =  pathinfo($original_name, PATHINFO_EXTENSION);

					$image_name = 'default_'.uniqid().'.'.$ext;

					$path = $request->image->storeAs('default_images', $image_name);
					$SliderControllerobj = new SliderController();
					 $ext = pathinfo(base_path() . '/storage/app/public/'. $path, PATHINFO_EXTENSION);
                $SliderControllerobj->correctImageOrientation(550, base_path() . '/storage/app/public/'. $path, $ext, base_path() . '/storage/app/public/default_images');
					$data['slider_id'] = null;
					$data['image'] = $path;	
					//dd($data['image']);
	        	}

	        }

		}

		if(!empty($request->background_image)) {
			$background_image = $request->background_image;
			$path = public_path(). '/uploads/page_background_image';
			$filename = time() . '.' . $background_image->getClientOriginalName();
			$file = $background_image->move($path, $filename);
			$data['background_image'] = $filename;	
		}
		
		if(!isset($data['is_visible'])){
			$data['is_visible'] = 0;
		}

        $data['id'] = $id;

        $page->updatePage($data);

        return redirect('/admin/pages')->with('success', 'Page has been updated!!');
    }

	public function delete($id){
		
		$childPages = Page::where('parent_lang_id',$id)->get();		//other langauage references

		//deleting from navigation
		Navigation::where([['type','=',1],['reference_type_id','=',$id]])->delete();  // english
		//deleting other language data
		if(count($childPages)>0){
			foreach($childPages as $cdpg){
				$childOP = Navigation::where([['type','=',1],['reference_type_id','=',$cdpg['id']]])->delete();
			}
		}
		$childPage = Page::where('parent_lang_id',$id)->delete();

		//delete main page
        $page = Page::find($id);
        $page->delete();
        return redirect('/admin/pages')->with('success', 'Page has been deleted!!');
    }

	public function getCleanUrl($cleanurl, $i = 0){
		global $db, $connMCP;
		if($i>0){
			$cleanurl = $cleanurl."".$i;
		}

		$page = Page::where('clean_url', $cleanurl)->first();

		if(isset($page->id)){
			$i = $i + 1;
			$cleanurl = $this->getCleanUrl($cleanurl, $i);
		}

		return $cleanurl;
	}

	 /**
     * Display the specified resource.
     *
     * @param  string  $clearnurl
     * @return \Illuminate\Http\Response
     */
    public function view($cleanurl){
		global $lang_val;
        $page = Page::where('clean_url', $cleanurl)
                        ->first();
		
		if(!isset($page->id)){
			 abort(404);
		}
	
        return view('site.pages.view', compact('page', 'cleanurl'));
    }

	 /**
     * Contact us form post
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function contact(Request $request, $cleanurl){
        
        $data = $this->validate($request, [
            'contact_name'=>'required',
            'contact_email'=> 'required|email',
			'contact_phone'=> 'required|min:10',
			'contact_message'=> 'required|min:10'
        ]);

		/* echo "<pre>";
		print_r($data);
		die; */

        return redirect('/admin/pages')->with('success', 'Page has been updated!!');

    }
	public function addInSpanish($id)
    {
        $page = Page::where('id', $id)
                        ->first();
        return view('admin.pages.add_spanish', compact('page', 'id'));
    }
	public function storeInlang(Request $request, $id)
	{
        $page = new Page();
        $data = $this->validate($request, [
            'page_description'=>'required',
            'page_title'=> 'required',
			'permalink'=> 'required',
			'page_keywords'=> '',
			'lang_code'=>'',
			'parent'=>'',
			'is_visible'=> ''
        ]);

		$cleanurl = $this->toAscii($data['permalink']);

		$cleanurl = $this->getCleanUrl($cleanurl);

		$data['clean_url'] = $cleanurl;

		if(!isset($data['is_visible'])){
			$data['is_visible'] = 0;
		}
       
        $lastinsetid = $page->savePage($data);
        return redirect('/admin/pages')->with('success', 'page has been created in dutch.');
    }

    public static function getImage($id){
		$page = Page::where('id', $id)
                        ->first();

		if($page->default_image !=''){
			$path = storage_path('app/public/') . $page->default_image;
            if(!File::exists($path)){ 
                $path = storage_path('app/public/') . 'no-img.jpg';
            }

            $file = File::get($path);
            $type = File::mimeType($path);

            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);

            return $response;

		}elseif($page->slider_id !=''){
			$slider = Slider::where('id', $page->slider_id)->first();
			$url = url('/public/storage/slider_images/' . $slider->image);
		} else {
			$url = url('/public/storage/intro-img-top.png');
		}
		
        return $url;
	}

}

<?php

namespace App\Http\Controllers;

use App\Property;
use App\PropertyMetaData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use File;
use Response;
use Illuminate\Support\Facades\Storage;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
		$properties = Property::all();
        return view('admin.properties.index',compact('properties'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.properties.create');
    }
	
	private function toAscii($vp_string) {
		$vp_string = trim($vp_string);
    
		$vp_string = html_entity_decode($vp_string);
		
		$vp_string = strip_tags($vp_string);
		
		$vp_string = strtolower($vp_string);
		
		$vp_string = preg_replace('~[^ a-z0-9_.]~', ' ', $vp_string);
		
		$vp_string = preg_replace('~ ~', '-', $vp_string);
		
		$vp_string = preg_replace('~-+~', '-', $vp_string);
			
		return $vp_string;
	}

	public function getCleanUrl($cleanurl, $i = 0){
		global $db, $connMCP;
		if($i>0){
			$cleanurl = $cleanurl."".$i;
		}

		$page = Property::where('slug', $cleanurl)->first();

		if(isset($page->id)){
			$i = $i + 1;
			$cleanurl = $this->getCleanUrl($cleanurl, $i);
		}

		return $cleanurl;
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $property = new Property();
		
        $data = $this->validate($request, [
            'title'=>'required|max:190',
            'description'=> 'required',
			'residential_type'=> 'required',
			'sale_type'=> 'required',
			'price'=> 'required',
			'property_area'=> 'required',
			'address'=> 'required',
			'county'=> 'required|alpha',
			'state'=> 'required',
			'zipcode'=> 'required|digits_between:4,8',
			'state'=> 'required|alpha',
			'email_address' => 'email',
			'main_image' => 'required|mimes:jpeg,bmp,png,gif,jpg',
			
			'property_bedrooms'=> 'required',
			'property_bathrooms'=> 'required',
			'property_garages'=> 'required',
			'property_area_type'=> 'required',
			'property_floors'=> 'digits_between:1,8',
			'contact_no'=> 'alpha_dash',
			'street_address' => "nullable"
        ]);

		$cleanurl = $this->toAscii($data['title']);

		$cleanurl = $this->getCleanUrl($cleanurl);
		
		$original_name = $request->main_image->getClientOriginalName();

		$ext =  pathinfo($original_name, PATHINFO_EXTENSION);

		$image_name = 'property_'.uniqid().'.'.$ext;

		$path = $request->main_image->storeAs('property_images', $image_name);
		
		$data['image'] = $path;

		$data['slug'] = $cleanurl;

		if(!isset($data['is_active'])){
			$data['is_active'] = 0;
		} else {
			$data['is_active'] = 1;
		}

		$metadataarray = array('no_of_bedrooms' => $data['property_bedrooms'], 'no_of_bathrooms' => $data['property_bathrooms'], 'no_of_garages' => $data['property_garages'], 'area_type' => $data['property_area_type'], 'no_of_floors' => $data['property_floors'], 'contact_no' => $data['contact_no'], 'email_address' => $data['email_address'], 'address' => $data['address'], 'county' => $data['county'], 'state' => $data['state'], 'property_area' => $data['property_area'], 'property_price' => $data['price'], 'street_address' => $data['street_address'], 'zipcode' => $data['zipcode']);

		$id = $property->saveProperty($data);


		foreach($metadataarray as $metakey => $metvalue){
			$propertymetanew = new PropertyMetaData();
			$propertymetanew->property_id = $id;
			$propertymetanew->key = $metakey;
			$propertymetanew->value = $metvalue;
			$propertymetanew->save();
		}

        return redirect('/admin/properties')->with('success', 'New Property has been created');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function show(Property $property)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $property = Property::find($id);

		$propertymetadatas = $property->metadatas()->get();
		
		$propertymetadata = array();

		foreach($propertymetadatas as $keymeta => $propertymeta){
			$propertymetadata[$propertymeta->key] = $propertymeta->value;
		}

        return view('admin.properties.edit', compact('property', 'id', 'propertymetadata'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

		$propDetails = Property::where('id', $id)
                        ->first();

        $property = new Property();

        $data = $this->validate($request, [
            'title'=>'required|max:190',
            'description'=> 'required',
			'residential_type'=> 'required',
			'sale_type'=> 'required',
			'price'=> 'required',
			'property_area'=> 'required',
			'address'=> 'required',
			'county'=> 'required|alpha',
			'zipcode'=> 'required|digits_between:4,8',
			'state'=> 'required|alpha',
			'email_address' => 'email',
			
			'property_bedrooms'=> 'required',
			'property_bathrooms'=> 'required',
			'property_garages'=> 'required',
			'property_area_type'=> 'required',
			'property_floors'=> 'digits_between:1,8',
			'contact_no'=> 'alpha_dash',
			'street_address' => "nullable"
        ]);

		$metadataarray = array('no_of_bedrooms' => $data['property_bedrooms'], 'no_of_bathrooms' => $data['property_bathrooms'], 'no_of_garages' => $data['property_garages'], 'area_type' => $data['property_area_type'], 'no_of_floors' => $data['property_floors'], 'contact_no' => $data['contact_no'], 'email_address' => $data['email_address'], 'address' => $data['address'], 'county' => $data['county'], 'state' => $data['state'], 'property_area' => $data['property_area'], 'property_price' => $data['price'], 'street_address' => $data['street_address'], 'zipcode' => $data['zipcode']);
		
		foreach($metadataarray as $metakey => $metvalue){
			$propertymetadata = PropertyMetaData::where('property_id', $id)->where('key', $metakey)->first();

			if(isset($propertymetadata->id)){
				$propertymetadata->value = $metvalue;
				$propertymetadata->save();
			} else {
				$propertymetanew = new PropertyMetaData();
				$propertymetanew->property_id = $id;
				$propertymetanew->key = $metakey;
				$propertymetanew->value = $metvalue;
				$propertymetanew->save();
			}
		}

		if(!isset($request->main_image)){
			$path = $propDetails->image;
		} else {
			$original_name = $request->main_image->getClientOriginalName();
			$ext =  pathinfo($original_name, PATHINFO_EXTENSION);
			$image_name = 'blog_'.uniqid().'.'.$ext;
			$path = $request->main_image->storeAs('property_images', $image_name);
		}

		$data['image'] = $path;
		
		if(!isset($data['is_active'])){
			$data['is_active'] = 0;
		} else {
			$data['is_active'] = 1;
		}

		$data['id'] = $id;

        $property->updateProperty($data);

        return redirect('/admin/properties')->with('success', 'Property has been updated!!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $property = Property::find($id);
		
		if($property->image !=''){
			Storage::delete($property->image);
		}

        $property->delete();
        return redirect('/admin/properties')->with('success', 'Property has been deleted successfully!!');
    }

	public function getImage($id){
		$property = Property::where('id', $id)
                        ->first();

		if($property->image !=''){
			$path = storage_path('app/public/') . $property->image;
		} else {
			$path = storage_path('app/public/') . 'no-img.jpg';
		}

		

        if(!File::exists($path)){ 
            $path = storage_path('app/public/') . 'no-img.jpg';
		}

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
	}

	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($cleanurl){
        $property = Property::where('slug', $cleanurl)
                        ->first();
        return view('site.properties.view', compact('property', 'cleanurl'));
    }


    // Block/Unblock Property //
    public function block_unblock_property($id){
        $property = Property::find($id);
        //Toggle property status
        if($property->is_active==1){
            $new_status = 0;
        }else{
            $new_status = 1;
        }
        $property->is_active = $new_status;
        //dd($property);
        $property->save();
        if($new_status == 0)
            $return_msg = "Property has been blocked!!";
        else
            $return_msg = "Property has been unblocked!!";
        //echo $return_msg ; die;
        return redirect()->back()->with('success', $return_msg);


    }
    //---Block/Unblock Property--//

    //return view of Upload file page//
    public function uploadFile(){
        return view('admin.properties.upload_property_file');  
    }
    //----return view of Upload file page----//

    //Import properties from uploaded CSV into Database//
    public function importFile(Request $request){

        if($request->sample_file != ''){

            $original_name = $request->sample_file->getClientOriginalName();

            $ext =  pathinfo($original_name, PATHINFO_EXTENSION);

            $file_name = 'property_file'.uniqid().'.'.$ext;

            $path = $request->file('sample_file')->storeAs('property_files', $file_name);

            $csvFile = storage_path('app/public/property_files/').$file_name;
            //dd($csvFile);
            $csv_data = $this->csv_to_array($csvFile);
            //echo "printing out csv contents::::";
            //dd($csv_data);
            $data = array();
            foreach ($csv_data as $key => $value) {
                $property = new Property();

                //$property_data[] = ['title' => $value['title'], 'description' => $value['description'],'residential_type' => $value['residential_type'],'sale_type' => $value['sale_type'],'is_active' => $value['is_active'],'description' => $value['description']];

                $data['title'] = $value['title'];
                $data['description'] = $value['description'];
                $data['image'] = '';

                $cleanurl = $this->toAscii($value['title']);

                $cleanurl = $this->getCleanUrl($cleanurl);

                $data['slug'] = $cleanurl;

                $data['is_active'] = ucwords(trim($value['is_active']))=='Yes'?1:0;

                switch(ucwords(trim($value['residential_type']))){
                                case 'Apartment': 
                                    //echo "Apartment";
                                    $data['residential_type'] = 1;
                                    break;
                                case 'Builder Floor':
                                    $data['residential_type'] = 2;
                                    break;
                                case 'Villa':
                                    $data['residential_type'] = 3;
                                    break;
                                case 'Land':
                                    $data['residential_type'] = 4;
                                    break;
                                case 'Farm House':
                                    $data['residential_type'] = 5;
                                    break;
                                case 'Studio Apartment':
                                    $data['residential_type'] = 6;
                                    break;
                                case 'Serviced Apartment':
                                    $data['residential_type'] = 7;
                                    break;
                                Default:
                                    $data['residential_type'] = 0;
                                    break;
                }
                $data['sale_type'] = ucwords(trim($value['sale_type']))=='Resale'?1:2;
                //echo '<pre>';print_r($data);echo '</pre>';
                $id = $property->saveProperty($data);
                //echo "Id of saved poperty record = ".$id;
                $property_metadatas[$id] = ['price' => $value['price'],'property_bedrooms' => $value['property_bedrooms'],'property_bathrooms' => $value['property_bathrooms'],'property_garages' => $value['property_garages'],'property_area' => $value['property_area'],'property_area_type' => $value['property_area_type'],'property_floors' => $value['property_floors'],'address' => $value['address'],'street_address' => $value['street_address'],'county' => $value['county'],'state' => $value['state'],'zipcode' => $value['zipcode'],'contact_no' => $value['contact_no'],'email_address' => $value['email_address']];
 

            }
            //echo '<pre>';print_r($property_metadatas);echo '</pre>';
            foreach($property_metadatas as $property_metakey => $property_metvalue){
                $property_id = $property_metakey;
                foreach($property_metvalue as $metakey => $metvalue){
                    $propertymetanew = new PropertyMetaData();
                    $propertymetanew->property_id = $property_id;
                    $propertymetanew->key = $metakey;
                    $propertymetanew->value = $metvalue;
                    $propertymetanew->save(); 
                }

            }

            return redirect('/admin/properties')->with('success', 'Property File has been uploaded and processed successfully');

        }

        //dd('Request data does not have any files to import.');      

    } 
    //---Import properties from uploaded CSV into Database----//

    //function to parse uploaded csv file of properties and convert to array//
    public function csv_to_array($filename='', $delimiter=','){
        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;
        
        //$header = NULL;
        $header = array('title','description','residential_type','sale_type','price','property_bedrooms','property_bathrooms','property_garages','property_area','property_area_type','property_floors','address','street_address','county','state','zipcode','contact_no','email_address','is_active');
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {   
            $row = fgetcsv($handle, 1000, $delimiter);//skips first row i.e. header row
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                //$data[] = $row;
                $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }
    //---function to parse uploaded csv file of properties and convert to array----//

}

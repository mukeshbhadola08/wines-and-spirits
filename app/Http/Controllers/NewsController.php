<?php

namespace App\Http\Controllers;

use App\News;
use App\Slider;
use App\Http\Controllers\SliderController;
use File;
use Response;
use Auth;
use App\Invoice;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth', ['except' => ['view', 'getImage', 'list']]);
    }
    /**
     * Display a listing of the pages.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::where('lang_code','en')->get();
        return view('admin.news.index',compact('news'));
    }

	public function create(){
		return view('admin.news.create');
	}

	private function toAscii($vp_string) {
		$vp_string = trim($vp_string);
    
		$vp_string = html_entity_decode($vp_string);
		
		$vp_string = strip_tags($vp_string);
		
		$vp_string = strtolower($vp_string);
		
		$vp_string = preg_replace('~[^ a-z0-9_.]~', ' ', $vp_string);
		
		$vp_string = preg_replace('~ ~', '-', $vp_string);
		
		$vp_string = preg_replace('~-+~', '-', $vp_string);
			
		return $vp_string;
	}

    //
	public function store(Request $request)
	{
        $newObj = new News();	

        $data = $this->validate($request, [
            'description'=>'required',
			'short_description'=>'required|max:200',
            'title'=> 'required',
			'is_visible'=> '',
			'video_url'=> '',
			'lang_code'=>'',
			'parent'=>'',
			'image' => '',
			'event_date' => 'required',
			'event_venue' => 'required',
			'news_file' => '',
			'is_paid' => '',
			'event_amount' => 'nullable|numeric'
        ]);

		if(isset($data['image']) && is_numeric($data['image'])){
        	$image = Slider::where('id',$data['image'])->pluck('id')->all();
        	$data['image'] = null;	
        	$data['slider_id'] = $image[0];
        }else{
        	if(!isset($data['image'])){
        		$data['image'] = null;
        		$data['slider_id'] = null;
        	}else{
        		$original_name = $request->image->getClientOriginalName();
	       	
				$ext =  pathinfo($original_name, PATHINFO_EXTENSION);

				$image_name = 'default_'.uniqid().'.'.$ext;

				$path = $request->image->storeAs('default_images', $image_name);
				$SliderControllerobj = new SliderController();
				 $ext = pathinfo(base_path() . '/storage/app/public/'. $path, PATHINFO_EXTENSION);
                $SliderControllerobj->correctImageOrientation(550, base_path() . '/storage/app/public/'. $path, $ext, base_path() . '/storage/app/public/default_images');
				$data['slider_id'] = null;
				$data['image'] = $path;	
        	}
        }
		/*if($data['video_url']==''){
			return redirect()
                    ->back()->withErrors('Please select at least one file or enter video url.')->withInput();
		}*/
		
		$data['event_date'] = date('Y-m-d', strtotime($data['event_date']));

		$cleanurl = $this->toAscii($data['title']);

		$cleanurl = $this->getCleanUrl($cleanurl);

		$data['clean_url'] = $cleanurl;

		if(!isset($data['is_visible'])){
			$data['is_visible'] = 0;
		}
		if(!isset($data['is_paid'])){
			$data['is_paid'] = 0;
			$data['event_amount'] = 0;
		}
       
        $lastinsetid = $newObj->saveNews($data);
        return redirect('/admin/news')->with('success', 'Event has been created');
    }

	public function edit($id)
    {
        $news = News::where('id', $id)
                        ->first();
        return view('admin.news.edit', compact('news', 'id'));
    }

	public function update(Request $request, $id)
    {

		$newDetails = News::where('id', $id)
                        ->first();

        $newObj = new News();

        $data = $this->validate($request, [
            'description'=>'required',
			'short_description'=>'required|max:200',
            'title'=> 'required',
			'is_visible'=> '',
			'image' => '',
			'event_date' => 'required',
			'event_venue' => 'required',
			'results' => '',
			'video_url'=> '',
			'news_file' => '',
			'is_paid' => '',
			'event_amount' => 'nullable|numeric'
        ]);

		if(isset($data['image']) && is_numeric($data['image'])){
        	$image = Slider::where('id',$data['image'])->pluck('id')->all();
        	$data['image'] = null;	
        	$data['slider_id'] = $image[0];
        }else{

        	if(!isset($data['image'])){
        		$data['image'] = null;
        		$data['slider_id'] = null;
        	}else{
        		$original_name = $request->image->getClientOriginalName();
	       	
				$ext =  pathinfo($original_name, PATHINFO_EXTENSION);

				$image_name = 'default_'.uniqid().'.'.$ext;

				$path = $request->image->storeAs('default_images', $image_name);
				$SliderControllerobj = new SliderController();
				 $ext = pathinfo(base_path() . '/storage/app/public/'. $path, PATHINFO_EXTENSION);
                $SliderControllerobj->correctImageOrientation(550, base_path() . '/storage/app/public/'. $path, $ext, base_path() . '/storage/app/public/default_images');
				$data['slider_id'] = null;
				$data['image'] = $path;	
        	}
        	
        }
		/*if($data['video_url']==''){
			return redirect()
                    ->back()->withErrors('Please select at least one file or enter video url.')->withInput();
		}
*/
		$data['event_date'] = date('Y-m-d', strtotime($data['event_date']));
		
		if(!isset($data['is_visible'])){
			$data['is_visible'] = 0;
		}
		if(!isset($data['is_paid'])){
			$data['is_paid'] = 0;
			$data['event_amount'] = 0;
		}

		$data['id'] = $id;

        $newObj->updateNews($data);

        return redirect('/admin/news')->with('success', 'Event has been updated!!');
    }

	public function delete($id){

		// remove related language data
        $langData = News::where('parent_lang_id',$id)->delete();

        $page = News::find($id);

		if($page->file_name !=''){
			$path = storage_path('app/public/') . $page->file_name;

			if(File::exists($path)){ 
				Storage::delete($page->file_name);
			}
			
		}

        $page->delete();
        return redirect('/admin/news')->with('success', 'Event has been deleted!!');
    }

	public function getCleanUrl($cleanurl, $i = 0){
		if($i>0){
			$cleanurl = $cleanurl."".$i;
		}

		$page = News::where('permalink', $cleanurl)->first();

		if(isset($page->id)){
			$i = $i + 1;
			$cleanurl = $this->getCleanUrl($cleanurl, $i);
		}

		return $cleanurl;
	}

	 /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($cleanurl){
		global $lang_val;
		
        $news = News::where('permalink', $cleanurl)
                        ->first();
		if($lang_val != $news->lang_code){
			if(($news->parent_lang_id) > 0){
				$eventdetails = News::find($news->parent_lang_id);
				if(!empty($eventdetails)){
					$news = $eventdetails;
				}
			} else {
				$eventdetails = News::where('parent_lang_id',$news->id)->first();
				if(!empty($eventdetails)){
					$news = $eventdetails;
				}
			}
		}
		if($news->is_paid == 1){
			if($user = Auth::user()){
				$paymentDetails = Invoice::where([['news_id','=',$news->id],['user_id','=',Auth::user()->id],['is_paid','=',1]])->first();
				if(empty($paymentDetails) && Auth::user()->user_type !='S'){
					return redirect('/');
				}
			} else {
				return redirect('/');
			}
		}
        return view('site.news.view', compact('news', 'cleanurl'));
    }

	public function list(){
		$allNews = News::orderBy('event_date','DESC')->paginate(9);
		return view('site.news.list', compact('allNews'));
	}

	public function resultlist(){
		$allNews = News::where([['is_visible','=',1],['lang_code','=','en'],['results','!=', NULL]])->orderBy('event_date','DESC')->paginate(9);
		return view('site.news.resultlist', compact('allNews'));
	}


	public static function getImage($id){
		$new = News::where('id', $id)
                        ->first();
                        
        if($new->default_image !=''){
			$path = storage_path('app/public/') . $new->default_image;
            if(!File::exists($path)){ 
                $path = storage_path('app/public/') . 'no-img.jpg';
            }

            $file = File::get($path);
            $type = File::mimeType($path);

            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);

            return $response;

		}elseif($new->slider_id !=''){
			$slider = Slider::where('id', $new->slider_id)->first();
			$url = url('/public/storage/slider_images/' . $slider->image);
		} else {
			$url = url('/public/storage/intro-img-top.png');
		}
		
        return $url;

	}
	public function addInSpanish($id){
		$news = News::where('id', $id)
                        ->first();
        return view('admin.news.add_spanish', compact('news', 'id'));
	}
	public function storeInlang(Request $request, $id)
	{
		$newDetails = News::where('id', $id)
                        ->first();

        $newObj = new News();	

        $data = $this->validate($request, [
            'description'=>'required',
			'short_description'=>'required|max:200',
            'title'=> 'required',
			'is_visible'=> '',
			'video_url'=> '',
			'lang_code'=>'',
			'parent'=>'',
			'news_file' => '',
			'is_paid' => '',
			'event_amount' => 'nullable|numeric'
        ]);

		if(isset($request->news_file)){
		
			$original_name = $request->news_file->getClientOriginalName();

			$ext =  pathinfo($original_name, PATHINFO_EXTENSION);
			$accept_types = array('jpeg','bmp','png','gif','jpg','m4a','mp3','wav','ogg');
			
			if(!in_array(strtolower($ext),$accept_types)){
				return redirect()
                    ->back()->withErrors('file type is not correct');
			} 

			$file_name = 'news_'.uniqid().'.'.$ext;

			$path = $request->news_file->storeAs('news', $file_name);
		} else {
			$path = $newDetails->file_name;
		}

		if($path == '' && $data['video_url']==''){
			return redirect()
                    ->back()->withErrors('Please select at least one file or enter video url.');
		}

		$data['file_path'] = $path;

		$cleanurl = $this->toAscii($data['title']);

		$cleanurl = $this->getCleanUrl($cleanurl);

		$data['clean_url'] = $cleanurl;

		if(!isset($data['is_visible'])){
			$data['is_visible'] = 0;
		}
		if(!isset($data['is_paid'])){
			$data['is_paid'] = 0;
			$data['event_amount'] = 0;
		}
       
        $lastinsetid = $newObj->saveNews($data);
        return redirect('/admin/news')->with('success', 'Event has been added in dutch.');
    }
}

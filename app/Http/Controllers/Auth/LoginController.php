<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Crypt;
use Socialite;
use App\Invoice;
use App\News;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';
    // protected $redirectTo = '/profile';

	protected function authenticated(\Illuminate\Http\Request $request, $user)
	{
		if(session('paypal_request_id') !== NULL){
			$nw_id = Crypt::decrypt(session('paypal_request_id'));
			$is_paid = Invoice::where([['news_id','=',$nw_id],['user_id','=',$user->id],['is_paid','=',1]])->first();
			if(empty($is_paid)){
				return redirect('/paypal/payment/'.$nw_id);
			}else{
				$newsDetails = News::find($nw_id);

				return redirect('/news/'.$newsDetails->permalink);
			}
		} 

		if ( $user->user_type == 'S' ) {// admin user
			return redirect('/admin');
		}

		return redirect('/profile');
	}

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();

        // $user->token;
    }



/*Override the default login() function in AuntnticatesUsers trait to check unblocked users while login*/

public function login(\Illuminate\Http\Request $request) {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        /* This section is the only change */
        if ($this->guard()->validate($this->credentials($request))) {
            $user = $this->guard()->getLastAttempted();
            //echo !($user->block_status);
            //dd($user);

            // Make sure the user is not blocked
            if (!($user->block_status) && $this->attemptLogin($request)) {
                // Send the normal successful login response
                return $this->sendLoginResponse($request);
            } else {
                // Increment the failed login attempts and redirect back to the
                // login form with an error message.
                $this->incrementLoginAttempts($request);
                return redirect()
                    ->back()
                    ->withInput($request->only($this->username(), 'remember'))
                    ->withErrors(['user_blocked_message' => 'Your account has been blocked by admin.Please contact admin to activate it again!']);
            }
        }
        /* /This section is the only change */

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
/*/Override the default login() function in AuntnticatesUsers trait to check unblocked users while login*/
}

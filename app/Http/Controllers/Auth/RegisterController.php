<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserProfile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/home';
    //protected $redirectTo = '/profile';

	protected function authenticated(\Illuminate\Http\Request $request, $user)
	{
		if(session('paypal_request_id') !== NULL){
			$nw_id = Crypt::decrypt(session('paypal_request_id'));		
			return redirect('/paypal/payment/'.$nw_id);
		} 

		return redirect('/profile');
	}

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'user_type' => 'required|string|max:255',
            'block_status' => 'required|max:5',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    /*protected function create(array $data){   
        //dd($data);
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'user_type' => $data['user_type'],
            'block_status' => $data['block_status'],

        ]);
    }*/
    protected function create(array $data){   
        //dd($data);
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'user_type' => $data['user_type'],
            'block_status' => $data['block_status'],

        ]);

        $userId = $user->id;
        $user_info_keys = array('profile_pic','mobile_no','phone_no','address');
        foreach($user_info_keys as $user_key){
            //echo $user_key."<br>" ;
            $userprofile = new UserProfile();
            $profile = $userprofile->saveProfile([
                'userId' => $userId,
                'key' => $user_key,
                'value' => '',
            ]);
            //dd($profile);
            //echo "<pre>";print_r($profile);echo "</pre>";
        }
        //die;
        return $user;
    }

     public function showRegistrationForm($user_type = null)
    {
        if (!$user_type){//customer registration
            $user_type = 'C';
        }else if($user_type == 'agent'){//service agent registration
            $user_type = 'A';
            //return "hii agent";
        }
        return view('auth.register',compact('user_type')); 
        
    }
}

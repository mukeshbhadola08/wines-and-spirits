<?php

namespace App\Http\Controllers;

use App\LanguageVariable;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class LanguageController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the pages.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $langVariables = LanguageVariable::where('lang_code','en')->get();
        return view('admin.localisation.index',compact('langVariables'));
    }
	public function create()
    {
        return view('admin.localisation.create_variable');
    }

	public function store(Request $request)
	{
        $langObj = new LanguageVariable();		

        $data = $this->validate($request, [
            'var_name'=>'required',
			'var_type'=>'required',
            'var_text'=> 'required',
			'lang_code'=> ''
			
        ]);
		
        $lastinsetid = $langObj->saveVariable($data);
        return redirect('/admin/localisations')->with('success', 'New Variable has been added');
    }

	public function edit($id)
    {
		$langVar = LanguageVariable::find($id);
        return view('admin.localisation.edit_variable',compact('langVar'));
    }
	public function editSpan($id)
    {
		$langEngVar = LanguageVariable::find($id);
		$lang_code = 'es';
		$langVar = LanguageVariable::where([['lang_code','=','es'],['var_name','=',$langEngVar->var_name]])->first();
		if(empty($langVar)){
			$langVar = $langEngVar;
		}
        return view('admin.localisation.edit_variable',compact('langVar','lang_code'));
    }
	public function update(Request $request, $id)
	{
        $langObj = new LanguageVariable();		

        $data = $this->validate($request, [
            'var_name'=>'required',
            'var_text'=> 'required',
			'lang_code'=> 'required'
			
        ]);
		
		$type = $request->input('var_type');

		$checkVariables = LanguageVariable::where([['type','=',$type],['var_name','=',$data['var_name']],['lang_code','=',$data['lang_code']]])->first();
		if(!empty($checkVariables)){
			$data['id'] = $checkVariables->id;
			$langObj->updateVariable($data);		

		} else {
			$data['var_type'] = $type;
			$langObj->saveVariable($data);
			
		}	
       
        return redirect('/admin/localisations')->with('success', 'Variable has been saved');
    }
	public function filterRecord(Request $request)
	{
		$language = $request->input('language');
		$langVariables = LanguageVariable::where('lang_code',$language)->get();
        return view('admin.localisation.index',compact('langVariables','language'));
	}

}

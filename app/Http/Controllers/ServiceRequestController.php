<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use File;
use Response;
use Mail;
use App\User;
use App\UserMetaData;
use App\Services;
use App\ServicesCustomForms;
use App\ServiceRequest;
use App\ServiceRequestMetaData;
use App\ServicesCustomFormOptions;

class ServiceRequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except'=>['addRequest','getServiceForm','getServiceCost','saveServiceRequest']]);
    }
	
	public function index(){
		$data = ServiceRequest::with(['user'])->get();
        return view('admin.services.services_request_list',compact('data'));  
    }
	
	public function addRequest(){

		$services = Services::select('id','name')->where([['is_visible','=',1],['parent','=',0]])->get();
		
		$userdata = array();

		if(auth()->user()){

			$uid = auth()->user()->id;
			$checkMobileDetail = UserMetaData::where([['key','=','mobile_no'],['user_id','=',$uid]])->first();
			$checkAddressDetail = UserMetaData::where([['key','=','address'],['user_id','=',$uid]])->first();

			if($uid >0){
				$userdata['name'] = auth()->user()->name;
				$userdata['email'] = auth()->user()->email;
				if(!empty($checkMobileDetail)){
					$userdata['mobile'] = $checkMobileDetail->value;
				}
				if(!empty($checkAddressDetail)){
					$userdata['address'] = $checkAddressDetail->value;
				}
			}
		}

		return view('site.services.service_request', compact('services', 'userdata'));
	}
	public function getServiceCost(Request $request){
		$service = $request->input('service');
		$serviceDetail = Services::find($service);
		$cost = $serviceDetail['cost'];
		if($serviceDetail['parent']>0){
			$parentDetail = Services::find($serviceDetail['parent']);
			$cost = $cost+$parentDetail['cost'];
		}
		echo $cost;

	}
	public function getServiceForm(Request $request){

		$service = $request->input('service');
		//check service detail
		$serviceDetail = Services::find($service);
		if($serviceDetail['parent']>0){

			// show parent selected fields
			$allParentFormsElements = ServicesCustomForms::where('service_id',$serviceDetail['parent'])->get();
			if(count($allParentFormsElements)>0){
				foreach($allParentFormsElements as $element){
					if($element['field_type']=='text'){
						echo '<div class="form-group">
							<label>'.$element['field_label'].':</label>
							<input type="text" class="form-control" name="customform['.$element['id'].']">
						</div>';
					}
					if($element['field_type']=='file'){
						echo '<div class="form-group">
							<label>'.$element['field_label'].':</label><br/>
							<input type="file" name="customform['.$element['id'].']">
						</div>';
					}
					if($element['field_type']=='select'){
						$options ='';
						$fieldoptions = ServicesCustomFormOptions::where([['form_id','=',$element['id']],['is_visible','=','1']])->get();
						if(count($fieldoptions)>0){
							foreach($fieldoptions as $option){
								$options .= '<option value="'.$option['field_value'].'">'.$option['field_value'].'</option>';
							}
						}
						echo '<div class="form-group">
							<label>'.$element['field_label'].':</label>
							<select name="customform['.$element['id'].']" class="form-control">
								<option value=""> Select '.$element['field_label'].'</option>'.$options.'
							</select>
						</div>';
					}
					if($element['field_type']=='radio' || $element['field_type']=='checkbox'){
						$fieldoptions = ServicesCustomFormOptions::where([['form_id','=',$element['id']],['is_visible','=','1']])->get();
						echo '<div class="form-group">
							<label>'.$element['field_label'].':</label><br/>';
						if(count($fieldoptions)>0){
							foreach($fieldoptions as $option){
								echo '<label><input type="'.$element['field_type'].'" name="customform['.$element['id'].']" value="'.$option['field_value'].'">&nbsp;'.$option['field_value'].'</label>&nbsp;&nbsp;';
							}
						}
							
						echo '</div>';
					}
					if($element['field_type']=='textarea'){
						echo '<div class="form-group">
							<label>'.$element['field_label'].':</label>
							<textarea name="customform['.$element['id'].']" class="form-control" ></textarea>
						</div>';
					}
				}
			}
			
		}
		// show current selected fields
		$allFormsElements = ServicesCustomForms::where('service_id',$service)->get();
		if(count($allFormsElements)>0){
			foreach($allFormsElements as $element){
				if($element['field_type']=='text'){
					echo '<div class="form-group">
						<label>'.$element['field_label'].':</label>
						<input type="text" class="form-control" name="customform['.$element['id'].']">
					</div>';
				}
				if($element['field_type']=='file'){
					echo '<div class="form-group">
						<label>'.$element['field_label'].':</label><br/>
						<input type="file" name="customform['.$element['id'].']">
					</div>';
				}
				if($element['field_type']=='select'){
					$options ='';
					$fieldoptions = ServicesCustomFormOptions::where([['form_id','=',$element['id']],['is_visible','=','1']])->get();
					if(count($fieldoptions)>0){
						foreach($fieldoptions as $option){
							$options .= '<option value="'.$option['field_value'].'">'.$option['field_value'].'</option>';
						}
					}
					echo '<div class="form-group">
						<label>'.$element['field_label'].':</label>
						<select name="customform['.$element['id'].']" class="form-control">
							<option value=""> Select '.$element['field_label'].'</option>'.$options.'
						</select>
					</div>';
				}
				if($element['field_type']=='radio' || $element['field_type']=='checkbox'){
					$fieldoptions = ServicesCustomFormOptions::where([['form_id','=',$element['id']],['is_visible','=','1']])->get();
					echo '<div class="form-group">
						<label>'.$element['field_label'].':</label><br/>';
					if(count($fieldoptions)>0){
						foreach($fieldoptions as $option){
							echo '<label><input type="'.$element['field_type'].'" name="customform['.$element['id'].']" value="'.$option['field_value'].'">&nbsp;'.$option['field_value'].'</label>&nbsp;&nbsp;';
						}
					}
						
					echo '</div>';
				}
				if($element['field_type']=='textarea'){
					echo '<div class="form-group">
						<label>'.$element['field_label'].':</label>
						<textarea name="customform['.$element['id'].']" class="form-control" ></textarea>
					</div>';
				}
			}
		}

	}
	public function saveServiceRequest(Request $request){

		if(auth()->user()){
			$userId = auth()->user()->id;
		}

		if(isset($userId) && $userId >0){
			$data = $this->validate($request, [
				'name'=>'required',
				'email'=> 'required|email',
				'mobile'=> 'required',
				'address'=> 'required',
				'service'=> 'required'
			]);
		} else {
			$data = $this->validate($request, [
				'name'=>'required',
				'email'=> 'required|email|unique:users',
				'mobile'=> 'required',
				'address'=> 'required',
				'service'=> 'required'
			]);
		}

		//$customForms = $request->file('customform');
		//dd($customForms);
		$name = $request->input('name');
		$email = $request->input('email');
		$mobile = $request->input('mobile');
		$address = $request->input('address');
		$password = uniqid();
		$cost = $request->input('cost');
		$service = $request->input('service');
		$description =$request->input('description');
		
		

		if(isset($userId) && $userId >0){
			//ok user already logged in
		} else {
			$user = User::create([
				'name' => $name,
				'email' => $email,
				'password' => Hash::make($password),
				'user_type' => 'C',
				'block_status' => 0,

			]);
			$userId = $user->id;

			$receivername =$name;

			$viewdata = array("name"=>$receivername,"email" =>$email,"password"=>$password);
			$receiveremail = $email;

			$userDetail = array('name'=>$receivername,'email'=>$receiveremail);
			
			Mail::send('email.login', $viewdata, function($message) use ($userDetail) {
				$message->to($userDetail['email'],$userDetail['name'])->subject
					('Account Detail');
				$message->from('no_reply@gmail.com','Property Manager');
			});
		}

		$usermetaObj = new UserMetaData();
		$checkMobileDetail = UserMetaData::where([['key','=','mobile_no'],['user_id','=',$userId]])->first();
		if(!empty($checkMobileDetail)){
			$usermetaObj->updateMetaData(array($userId,'mobile_no',$mobile),$checkMobileDetail->id);
		}else{
			$usermetaObj->saveMetaData(array($userId,'mobile_no',$mobile));
		}
		$usermetaObj1 = new UserMetaData();
		$checkAddressDetail = UserMetaData::where([['key','=','address'],['user_id','=',$userId]])->first();
		if(!empty($checkAddressDetail)){
			$usermetaObj1->updateMetaData(array($userId,'address',$address),$checkAddressDetail->id);
		} else {
			$usermetaObj1->saveMetaData(array($userId,'address',$address));
		}

		$serviceRequestObj= new ServiceRequest();

		$request_id =$serviceRequestObj->saveRequest(array($service,$userId,$address,$cost));

		if($description !=''){

			$serviceRequestMetaObj= new ServiceRequestMetaData();
			$serviceRequestMetaObj->saveMetaData(array($request_id,'description',$description, 0));
		}

		if ($request->hasFile('photograph')) {
			$file = $request->file('photograph');
			$original_name = $file->getClientOriginalName();
			$exe =  pathinfo($original_name, PATHINFO_EXTENSION);
			$image_name = 'service_'.uniqid().'.'.$exe;
			$file->storeAs('service_requests', $image_name); 
			$serviceRequestMetaObj1= new ServiceRequestMetaData();
			$serviceRequestMetaObj1->saveMetaData(array($request_id,'photograph',$image_name, 0));
			
		}

		$customForms = $request->input('customform');
		if(isset($customForms) && count($customForms)>0){
			foreach($customForms as $key=>$value){
				if($value !=''){
					$serviceRequestMetaObj2= new ServiceRequestMetaData();
					$serviceRequestMetaObj2->saveMetaData(array($request_id,$key,$value, 1));
				}
			}
		}
		if($request->hasFile('customform')){
			$customFiles = $request->file('customform');
			foreach($customFiles as $key=>$value){
				$file = $value;
				$original_name = $file->getClientOriginalName();
				$exe =  pathinfo($original_name, PATHINFO_EXTENSION);
				$file_name = 'customfile_'.uniqid().'.'.$exe;
				$file->storeAs('service_requests', $file_name); 
				$filevalue = serialize(array('file'=>$file_name,'original_name'=>$original_name));
				$serviceRequestMetaObj3 = new ServiceRequestMetaData();
				$serviceRequestMetaObj3->saveMetaData(array($request_id,$key,$filevalue, 1));
			}

		}
		

		return redirect('/paypal/payment/'.$request_id)->with('success', 'service request has been added successfully.');

	}
	public function myRequest(){

		$userId = auth()->user()->id;

		$user_type = auth()->user()->user_type;

		$service_requests=  ServiceRequest::where('user_id',$userId)->get();
		return view('site.services.myservice_request',compact('service_requests', 'user_type'));
	}

	public function getRequestPhotograph($id){
		$service = ServiceRequestMetaData::find($id);
		if($service->value !=''){
			$path = storage_path('app/public/service_requests/') . $service->value;
		} else {
			$path = storage_path('app/public/') . 'no-img.jpg';
		}
        if(!File::exists($path)){ 
            $path = storage_path('app/public/') . 'no-img.jpg';
		}

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
	}
	public function getServiceMetaData(Request $request){
		$service_request = $request->input('service');
		$serviceMetaData =  ServiceRequestMetaData::where([['service_rquest_id','=',$service_request],['is_custom_data','=','1']])->get();
		if(count($serviceMetaData)>0){
			foreach($serviceMetaData as $request){
				$metadataForm = ServicesCustomForms::find($request->key);
				if($metadataForm['field_type'] == 'file'){
					$fileArray=unserialize($request->value);
					echo '<p>'.$metadataForm['field_label'].' : '.stripslashes($fileArray['original_name']).'</p>';
				} else {
					echo '<p>'.$metadataForm['field_label'].' : '.$request->value.'</p>';
				}
			}
		} else {
			echo '<p>There is no any more info for this request.</p>';
		}

	}
	public function getServiceRequestDetails($id){
		$serviceRequestDetails = ServiceRequest::find($id);
		return view('admin.services.view_service_request', compact('serviceRequestDetails'));
	}
	public function updateStatus(Request $request){
		$service_id = $request->input('service');
		ServiceRequest::where('id',$service_id)->update(['status'=>'1']);
		echo 'Done';
		exit();
	}
	public function setServiceAgent(Request $request){
		$service_id = $request->input('service');
		$agent  = $request->input('agent');
		ServiceRequest::where('id',$service_id)->update(['agent_id'=>$agent]);
		return redirect('/admin/servicesrequest')->with('success', 'Agent has been awarded successfully.');

		exit();
	}
    
}

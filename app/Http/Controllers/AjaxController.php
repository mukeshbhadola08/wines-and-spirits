<?php

namespace App\Http\Controllers;
use App\Slider;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AjaxController extends Controller {
   public function index() {
     $ids = Slider::all();
      $data = [];
      
      if(!empty($ids)){
      	foreach ($ids as $key => $value) {
            
            if( $value->image ){
               $img = url('/public/storage/slider_images/' . $value->image );
            }else{
               continue;
            }

            $data[] = array("id"=>$value->id, "url"=> $img);
      	}
      }
      return response()->json(array('data'=> $data), 200);
   }
}
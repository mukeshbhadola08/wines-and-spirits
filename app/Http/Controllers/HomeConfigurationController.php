<?php

namespace App\Http\Controllers;

use App\HomeConfiguration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeConfigurationController extends Controller
{
    public function settings()
    {
        $homesettings = HomeConfiguration::all();
        return view('admin.homesettings',compact('homesettings'));
    }

	public function update(Request $request)
    {
		

		$input = $request->all();
		
		$HomeConfigurationController = new HomeConfiguration();
		
		if(isset($input['settings'])){
			foreach($input['settings'] as $option_name => $option_value){
				$conDetails = HomeConfiguration::where('option_name', $option_name)->first();
				
				$option_value = (trim($option_value) == "")?"":trim($option_value);

				$data = array("option_name" => $option_name, "option_value" => $option_value);

				if(isset($conDetails->id)){
					$data['id'] = $conDetails->id;
					$HomeConfigurationController->updateRecord($data);
				} else {
					$idgenerated = $HomeConfigurationController->insertRecord($data);
					$data['id'] = $idgenerated;
				}
			}
		}

		$settings = HomeConfiguration::all();
		 return redirect('/admin/homepagesection')->with('success', 'Homepage configuration updated successfully.');
	}
}

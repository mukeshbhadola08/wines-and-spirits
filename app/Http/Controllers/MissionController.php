<?php

namespace App\Http\Controllers;

use App\Mission;
use File;
use Response;
use Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

use App\Http\Controllers\Controller;

class MissionController extends Controller
{
	public function __construct()
    {
      $this->middleware('auth');
    }
    /**
     * Display a listing of the pages.
     *
     * @return \Illuminate\Http\Response
     */
    public function addListing(
        Request $request
    ) {
        $mission = new Mission();
        $mission->description = $request->input('item_content');
        $mission_image = $request->file('image_url');
        $path_img = '';
        $httpStatus = 400;
        $responseMessage = 'failed to add list!';
        if (!empty($mission_image)) {
            $original_name = $mission_image->getClientOriginalName();
            $ext =  pathinfo($original_name, PATHINFO_EXTENSION);
            $image_name = 'home_' . uniqid().'.'.$ext;
            $path = $mission_image->move(base_path() . '/public/storage/slider_images', $image_name);
            $path_img = url('/').'/storage/slider_images/'.$image_name;

            $mission->image = $path_img;
            $mission->save();
            $httpStatus = 200;
        }
		
         return response()->json([
            'message' => $responseMessage
         ], $httpStatus);
    }

    public function missionRemoveItem(Request $request) {
        $itemId = $request->itemDeleteIS;
        $delete = Mission::find($itemId);
        $httpStatus = 400;
        $responseMessage = 'failed to remove from list!';
        if (!empty($delete)) {
            $delete->delete();
            $httpStatus = 200;
        }
        return response()->json([
        'message' => $responseMessage
        ], $httpStatus);
    }

    public function allMissionListings(Request $request)
    {
        $totalMission = Mission::count();
		$all_request_page_row = [];
		$all_request_page_table = [];
		$limit = $request->iDisplayLength??10;
		$offset = $request->iDisplayStart??0;

		$listMission = Mission::orderBy('id')->skip($offset)->take($limit)->get();
		if (!empty($listMission)) {
			 foreach ($listMission as $listItem) {
                 static $i = 0;
                $encryptedlistId = $listItem->id;
                $actionlink = '<a href="javascript:void(0)" data-type="text" data-delete_listitem="'.$encryptedlistId.'" class="btn btn-danger fa fa-times"></a>';
                $imageLink = '<a href="'.$listItem->image.'">View</a>';
				$all_request_page_row['index'] = ++$i;
				$all_request_page_row['content'] = $listItem->description;
				$all_request_page_row['image'] = $imageLink;
				$all_request_page_row['action'] = $actionlink;
				array_push($all_request_page_table, $all_request_page_row);
			}
		}
		return response()->json([
			'sEcho' => intval($request->sEcho),
			'iTotalRecords' => count($listMission),
			'iTotalDisplayRecords' => $totalMission,
			'aaData' => $all_request_page_table
		]);
	}
}


<?php

namespace App\Http\Controllers;

use App\Routines;
use App\Slider;
use App\Http\Controllers\SliderController;
use File;
use Response;
use Auth;
use App\Invoice;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class RoutinesController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth', ['except' => ['routines', 'getImage']]);
    }
    /**
     * Display a listing of the pages.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routines = Routines::get();
        return view('admin.routines.index',compact('routines'));
    }

	public function create(){
		return view('admin.routines.create');
	}

	private function toAscii($vp_string) {
		$vp_string = trim($vp_string);
    
		$vp_string = html_entity_decode($vp_string);
		
		$vp_string = strip_tags($vp_string);
		
		$vp_string = strtolower($vp_string);
		
		$vp_string = preg_replace('~[^ a-z0-9_.]~', ' ', $vp_string);
		
		$vp_string = preg_replace('~ ~', '-', $vp_string);
		
		$vp_string = preg_replace('~-+~', '-', $vp_string);
			
		return $vp_string;
	}

    //
	public function store(Request $request)
	{
        $newObj = new Routines();	

        $data = $this->validate($request, [
            'description'=>'required',
            'title'=> 'required',
			'is_visible'=> '',
			'working_on' => 'required',
            'additional_skills' => '',
            'level' => '',
           'video_url'=> '',
            'image' => '',
            'video_time' => '',
        ]);

		if(!isset($data['is_visible'])){
			$data['is_visible'] = 0;
		}
       if(isset($data['image']) && is_numeric($data['image'])){
            $image = Slider::where('id',$data['image'])->pluck('id')->all();
            $data['image'] = null;  
            $data['slider_id'] = $image[0];
        }else{
            if(!isset($data['image'])){
                $data['image'] = null;
                $data['slider_id'] = null;
            }else{
                $original_name = $request->image->getClientOriginalName();
            
                $ext =  pathinfo($original_name, PATHINFO_EXTENSION);

                $image_name = 'default_'.uniqid().'.'.$ext;

                $path = $request->image->storeAs('default_images', $image_name);
                $SliderControllerobj = new SliderController();
                $ext = pathinfo(base_path() . '/storage/app/public/'. $path, PATHINFO_EXTENSION);
                $SliderControllerobj->correctImageOrientation(550, base_path() . '/storage/app/public/'. $path, $ext, base_path() . '/storage/app/public/default_images');
                $data['slider_id'] = null;
                $data['image'] = $path; 
            }
        }

        $lastinsetid = $newObj->saveRoutines($data);
        return redirect('/admin/routines')->with('success', 'Routine has been created');
    }

	public function edit($id)
    {
        $routines = Routines::where('id', $id)
                        ->first();
        return view('admin.routines.edit', compact('routines', 'id'));
    }

    public function routines(){
		$routines = Routines::get();
		return view('site.routines.listing',compact('routines'));
	}

	public function update(Request $request, $id)
    {

		$routinesDetails = Routines::where('id', $id)
                        ->first();
       $routines = new Routines();                 
        $routinesObj = new Routines();

        $data = $this->validate($request, [
            'description'=>'required',
            'title'=> 'required',
			'is_visible'=> '',
			'working_on' => 'required',
            'additional_skills' => '',
            'level' => '',
           'video_url'=> '',
            'image' => '',
            'video_time' => '',
        ]);

		if(!isset($data['is_visible'])){
			$data['is_visible'] = 0;
		}
        if(!isset($request->image)){
            $path = $routines->image;
        } else {

            if(isset($data['image']) && is_numeric($data['image'])){
                $image = Slider::where('id',$data['image'])->pluck('id')->all();
                $data['image'] = null;  
                $data['slider_id'] = $image[0];
            }else{
                if(!isset($data['image'])){
                    $data['image'] = null;
                    $data['slider_id'] = null;
                }else{
                    $original_name = $request->image->getClientOriginalName();
                
                    $ext =  pathinfo($original_name, PATHINFO_EXTENSION);

                    $image_name = 'default_'.uniqid().'.'.$ext;

                    $path = $request->image->storeAs('default_images', $image_name);
                    $SliderControllerobj = new SliderController();
                    $ext = pathinfo(base_path() . '/storage/app/public/'. $path, PATHINFO_EXTENSION);
                    $SliderControllerobj->correctImageOrientation(550, base_path() . '/storage/app/public/'. $path, $ext, base_path() . '/storage/app/public/default_images');
                    $data['slider_id'] = null;
                    $data['image'] = $path; 
                    //dd($data['image']);
                }

            }

        }

		$data['id'] = $id;
        $routinesObj->updateRoutines($data);

        return redirect('/admin/routines')->with('success', 'Routine has been updated!!');
    }

	public function delete($id){

		
        $page = Routines::find($id);

        $page->delete();
        return redirect('/admin/routines')->with('success', 'Routine has been deleted!!');
    }

    public static function getImage($id){
        $page = Routines::where('id', $id)
                        ->first();

        if($page->default_image !=''){
            $path = storage_path('app/public/') . $page->default_image;
            if(!File::exists($path)){ 
                $path = storage_path('app/public/') . 'no-img.jpg';
            }

            $file = File::get($path);
            $type = File::mimeType($path);

            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);

            return $response;

        }elseif($page->slider_id !=''){
            $slider = Slider::where('id', $page->slider_id)->first();
            $url = url('/public/storage/slider_images/' . $slider->image);
        } else {
            $url = url('/public/storage/intro-img-top.png');
        }
        
        return $url;

    }

   /* public function getImage($id){
        $page = Routines::where('id', $id)
                        ->first();

        if($page->default_image !=''){
            $path = storage_path('app/public/') . $page->default_image;
        }elseif($page->slider_id !=''){
            $slider = Slider::where('id', $page->slider_id)->first();
            $path = storage_path('app/public/') . $slider->image;
        } else {
            $path = storage_path('app/public/') . 'intro-img-top.png';
        }

        if(!File::exists($path)){ 
            $path = storage_path('app/public/') . 'no-img.jpg';
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }*/
}

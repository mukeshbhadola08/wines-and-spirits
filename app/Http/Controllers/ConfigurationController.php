<?php

namespace App\Http\Controllers;

use App\Configuration;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use File;

class ConfigurationController extends Controller
{
	const CONFIG_GENERAL = 1;
	const CONFIG_HOME = 2;
    public function settings()
    {
        $settings = Configuration::where('type', '1')->get();
        return view('admin.settings',compact('settings'));
    }

    public function homesettings()
    {
        $settings = Configuration::where('type', '2')->get();
        return view('admin.homesettings',compact('settings'));
    }

	public function update(Request $request)
    {
		$input = $request->all();
		$configuration = new Configuration();
		if(isset($input['settings'])){
			foreach($input['settings'] as $option_name => $option_value){

				if ($option_name == 'wines_logo' || $option_name == 'wines_footer_logo') {
					$original_name = $input['settings'][$option_name]->getClientOriginalName();
					$ext = pathinfo($original_name, PATHINFO_EXTENSION);
					$image_name = 'home_' . uniqid() . '.' . $ext;
					$path = $input['settings'][$option_name]->move(base_path() . '/public/storage/slider_images', $image_name);
					$ext = pathinfo(base_path() . '/public/storage/' . $path, PATHINFO_EXTENSION);
					$option_value = url('/storage/slider_images').'/'.$image_name;
				}

				$conDetails = Configuration::where('option_name', $option_name)->first();
				
				$option_value = (trim($option_value) == '')?'':trim($option_value);
				$data = [
					'option_name' => $option_name, 'option_value' => $option_value
				];
				$data['type'] = '1';
				if(isset($conDetails->id)){
					$data['id'] = $conDetails->id;
					$configuration->updateRecord($data);
				} else {
					$idgenerated = $configuration->insertRecord($data);
					$data['id'] = $idgenerated;
				}
			}
		}

		$settings = Configuration::all();
		 return redirect('/admin/settings')->with('success', 'Configuration updated successfully.');
	}

	public function HomePageSettings()
	{
		$settingtype = request()->type;
		$hommePageSettings = Configuration::where('type', static::CONFIG_HOME)->get();
        return view('admin.settings_homepage',compact('hommePageSettings', 'settingtype'));
	}

	public function commentOptionSettings()
	{
		#to do's
		$settingtype = request()->type;
		$commentOptionsSettings = Configuration::where('type', static::CONFIG_HOME)->get();
        return view('admin.settings_comments',compact('commentOptionsSettings', 'settingtype'));
	}

	public function getCommentOptionSettings()
	{
		return Configuration::where('type', static::CONFIG_HOME)->get();
	}

	public function HomePageSettingsUpdate(Request $request)
    {
    	$input = $request->all();
    	$settingtype = request()->type;
		$validator = Validator::make($input['settings'], [
            'top_image' => 'dimensions:max_width=582,max_height=493',
            'right_image' => 'dimensions:min_width=320,max_height=699'
        ]);
       
        if ($validator->passes()) {
            if(isset($input['settings'])){
            	$configuration = new Configuration();
				foreach($input['settings'] as $option_name => $option_value){
					if(($option_name == "background_image" || $option_name == "history_background_image" || $option_name == "wine_background_image" || $option_name == "mission_background_image" || $option_name == "mission_side_image" || $option_name == "singlepage_wine_background_image" || $option_name == "singlepage_distellery_background_image" || $option_name == "singlepage_product_background_image")) { 
		 				$original_name = $input['settings'][$option_name]->getClientOriginalName();
						$ext =  pathinfo($original_name, PATHINFO_EXTENSION);
						$image_name = 'home_' . uniqid().'.'.$ext;
						$path = $input['settings'][$option_name]->move(base_path() . '/public/storage/slider_images', $image_name);
						$path_img = URL::to('/').'/storage/slider_images/'.$image_name;
						$option_value = $path_img;	
					}
					$conDetails = Configuration::where('option_name', $option_name)->first();
					
					$option_value = (trim($option_value) == "")?"":trim($option_value);

					$data = array("option_name" => $option_name, "option_value" => $option_value);
					$data['type'] = '1';
					if(isset($conDetails->id)){
						$data['id'] = $conDetails->id;
						$configuration->updateRecord($data);
					} else {
						$idgenerated = $configuration->insertRecord($data);
						$data['id'] = $idgenerated;
					}
				}
			}
			return redirect('/admin/setting_blog?type='.$settingtype)->with('success', 'configuration is updated successfully.');
        }
	}

	public function blogCommentSettingsUpdate(Request $request)
    {
    	$input = $request->all();
		$validator = Validator::make($input['settings'], [
            'top_image' => 'dimensions:max_width=582,max_height=493',
            'right_image' => 'dimensions:min_width=320,max_height=699'
        ]);
       
        if ($validator->passes()) {
            if(isset($input['settings'])){
            	$configuration = new Configuration();
				foreach($input['settings'] as $option_name => $option_value){
					$conDetails = Configuration::where('option_name', $option_name)->first();
					$option_value = (trim($option_value) == "")?"":trim($option_value);

					$data = array("option_name" => $option_name, "option_value" => $option_value);
					$data['type'] = '1';
					if(isset($conDetails->id)){
						$data['id'] = $conDetails->id;
						$configuration->updateRecord($data);
					} else {
						$idgenerated = $configuration->insertRecord($data);
						$data['id'] = $idgenerated;
					}
				}
			}

			return response()->json([
				'message' => 'Settings updated successfully!'
			]);
        }
	}

	public function resetSettingsDefault(Request $request)
    {
		$input = $request->itemDelete??'none';
		$option_value = '';
		if(isset($input)){
			$configuration = new Configuration();
			$conDetails = Configuration::where(
				'option_name',
				$input
			)->first();

			$data = ['option_name' => $input, 'option_value' => $option_value];
			$data['type'] = '1';
			if(isset($conDetails->id)){
				$data['id'] = $conDetails->id;
				$configuration->updateRecord($data);
			} else {
				$idgenerated = $configuration->insertRecord($data);
				$data['id'] = $idgenerated;
			}
		}
		return response()->json([
			'message' => 'Settings updated successfully!'
		]);
	}

}

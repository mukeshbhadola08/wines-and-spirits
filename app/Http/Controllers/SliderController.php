<?php

namespace App\Http\Controllers;

use App\Slider;
use App\Page;
use File;
use Response;
use Storage;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class SliderController extends Controller
{
	public function __construct()
    {
       $this->middleware('auth', ['except' => ['getImage', 'portfolio']]);
    }
    /**
     * Display a listing of the pages.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {		
    	
    	$slides = Slider::where('lang_code','en')->orderBy('nav_order', 'asc')->get();
        return view('admin.slider.index',compact('slides'));
    }
    
	public function create(){
		return view('admin.slider.create');
	}

	private function toAscii($vp_string) {
		$vp_string = trim($vp_string);
    
		$vp_string = html_entity_decode($vp_string);
		
		$vp_string = strip_tags($vp_string);
		
		$vp_string = strtolower($vp_string);
		
		$vp_string = preg_replace('~[^ a-z0-9_.]~', ' ', $vp_string);
		
		$vp_string = preg_replace('~ ~', '-', $vp_string);
		
		$vp_string = preg_replace('~-+~', '-', $vp_string);
			
		return $vp_string;
	}

	public function store(Request $request)
	{
        $slider = new Slider();
        $data = $this->validate($request, [
            'title'=> 'required',
            'image' => 'required',
			'is_visible'=> '',
			'lang_code'=> '',
			'description' =>'required'
        ]);
		$cleanurl = $this->toAscii($data['title']);
		if(!isset($data['is_visible'])){
			$data['is_visible'] = 0;
		}
		if($data['image']){
			$original_name = $request->image->getClientOriginalName();
			$ext =  pathinfo($original_name, PATHINFO_EXTENSION);
			$image_name = 'slider_' . uniqid().'.'.$ext;   
			$path = $request->file('image')->move(base_path() . '/public/uploads/slider_images', $image_name);
			$ext = pathinfo(base_path() . '/public/uploads/'. $path, PATHINFO_EXTENSION);
			$this->correctImageOrientation(550, $path, $ext, base_path() . '/public/uploads/slider_images');
			$data['image'] = $image_name;
	        $lastinsetid = $slider->saveSlider($data);
		}
        return redirect('/admin/photo-gallery')->with('success', 'New Slider has been created');
    }

	public function edit($id)
    {
        $slide = Slider::where('id', $id)->first();
        return view('admin.slider.edit', compact('slide', 'id'));
    }


	public function update(Request $request, $id)
    {

		$slideDetails = Slider::where('id', $id)->first();
        $slider = new Slider();
        $data = $this->validate($request, [
            'title'=> 'required',
			'is_visible'=> '',
			'lang_code'=> '',
            'image' => '',
            'description' => ''
        ]);
        if(!isset($data['is_visible'])){
			$data['is_visible'] = 0;
		}

        if(isset($data['image'])){
			$original_name = $request->image->getClientOriginalName();
			$ext =  pathinfo($original_name, PATHINFO_EXTENSION);

			$image_name = 'slider_' . uniqid().'.'.$ext;
			 $image =  time() .'_'. $request->file('image')->getClientOriginalName();   
			$path = $request->file('image')->move(base_path() . '/public/uploads/slider_images', $image_name);
			$ext = pathinfo(base_path() . '/public/uploads/'. $path, PATHINFO_EXTENSION);
			$this->correctImageOrientation(550, $path, $ext, base_path() . '/public/uploads/slider_images');
			$data['image'] = $image_name;
					
		}
		$lastinsetid = $data['id'] = $id;
		$slider->updateSlider($data);
		return redirect('/admin/photo-gallery')->with('success', 'Slider has been updated!!');
    }

	public function delete($id){
        $page = Slider::find($id);
		if($page->image !=''){
			Storage::delete($page->image);
		}
        $page->delete();
        return redirect('/admin/photo-gallery')->with('success', 'Slider has been deleted!!');
    }

	public static function getImage($id){
		$slider = Slider::where('id', $id)->first();
		if($slider->image !=''){
			$url = '/public/uploads/slider_images/' . $slider->image;
		} else {
			$url = '/public/uploads/slider_images/no-img.jpg';
		}
        return $url;
	}

	public function setMediaOrder(Request $request)
    {
        $orders = $request->input('order');
        if (!empty($orders)) {
            foreach ($orders as $k => $ord) {
                $slider = slider::find($ord);
                $slider->update(['nav_order' => $k + 1]);
            }
        }
    }	

	public function correctImageOrientation(
		$dimension,
		$source,
		$extension
	) {
		//get the image size
		$size = getimagesize($source);

		//determine dimensions
		$width = $size[0];
		$height = $size[1];

		switch ($extension) {
				//its a gif
			case 'gif':
			case 'GIF':
				//create a gif from the source
				$sourceImage = imagecreatefromgif($source);
				break;
			case 'jpg':
			case 'JPG':
			case 'jpeg':
				//create a jpg from the source
				$sourceImage = imagecreatefromjpeg($source);
				break;
			case 'png':
			case 'PNG':
				//create a png from the source
				$sourceImage = imagecreatefrompng($source);
				break;
		}

		// find the largest dimension of the image
		// then calculate the resize perc based upon that dimension
		$percentage = ($width >= $height) ? 100 / $width * $dimension : 100 / $height * $dimension;

		// define new width / height
		$exif = @exif_read_data($source);

		if (isset($exif['Orientation'])) {
			$ort = $exif['Orientation'];
			if ($extension == "jpg" && $ort != 1 || $extension == "jpeg" && $ort != 1) {
				//determine what oreientation the image was taken at
				switch ($ort) {
					case 6: // 90 rotate right
						$destinationImage = imagerotate($sourceImage, 270, 0);
						break;
					case 8: // 90 rotate left
						$destinationImage = imagerotate($sourceImage, 90, 0);
						break;
				}
			return imagejpeg($destinationImage, $source, 50);
			}
		}       
	}
}


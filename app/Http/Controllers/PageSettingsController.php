<?php

namespace App\Http\Controllers;

use App\PageSetting;
use App\Page;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class PageSettingsController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $pageSettingsDetails = PageSetting::all();
		return view('admin.page_settings.index', compact('pageSettingsDetails'));
    }

    public function store(Request $request) {
        $requestArr = $request->all();
        $pageSettingObj = new PageSetting();
        
        $i=1;
        foreach($requestArr['settings'] as $page => $background_color) {
            $pageKey = str_replace("'", '', $page);
            $filename = '';
            $updateArr = [];
            if(!empty($requestArr['background_image']["'$pageKey'"])) {
                $image = $requestArr['background_image']["'$pageKey'"];
                $path = public_path(). '/uploads/static_page_background_image';
                $filename = time() . '.' . $image->getClientOriginalName();
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                $filename = time() . '.' . $ext;
                $filename = preg_replace('/\s+/', '', $filename);
                $image->move($path, $filename);
            }

            
            $updateArr['static_page_name'] = $pageKey;
            if(!empty($filename)){
                $updateArr['background_image'] = $filename;
            }
            $updateArr['background_color'] = $background_color;

            DB::table('page_settings')
                ->where('id', $i)
                ->update($updateArr);

            $i++;
        }

        return redirect('/admin/page_settings')->with('success', 'Page settings has been updated');
    }

    public function remove_background_image(Request $request){
        $background_image_id = $request->background_image_id;
        $result = '';
        if($background_image_id=='contact_us_background_image_block'){
            $result = PageSetting::where('id', 1)->update(['background_image'=>'']);
        }
        if($background_image_id=='wine_background_image_block'){
            $result = PageSetting::where('id', 2)->update(['background_image'=>'']);
        }
        if($background_image_id=='spirit_background_image_block'){
            $result = PageSetting::where('id', 3)->update(['background_image'=>'']);
        }
        if($background_image_id=='blog_background_image_block'){
            $result = PageSetting::where('id', 4)->update(['background_image'=>'']);
        }
        if($background_image_id=='single_blog_background_image_block'){
            $result = PageSetting::where('id', 5)->update(['background_image'=>'']);
        }
        if($background_image_id=='productlisting_background_image_block'){
            $result = PageSetting::where('id', 6)->update(['background_image'=>'']);
        }
        return $result;
    }

}

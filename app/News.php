<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news';

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
	/**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    //protected $dateFormat = 'U';
	
    //protected $connection = 'connection-name' //By default, all Eloquent models will use the default database connection configured for your application. If you would like to specify a different connection for the model, use the $connection property:

	/**
     * The attributes that aren mass assignable.
     *
     * @var array
     */
	protected $fillable = ['added_by', 'title', 'description','video_url','file_name','is_visible','lang_code','parent_lang_id'];

	/**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['permalink','summery'];


	public function saveNews($data)
	{
		$this->added_by = auth()->user()->id;
		$this->title = $data['title'];
		$this->description = $data['description'];
		$this->summery = $data['short_description'];
		$this->permalink = $data['clean_url'];
		$this->is_visible = $data['is_visible'];
		$this->video_url = $data['video_url'];
		 if(!empty($data['image'])){
            $this->default_image = $data['image'];
        }
        if(isset($data['slider_id']))
            $this->slider_id = $data['slider_id'];
        
        $this->event_venue = $data['event_venue'];
        $this->event_date = $data['event_date'];
		$this->lang_code = $data['lang_code'];
		$this->parent_lang_id = $data['parent'];
		$this->is_paid = $data['is_paid'];
		$this->amount = $data['event_amount'];
		$this->save();
		return $this->id;
	}
	public function updateNews($data)
	{
		$news = $this->find($data['id']);
		$news->added_by = auth()->user()->id;
		$news->title = $data['title'];
		$news->description = $data['description'];
		$news->summery = $data['short_description'];
		$news->is_visible = $data['is_visible'];
		$news->video_url = $data['video_url'];
		//dd($data['image']);
        if(!empty($data['image']) && isset($data['image'])){
            $news->default_image = $data['image'];
            $news->slider_id = null;
        }elseif(isset($data['slider_id'])){
            $news->slider_id = $data['slider_id'];
        }
        $news->event_venue = $data['event_venue'];
        $news->event_date = $data['event_date'];
        $news->results = $data['results'];
		$news->is_paid = $data['is_paid'];
		$news->amount = $data['event_amount'];
		$news->save();
		return 1;
	}
	public static function youtube_id_from_url($url) {
		$pattern = 
			'%^# Match any youtube URL
			(?:https?://)?  # Optional scheme. Either http or https
			(?:www\.)?      # Optional www subdomain
			(?:             # Group host alternatives
			  youtu\.be/    # Either youtu.be,
			| youtube\.com  # or youtube.com
			  (?:           # Group path alternatives
				/embed/     # Either /embed/
			  | /v/         # or /v/
			  | /watch\?v=  # or /watch\?v=
			  )             # End path alternatives.
			)               # End host alternatives.
			([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
			%x'
			;
		$result = preg_match($pattern, $url, $matches);
		if (false !== $result) {
			if(isset($matches[1])){
				return $matches[1];
			}
		}
		return false;
	}

}

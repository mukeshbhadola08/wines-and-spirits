<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Routines extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'routines';

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
	/**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    //protected $dateFormat = 'U';
	
    //protected $connection = 'connection-name' //By default, all Eloquent models will use the default database connection configured for your application. If you would like to specify a different connection for the model, use the $connection property:

	/**
     * The attributes that aren mass assignable.
     *
     * @var array
     */
	protected $fillable = ['title', 'description','is_visible', 'working_on'];

	/**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['permalink','summery'];


	public function saveRoutines($data)
	{
		$this->title = $data['title'];
		$this->description = $data['description'];
		$this->working_on = $data['working_on'];
		$this->additional_skills = $data['additional_skills'];
        $this->video_url = $data['video_url'];
        $this->level = $data['level'];
        $this->video_time = $data['video_time'];
         if(!empty($data['image'])){
                $this->default_image = $data['image'];
            }
            if(isset($data['slider_id']))
                $this->slider_id = $data['slider_id'];

		$this->save();
		return $this->id;
	}
	public function updateRoutines($data)
	{	
		$routines = $this->find($data['id']);
		$routines->title = $data['title'];
		$routines->description = $data['description'];
		$routines->working_on = $data['working_on'];
        $routines->video_url = $data['video_url'];
        $routines->level = $data['level'];        
        $routines->video_time = $data['video_time'];        
		$routines->is_visible = $data['is_visible'];
        $routines->additional_skills = $data['additional_skills'];
         if(!empty($data['image']) && isset($data['image'])){
            $routines->default_image = $data['image'];
            $routines->slider_id = null;
        }elseif(isset($data['slider_id'])){
            $routines->slider_id = $data['slider_id'];
        }
		$routines->save();
		return 1;
	}

}

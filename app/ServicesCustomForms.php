<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesCustomForms extends Model
{
	protected $table = 'services_custom_forms';
	protected $fillable = ['field_label','field_type','service_id','is_visible'];

	public function saveForm($data)
	{
		$this->field_label = $data['field_label'];
		$this->field_type = $data['field_type'];
		$this->service_id = $data['service'];
		$this->is_visible = isset($data['visible'])?:0;
		$this->save();
		return $this->id;
	}
	public function updateForm($data)
	{
		$form = $this->find($data['id']);
		$form->field_label = $data['field_label'];
		$form->field_type = $data['field_type'];
		$form->service_id = $data['service'];
		$form->is_visible = isset($data['visible'])?:0;
		$form->save();
	}
	
}
?>
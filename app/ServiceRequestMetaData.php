<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestMetaData extends Model
{
	protected $table = 'service_request_metadata';
	public $timestamps = false;
	protected $fillable = ['service_rquest_id','key','value','is_custom_data'];

	public function saveMetaData($data)
	{
		$this->service_rquest_id = $data[0];
		$this->key = $data[1];
		$this->value = $data[2];
		$this->is_custom_data=$data[3];
		$this->save();	
	}
	
}
?>
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pages';

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
	/**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    //protected $dateFormat = 'U';
	
    //protected $connection = 'connection-name' //By default, all Eloquent models will use the default database connection configured for your application. If you would like to specify a different connection for the model, use the $connection property:

	/**
     * The attributes that aren mass assignable.
     *
     * @var array
     */
	protected $fillable = ['added_by', 'page_title', 'page_description','lang_code','parent_lang_id'];

	/**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['page_keywords', 'clean_url'];


	public function savePage($data)
	{      
			$this->added_by = auth()->user()->id;
			$this->page_title = $data['page_title'];
			$this->page_description = $data['page_description'];
			$this->clean_url = $data['clean_url'];
			$this->page_keywords = $data['page_keywords'];
            $this->is_visible = $data['is_visible'];
            $this->background_color = !empty($data['background_color'])?$data['background_color']:'';
            $this->background_image = !empty($data['background_image'])?$data['background_image']:'';
            if(!empty($data['image'])){
                $this->default_image = $data['image'];
            }
            if(isset($data['slider_id']))
                $this->slider_id = $data['slider_id'];
			$this->lang_code = $data['lang_code'];
			$this->parent_lang_id = $data['parent'];
			$this->save();
			return $this->id;
	}

	public function updatePage($data){
        $page = $this->find($data['id']);
        $page->page_title = $data['page_title'];
        $page->page_description = $data['page_description'];
        $page->page_keywords = $data['page_keywords'];
        $page->is_visible = $data['is_visible'];
        $page->background_color = !empty($data['background_color'])?$data['background_color']:'';
        $page->background_image = !empty($data['background_image'])?$data['background_image']:'';
        if(!empty($data['image']) && isset($data['image'])){
            $page->default_image = $data['image'];
            $page->slider_id = null;
        }elseif(isset($data['slider_id'])){
            $page->slider_id = $data['slider_id'];
        }
        $page->save();
        return 1;
	}

    public function updatePageDefaultImage($data){
        $page = $this->find($data['id']);
        $page->default_image = $data['image'];
        $page->save();
        return 1;
    }

}

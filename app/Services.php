<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Services extends Model
{
   protected $fillable = ['name', 'parent','added_by','description','summery','cost','created_on','is_visible','image'];
   public function saveServices($data)
	{
		$is_visible = isset($data['visible'])?$data['visible']:0;
		$image_name = '';
		if(isset($data['image'])){
			$original_name = $data['image']->getClientOriginalName();
			$exe =  pathinfo($original_name, PATHINFO_EXTENSION);
			$image_name = 'service_'.uniqid().'.'.$exe;
			$data['image']->storeAs('services', $image_name);
			
		}
		$id = DB::table('services')->insertGetId(
			['name'=>$data['title'], 'parent'=>$data['parent'],'added_by'=>$data['user'],'description'=>$data['description'],'summery'=>$data['summery'],'cost'=>$data['cost'],'is_visible'=>$is_visible,'image'=>$image_name,'created_on'=>date('Y-m-d H:i:s')]
		);
		return $id;
	}
	public function updateServices($data,$id){
		$is_visible = isset($data['visible'])?$data['visible']:0;
		$service = DB::table('services')->find($id);
		$image_name = $service->image;
		if(isset($data['image'])){

			$this->deleteImage($id);
			$original_name = $data['image']->getClientOriginalName();
			$exe =  pathinfo($original_name, PATHINFO_EXTENSION);
			$image_name = 'service_'.uniqid().'.'.$exe;
			$data['image']->storeAs('services', $image_name);
			
		}
		DB::table('services')
            ->where('id', $id)
            ->update(['name'=>$data['title'], 'parent'=>$data['parent'],'description'=>$data['description'],'summery'=>$data['summery'],'cost'=>$data['cost'],'is_visible'=>$is_visible,'image'=>$image_name]);
	}
	public function deleteImage($id){
		$service = DB::table('services')->find($id);
		Storage::delete('services/'.$service->image);
	}
	public function getAllServices(){
		return Services::where('is_visible', 1)->get();
	}
	public function getSubServices($id){
		return Services::where('parent', $id)->get();
	}
}
?>
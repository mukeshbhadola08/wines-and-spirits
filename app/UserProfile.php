<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_metadata';

	/**/
    /**
    *By default, Eloquent expects created_at and updated_at columns to exist on your tables. If you do not *wish to have these columns automatically managed by Eloquent, set the $timestamps property on your *model to false
    **/
    public $timestamps = false;
    /**/

	public function saveProfile($data){
		$this->user_id = $data['userId'];
		$this->key = $data['key'];
		$this->value = $data['value'];
		$this->save();
		return $this->id;
	}

	public function updateProfile($data){
        $user = $this->where('user_id','=',$data['userId'])->where('key','=',$data['key'])->first();
		if(!empty($user)){
			$user->key = $data['key'];
			$user->value = $data['value'];
			$user->save();
			return 1;
		} else {
			$data['value'] = ($data['value']!='')?$data['value']:'';
			$this->saveProfile($data);
		}
	}

        public function user()
    {
        //$this->belongsTo(User::class);
        return $this->belongsTo('App\User');
    }
}

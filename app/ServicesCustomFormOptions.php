<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesCustomFormOptions extends Model
{
	protected $table = 'services_custom_form_options';
	protected $fillable = ['field_value','form_id','is_visible'];
	
	public function saveFormOptions($data){

		$this->field_value = $data['name'];
		$this->form_id = $data['form'];
		$this->is_visible = $data['check'] ;
		$this->save();

	}
	public function updateFormOptions($data, $id){
		$form =  $this->find($id);
		$form->field_value = $data['name'];
		$form->form_id = $data['form'];
		$form->is_visible = $data['check'] ;
		$form->save();

	}
	
}
?>
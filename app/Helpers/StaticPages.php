<?php

namespace App\Helpers;

class StaticPages
{
    const STATIC_PAGES_TYPE = 3;
    const STATIC_BLOG_PAGES_TYPE = 2;
    const STATIC_CMS_PAGES_TYPE = 1;
    
    public static $pages = [
        'homepage',
        'eventslisting',
        'resultslisting',
        'portfoliolisting',
        'bloglisting',
        'wineslisting',
        'spiritslisting',
        'contactus'
    ];
}
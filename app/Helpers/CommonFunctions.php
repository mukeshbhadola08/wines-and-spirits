<?php

namespace App\Helpers;

class CommonFunctions
{
    public static function getUploadDir(): string
    {

        $path = storage_path('app/public/');
        $uploads = 'uploads/';
        $year = date('Y');
        $month = date('m');
        
        !file_exists($path.$uploads) && mkdir($path.$uploads , 0777);
        
        $path = $path.$uploads;
        $yearFolder = $path . $year;
        $monthFolder = $yearFolder . '/' . $month;

        !file_exists($yearFolder) && mkdir($yearFolder , 0777);
        !file_exists($monthFolder) && mkdir($monthFolder, 0777);

        return $uploads . $year . '/' . $month;
    }
}
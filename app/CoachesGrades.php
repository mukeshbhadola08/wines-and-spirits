<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class CoachesGrades extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'coaches_grades';

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
	/**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    //protected $dateFormat = 'U';
	
    //protected $connection = 'connection-name' //By default, all Eloquent models will use the default database connection configured for your application. If you would like to specify a different connection for the model, use the $connection property:

	/**
     * The attributes that aren mass assignable.
     *
     * @var array
     */
	protected $fillable = ['current_coach', 'coach_name', 'coach_email', 'coach_status','school_name','school_grade'];




	public function saveCoachesGrades($data)
	{       
		
			CoachesGrades::insert($data["data"]);
            
			return redirect(url('/admin/coaches-grades'));

	}
	
	public function updateCoachesGrades($data, $id)
	{       
	        
			CoachesGrades::where('id',$id)->update($data["data"]);
            
			return redirect(url('/admin/coaches-grades'));

	}
}

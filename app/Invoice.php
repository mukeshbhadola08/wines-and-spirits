<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    
	 /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'invoices';

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
	/**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    //protected $dateFormat = 'U';
	
    //protected $connection = 'connection-name' //By default, all Eloquent models will use the default database connection configured for your application. If you would like to specify a different connection for the model, use the $connection property:

	/**
     * The attributes that aren mass assignable.
     *
     * @var array
     */
	protected $fillable = ['news_id', 'user_id', 'title', 'price', 'is_paid'];

	/**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['token'];

	public function user()
    {
		return $this->belongsTo('App\User');
	}

	public function updateInvoiceToken($invoiceid, $token){
        $invoice = $this->find($invoiceid);
        $invoice->token = $token;
        $invoice->save();
        return 1;
	}

	public function ServiceRequest(){
		return $this->belongsTo('App\News', 'news_id');	
	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class Contactus extends Model
{

	protected $table = 'contactus';

	public $timestamps = true;

	protected $fillable = ['address', 'email', 'contact','show_contact', 'latitude', 'longitude'];



	public function savecontact($data)
    {
        $this->address = $data['address'];
        $this->contact = $data['contact'];
        $this->email = $data['email'];
        $this->show_contact = $data['show_contact'];
        $this->latitude = $data['latitude'];
        $this->longitude = $data['longitude'];
        $this->save();
        return $this->id;
    }

	public function updateContact($data)
	{
		$contactus = $this->find($data['id']);
        $contactus->address = $data['address'];
        $contactus->contact = $data['contact'];
        $contactus->email = $data['email'];
        $contactus->show_contact = $data['show_contact'];
        $contactus->latitude = $data['latitude'];
        $contactus->longitude = $data['longitude'];
        $contactus->save();
        return $contactus->id;
	}

}

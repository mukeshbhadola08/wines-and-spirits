<?php



namespace App;



use Illuminate\Database\Eloquent\Model;



class HomeConfiguration extends Model

{

    /**

     * The table associated with the model.

     *

     * @var string

     */

    protected $table = 'home_configurations';



	/**

     * Indicates if the model should be timestamped.

     *

     * @var bool

     */

    public $timestamps = true;

	/**

     * The storage format of the model's date columns.

     *

     * @var string

     */

    //protected $dateFormat = 'U';

	

    //protected $connection = 'connection-name' //By default, all Eloquent models will use the default database connection configured for your application. If you would like to specify a different connection for the model, use the $connection property:



	/**

     * The attributes that aren mass assignable.

     *

     * @var array

     */

	protected $fillable = ['option_name', 'option_value'];



	/**

     * The attributes that aren't mass assignable.

     *

     * @var array

     */

    protected $guarded = [];



	public function insertRecord($data){

		$configuration = new HomeConfiguration();

		$configuration->option_name = $data['option_name'];

		$configuration->option_value = $data['option_value'];

		$configuration->save();

		return $configuration->id;

	}



	public function updateRecord($data){

		$blog = $this->find($data['id']);

        $blog->option_name = $data['option_name'];

        $blog->option_value = $data['option_value'];

        $blog->save();

        return 1;

	}

}


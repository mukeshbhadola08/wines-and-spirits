<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogComments extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'blog_comments';

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
	/**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    //protected $dateFormat = 'U';
	
    //protected $connection = 'connection-name' //By default, all Eloquent models will use the default database connection configured for your application. If you would like to specify a different connection for the model, use the $connection property:

	/**
     * The attributes that aren mass assignable.
     *
     * @var array
     */
	protected $fillable = ['user_id'];

	/**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */


	public function userPic()
    {
        return $this->belongsTo('App\UserMetaData', 'user_id')->select(array('value'))->where("key","profile_pic");
    }

    public function blog()
    {
        return $this->belongsTo('App\Blog');
    }
	
	public function blogtitle()
    {
        return $this->belongsTo('App\Blog', 'blog_id')->select('blog_title');
    }
	
	public function user()
    {
        return $this->belongsTo('App\User', 'user_id')->select(array('id','name','email'));
    }

    public function parent()
    {
        return $this->belongsTo('App\BlogComments', 'comment_id');
    }

    public function children()
    {
        return $this->hasMany('App\BlogComments', 'comment_id');
    }
	
	public function addcomment($comment, $blog_id, $parent_id = 0, $status = 0)
	{
		$this->user_id = auth()->user()->id;
		$this->blog_id = $blog_id;
		$this->comment = $comment;
		$this->status = $status;
		$this->comment_id = $parent_id;
		$this->save();
		return $this->id;
    }
    
    public function addGuestComment(
        int $blog_id,
        int $status,
        string $comment,
        string $name,
        string $email,
        int $parent_id = 0
    ): int {
        $this->user_id = 0;
		$this->blog_id = $blog_id;
		$this->comment = $comment;
		$this->guest_name = $name;
		$this->guest_email = $email;
        $this->status = $status;
        $this->comment_id = $parent_id;
		$this->save();
		return $this->id;
    }
	
	public function approvecomment($comment_id)
	{
		$comment = $this->find($comment_id);
		$comment->status = 1;
		$comment->save();
    }
    
    public function updateCommentMessage(
        $comment_id,
        $commentNew
    ) {
        $comment = $this->find($comment_id);
		$comment->comment = $commentNew;
		$comment->save();
    }
	
	
}

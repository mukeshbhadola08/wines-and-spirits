<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMetaData extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_metadata';

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
	/**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    //protected $dateFormat = 'U';
	
    //protected $connection = 'connection-name' //By default, all Eloquent models will use the default database connection configured for your application. If you would like to specify a different connection for the model, use the $connection property:

	/**
     * The attributes that aren mass assignable.
     *
     * @var array
     */
	protected $fillable = ['user_id', 'key', 'value'];

	/**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['value'];

	public function saveMetaData($data){
		$this->user_id = $data[0];
		$this->key = $data[1];
		$this->value = $data[2];
		$this->save();
	}
	public function updateMetaData($data,$id){
		$meta = $this->find($id);
		$meta->user_id = $data[0];
		$meta->key = $data[1];
		$meta->value = $data[2];
		$meta->save();
	}
}

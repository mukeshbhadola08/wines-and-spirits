@include('site.partials.header')
@php
global $languageVariables;
@endphp
<!--section-->
	<section class="log-background">
		<!--container-->
		<div class="container">
			<div class="row ">
				<div class="col-sm-6 col-sm-offset-3 col-xs-12 ">
					<!--background-form-->
					<div class="bg-form">
						<h2>  {{$languageVariables['label']['lbl_login_account']}}</h2>
						<form method="POST" action="{{ route('login') }}" class="login100-form validate-form"> 
						@csrf

						@if ($errors->has('user_blocked_message'))
								<div class="invalid-feedback text-danger">
									<strong>{{ $errors->first('user_blocked_message') }}</strong>
								</div>
						 @endif
						<div class="form-group">
							<div class="input-group input-group-lg">
								 <input class="form-control" type="email" name="email" placeholder="{{$languageVariables['label']['lbl_email_address']}}" aria-describedby="basic-addon1">
								  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope fa-icon" aria-hidden="true"></i></i></span>
								  
							</div>
							@if ($errors->has('email'))
									<span class="invalid-feedback text-danger">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
							 @endif
						</div>
						<div class="form-group">
							<div class="input-group input-group-lg">
								<input type="password" name="password" class="form-control" placeholder="{{$languageVariables['label']['lbl_password']}}" aria-describedby="basic-addon1">
								  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock" aria-hidden="true"></i></span>
							</div>
							@if ($errors->has('password'))
								<span class="invalid-feedback text-danger">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
						</div>
							<div class="text-center">
									<button type="submit" class="btn btn-log-in"> {{$languageVariables['label']['lbl_login']}} </button>
							</div>
						    <div class="forgotlink"><a href="{{ route('password.request') }}"> {{$languageVariables['label']['lbl_forget_password']}} </a></div>
							<div class="loginSignUpSeparator"><span class="textInSeparator">{{$languageVariables['label']['lbl_or']}}</span></div>
							<div class="text-center">
								<a class="btn btn-log-in" href="{{ route('register') }}" >{{$languageVariables['label']['lbl_sign_up']}}</a>
							</div>
						</form>
					</div>
					<!--background-form-->
				</div>
			</div>
		</div>
		<!--/container-->
	</section>
	<!--/section-->

@include('site.partials.footer')
@include('site.partials.header')
@php
global $languageVariables;
@endphp
<section class="log-background">

<!--section-->
	<section class="log-background">
		<!--container-->
		<div class="container">
			<div class="row ">
				<div class="col-sm-6 col-sm-offset-3 col-xs-12 ">
					<!--background-form-->
					<div class="bg-form">
						<h2>  {{$languageVariables['label']['lbl_reset_password']}} </h2>
						<form method="POST" action="{{ route('password.request') }}">
						 @csrf
							 <input type="hidden" name="token" value="{{ $token }}">
							<div class="form-group">
								<div class="input-group input-group-lg">
									 <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{$languageVariables['label']['lbl_email_address']}}" name="email" value="{{ $email ?? old('email') }}" required autofocus>
									  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope fa-icon" aria-hidden="true"></i></i></span>
									  
								</div>
								@if ($errors->has('email'))
									<span class="invalid-feedback text-danger">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								 @endif
							</div>
							<div class="form-group">
								<div class="input-group input-group-lg">
									<input id="password" type="password" placeholder="{{$languageVariables['label']['lbl_new_password']}}" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
									  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock" aria-hidden="true"></i></span>
								</div>
								@if ($errors->has('password'))
									<span class="invalid-feedback text-danger">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif
							</div>
							<div class="form-group">
								<div class="input-group input-group-lg">
									 <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="{{$languageVariables['label']['lbl_confirm_password']}}" required>
									  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock" aria-hidden="true"></i></span>
									   
								</div>
							</div>
							<div class="form-group">
								<button type="submit" class="btn sign-btn"> {{$languageVariables['label']['lbl_reset_password']}} </button>
							</div>
							
						</form>
					</div>
					<!--background-form-->
				</div>
			</div>
		</div>
		<!--/container-->
	</section>
	<!--/section-->

@include('site.partials.footer')

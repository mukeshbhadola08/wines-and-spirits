@include('site.partials.header')
@php
global $languageVariables;
@endphp
<!--section-->
	<section class="log-background">
		<!--container-->
		<div class="container">
			<div class="row ">
				<div class="col-sm-6 col-sm-offset-3 col-xs-12 ">
					<!--background-form-->
					@if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
					<div class="bg-form">
						<h2>  {{$languageVariables['label']['lbl_reset_password']}} </h2>
						<form method="POST" action="{{ route('password.email') }}" >
							 @csrf
							<div class="form-group">
								<div class="input-group input-group-lg">
									 <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{$languageVariables['label']['lbl_email_address']}}" name="email" value="{{ old('email') }}" required>
									  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope fa-icon" aria-hidden="true"></i></i></span>
								</div>
								@if ($errors->has('email'))
                                    <span class="invalid-feedback text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
							</div>
							<div class="form-group">
								<button type="submit" class="btn sign-btn">{{$languageVariables['label']['lbl_send_password_reset_link']}} </button>
							</div>
						</form>
					</div>
					<!--background-form-->
				</div>
			</div>
		</div>
		<!--/container-->
	</section>
	<!--/section-->
@include('site.partials.footer')
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sign Up</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('/') }}dist/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('/') }}dist/css/login.css">
</head>
<body style="background-color: #999999;">
    <div class="login-wrapper">
        
    </div>
    <div class="limiter">
        <div class="container-login100">
            <div class="login100-more" style="background-image: url('<?php echo URL::to('dist/images/login-bg.jpeg');?>');">
                
            </div>
            <div class="wrap-login100">
                <form method="POST" action="{{ route('register') }}" class="login100-form validate-form">
                    <!-- <div class="card-header">{{ __('Register') }}</div> -->

                    <span class="login100-form-title">
                        <img src="<?php echo URL::to('dist/images/logo.png');?>">
                        <span style="float:right">
                            <?php 
                            if($user_type == 'A'){ 
                            echo "Join";
                            }else if($user_type== 'C'){
                            echo "Sign Up";
                            }
                            ?>
                       </span>
                    </span>

                    
                        
                    @csrf

                   
                    <div class="wrap-input100 validate-input" data-validate="Name is required">
                        <span class="label-input100">Full Name</span>
                        <input id="name" class="input100 form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" type="text" name="name" placeholder="Name..." value="{{ old('name') }}" required>
                        <span class="focus-input100"></span>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                       
                    <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                        <span class="label-input100">Email</span>
                        <input id="email" class="input100 form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" name="email" placeholder="Email addess..."  value="{{ old('email') }}" required>
                        <span class="focus-input100"></span>
                        @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                        @endif
                    </div>

                    <div class="wrap-input100 validate-input" data-validate = "Password is required">
                        <span class="label-input100">Password</span>
                        <input id="password" class="input100 form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" placeholder="*************" required>
                        <span class="focus-input100"></span>
                        @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                        @endif
                    </div>

                    <div class="wrap-input100 validate-input" data-validate = "Repeat Password is required">
                        <span class="label-input100">Repeat Password</span>
                        <input id="password-confirm" class="input100 form-control" type="password" name="password_confirmation" placeholder="*************" required>
                        <span class="focus-input100"></span>
                    </div>
                    <!--for adding user-type(Customer,Agent) and block status-->
                    <input id="user_type" type="hidden" class="form-control" name="user_type" value={{$user_type}} required>
                    <input id="block_status" type="hidden" class="form-control" name="block_status" value="0" required>
                    <div class="button-bottom">
                        <div class="wrap-login100-form-btn">                            
                            <button type="submit" class="btn-login-submit">
                                Sign Up
                            </button>
                        </div>

                        <a href="{{ route('login') }}" class="dis-block txt3 hov1 p-r-30 p-t-10 p-b-10 p-l-30">
                            Sign in
                            <i class="fa fa-long-arrow-right m-l-5"></i>
                        </a>
                    </div>
                    <!--/for adding user-type(Customer,Agent) and block status-->

                </form>
                    
            </div>
          
        </div>
    </div>
</body>
</html>


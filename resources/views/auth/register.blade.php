@include('site.partials.header')
@php
global $languageVariables;
@endphp
<!--section-->
	<section class="log-background">
		<!--container-->
		<div class="container">
			<div class="row ">
				<div class="col-sm-6 col-sm-offset-3 col-xs-12 ">
					<!--background-form-->
					<div class="bg-form">
						<h2> {{$languageVariables['label']['lbl_singup_account']}} </h2>
						<form method="POST" action="{{ route('register') }}" class="login100-form validate-form">
						@csrf
						<div class="form-group">
								<div class="input-group input-group-lg">
									 <input type="text" name="name" class="form-control" placeholder="{{$languageVariables['label']['lbl_full_name']}}" aria-describedby="basic-addon1">
									  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user" aria-hidden="true"></i></span>
								</div>
								@if ($errors->has('name'))
									<span class="invalid-feedback text-danger">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
								@endif
							</div>	
							<div class="form-group">
								<div class="input-group input-group-lg">
									 <input type="email" name="email" class="form-control" placeholder="{{$languageVariables['label']['lbl_email_address']}}" aria-describedby="basic-addon1">
									  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope fa-icon" aria-hidden="true"></i></i></span>
								</div>
								@if ($errors->has('email'))
										<span class="invalid-feedback text-danger">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
								@endif
							</div>
							<div class="form-group">
								<div class="input-group input-group-lg">
									<input type="password" name="password" class="form-control" placeholder="{{$languageVariables['label']['lbl_password']}}" aria-describedby="basic-addon1">
									  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock" aria-hidden="true"></i></span>
								</div>
								@if ($errors->has('password'))
										<span class="invalid-feedback text-danger">
											<strong>{{ $errors->first('password') }}</strong>
										</span>
								@endif
							</div>
							<div class="form-group">
								<div class="input-group input-group-lg">
									<input type="password" name="password_confirmation" class="form-control" placeholder="{{$languageVariables['label']['lbl_confirm_password']}}" aria-describedby="basic-addon1">
									  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
								</div>
							</div>
							<div class="form-group">
								<input id="user_type" type="hidden" class="form-control" name="user_type" value="C" required>
								<input id="block_status" type="hidden" class="form-control" name="block_status" value="0" required>
								<button type="submit" class="btn sign-btn"> {{$languageVariables['label']['lbl_sign_up']}} </button>
							</div>
						</form>
					</div>
					<!--/background-form-->
				</div>
			</div>
		</div>
		<!--/container-->
	</section>
	<!--/section-->
@include('site.partials.footer')
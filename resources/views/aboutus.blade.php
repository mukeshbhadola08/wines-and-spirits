@include('site.partials.header')

<!-- intro -->

    <section id="intro" class="clearfix">

        <div class="container">

            <div class="row">

                <div class="col-sm-6 intro-img order-md-first order-last">

                    <img src="images/intro-img-top.png" alt="" class="img-fluid wow fadeInUp">

                </div>

                <div class="col-sm-6 intro-info  wow fadeInDown order-md-last order-first">

                    <h2>About <br>MYRA SAXENA </h2>

                </div>

            </div>



        </div>

    </section>

    <!-- /intro -->

    <!-- main content -->

    <section class="inner-page-section">

        <div class="container">

            <div class="row row-flex wow fadeInDown">

                <!-- Name -->

                <div class="col-sm-4">

                    <div class="features-about">

                        <div class="feature-img">

                            <img src="images/user.svg">                         

                        </div>

                        <div class="feature-content">

                            <h4>Name:</h4>

                            <p>Myra Saxena</p>

                        </div>

                    </div>

                </div>

                <!-- /Name -->

                <!-- Date of birth -->

                <div class="col-sm-4">

                    <div class="features-about">

                        <div class="feature-img">

                            <img src="images/calendar-icon.svg">

                        </div>

                        <div class="feature-content">

                            <h4>Date of Birth:</h4>

                            <p>30 March 2001</p>

                        </div>

                    </div>

                </div>

                <!-- /Date of birth -->             

                <!-- Parents -->

                <div class="col-sm-4">

                    <div class="features-about">

                        <div class="feature-img">

                            <img src="images/parents-icon.svg">

                        </div>

                        <div class="feature-content">

                            <h4>Parents:</h4>

                            <p>Junipia Contento-Suslu and Tim Walther</p>

                        </div>

                    </div>

                </div>

                <!-- /Parents -->

                <!-- Siblings: -->

                <div class="col-sm-4">

                    <div class="features-about">

                        <div class="feature-img">

                            <img src="images/sibling-icon.svg">

                        </div>

                        <div class="feature-content">

                            <h4>Siblings:</h4>

                            <p>Two brothers, Sam and Dan</p>

                        </div>

                    </div>

                </div>

                <!-- /Siblings: -->

                <!-- Address -->

                <div class="col-sm-4">

                    <div class="features-about">

                        <div class="feature-img">

                            <img src="images/location-icon.svg">

                        </div>

                        <div class="feature-content">

                            <h4>Address:</h4>

                            <p>24 SL Road, London. UK</p>

                        </div>

                    </div>

                </div>

                <!-- /Address -->

                <!-- School -->

                <div class="col-sm-4">

                    <div class="features-about">

                        <div class="feature-img">

                            <img src="images/school-icon.svg">

                        </div>

                        <div class="feature-content">

                            <h4>School:</h4>

                            <p>Brunswick High School</p>

                        </div>

                    </div>

                </div>

                <!-- /School -->

                <!-- Grade -->

                <div class="col-sm-4">

                    <div class="features-about">

                        <div class="feature-img">

                            <img src="images/grade-icon.svg">

                        </div>

                        <div class="feature-content">

                            <h4>Grade:</h4>

                            <p>11th (2017-2018), Class of 2019</p>

                        </div>

                    </div>

                </div>

                <!-- /Grade -->

                <!-- GPA -->

                <div class="col-sm-4">

                    <div class="features-about">

                        <div class="feature-img">

                            <img src="images/GPA-icon.svg">

                        </div>

                        <div class="feature-content">

                            <h4>GPA:</h4>

                            <p>4.3 (weighted) </p>

                        </div>

                    </div>

                </div>

                <!-- /GPA -->

                <!-- Awards -->

                <div class="col-sm-4">

                    <div class="features-about">

                        <div class="feature-img">

                            <img src="images/awards-icon.svg">

                        </div>

                        <div class="feature-content">

                            <h4>Awards:</h4>

                            <p>Presidency Award, Student of the Term, Honors, National Honor Society</p>

                        </div>

                    </div>

                </div>

                <!-- /Awards -->

                <!-- Contact -->

                <div class="col-sm-4">

                    <div class="features-about">

                        <div class="feature-img">

                            <img src="images/contact-icon.svg">

                        </div>

                        <div class="feature-content">

                            <h4>Contact:</h4>

                            <p>myraSexena@abc.com</p>

                        </div>

                    </div>

                </div>

                <!-- /Awards -->

                

            </div>

            <!-- /row close -->

            <div class="about-bottom-area">

                <img src="images/about-img.jpg" class="about-img-area wow fadeInLeftBig">

                    <div class="wow fadeInRightBig">

                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. <br><br>

                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

                    </div>

            </div>

            

        </div>

    </section>

    <!-- main content -->

@include('site.partials.footer')


@extends('backpack::layout')

@section('header')
<section class="content-header">
	@if($type == 'winery')
		<h1>Add Winery</h1>
	@elseif($type == 'distellery')
		<h1>Add Distillery</h1>
	@endif
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('pages') }}">Wines & Spirits</a></li>
		<li>Add Partner</li>
	</ol>
</section>
@endsection



@section('content')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<div class="body">
	
	<div class="row">
		<div class="col-sm-12">
			@if($type == 'winery')
				<form method="post" action="{{backpack_url('winesandspirits/create?type=winery')}}" enctype="multipart/form-data">
			@else
				<form method="post" action="{{backpack_url('winesandspirits/create?type=distellery')}}" enctype="multipart/form-data">
			@endif
			
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
			@if($type == 'winery')
				<input type="hidden" name="type" value="winery" readonly>
			@elseif($type == 'distellery')
				<input type="hidden" name="type" value="distillery" readonly>
			@endif
			</div>
			<div id="details">
				<div class="form-group">
					<label for="cat_title">Name:</label>
					<input type="text" class="form-control" name="cat_title" value ="{{ old('cat_title') }}" />
					@if ($errors->has('cat_title'))
						<span class="invalid-feedback text-danger">
							<strong>{{ $errors->first('cat_title') }}</strong>
						</span>
					@endif
				</div>
				
				<div class="form-group">
					<label for="description">Description:</label>
					<textarea name="description" class="form-control" id="page-ckeditor">{{ old('page_description') }}</textarea>
					@if ($errors->has('description'))
						<span class="invalid-feedback text-danger">
							<strong>{{ $errors->first('description') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group images">	
					<label for="image">Logo:</label>
					<input type="file" onchange="$('.added-img').remove();" name="logo" id="upload" />
					<?php if($errors->has('logo')): ?>
						<span class="invalid-feedback text-danger">
							<strong><?php echo e($errors->first('logo')); ?></strong>
						</span>
					<?php endif; ?>
				</div>
				<div class="form-group images">	
					<label for="image">PDF File:</label>
					<input type="file" name="pdf_file" id="pdf_upload" />
					<?php if($errors->has('pdf_file')): ?>
						<span class="invalid-feedback text-danger">
							<strong><?php echo e($errors->first('pdf_file')); ?></strong>
						</span>
					<?php endif; ?>
				</div>
				<div class="form-group">
					<label for="region">Region:</label>&nbsp;
					<input type="text" class="form-control" name="region" value ="{{ old('region') }}" />
					@if ($errors->has('region'))
						<span class="invalid-feedback text-danger">
							<strong>{{ $errors->first('region') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group">
					<label for="country">Country:</label>&nbsp;
					<select name="country_id" id="country">
						<option value="">Select</option>
						@foreach($countries as $country)
							<option value="{{$country->id}}">{{$country->nicename}}</option>
						@endforeach
					</select>
					@if ($errors->has('country_id'))
						<span class="invalid-feedback text-danger">
							<strong>{{ $errors->first('country_id') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group images">	
					<label for="image">Background Image:</label>
					<input type="file" onchange="$('.added-img').remove();" name="background_image" id="upload" />
					<?php if($errors->has('background_image')): ?>
						<span class="invalid-feedback text-danger">
							<strong><?php echo e($errors->first('background_image')); ?></strong>
						</span>
					<?php endif; ?>
				</div>
				<input type ="hidden" name="added_by" value="{{ Auth::user()->id }}" / >
				<button type="submit" class="btn btn-primary">Create</button>	
			</div>
			</form>
		</div>
	</div>

</div>


<script>
	window.onload = function() {
		CKEDITOR.replace( 'page-ckeditor', {
			filebrowserUploadUrl: '{{route("upload",["_token" => csrf_token()])}}'
		});
	};
</script>

@endsection

@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        Spirits
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Spirits</li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
  @if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif

  <br/>
    <div class="row">
    <div class="col-sm-12">
      @if(isset($data) && count($data)>0)
        <table class="table table-striped" id="menu_table">
          <thead>
            <tr>
              <th width="7%"></th>
              <th width="5%">Title</th>
              <th width="5%">Type</th>
              <th width="10%">Descripton</th>
              <th width="5%">Region</th>
              <th width="5%"># of Products</th>
              <th width="5%">Country</th>
              <th width="5%">Logo</th>
              <th width="25%">Action</th>
            </tr>
          </thead>
          <tbody id="nav-menu-body">
            @foreach($data as $dataDetails)
            <tr class="dd-item-tr" data-id="{{$dataDetails->id}}">
                <td class="dd-handle-field"><div style="width:40px; text-align:center;"><i class="fa fa-arrows"></i></div></td>
                <td><a href='{{url("/admin/products/".$dataDetails->id)}}'>{{$dataDetails->cat_title}}</a></td>
                <td>{{$dataDetails->type}}</td>
                <td>{!! str_limit($dataDetails->description, 20) !!}</td>
                <td>{{$dataDetails->region}}</td>
                <td>{{$dataDetails->products->count()}}</td>
                <td>{{$dataDetails->country->nicename}}</td>
                <td><img src="{{URL::to('/')}}/uploads/{{ $dataDetails->logo }}" height="30" width="50"></td>
                <td>
                    <a href="{{action('ProductsController@addProduct',[$dataDetails->id, $dataDetails->type])}}" class="btn btn-success"><i class="fa fa-product-hunt"></i>&nbsp;Add Product</a>
                    <a href="{{action('WinesSpiritsController@edit',$dataDetails->id.'?type=distellery')}}" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>&nbsp;
                    <a href="{{action('WinesSpiritsController@delete',$dataDetails->id.'?type=distellery')}}" onclick="return confirm('Are you sure to delete this distellery and its products?')" class="btn btn-danger delete_page"><i class="fa fa-trash-o"></i>&nbsp;DELETE</a>
                </td>
          </tr>
        @endforeach
        </tbody>
      </table>
      @else
        <div class="alert alert-info">
          There are no spirits added yet.
        </div>
      @endif
    </div>
  </div>
</div>
@endsection
@section('after_scripts')
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('#menu_table').DataTable( {
    "aoColumnDefs" : [
      {
      'bSortable' : false,
      'aTargets' : [ 6, 7 ]
      }
    ]
    });
  $('#menu_table_paginate').addClass('pull-right');
});
$(function() {
  var fieldOrder = [];
  $( "#nav-menu-body" ).sortable({      
    handle: ".dd-handle-field",
    update: function(event, ui) {
        $('.dd-item-tr').each(function(){         
            fieldOrder.push($(this).attr('data-id'));
        });
        $.ajax({
            type:'POST',
            url:'/admin/distillery_order',
            data:{'_token': '<?php echo csrf_token() ?>','order':fieldOrder},
            success:function(cost){
                new PNotify({'text':'Distillery has been re-orders.','type':'success'});
                fieldOrder.length = 0;
            }
        });
    }
  });
});
</script>
@endsection

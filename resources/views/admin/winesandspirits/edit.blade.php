@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Edit Partner</h1>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('pages') }}">Wines & Spirits</a></li>
		<li>Edit Partner</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')

<div class="body">
	
	<div class="row">
		<div class="col-sm-12">
			@if($type == 'winery')
				<form method="post" action="{{action('WinesSpiritsController@update',$id.'?type=winery')}}" enctype="multipart/form-data">
			@else
				<form method="post" action="{{action('WinesSpiritsController@update',$id.'?type=distellery')}}" enctype="multipart/form-data">
			@endif
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="title">Name:</label>
				<input type="text" class="form-control" name="cat_title" value ="{{$edit->cat_title}}" />
				@if ($errors->has('cat_title'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('cat_title') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
				<label for="description">Description:</label>
				<textarea name="description" class="form-control" id="page-ckeditor">{{$edit->description}}</textarea>
				@if ($errors->has('description'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('description') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group images">	
				<label for="image">Logo:</label>
				<input type="file" onchange="$('.added-img').remove();" name="logo" id="upload" value="{{$edit->logo}}" />
				<?php if($errors->has('image')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('image')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group">
				<img src="{{URL::to('/')}}/uploads/{{ $edit->logo }}" height="100" width="200">
			</div>
			<div class="form-group images">	
				<label for="image">PDF File:</label>
				<input type="file" name="pdf_file" id="pdf_upload" value="{{ $edit->pdf_file }}"/>
				<?php if($errors->has('pdf_file')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('pdf_file')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group">
				<label for="region">Region:</label>
				 <input type="text" class="form-control" name="region" value ="{{$edit->region}}" />
				@if ($errors->has('region'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('region') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
				<label for="country">Country:</label>&nbsp;
				<select name="country_id" id="country">
					@foreach($countries as $country)
						<?php 
						$selected = '';
						if($country->id == $edit->country_id){
							$selected = 'selected';
						}
						?>
						<option value="{{$country->id}}" {{$selected}} >{{$country->nicename}}</option>
					@endforeach
				</select>
				@if ($errors->has('country_id'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('country_id') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group images">	
				<label for="image">Background Image:</label>
				<input type="file" onchange="$('.added-img').remove();" name="background_image" id="upload" />
				<?php if($errors->has('background_image')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('background_image')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group">
				<img src="{{URL::to('/')}}/uploads/background_img/{{ $edit->background_image }}" height="100" width="200">
			</div>
			<input type ="hidden" name="added_by" value="{{ Auth::user()->id }}" / >
			<button type="submit" class="btn btn-primary" id="add">Update</button>
			</form>
		</div>
	</div>

</div>


<script>
	window.onload = function() {
		CKEDITOR.replace( 'page-ckeditor', {
			filebrowserUploadUrl: '{{route("upload",["_token" => csrf_token()])}}'
		});
	};
</script>
@endsection

@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Sub Services
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Services  </li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if(isset($success))
        <div class="alert alert-success">
            {{$success}}
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            
				<a href="{{action('ServiceController@addSubService',$id)}}" class="btn btn-primary">Create Sub Service</a>
			
        </div>
    </div>
	<div class="row">
		<div class="col-sm-12">
			@if(isset($services) && count($services)>0)
				<table class="table table-striped">
					<thead>
						<tr>
							<th> Image </th>
							<th>Title</th>
							<th width="25%">Description</th>
							<th>Summery</th>
							<th>cost</th>
							<th>visibility</th>
							<th width="25%">Action</th>
						</tr>
					</thead>
					<tbody>
						
					@foreach($services as $ser)
						<tr>
							<td>							
									
								<img src="{{action('ServiceController@getServiceImage',$ser->id)}}" width="50" /> 
								
							</td>
							<td>{{$ser->name}}</td>
							<td>{{$ser->description}}</td>
							<td>{{$ser->summery}}</td>
							<td>{{$ser->cost}}</td>
							<td>
								@if($ser->is_visible == 1)
									Yes
								@else
									No
								@endif
							</td>
							<td><a href="{{action('ServiceController@serviceCustomForms',$ser->id)}}" class="btn btn-info">custom Forms</a>&nbsp;<a href="{{action('ServiceController@editService',$ser->id)}}" class="btn btn-primary">Edit</a>&nbsp;&nbsp;
							<a href="{{action('ServiceController@deleteService',$ser->id)}}" class="btn btn-danger delete_service">DELETE</a> </td>
						</tr>
					@endforeach	
					</tbody>
				</table>
			
			@endif

		</div>
	</div>
</div >
@endsection

@section('after_scripts')
<script>		 
jQuery(document).on('click','.delete_service',function(e){
	if(confirm("Are your sure all related information also be deleted?")){
		return true;
	} else{
		return false;
	}
});		
</script>
@endsection
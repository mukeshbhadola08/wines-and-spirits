@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Services Request
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">ServicesRequest</li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	<div class="row">
		<div class="col-sm-12">

			<div class="row">
				<div class="col-sm-3">
					<strong>Service:</strong>
				</div>
				<div class="col-sm-9">
					{{$serviceRequestDetails->service->name}}
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					<strong>Photograph:</strong>
				</div>
				<div class="col-sm-9">
					@php
						$metadataDetails = $serviceRequestDetails->service_request_data;
					@endphp
					@if(count($metadataDetails)>0)
						@if(!empty($metadataDetails->where('key','photograph')->first()))
						<img src="{{action('ServiceRequestController@getRequestPhotograph',$metadataDetails->where('key','photograph')->first()->id)}}" width="50" /> 
						@endif

					@endif	
				</div>
				
			</div>
			<div class="row">
				<div class="col-sm-3">
					<strong>Description:</strong>
				</div>
				<div class="col-sm-9">
					@php
						if(count($metadataDetails)>0){
							if(!empty($metadataDetails->where('key','description')->first())){
								echo($metadataDetails->where('key','description')->first()->value);
							}
						}
					@endphp
				</div>
			</div>
			@if(count($metadataDetails)>0)
				@php
					$mataFormData = App\ServiceRequestMetaData::where([['service_rquest_id','=',$serviceRequestDetails->id],['is_custom_data','=','1']])->get();					
				@endphp
				@if(count($mataFormData)>0)
					@foreach($mataFormData as $request)
						<div class="row">
						@php
						$metadataForm = App\ServicesCustomForms::find($request->key);
						@endphp
						@if($metadataForm['field_type'] == 'file')
							@php
							$fileArray=unserialize($request->value);
							@endphp
							<div class="col-sm-3">
								<strong>{{$metadataForm['field_label']}}:</strong>
							</div>
							<div class="col-sm-9">
								{{stripslashes($fileArray['original_name'])}}
							</div>
						@else 
							<div class="col-sm-3">
								<strong>{{$metadataForm['field_label']}}:</strong>
							</div>
							<div class="col-sm-9">
								{{stripslashes($request->value)}}
							</div>
						@endif
						</div>
					@endforeach
				@endif
			@endif			
		</div>
	</div>

</div>

@endsection


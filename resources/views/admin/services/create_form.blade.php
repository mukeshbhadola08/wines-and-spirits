@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Create Custom Form
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Create Custom Form </li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
		@foreach($errors->all() as $error)
			<li>{{$error}}</li>
		@endforeach
		</ul>
	</div><br />
	@endif
	<div class="row">
		<div class="col-sm-12">
			<form class="form-horizontal" method="post" action="{{url('/admin/save_custom_form')}}" enctype="multipart/form-data">
				<div class="form-group">
					<input type="hidden" value="{{csrf_token()}}" name="_token" />
					<label class="col-sm-2" for="field_label">Field Label:</label>
					<div class="col-sm-6">
					<input type="text" class="form-control" name="field_label" />
					</div>
				</div>
				<div class="form-group">
				
					<label class="col-sm-2" for="field_type">Field Type:</label>
					<div class="col-sm-6">
						<select name="field_type" class="form-control field_type" >
						
								<option value="text" >Input</option>
								<option value="checkbox">Check Box</option>
								<option value="radio" >Radio</option>
								<option value="file" >File</option>
								<option value="select" >Dropdown</option>
								<option value="textarea" >Textarea</option>
											
						</select>
					</div>
					
				</div>
				<div class="form-group" id="field_options" style="display:none;" >
				<label class="col-sm-2" for="field_type">Field Options:</label>
				<div class="col-sm-6">
					<table width="100%" id="option_table">
						<tr class="option_field">
							<td width="5%">
							<a href="javascript:;" class="btn-sm btn-danger option_delete"><i class="fa fa-trash-o"></i></a>&nbsp;</td>
							<td width="20%"><input type="text" name="option_label[]" class="form-control value_field" placeholder="Value Label"></td>
							<td width="20%"><label style="cursor:pointer;margin-left:5px;"><input type="checkbox"   checked> Visible</label>
							<input type="hidden" name="option_visible[]" class="option_visible" value="1" >
							</td>
										
						</tr> 
						<tr class="clone_option" style="display:none;">
							<td width="5%">
							<a href="javascript:;" class="btn-sm btn-danger option_delete"><i class="fa fa-trash-o"></i></a>&nbsp;</td>
							<td width="20%"><input type="text" name="option_label[]" class="form-control value_field" placeholder="Value Label" disabled></td>
							<td width="20%"><label style="cursor:pointer;margin-left:5px;"><input type="checkbox" class="vis_check" checked > Visible</label>
							<input type="hidden" name="option_visible[]" class="option_visible" value="1" disabled > </td>
										
						</tr> 
					
					</table>
					
						<button class="btn btn-primary btn-sm" id="add_options"> Create New Option</button>                             
					
					</div>
				</div>
				<div class="form-group">
					
					<label  class="col-sm-2" for="visible">Visible:</label>
					<div class="col-sm-6">
					<input type="checkbox"  name="visible" value="1" />
					</div>
				</div>

				<div class="form-group">
				<div class="col-sm-2"></div>
				<div class="col-sm-6">
				<input type ="hidden" name="service" value="{{ $id }}" / >
				
				<button type="submit" class="btn btn-primary">Create Form</button>
				</div>
				</div>
			</form>
		</div>
	</div>
	
</div >
@endsection
@section('after_scripts')
<script>

		 jQuery(document).on('change','.field_type',function(){
			var type= jQuery(this).val();
			if(type == 'radio' || type == 'checkbox' || type == 'select'){
				jQuery('#field_options').show();
			} else {
				jQuery('#field_options').hide();
			}
		}); 
		jQuery(document).on('click','#add_options',function(e){
			e.preventDefault();
			var clonnedField = $('.clone_option').clone(true);
			clonnedField.removeClass('clone_option').addClass('option_field');
			clonnedField.find('.value_field').removeAttr('disabled');
			clonnedField.find('.option_visible').removeAttr('disabled');
			clonnedField.show();
			clonnedField.insertBefore('.clone_option');

		});
		jQuery(document).on('click','.option_delete',function(e){
			e.preventDefault();
			$(this).closest('tr').remove();
		});
		jQuery(document).on('click','.vis_check',function(e){
			var check = $(this);
			var vis_input = check.closest('tr').find('.option_visible');
			if(check.is(':checked')){
				vis_input.val(1);
			} else {
				vis_input.val(0);
			}
		});
</script>
@endsection

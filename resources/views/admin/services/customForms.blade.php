@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Custom Forms
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Custom Forms </li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            
				<a href="{{action('ServiceController@create_form',$id)}}" class="btn btn-primary">Create Custom Forms</a>
			
        </div>
    </div>
	<div class="row">
		<div class="col-sm-12">
			@if(isset($customForms) && count($customForms)>0)
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Field label</th>
							<th>Field Type</th>
							<th>Visibility</th>
							<th width="20%">Action</th>
						</tr>
					</thead>
					<tbody>
						
					@foreach($customForms as $form)
						<tr>
							<td>{{$form->field_label}}</td>
							<td>
							@if($form->field_type=='text')
								Input
							@elseif($form->field_type=='select')
								Dropdown
							@elseif($form->field_type=='checkbox')
								Check Box
							@else
								{{ucfirst($form->field_type)}}
							@endif
							</td>
							<td>
								@if($form->is_visible == 1)
									Yes
								@else
									No
								@endif
							</td>
							<td><a href="{{action('ServiceCustomFormController@edit_form',$form->id)}}" class="btn btn-primary">EDIT</a>&nbsp;<a href="{{action('ServiceCustomFormController@deleteform',$form->id)}}" class="btn btn-danger">DELETE</a></td>
						</tr>
					@endforeach	
					</tbody>
				</table>
			@else
			<div class="text-center">No Form Added.</div>
			@endif
		</div>
	</div>
</div >
@endsection

@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Services Request
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">ServicesRequest</li>
      </ol>
    </section>
@endsection


@section('content')

<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
	<hr>

	<div class="row">
		<div class="col-sm-12">
			@if(isset($data) && count($data)>0)
			
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
						  <th>Service Name</th>
						  <th>Total Cost</th>
						  <th>Requested by </th>
						  <th>Requested Date </th>
						  <th>Awarded </th>
						  <th>Action</th>
						  <th>Status</th>
						</tr>
					</thead>
					<tbody>
						
					@foreach($data as $each)
						<tr>							
							<td>{{$each->service->name}}</td>
							<td>{{$each->total_cost }}</td>
							<td>{{$each['user']->name}}</td>
							<td>{{ date('d-m-Y H:i', strtotime($each->created_at))}}</td>
							<td>
								
								@php
									$allAgents = App\AgentService::where('service_id',$each->service_id)->get();
								@endphp
								<form class="assignAgentForm" method="post" action="{{url('/admin/setServiceAgent')}}">
								@csrf
								<select class="award_request" name="agent" data-id="{{$each->id}}" >
									<option value="0">Select Agent </option>
									@if(count($allAgents)>0)
										@foreach($allAgents as $agent)
											<option value="{{$agent->user->id}}" @if($each->agent_id==$agent['user']->id) selected="selected"  @endif > {{$agent['user']->name}}</option>
											
										@endforeach
									@endif

								</select>
								<input type="hidden" name="service" value="{{$each->id}}" />
								</form>
							</td>
							<td>
								<a href="{{action('ServiceRequestController@getServiceRequestDetails',$each->id)}}" class="btn btn-success" target="_blank"><i class="fa fa-eye"></i>&nbsp;View</a>&nbsp;
								<a href="{{action('MessagesController@chatsbyservice',$each->id)}}" class="btn btn-info"><i class="fa fa-comment"></i>&nbsp;Chat</a>&nbsp;
								
							</td>
							<td>
								@if($each->status=='1')
									Done
								@else
									<a href="#" class="btn btn-primary request_process" data-id="{{$each->id}}">Process</a>
								@endif
							</td>
						</tr>
					@endforeach	
					</tbody>
				</table>
			@else
			<div class="alert alert-info">
				There is no service request found.
			</div>
			@endif
		</div>
	</div>

</div>

@endsection

@section('after_scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "order": [[ 0, "desc" ]]
    });
});
$(document).on('click','.request_process',function(){
	var id = $(this).data("id");
	var obj = $(this);
	$.ajax({
	   type:'POST',
	   url:'/admin/updateStatusRequest',
	   data:{'_token': '<?php echo csrf_token() ?>','service':id},
	   success:function(response){
		 obj.parent().closest('td').html(response);
	   }
	});
	
});
$(document).on('change','.award_request',function(){
	$(this).closest('form').submit();
	/* var request = $(this).data('id');
	var agent  = $(this).val();
	 $.ajax({
	   type:'POST',
	   url:'/admin/setServiceAgent',
	   data:{'_token': '<?php echo csrf_token() ?>','service':request,'agent':agent},
	   success:function(response){
	   }
	}); */

});
</script>
@endsection




@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Edit Custom Form
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Create Custom Form </li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
		@foreach($errors->all() as $error)
			<li>{{$error}}</li>
		@endforeach
		</ul>
	</div><br />
	@endif
	<div class="row">
		<div class="col-sm-12">
			<form class="form-horizontal" method="post" action="{{action('ServiceCustomFormController@updateCustomForm',$customform->id)}}" enctype="multipart/form-data">
				{{csrf_field()}}
				<div class="form-group">
					<input type="hidden" value="{{csrf_token()}}" name="_token" />
					<label class="col-sm-2" for="field_label">Field Label:</label>
					<div class="col-sm-6">
					<input type="text" class="form-control" name="field_label" value="{{$customform->field_label}}" />
					</div>
				</div>
				<div class="form-group">
				
					<label class="col-sm-2" for="field_type">Field Type:</label>
					<div class="col-sm-6">
						<select name="field_type" class="form-control field_type" >
								<option value="" >Select Option</option>
								<option value="text" {{ $sel=($customform->field_type =='text')?'selected':''}} >Input</option>
								<option value="checkbox" {{ $sel=($customform->field_type =='checkbox')?'selected':''}} >CheckBox</option>
								<option value="radio" {{ $sel=($customform->field_type =='radio')?'selected':''}} >Radio</option>
								<option value="file" {{ $sel=($customform->field_type =='file')?'selected':''}} >File</option>
								<option value="select" {{ $sel=($customform->field_type =='select')?'selected':''}} >Dropdown</option>
								<option value="textarea" {{ $sel=($customform->field_type =='textarea')?'selected':''}} >Text Area</option>
											
						</select>
					</div>
					
				</div>
				<div class="form-group" id="field_options" style="{{ $display = ($customform->field_type == 'select' || $customform->field_type == 'radio' || $customform->field_type == 'checkbox')?'':'display:none;'}}" >
				<label class="col-sm-2" for="field_type">Field Options:</label>
				<div class="col-sm-6">
					<table width="100%" id="option_table">
						@if(count($formOptions)>0)
							@foreach($formOptions as $option)
							<tr class="option_field">
							<td width="5%">
							<a href="javascript:;" class="btn-sm btn-danger option_delete" option="{{$option->id}}"><i class="fa fa-trash-o"></i></a>&nbsp;</td>
							<td width="20%"><input type="text" name="option_label[{{$option->id}}]" class="form-control value_field" placeholder="Value Label" value="{{$option->field_value}}"></td>
							<td width="20%"><label style="cursor:pointer;margin-left:5px;"><input type="checkbox"   {{$ck=($option->is_visible='1')?'checked':''}}> Visible</label>
							<input type="hidden" name="option_visible[{{$option->id}}]" class="option_visible" value="{{$option->is_visible}}" >
							</td>
							@endforeach			
						</tr> 
						@else
						<tr class="option_field">
							<td width="5%">
							<a href="javascript:;" class="btn-sm btn-danger option_delete" option="0"><i class="fa fa-trash-o"></i></a>&nbsp;</td>
							<td width="20%"><input type="text" name="option_label[0][]" class="form-control value_field" placeholder="Value Label"></td>
							<td width="20%"><label style="cursor:pointer;margin-left:5px;"><input type="checkbox"   checked> Visible</label>
							<input type="hidden" name="option_visible[0][]" class="option_visible" value="1" >
							</td>
										
						</tr> 
						@endif						
						<tr class="clone_option" style="display:none;">
							<td width="5%">
							<a href="javascript:;" class="btn-sm btn-danger option_delete" option="0"><i class="fa fa-trash-o"></i></a>&nbsp;</td>
							<td width="20%"><input type="text" name="option_label[0][]" class="form-control value_field" placeholder="Value Label" disabled></td>
							<td width="20%"><label style="cursor:pointer;margin-left:5px;"><input type="checkbox" class="vis_check" checked > Visible</label>
							<input type="hidden" name="option_visible[0][]" class="option_visible" value="1" disabled > </td>
										
						</tr> 
					
					</table>
					
						<button class="btn btn-primary btn-sm" id="add_options"> Create New Option</button>                             
					
					</div>
				</div>
				<div class="form-group">
					
					<label  class="col-sm-2" for="visible">Visible:</label>
					<div class="col-sm-6">
					@if($customform->is_visible == 1)
						<input type="checkbox"  name="visible" value="1" checked="true" />
					@else
						<input type="checkbox"  name="visible" value="1"  />
					@endif
					</div>
				</div>

				<div class="form-group">
				<div class="col-sm-2"></div>
				<div class="col-sm-6">
				<input type ="hidden" name="service" value="{{ $customform->service_id }}" / >
				<input type ="hidden" name="detete_option" value="" id="delete_option" / >
				<button type="submit" class="btn btn-primary">Update Form</button>
				</div>
				</div>
			</form>
		</div>
	</div>
	
</div >
@endsection
@section('after_scripts')
<script>

		 jQuery(document).on('change','.field_type',function(){
			var type= jQuery(this).val();
			if(type == 'radio' || type == 'checkbox' || type == 'select'){
				jQuery('#field_options').show();
			} else {
				jQuery('#field_options').hide();
			}
		}); 
		jQuery(document).on('click','#add_options',function(e){
			e.preventDefault();
			var clonnedField = $('.clone_option').clone(true);
			clonnedField.removeClass('clone_option').addClass('option_field');
			clonnedField.find('.value_field').removeAttr('disabled');
			clonnedField.find('.option_visible').removeAttr('disabled');
			clonnedField.show();
			clonnedField.insertBefore('.clone_option');

		});
		jQuery(document).on('click','.option_delete',function(e){
			e.preventDefault();
			var op_id = $(this).attr('option');
			var del_options = $('#delete_option').val();
			if(op_id>0){
				if(del_options ==''){
					del_options = op_id
				} else {
					del_options = del_options + ',' + op_id;
				}
				$('#delete_option').val(del_options);
			}
			$(this).closest('tr').remove();
		});
		jQuery(document).on('click','.vis_check',function(e){
			var check = $(this);
			var vis_input = check.closest('tr').find('.option_visible');
			if(check.is(':checked')){
				vis_input.val(1);
			} else {
				vis_input.val(0);
			}
		});
</script>
@endsection

@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Pending Comments
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Pending Comments</li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif

	<div class="row">
		<div class="col-sm-12">
			@if(isset($pendingcomments) && count($pendingcomments)>0)
				<table class="table table-striped">
					<thead>
						<tr>
						  <th width="22%">Blog Title</th>
						  <th width="78%">Comment</th>
						  <th width="10%">Action</th>
						</tr>
					</thead>
					<tbody>
						
					@foreach($pendingcomments as $pendingcomment)
							
							
						<tr>
							<td>{{$pendingcomment->blogtitle->blog_title}}</td>
							<td>{{$pendingcomment->comment}}</td>
							<td>							
								<a href="{{action('BlogController@approvecomment',$pendingcomment->id)}}" class="btn btn-success">Approve</a>
								
							</td>
						</tr>
					@endforeach	
					</tbody>
				</table>
			@else
			<div class="alert alert-info">
				There is no Pending Comment on the Blog.
			</div>
			@endif
		</div>
	</div>

</div >
@endsection

@extends('backpack::layout')

@section('header')
<section class="content-header">
    <h1>
        Blog Comments
    </h1>
</section>
@endsection


@section('content')
<div class="body">
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-striped" id="blogs_comment_table">
                <thead>
                    <tr>
                        <th>Blog Title</th>
                        <th>Comment</th>
                        <th>By Name</th>
                        <th>By Email</th>
                        <th>User Type</th>
                        <th>Status</th>
                        <th>Posted At</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('after_scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/') }}dist/js/bootbox.min.js"></script>
<link href="{{ asset('/') }}dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<script src="{{ asset('/') }}dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script>
    $(document).ready(function() {
        var oTable = $('#blogs_comment_table').DataTable({
            "oLanguage": {
                "sSearch": "",
                "sEmptyTable": "No Record's Found For your Query!",
                'sLengthMenu': '<select class="search-select form-control" id="lx23">' +
                    '<option value="10">Show 10</option>' +
                    '<option value="20">Show 20</option>' +
                    '<option value="50">Show 50</option>' +
                    '<option value="-1">Show All</option>' +
                    '</select>'
            },
            "bProcessing": false,
            "bServerSide": true,
            'iDisplayLength': 10,
            'sPaginationType': 'full_numbers',
            "bPaginate": true,
            'sErrMode': 'none',
            "sAjaxSource": '{{URL::to("admin/commentListings")}}',
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "_token",
                    "value": 'token_value'
                });
            },
            "aaSorting": [],
            "aoColumns": [{
                    mData: 'blog_title',
                    sWidth: "40px",
                },
                {
                    mData: 'comment',
                    sWidth: "150px"
                },
                {
                    mData: 'name',
                    sWidth: "150px"
                },
                {
                    mData: 'email',
                    sWidth: "150px"
                },
                {
                    mData: 'user_type',
                    sWidth: "150px"
                },
                {
                    mData: 'status',
                    sWidth: "150px"
                },
                {
                    mData: 'created_at',
                    sWidth: "150px"
                },
                {
                    mData: 'action',
                    sWidth: "150px",
                    bSortable: false
                }
            ],
            "fnInfoCallback": function(
                oSettings,
                iStart,
                iEnd,
                iMax,
                iTotal,
                sPre
            ) {
                editableCallback();
                iStart = iTotal > 0 ? iStart : 0;
                return "Showing " + iStart + " to " + iEnd + " of " + iTotal + " entries";
            }
        });

        $(document).on('change', '.commentActions', function() {
            if (($(this).val()).length) {
                let actionText = $('option:selected', this).text();
                let commentToBeActioned = $(this).data('refrance');
                let commentAction = $(this).val();
                bootbox.confirm({
                    title: "Comment Action?",
                    message: "Please confirm you want to "+actionText+" this comment!",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancel'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Confirm'
                        }
                    },
                    callback: function(result) {
                        if (result) {
                            $.ajax({
                                type: 'post',
                                url: '{{url("/commentactions")}}',
                                data: {
                                    commentAction: commentAction,
                                    commentToBeActioned: commentToBeActioned
                                },
                                dataType: 'json',
                                success: function() {
                                    oTable.ajax.reload();
                                },
                                error: function(xhr, statu, error) {
                                    console.log('error in script');
                                }
                            });
                        }
                    }
                });
            }
        });

    $.fn.editable.defaults.mode = 'inline';
    function updateCommentValue(newValue, commentIs)
    {
        $.ajax({
            type: 'post',
            url: '{{url("/commentValUpdate")}}',
            data: {commentVal:newValue, comment:commentIs},
            dataType: 'json',
            success: function() {
                console.log('success');
            },
            error: function(xhr, statu, error) {
                console.log('error in script');
            }
        });
    }
    function editableCallback()
    {
        $(document).find('a.commentEditable').editable({
            type: 'text',
            step: 'any',
            title: 'Enter New Value',
            success: function(response, newValue) {
                let commentIs = $(this).data('modify_comment');
                if ($.trim(newValue).length < 1) {
                    alert('Please add some text before update comment!');
                    return false;
                }
                var cnfrm_mesg =
                        "Please Confirm you want to change comment";
                bootbox.confirm(cnfrm_mesg, function(result) {
                    if (result) {
                        updateCommentValue(
                            newValue,
                            commentIs
                        );
                    }
                });
            }
        });
    }
});
</script>
@endsection
@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Blog
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Blog</li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12"><a href="{{url('/admin/blogs/create')}}" class="btn btn-primary">Add Blog</a></div>
    </div><hr>

	<div class="row">
		<div class="col-sm-12">
			@if(isset($blogs) && count($blogs)>0)
				<table class="table table-striped" id="blogs-table">
					<thead>
						<tr>
						  <th width="10%">Image</th>
						  <th width="10%">Slug</th>
						  <th width="15%">Title</th>
						  <th width="10%">
							  <i class="fa fa-comments"></i>U/T</th>
						  <th width="15%">Summary</th>
						  <th width="7%" class="hidden">Visible</th>
						  <th width="8%" class="hidden">Dutch</th>
						  <th width="25%">Action</th>
						</tr>
					</thead>
					<tbody>
						
					@foreach($blogs as $blogDetails)
						<tr>
							<td>
								<img class="media-object" src="{{action('BlogController@getImage',$blogDetails->id)}}" alt="no-img" height="100" width="100">
							</td>
							<td>{{$blogDetails->clean_url}}</td>
							<td>{{$blogDetails->blog_title}}</td>
							<td>{{count($blogDetails->comments->filter(function($item){
				return $item->status == 0;
			}))}}/{{count($blogDetails->comments)}}</td>
							<td>{!! str_limit($blogDetails->blog_description, 20) !!}</td>
							<td class="hidden">@if($blogDetails->is_visible==1) YES @else No @endif</td>
							<td class="hidden">
								@php
								 $parentDetails = App\Blog::where('parent_lang_id',$blogDetails->id)->first();
								@endphp
								@if(empty($parentDetails))
									<a href="{{action('BlogController@addInSpanish',$blogDetails->id)}}" class="btn btn-info" ><i class="fa fa-language" ></i>&nbsp;Add Dutch</a>
								@else
									<a href="{{action('BlogController@edit',$parentDetails->id)}}" class="btn btn-info"><i class="fa fa-language" ></i>&nbsp;Edit Dutch</a>
								@endif
							</td>
							<td>
								<a href="{{action('BlogController@view',$blogDetails->clean_url)}}" class="btn btn-success" target="_blank"><i class="fa fa-eye"></i>&nbsp;View</a>&nbsp;
								<a href="{{action('BlogController@edit',$blogDetails->id)}}" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>&nbsp;								
								<a href="{{action('BlogController@delete',$blogDetails->id)}}" class="btn btn-danger delete_blog"><i class="fa fa-trash-o"></i>&nbsp;DELETE</a>
								
							</td>
						</tr>
					@endforeach	
					</tbody>
				</table>
			@else
			<div class="alert alert-info">
				There are no blogs added yet.
			</div>
			@endif
		</div>
	</div>

</div >
@endsection

@section('after_scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('#blogs-table').DataTable( {
		"columnDefs": [ {
		"targets": [0, 1, 2, 5],
		"orderable": false,
		}],
    });
	$('#blogs-table_paginate').addClass('pull-right');
});	
jQuery(document).on('click','.delete_blog',function(e){
	if(confirm("Are you sure want to delete this blog?")){
		return true;
	} else{
		return false;
	}
});		
</script>
@endsection

@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Add Dutch Blog</h1>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('blogs') }}">Blogs</a></li>
		<li>Add Blog</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">
	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{action('BlogController@storeInlang',$id)}}" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="title">Blog Title:</label>
				<input type="text" class="form-control" name="blog_title" value="{{$blog->blog_title}}"/>
			</div>
			<div class="form-group">
			
				<label for="permalink">Permalinks:</label>
				<input type="text" class="form-control" name="permalink" required />
			</div>
			<div class="form-group">
				<label for="description">Description:</label>
				<textarea name="blog_description" class="form-control" id="blog-ckeditor">{{$blog->blog_description}}</textarea>
			</div>

			<div class="form-group">
				<label for="description">Blog Summary</label>
				<textarea name="short_description" class="form-control">{{$blog->short_description}}</textarea><i>This summary will be used in the blog listing</i>
			</div>

			<div class="form-group">	
				<label for="image">Old Image:</label>
				<img class="media-object" src="{{action('BlogController@getImage',$blog->id)}}" alt="no-img" height="150" width="150">
			</div>

			<div class="form-group">	
				<label for="image">Featured Image:</label>
				<input type="file" class="form-control" name="blog_image" id="upload" />
			</div>


			<input type="hidden" name="is_visible" value="1">

			<input type ="hidden" name="added_by" value="{{ Auth::user()->id }}" / >
			<input type ="hidden" name="lang_code" value="es" / >
			<input type ="hidden" name="parent" value="{{$id}}" / >
			<button type="submit" class="btn btn-primary" id="add">Create</button>
			</form>
		</div>
	</div>

</div >
<script>
	CKEDITOR.replace( 'blog-ckeditor' );
</script>

@endsection

@section('after_scripts')
<script>
$(function () {
	$("#upload").on("change", function () {
		 var file, img;
		if ((file = this.files[0])) {
			if ( (/\.(png|jpeg|bmp|jpg|gif)$/i).test(file.name) ) {
				
				$('#add').removeAttr('disabled');
				
		  } else {
			errors = file.name +" Unsupported Image extension.";  
			new PNotify({'text':errors,'type':'error'});
				 $('#add').attr('disabled','disabled');
		  }
			
		}
	});
});
		 
</script>
@endsection

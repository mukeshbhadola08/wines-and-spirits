@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Add Blog</h1>
	<div class="alert alert-info" style="margin-bottom: 0;">
	    <strong>Note: </strong>
	    Please note that recommended dimension of image is 700 by 350 or 350 by 350 pixels.
	</div>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('blogs') }}">Blog</a></li>
		<li>Add Blog</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">

	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{backpack_url('blogs/create')}}" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="title">Blog Title:</label>
				<input type="text" class="form-control" name="blog_title" value="{{ old('blog_title') }}"/>
				@if ($errors->has('blog_title'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('blog_title') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
			
				<label for="permalink">Permalinks:</label>
				<input type="text" class="form-control" name="permalink" value="{{ old('permalink') }}" />
				@if ($errors->has('permalink'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('permalink') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
				<label for="description">Description:</label>
				<textarea name="blog_description" class="form-control" id="blog-ckeditor">{{ old('blog_description') }}</textarea>
				@if ($errors->has('blog_description'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('blog_description') }}</strong>
					</span>
				@endif
			</div>

			<div class="form-group">
				<label for="description">Blogs Summary</label>
				<textarea name="short_description" class="form-control">{{ old('short_description') }}</textarea><i>This summary will be used in the Blogs listing</i>
				@if ($errors->has('short_description'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('short_description') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group images">	
				<label for="image">Add Image:</label>
				<input type="file" onchange="$('.added-img').remove();" name="image" id="upload" />
				<?php if($errors->has('image')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('image')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group hidden">
				
				<input type="checkbox" name = "is_visible" value="1" checked />&nbsp;
				<label for="is_visible">Is Visible</label>
			</div>

			<input type ="hidden" name="added_by" value="{{ Auth::user()->id }}" / >
			<input type ="hidden" name="lang_code" value="en" / >
			<input type ="hidden" name="parent" value="0" / >
			<button type="submit" class="btn btn-primary" id="add">Create</button>
			</form>
		</div>
	</div>

</div >
<!-- The Modal -->
<div class="modal" id="portfolioModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Photo</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" style="display: block;
    float: left;">
        <ul style="list-style: none;"></ul>
      </div>

      <!-- Modal footer -->

      <div class="modal-footer">
      	 <button id="save"disabled onclick="save()" class="btn btn-width bkgrnd-cyan save-details" type="button" name="save-details">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
       
      </div>

    </div>
  </div>
</div>
<script>
	window.onload = function() {
		CKEDITOR.replace( 'blog-ckeditor', {
			filebrowserUploadUrl: '{{route("upload",["_token" => csrf_token()])}}'
		});
	};
</script>

@endsection
@section('after_scripts')
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style type="text/css">
	.custom-border{
		 border: 1px solid #57549C !important;
	}
	.border{
		border: 1px solid transparent;
	}
</style>

<script>
  
  function save(){
        var imageDIV = $('.images');
        imageDIV.find('.added-img').remove();

        var imagurl = $( "#portfolioModal" ).find('span.custom-border img').attr('src');
        var imagid = $( "#portfolioModal" ).find('span.custom-border img').data('id');

        var image = "<div class='added-img' style='margin-top:10px;'><img class=\"img-responsive\" style=\"width: 150px;height: 150px; \" src='"+imagurl+"'><input type='hidden' name='image' value='"+imagid+"'/></div>";
        imageDIV.html(image);

        $("#upload").val(''); 
        $( "#portfolioModal" ).modal('hide');
    
        $( "#portfolioModal" ).find('.modal-body ul').empty();  	     
  }


  jQuery(function() {

  	$( "#portfolioModal" ).on('hidden.bs.modal', function(){

      $( "#portfolioModal .modal-footer #save").attr("disabled", 1);
      $( "#portfolioModal .modal-footer #save").removeClass('btn-primary').addClass('bkgrnd-cyan');

    });

    $( "#portfolioModal" ).on('shown.bs.modal', function(){
      $( "#portfolioModal" ).find('.modal-body ul').empty();
	     jQuery.ajax({
         type:'POST',
         url:'/admin/getportfolios',
         data:'_token = <?php echo csrf_token(); ?>',
         success:function(res) {
         	 $.each(res.data, function(i){
	            $( "#portfolioModal" ).find('.modal-body ul').append("<li class='inline-block pull-left'><span style='display:block;margin: 4px;' class='border'><img onclick=\"$( this ).closest('ul').find('span').removeClass('custom-border'); $(this).parent().addClass('custom-border'); $(this).closest('.modal-content').find('.modal-footer #save').removeAttr('disabled').removeClass('bkgrnd-cyan').addClass('btn-primary'); \" data-id='"+res.data[i].id+"'class='img-responsive' style='width: 150px;height: 150px;     margin: 8px; cursor:pointer;' src='"+res.data[i].url+"'></span></li>");
	        });    
          
         }
      
      });
	});
});

</script>
@endsection

@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Transactions Chart
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Transactions  </li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
	<div class="row">
		<div class="col-sm-3">
			<form id="filterchartForm" class="form-horizontal" action="{{url('/admin/getSalesChart')}}" method="post">
			<input type="hidden" value="{{csrf_token()}}" name="_token" />
			<select id="select_year" class="form-control" name="year">
				<option value="">Select Year</option>
				@php
				  $year = isset($year)?$year:date('Y');	
				@endphp
				@for($i=2016;$i<=2030;$i++)
					<option value="{{$i}}" @if($i==$year) selected="selected" @endif >{{$i}}</option>
				@endfor
			</select>
			</form>
		</div>
	</div><br />
	<div class="row">
		<div class="col-sm-12">
			<div id="chart1"></div>
			{!! $chart1 !!}
		</div>
	</div>
</div >
@endsection
@section('after_scripts')
<script>
	$(document).on('change','#select_year',function(){	
		$('#filterchartForm').submit();	
	});
</script>
@endsection
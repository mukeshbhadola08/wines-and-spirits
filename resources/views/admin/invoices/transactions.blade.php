@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Transections
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Transections  </li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
	<div class="row">
		<div class="col-sm-12">
		@if(isset($invoices) && count($invoices)>0)
		<table class="table">
			<thead>
			<tr>
				<th>Invoice ID</th>
				<th>Service Name</th>
				<th>Transactions ID</th>
				<th>Full Name</th>
				<th>Email</th>
				<th>Date</th>
				<th>Price</td>
			</tr>
			</thead>
			<tbody>
				@foreach($invoices as $trans)
				<tr>
					<td>
						<a href="/admin/invoices/{{$trans->id}}"><?php echo str_pad($trans->id, 6, "0", STR_PAD_LEFT); ?></a>
					</td>
					<td>
					 @php
						$serviceRequest = $trans->ServiceRequest;
						if(!empty($serviceRequest)){
							$serviceDetails = App\Services::find($serviceRequest->service_id);
							echo  $serviceDetails->name;
						}
					 @endphp
					</td>
					<td>
					{{$trans->paypal_txn_id}}
					</td>
					<td>
					{{$name =isset($trans->user->name)?$trans->user->name:''}}
					</td>
					<td>
					{{$email =isset($trans->user->email)?$trans->user->email:''}}
					</td>
					<td>
					{{ date('d-m-Y H:i', strtotime($trans->created_at))}}
					</td>
					<td>
					${{$trans->price}}
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@else
		<p>There is no transaction history.</p>
		@endif
		</div>
	</div>
</div >
@endsection

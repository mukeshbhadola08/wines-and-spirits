@extends('backpack::layout')



@section('header')

<section class="content-header">

	<h1>Add Event</h1>
	<div class="alert alert-info" style="margin-bottom: 0;">
	    <strong>Note: </strong>
	    Please note that recommended dimension of image is 570 by 472 pixels.
	</div>
	<ol class="breadcrumb">

		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>

		<li><a href="{{ backpack_url('news') }}">Events</a></li>

		<li>Add Event</li>

	</ol>

</section>

@endsection



@section('content')

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<div class="body">

	@if ($errors->any())

		<div class="alert alert-danger">

			<ul>

				@foreach ($errors->all() as $error)

					<li>{{ $error }}</li>

				@endforeach

			</ul>

		</div>

	@endif
	<div class="row">

		<div class="col-sm-12">

			<form method="post" action="{{backpack_url('news/create')}}" enctype="multipart/form-data">

			<div class="form-group">

				<input type="hidden" value="{{csrf_token()}}" name="_token" />

				<label for="title">Event Title:</label>

				<input type="text" class="form-control" name="title" value="{{ old('title') }}"/>

			</div>

			<div class="form-group">

				<label for="description">Description :</label>
				<textarea name="description" id="blog-ckeditor" class="form-control">{{ old('description') }}</textarea>

			</div>

			<div class="form-group">

				<label for="short_description">Event Summary</label>

				<textarea name="short_description" class="form-control">{{ old('short_description') }}</textarea><i>This summary will be used in the event listing</i>

			</div>

			<div class="form-group">

				<label for="event_venue">Event Venue:</label>

				<input name="event_venue" class="form-control" value="{{ old('event_venue') }}" />

			</div>

			<div class="form-group">

				<label for="event_date">Event Date :</label>

				<input name="event_date" class="form-control" style="width:20%;" id="datepicker"  autocomplete="off" value="{{ old('event_date') }}" />

			</div>

			<div class="form-group">	

				<label for="image">Youtube Video Url:</label>

				<input type="text" class="form-control" name="video_url" id="video_url" value="{{ old('video_url') }}" />

			</div>



			<!-- <div class="form-group">	

				<label for="image">Upload File:</label>

				<input type="file" class="form-control" name="news_file" /><i>Upload image or audio file</i>

			</div> -->

			<div class="form-group images">	
				<label for="image">Add Image:</label>
				<input type="file" class="form-control" onchange="$('.added-img').remove();" name="image" id="upload" />
				<?php if($errors->has('image')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('image')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group">	
				<label>OR</label>
			</div>
			<div class="form-group">
				<button type="button" class="btn btn-primary addimage" data-toggle="modal" data-target="#portfolioModal">Add Media</button>
			</div>


			<div class="form-group hidden">
				<input type="checkbox" name = "is_visible" value="1" checked />&nbsp;

				<label for="is_visible">Is Visible</label>
			</div>
			
			<!-- <div class="form-group">
				<input type="checkbox" name="is_paid" value="1" class="is_paid" {{ old('is_paid') ? 'checked' : '' }} />&nbsp;

				<label for="is_paid">Is Paid </label>
			</div> -->
			
			<div class="form-group event_amount_div" style="{{ old('is_paid') ? '' : 'display:none;' }}">
				<label for="is_visible">Amount</label>
				<input type="text" class="form-control" name="event_amount" value="{{old('event_amount')}}" style="width:250px;"/>
			</div>



			<input type ="hidden" name="added_by" value="{{ Auth::user()->id }}" / >

			<input type ="hidden" name="lang_code" value="en" / >

			<input type ="hidden" name="parent" value="0" / >

			<button type="submit" id="saveForm" class="btn btn-primary">Create</button>

			</form>

		</div>

	</div>



</div>

@endsection

@section('after_scripts')

 <!-- The Modal -->
<div class="modal" id="portfolioModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Photo</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" style="display: block;
    float: left;">
        <ul style="list-style: none;"></ul>
      </div>

      <!-- Modal footer -->

      <div class="modal-footer">
         <button id="save"disabled onclick="save()" class="btn btn-width bkgrnd-cyan save-details" type="button" name="save-details">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style type="text/css">
	.custom-border{
		 border: 1px solid #57549C !important;
	}
	.border{
		border: 1px solid transparent;
	}
</style>

<script>
   function save(){
        var imageDIV = $('.images');
        imageDIV.find('.added-img').remove();

        var imagurl = $( "#portfolioModal" ).find('span.custom-border img').attr('src');
        var imagid = $( "#portfolioModal" ).find('span.custom-border img').data('id');

        var image = "<div class='added-img' style='margin-top:10px;'><img class=\"img-responsive\" style=\"width: 150px;height: 150px; \" src='"+imagurl+"'><input type='hidden' name='image' value='"+imagid+"'/></div>";
        imageDIV.html(image);

        $("#upload").val(''); 
        $( "#portfolioModal" ).modal('hide');
    
        $( "#portfolioModal" ).find('.modal-body ul').empty();  	     
  }


  jQuery(function() {

  	$( "#portfolioModal" ).on('hidden.bs.modal', function(){

      $( "#portfolioModal .modal-footer #save").attr("disabled", 1);
      $( "#portfolioModal .modal-footer #save").removeClass('btn-primary').addClass('bkgrnd-cyan');

    });

    $( "#portfolioModal" ).on('shown.bs.modal', function(){
      $( "#portfolioModal" ).find('.modal-body ul').empty();
	     jQuery.ajax({
         type:'POST',
         url:'/admin/getportfolios',
         data:'_token = <?php echo csrf_token(); ?>',
         success:function(res) {
         	 $.each(res.data, function(i){
	            $( "#portfolioModal" ).find('.modal-body ul').append("<li class='inline-block pull-left'><span style='display:block;margin: 4px;' class='border'><img onclick=\"$( this ).closest('ul').find('span').removeClass('custom-border'); $(this).parent().addClass('custom-border'); $(this).closest('.modal-content').find('.modal-footer #save').removeAttr('disabled').removeClass('bkgrnd-cyan').addClass('btn-primary'); \" data-id='"+res.data[i].id+"'class='img-responsive' style='width: 150px;height: 150px;     margin: 8px; cursor:pointer;' src='"+res.data[i].url+"'></span></li>");
	        });    
          
         }
      
      });
	});
});


</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css">
<script>

	CKEDITOR.replace('blog-ckeditor');
	
	$(document).on('change','.is_paid',function(){
		var ischecked= $(this).is(':checked');
		if(ischecked) {
			$(".event_amount_div").show();
		} else {
			$(".event_amount_div").hide();
		}
		
	});

	$(document).on('change','#video_url', function(){
		var url = $('#video_url').val();
		if(url !==''){

			var patt = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=))/;
			if(patt.test(url)){
				
				$('#saveForm').removeAttr('disabled');
				
			} else {
				new PNotify({'text':'Please enter valid youtube video url','type':'error'});
				 $('#saveForm').attr('disabled','disabled');
			}

		} else {
			
				$('#saveForm').removeAttr('disabled');
			
		}
	});
		
	$( "#datepicker" ).datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true
	});
</script>

@endsection

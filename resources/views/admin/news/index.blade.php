@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Upcoming Events
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Events</li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12"><a href="{{url('/admin/news/create')}}" class="btn btn-primary">Add Event</a></div>
    </div><hr>

	<div class="row">
		<div class="col-sm-12">
			@if(isset($news) && count($news)>0)
				<table class="table table-striped" id="events_table">
					<thead>
						<tr>
						  <th width="5%">Image</th>
						 <!--  <th width="10%">Slug</th> -->
						  <th width="10%">Title</th>
						  <th width="7%">Video</th>
						  <th width="7%">Summary</th>
						  <th width="10%">Venue</th>
						  <th width="10%">Date</th>
						  <th width="10%">Results</th>						  
						  <th width="25%">Action</th>
						</tr>
					</thead>
					<tbody>
						
					 @foreach($news as $nwDetails)
						<tr>
							<td>
								@if($nwDetails->slider_id)
									<img class="media-object" src="{{App\Http\Controllers\SliderController::getImage($nwDetails->slider_id)}}" alt="no-img" height="100" width="100">
								@else
									<img class="media-object" src="{{action('NewsController@getImage',$nwDetails->id)}}" alt="no-img" height="100" width="100">
								@endif
							</td>
							<!-- <td>{{$nwDetails->permalink}}</td> -->
							<td>{{$nwDetails->title}}</td>
							<td>{{$nwDetails->video_url}}</td>
							<td>{{$nwDetails->summery}}</td>
							<td>{{$nwDetails->event_venue}}</td>
							<td>{{$nwDetails->event_date}}</td>
							<td>{{$nwDetails->results}}</td>
							<td>
								<a href="{{action('NewsController@view',$nwDetails->permalink)}}" class="btn btn-success" target="_blank"><i class="fa fa-eye"></i>&nbsp;View</a>&nbsp;
								<a href="{{action('NewsController@edit',$nwDetails->id)}}" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>&nbsp;								
								<a href="{{action('NewsController@delete',$nwDetails->id)}}" class="btn btn-danger delete_event"><i class="fa fa-trash-o"></i>&nbsp;DELETE</a>
								
							</td>
						</tr>
					@endforeach	 
					</tbody>
				</table>
			@else
			<div class="alert alert-info">
				There are no news added yet.
			</div>
			@endif
		</div>
	</div>

</div >
@endsection

@section('after_scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('#events_table').DataTable( {
        "order": [[ 1, "asc" ]]
    });
	$('#events_table_paginate').addClass('pull-right');
});	
jQuery(document).on('click','.delete_event',function(e){
	if(confirm("Are you sure want to delete this event?")){
		return true;
	} else{
		return false;
	}
});		
</script>
@endsection

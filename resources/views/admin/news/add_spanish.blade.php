@extends('backpack::layout')



@section('header')

<section class="content-header">

	<h1>Add Event In Dutch</h1>

	<ol class="breadcrumb">

		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>

		<li><a href="{{ backpack_url('news') }}">Events</a></li>

		<li>Add Event</li>

	</ol>

</section>

@endsection



<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>



@section('content')

<div class="body">

	@if ($errors->any())

		<div class="alert alert-danger">

			<ul>

				@foreach ($errors->all() as $error)

					<li>{{ $error }}</li>

				@endforeach

			</ul>

		</div>

	@endif



	<div class="row">

		<div class="col-sm-12">

			<form method="post" action="{{action('NewsController@storeInlang',$id)}}" enctype="multipart/form-data">

			<div class="form-group">

				<input type="hidden" value="{{csrf_token()}}" name="_token" />

				<label for="title">Title:</label>

				<input type="text" class="form-control" name="title" value="{{$news->title}}"/>

			</div>

			<div class="form-group">

			

				<label for="description">Description:</label>

				<textarea name="description" class="form-control"  id="blog-ckeditor">{{$news->description}}</textarea>

			</div>



			<div class="form-group">

				<label for="description">Summary</label>

				<textarea name="short_description" class="form-control">{{$news->summery}}</textarea><i>This summary will be used in the blog listing</i>

			</div>





			<div class="form-group">	

				<label for="image">Youtube Video Url:</label>

				<input type="url" class="form-control" name="video_url" id="video_url" value="{{$news->video_url}}" />

			</div>



			@if($news->file_name !='')

			<div class="form-group">	

				<label for="image">Old File:</label>

				@php $filearray =explode('/',$news->file_name); 

					 echo $file = end($filearray);

				@endphp

			</div>

			@endif



			<div class="form-group">	

				<label for="image">Upload File:</label>

				<input type="file" class="form-control" name="news_file" /><i>Upload image or audio file</i>

			</div>
			

			<input type="hidden" name="is_paid" value="{{$news->is_paid}}">

			<input type="hidden" name="event_amount" value="{{$news->amount}}">

			<input type="hidden" name="is_visible" value="1">

			<input type ="hidden" name="added_by" value="{{ Auth::user()->id }}" / >

			<input type ="hidden" name="lang_code" value="es" / >

			<input type ="hidden" name="parent" value="{{$id}}" / >

			<button type="submit" class="btn btn-primary" id="saveForm" >Add</button>

			</form>

		</div>

	</div>



</div >


@endsection


@section('after_scripts')
<script>
		
	CKEDITOR.replace('blog-ckeditor');
	
	$(document).on('change','.is_paid',function(){
		var ischecked= $(this).is(':checked');
		if(ischecked) {
			$(".event_amount_div").show();
		} else {
			$(".event_amount_div").hide();
		}
		
	});

	$(document).on('change','#video_url', function(){
		var url = $('#video_url').val();
		if(url !==''){

			var patt = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=))/;
			if(patt.test(url)){
				
				$('#saveForm').removeAttr('disabled');
				
			} else {
				new PNotify({'text':'Please enter valid youtube video url','type':'error'});
				 $('#saveForm').attr('disabled','disabled');
			}

		} else {
			
				$('#saveForm').removeAttr('disabled');
			
		}
	});
		
</script>

@endsection



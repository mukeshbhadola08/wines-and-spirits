@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
		User Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Users</li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
            <button type="button" class="close" aria-label="Close" data-dismiss="alert">
  				<span aria-hidden="true">&times;</span>
			</button>
        </div>
    @endif
	<hr>

	<div class="row">
		<div class="col-sm-12">
			@if(isset($users) && count($users)>0)
				<table class="table table-striped">
					<thead>
						<tr>
						  <th width="10%">Name</th>
						  <th width="10%">Email</th>
						  <th width="10%">Status</th>
						  <th width="30%">Action</th>
						</tr>
					</thead>
					<tbody>
						
					@foreach($users as $userDetails)
						@php
						if($userDetails->user_type == 'S'){
								 continue;
						}
						@endphp
						<tr>
							<td>{{$userDetails->name}}</td>
							<td>{{$userDetails->email}}</td>
							<td>@if($userDetails->block_status==1) blocked @else Active @endif</td>
							
							<td>
								
								@if($userDetails->block_status==1) 
									<a href="{{action('UserController@block_unblock_user',$userDetails->id)}}" class="btn btn-success">&nbsp;Unblock</a> 
								@else 
									<a href="{{action('UserController@block_unblock_user',$userDetails->id)}}" class="btn btn-danger" style="width: 14%;">&nbsp;Block</a> 
								@endif
								&nbsp;
								<!-- <a href="#" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>&nbsp;								
								<a href="#" class="btn btn-danger"><i class="fa fa-trash-o"></i>&nbsp;DELETE</a> -->
								
								
							</td>
						</tr>
					@endforeach	
					</tbody>
				</table>
			@else
			<div class="alert alert-info">
				There are no blogs added yet.
			</div>
			@endif
		</div>
	</div>

</div >
@endsection

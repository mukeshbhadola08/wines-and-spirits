@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Grades
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Grades</li>
      </ol>
    </section>
@endsection

@section('content')
<?php 
    
    
$month = array ("1"=>"January", "2"=>"February", "3"=>"March", "4"=>"April", "5"=>"May", "6"=>"June", "7"=>"July",  "8"=>"August", "9"=>"September", "10"=>"October", "11"=>"November", "12"=>"December");

$curr_year = date("Y");
$dates = explode(" to ", $page->school_year);

$from = trim($dates[0]);
$to = trim($dates[1]);

$fromDate = explode(" ",$from);

$toDate = explode(" ",$to);
$select_year = "<select name=\"to_year\">\n";
for($i=$curr_year; $i>=1984; $i--) {
    $select_year .= "\t<option val=\"".$i."\"";
    if ($i == trim($toDate[1])) {
        $select_year .= " selected=\"selected\">".$i."</option>\n";
    } else {
        $select_year .= ">".$i."</option>\n";
    }
}
$select_year .= "</select>";


$select_month = "<select name=\"to_month\">\n";
foreach ($month as $key => $val) {
    $select_month .= "\t<option val=\"".$val."\"";
    if ($val == trim($toDate[0])) {
        $select_month .= " selected=\"selected\">".$val."</option>\n";
    } else {
        $select_month .= ">".$val."</option>\n";
    }
}
$select_month .= "</select>";

$select_date_to = $select_month."&nbsp;&nbsp;&nbsp;".$select_year;


$select_year = '';
$select_month = '';

$select_year = "<select name=\"from_year\">\n";
for($i=$curr_year; $i>=1984; $i--) {
    $select_year .= "\t<option val=\"".$i."\"";
    if ($i == trim($fromDate[1])) {
        $select_year .= " selected=\"selected\">".$i."</option>\n";
    } else {
        $select_year .= ">".$i."</option>\n";
    }
}
$select_year .= "</select>";

$select_month = "<select name=\"from_month\">\n";
foreach ($month as $key => $val) {
    $select_month .= "\t<option val=\"". $val."\"";
    if ($val == trim($fromDate[0])) {
        $select_month .= " selected=\"selected\">".$val."</option>\n";
    } else {
        $select_month .= ">".$val."</option>\n";
    }
}
$select_month .= "</select>";

$select_date_from = $select_month."&nbsp;&nbsp;&nbsp;".$select_year;

?>
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif

	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{action('CoachesGradesController@update',$id)}}" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				
			</div>
			<div class="form-group">
			
				<label for="name">School Name:</label>
				<input required name = "school_name" class="form-control" value="{{$page->school_name}}">
			</div>
			<div class="form-group">
			
				<label for="email">School Grade:</label>
				<input required name = "school_grade" class="form-control" value="{{$page->school_grade}}">
			</div>
			<div class="form-group">
			
				<label for="status">School Year:</label><br>
				<?php 
				    echo  "<label style='margin-right:20px; width:60px;' >From: </label>".$select_date_from."<br><label style='margin-right:20px;width:60px;' >To: </label>". $select_date_to;
				
				?>
			</div>
			    <input name = "grade" value="{{$id}}" type="hidden">
			<button type="submit" class="btn btn-primary">Update</button>
			</form>
		</div>
	</div>

</div>
@endsection


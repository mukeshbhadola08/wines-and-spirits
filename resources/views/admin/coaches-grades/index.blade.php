@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Coaches/Grades
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Coaches/Grades</li>
      </ol>
    </section>
@endsection

<style>
	button.submit{
		display:none;
	}
</style>
@section('content')
<?php 
$curr_month = date("m");
$month = array ("1"=>"January", "2"=>"February", "3"=>"March", "4"=>"April", "5"=>"May", "6"=>"June", "7"=>"July",  "8"=>"August", "9"=>"September", "10"=>"October", "11"=>"November", "12"=>"December");

$curr_year = date("Y");

$select_year = "<select name=\"year\">\n";
for($i=$curr_year; $i>=1984; $i--) {
    $select_year .= "\t<option val=\"".$i."\"";
    if ($i == $curr_year) {
        $select_year .= " selected=\"selected\">".$i."</option>\n";
    } else {
        $select_year .= ">".$i."</option>\n";
    }
}
$select_year .= "</select>";


$select_month = "<select name=\"month\">\n";
foreach ($month as $key => $val) {
    $select_month .= "\t<option val=\"".$key."\"";
    if ($key == $curr_month) {
        $select_month .= " selected=\"selected\">".$val."</option>\n";
    } else {
        $select_month .= ">".$val."</option>\n";
    }
}
$select_month .= "</select>";

$select_date = $select_month.$select_year;
?>
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
   
    <div class="row">
        <div class="col-sm-12">

            	<table class="table table-responsive table-striped table-bordered">
					<thead>
						<tr>
						    <th>Current Coach</th>
					    	<th>Coach Name</th>
					    	<th>Coach Email</th>
					    	<th>Dates</th>
					    	<th></th>
					    </tr>
					</thead>
					<tbody>
						@foreach($coaches as $coach)
							<tr>
							    <td>@if($coach->current_coach) Current @else N\A @endif</td>
								<td>{{$coach->coach_name}}</td>
						    	<td>{{$coach->coach_email}}</td>
						    	<td>
						    		@php
						        		if($coach->current_coach){
							        		$status = explode(" to ", $coach->coach_status); 
							        		echo $status[0].' to Present';
						        		} else{
						        			echo $coach->coach_status;			
							        	}    	
						        	@endphp
						    	</td>
						    	<td><a href="{{action('CoachesGradesController@edit',$coach->id)}}" type="button" class="btn btn-primary" ><i class="glyphicon glyphicon-edit"></i></a></td>
						    	<td><a href="{{action('CoachesGradesController@delete',$coach->id)}}" type="button" class="btn btn-danger remove"><i class="glyphicon glyphicon-remove-sign"></i></a></td>
							</tr>
						@endforeach
					</tbody>
								
				</table>
				<h4 class="well" style="background-color: #605ca8; color: white;">Add Coaches</h4>
				
				<section class="container-fluid">
					<div class="table table-responsive">
						<form method="post" class="form-horizontal" action="{{backpack_url('coaches-grades')}}">
							<input type="hidden" value="{{csrf_token()}}" name="_token" />
							<table class="table table-responsive table-striped table-bordered">
							<thead>
								<tr>
								    <td>Current Coach</td>
							    	<td>Coach Name</td>
							    	<td>Coach Email</td>
							    	<td>Dates</td>
							    	<td></td>
							    </tr>
							</thead>
							<tbody id="TextBoxContainer">
							</tbody>
							<tfoot>
							  <tr>
							    <th colspan="5">
							    <button id="btnAdd" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp; Add&nbsp;</button></th>
							  </tr>
							</tfoot>
							</table>
							<button type="submit" class="btn btn-primary submit">Create</button>
						</form>
					
					</div>
				</section>
        </div>
    </div>
	<br />
	<br />
	<br />
	<br />

	<div class="row">
        <div class="col-sm-12">
            	<table class="table table-responsive table-striped table-bordered">
					<thead>
						<tr>
						    
							<th>School Name</th>
					    	<th>School Grade</th>
					    	<th>School Year</th>
					    	<th></th>
					    </tr>
					</thead>
					<tbody>
						@foreach($grades as $grade)
							<tr>
								<td>{{$grade->school_name}}</td>
						    	<td>{{$grade->school_grade}}</td>
						    	<td>{{$grade->school_year}}</td>
						    	<td><a href="{{action('CoachesGradesController@gradesEdit',$grade->id)}}" type="button" class="btn btn-primary" ><i class="glyphicon glyphicon-edit"></i></a></td>
						    	<td><a href="{{action('CoachesGradesController@delete',$grade->id)}}" type="button" class="btn btn-danger remove"><i class="glyphicon glyphicon-remove-sign"></i></a></td>
							</tr>
						@endforeach
					</tbody>
								
				</table>
				<h4 class="well text-white" style="background-color: #605ca8; color: white;">Add Grades</h4>
				
				<section class="container-fluid">

					<div class="table table-responsive">
						<form method="post" class="form-horizontal" action="{{backpack_url('coaches-grades')}}">
							<textarea class="form-control" style="margin-bottom: 20px;" name="settings[grades_textarea]" placeholder="Grades" >{{ $grades_textarea[0] }}</textarea>
							<input type="hidden" value="{{csrf_token()}}" name="_token" />
							<table class="table table-responsive table-striped table-bordered">
							<thead>
								<tr>
								    
							    	<th>School Name</th>
							    	<th>School Grade</th>
							    	<th>School Year</th>
							    	<th></th>
							    </tr>
							</thead>
							<tbody id="TextBoxContainer2">
							</tbody>
							<tfoot>
							  <tr>
							    <th colspan="5">
							    <button id="btnAdd2" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp; Add&nbsp;</button></th>
							  </tr>
							</tfoot>
							</table>
							<button type="submit" class="btn btn-primary submit">Create</button>
						</form>
					
					</div>
				</section>
        </div>
    </div>
	<br />
	<div class="row">
		
		
	</div>


</div >

@endsection

@section('after_scripts')
<script>
 
var curr_month = '<?php echo $curr_month; ?>';
var month = '<?php echo json_encode($month); ?>';
var curr_year = '<?php echo $curr_year; ?>';
month = JSON.parse(month);
var select_month = "";
var select_year = "";
var select_date = "";
var select_date_from = "";
var select_date_to = "";
select_month +="<select name='from_month[]' required><option>Month</option>";
$.each(month, function(i){
     select_month += "<option val='"+month[i]+"'>"+month[i]+"</option>";
});
select_month +="</select>";

select_year +="<select name='from_year[]' required><option>Year</option>";
var x="";
for(x=curr_year; x>=1984;x--){
    select_year += "<option val='"+x+"'>"+x+"</option>";
}
select_year +="</select>";

select_date_from = select_month+"&nbsp;&nbsp;&nbsp;"+select_year;


select_month = "";
select_month +="<select name='to_month[]'><option>Month</option>";
$.each(month, function(i){
    select_month += "<option val='"+month[i]+"'>"+month[i]+"</option>";
});
select_month +="</select>";
select_year = "";
select_year +="<select name='to_year[]'><option>Year</option>";
var x="";
for(x=curr_year; x>=1984;x--){
    select_year += "<option val='"+x+"'>"+x+"</option>";
}
select_year +="</select>";

select_date_to = select_month+"&nbsp;&nbsp;&nbsp;"+select_year;

$(function () {

    $("#btnAdd").bind("click", function () {
    	$(this).closest('form').find(".submit").show();
        var div = $("<tr />");
        var tr_length = $("#TextBoxContainer tr").length;
        div.html(GetDynamicTextBox("", tr_length , select_date));
        $("#TextBoxContainer").append(div);
    });

    $("#btnAdd2").bind("click", function () {
    	$(this).closest('form').find(".submit").show();
        var div = $("<tr />");
        var tr_length2 = $("#TextBoxContainer2 tr").length;
        div.html(GetDynamicTextBox2("", tr_length2));
        $("#TextBoxContainer2").append(div);
    });

    

});


function functionTest(obj){


	if($('#TextBoxContainer tr').length == 1){

		$(obj).closest('form').find('.submit').hide();

	}

	$(obj).closest('tr').remove();

}


function functionTest2(obj){


	if($('#TextBoxContainer2 tr').length == 1){

		$(obj).closest('form').find('.submit').hide();

	}

	$(obj).closest('tr').remove();

}

var count1 = 0;
var count2 = 0;
	
function GetDynamicTextBox(value, tr_length, select_date) {
   
    return '<input type="hidden" value="0" name="page_type[]" /><input type="hidden" value="<?php echo date('Y-m-d H:i:s'); ?>" name="created_at[]" /><input type="hidden" value="<?php echo date('Y-m-d H:i:s'); ?>" name="updated_at[]" /><td><input class="current_coach" name = "current_coach[]" type="checkbox" /></td><td><input name = "coach_name[]" type="text" value = "'+ value +'" class="form-control" required/></td>'+'<td><input required name = "coach_email[]" type="text" value = "'+ value +'" class="form-control" /></td>' + '<td>From: &nbsp;'+ select_date_from +'<br/><br/>To: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ select_date_to +'</td><td><a href="javascript:;" type="button" class="btn btn-danger" data-index='+ (count1++) +' onclick="functionTest(this);"><i class="glyphicon glyphicon-remove-sign"></i></a></td>';
}


function GetDynamicTextBox2(value, tr_length) {
    return '<input type="hidden" value="1" name="page_type[]" /><input type="hidden" value="<?php echo date('Y-m-d H:i:s'); ?>" name="created_at[]" /><input type="hidden" value="<?php echo date('Y-m-d H:i:s'); ?>" name="updated_at[]" /><td><input required name = "school_name[]" type="text" value = "' + value + '" class="form-control" /></td>'+'<td><input name = "school_grade[]" required type="text" value = "' + value + '" class="form-control" /></td>'+ '<td>From: &nbsp;'+ select_date_from +'<br/><br/>To: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ select_date_to +'<td><a href="javascript:;" type="button" class="btn btn-danger" data-index='+ (count2++) +' onclick="functionTest2(this);"><i class="glyphicon glyphicon-remove-sign"></i></a></td>';
}

</script>
@endsection

@extends('backpack::layout')

@section('header')
<section class="content-header">
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li>Content</li>
	</ol>
</section>
@endsection


<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>



@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	
	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{backpack_url('updatefootersection')}}" enctype="multipart/form-data" class="form-horizontal">
			
			<input type="hidden" value="{{csrf_token()}}" name="_token" />
			
			<div class="sitesettings">
				<!--h3 class="text-center">Content</h3--><br/>
				<div class="form-group"> 
					<div class="col-sm-2">
						<label for="var_name">Select Language:</label>
					</div>
					<div class="col-sm-4">
						<select name="lang_code" class="form-control lang_code">
							@php
								$allLanguages = App\Language::where('disabled',0)->get();
							@endphp
							@if(isset($allLanguages) && count($allLanguages)>0)
								@foreach($allLanguages as $lang)
									<option value="{{trim($lang->code)}}" @if($lang->code == $lang_select) selected @endif >{{$lang->language}}</option>
								@endforeach
							@endif
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label for="site_title" class="col-sm-2 control-label">Footer Text</label>
					<div class="col-sm-10">
						<textarea class="form-control" id="contentckeditor" name="contents[footer_text]">{{$contents->meta_value}}</textarea>
					</div>
				</div>
			</div>


			<input type="hidden" name="primary_id" value="{{ $contents->id }}">
			<button type="submit" class="btn btn-primary">Update</button>
			</form>
		</div>
	</div>

</div >

<script>
	CKEDITOR.replace( 'contentckeditor');
</script>
@endsection

@section('after_scripts')
<script>
$(document).on('change','.lang_code',function(){
	var lang_code = $(this).val();
    window.location.href = "/admin/footersection/"+lang_code;
}); 
</script>
@endsection


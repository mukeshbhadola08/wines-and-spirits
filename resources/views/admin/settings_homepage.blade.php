@extends('backpack::layout')

@section('header')
<section class="content-header">
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li>Settings</li>
	</ol>
</section>
@endsection
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
@section('content')
<div class="body">
	<div class="mission hidden"></div>
	<div class="mission_danger alert alert-danger" style="display: none">Item Content Fields is Required</div>
	@if(\Session::has('success'))
	<div class="alert alert-success">
		{{\Session::get('success')}}
	</div>
	@endif
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	<div class="alert alert-info" style="margin-bottom: 0;">
	    <strong>Note: </strong>
	    Please note that recommended dimension of background image should be more than 1200 X 1200 pixels.
	</div>

	<?php
	foreach ($hommePageSettings as $setting) {
		$variable = $setting->option_name;
		$$variable = $setting->option_value;
	}
	?>
	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{backpack_url('setting_blog?type=')}}{{$settingtype}}" enctype="multipart/form-data" class="form-horizontal">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				@if($settingtype == 'history')
				<div class="sitesettings">
					<h3>History Settings</h3><br />
					<div class="form-group">
						<label for="site_title" class="col-sm-2 control-label">History Title</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="settings[homepage_history_title]" placeholder="Site Title" value="{{ config('constants.HOMEPAGE_HISTORY_TITLE') }}">
						</div>
					</div>
					<div class="form-group">
						<label for="background_color" class="col-sm-2 control-label">
							Background Color
							<span style="width: 10px;height: 10px;display: inline-block;background:{{ config('constants.HISTORY_BACKGROUND_COLOR') }}"></span>
						</label>
						<div class="col-sm-10">
							<input type="text" id="blog_bg_color" class="form-control upload_image  section_bg_color" name="settings[HISTORY_BACKGROUND_COLOR]" value="{{ config('constants.HISTORY_BACKGROUND_COLOR') }}">
							<button class="jscolor {valueElement:'chosen-value', onFineChange:'setTextColor(this)'}">
								Pick color
							</button>
						</div>
					</div>
					<div class="form-group">
						<label for="background_image" class="col-sm-2 control-label">Background Image
						</label>
						<div class="col-sm-10">
							<input type="file" class="form-control upload_image" name="settings[history_background_image]" value="{{ config('constants.HISTORY_BACKGROUND_IMAGE') }}">
						</div>
					</div>
					<div class="form-group">
						<label for="background_image" class="col-sm-2 control-label">
						</label>
						<div class="col-sm-10">
						@if(!empty(config('constants.HISTORY_BACKGROUND_IMAGE')))
								<img src="{{ config('constants.HISTORY_BACKGROUND_IMAGE') }}" height="100" width="150">
								<div>
									<button><a href="javascript:void(0)" data-reset_options="HISTORY_BACKGROUND_IMAGE" class="text-danger" >Delete</a></button>
								</div>
							@endif
						</div>
					</div>
					<div class="form-group">
						<label for="background_image" class="col-sm-2 control-label"> Number Of History</label>
						<div class="col-sm-10">
							<input type="text" class="form-control upload_image" name="settings[total_no_of_history]" value="{{ config('constants.TOTAL_NO_OF_HISTORY') }}">
						</div>
					</div>
					<button type="submit" class="btn btn-primary">Update Settings</button>
					@elseif($settingtype == 'wines')
					<input type="hidden" value="{{csrf_token()}}" name="_token" />
					<div class="sitesettings">
						<h3>Wines Settings</h3><br />
						<div class="form-group">
							<label for="site_title" class="col-sm-2 control-label">Wines Title</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="settings[homepage_wines_title]" placeholder="Site Title" value="{{ config('constants.HOMEPAGE_WINES_TITLE') }}">
							</div>
						</div>
						<div class="form-group">
							<label for="background_color" class="col-sm-2 control-label">
								Background Color
								<span style="width: 10px;height: 10px;display: inline-block;background:{{ config('constants.WINE_BACKGROUND_COLOR') }}"></span>
							</label>
							<div class="col-sm-10">
								<input type="text" id="blog_bg_color" class="form-control upload_image  section_bg_color" name="settings[WINE_BACKGROUND_COLOR]" value="{{ config('constants.WINE_BACKGROUND_COLOR') }}">
								<button class="jscolor {valueElement:'chosen-value', onFineChange:'setTextColor(this)'}">
									Pick color
								</button>
							</div>
						</div>
						<div class="form-group">
							<label for="background_image" class="col-sm-2 control-label">
								Background Image
							</label>
							<div class="col-sm-10">
								<input type="file" class="form-control upload_image" name="settings[wine_background_image]" value="{{ config('constants.WINE_BACKGROUND_IMAGE') }}">
							</div>
						</div>
						<div class="form-group">
							<label for="background_image" class="col-sm-2 control-label">
							</label>
							<div class="col-sm-10">
								@if(!empty(config('constants.WINE_BACKGROUND_IMAGE')))
									<img src="{{ config('constants.WINE_BACKGROUND_IMAGE') }}" height="100" width="150">
									<div>
										<button><a href="javascript:void(0)" data-reset_options="WINE_BACKGROUND_IMAGE" class="text-danger">Delete</a></button>
									</div>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label for="background_image" class="col-sm-2 control-label">
								Single wine Background Image
							</label>
							<div class="col-sm-10">
								<input type="file" class="form-control upload_image" name="settings[singlepage_wine_background_image]" value="{{ config('constants.SINGLEPAGE_WINE_BACKGROUND_IMAGE') }}">
							</div>
						</div>
						<div class="form-group">
							<label for="background_image" class="col-sm-2 control-label">
							</label>
							<div class="col-sm-10">
								@if(!empty(config('constants.SINGLEPAGE_WINE_BACKGROUND_IMAGE')))
									<img src="{{ config('constants.SINGLEPAGE_WINE_BACKGROUND_IMAGE') }}" height="100" width="150">
									<div>
										<button><a href="javascript:void(0)" data-reset_options="SINGLEPAGE_WINE_BACKGROUND_IMAGE" class="text-danger">Delete</a></button>
									</div>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label for="background_image" class="col-sm-2 control-label">
								Single Distellery Background Image
							</label>
							<div class="col-sm-10">
								<input type="file" class="form-control upload_image" name="settings[singlepage_distellery_background_image]" value="{{ config('constants.SINGLEPAGE_DISTELLERY_BACKGROUND_IMAGE') }}">
							</div>
						</div>
						<div class="form-group">
							<label for="background_image" class="col-sm-2 control-label">
							</label>
							<div class="col-sm-10">
								@if(!empty(config('constants.SINGLEPAGE_DISTELLERY_BACKGROUND_IMAGE')))
									<img src="{{ config('constants.SINGLEPAGE_DISTELLERY_BACKGROUND_IMAGE') }}" height="100" width="150">
									<div>
										<button><a href="javascript:void(0)" data-reset_options="SINGLEPAGE_DISTELLERY_BACKGROUND_IMAGE" class="text-danger">Delete</a></button>
									</div>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label for="background_image" class="col-sm-2 control-label">
								Single Product Background Image
							</label>
							<div class="col-sm-10">
								<input type="file" class="form-control upload_image" name="settings[singlepage_product_background_image]" value="{{ config('constants.SINGLEPAGE_PRODUCT_BACKGROUND_IMAGE') }}">
							</div>
						</div>
						<div class="form-group">
							<label for="background_image" class="col-sm-2 control-label">
							</label>
							<div class="col-sm-10">
								@if(!empty(config('constants.SINGLEPAGE_PRODUCT_BACKGROUND_IMAGE')))
									<img src="{{ config('constants.SINGLEPAGE_PRODUCT_BACKGROUND_IMAGE') }}" height="100" width="150">
									<div>
										<button><a href="javascript:void(0)" data-reset_options="SINGLEPAGE_PRODUCT_BACKGROUND_IMAGE" class="text-danger">Delete</a></button>
									</div>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label for="redmoretext" class="col-sm-2 control-label">Read More Text</label>
							<div class="col-sm-10">
								<input type="text" class="form-control upload_image" name="settings[read_more_text]" value="{{ config('constants.READ_MORE_TEXT') }}" required>
							</div>
						</div>
						<button type="submit" class="btn btn-primary">Update Settings</button>
						@elseif($settingtype == 'blog')
						<div class="sitesettings">
							<h3>Blog Settings</h3><br />
							<div class="form-group">
								<label for="site_title" class="col-sm-2 control-label">Blog Title Name</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="settings[homepage_blog_title]" placeholder="Site Title" value="{{ config('constants.HOMEPAGE_BLOG_TITLE') }}">
								</div>
							</div>
							<div class="form-group">
								<label for="background_color" class="col-sm-2 control-label">
									Background Color
									<span style="width: 10px;height: 10px;display: inline-block;background:{{ config('constants.HOMEPAGE_BLOG_BG_COLOR') }}"></span>
								</label>
								<div class="col-sm-10">
									<input type="text" id="blog_bg_color" class="form-control upload_image  section_bg_color" name="settings[HOMEPAGE_BLOG_BG_COLOR]" value="{{ config('constants.HOMEPAGE_BLOG_BG_COLOR') }}">
									<button class="jscolor {valueElement:'chosen-value', onFineChange:'setTextColor(this)'}">
										Pick color
									</button>
								</div>
							</div>
							<div class="form-group">
								<label for="background_image" class="col-sm-2 control-label"> Background Image URL
								</label>
								<div class="col-sm-10">
									<input type="file" class="form-control" name="settings[background_image]" value="{{ config('constants.BACKGROUND_IMAGE') }}">
								</div>
							</div>
							<div class="form-group">
								<label for="background_image" class="col-sm-2 control-label">
								</label>
								<div class="col-sm-10">
									@if(!empty(config('constants.BACKGROUND_IMAGE')))
										<img src="{{ config('constants.BACKGROUND_IMAGE') }}" height="100" width="150">
										<div>
											<button><a href="javascript:void(0)" data-reset_options="BACKGROUND_IMAGE" class="text-danger">Delete</a></button>
										</div>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label for="background_image" class="col-sm-2 control-label">
									Number Of blog</label>
								<div class="col-sm-10">
									<input type="text" class="form-control upload_image" name="settings[total_no_of_blogs]" value="{{ config('constants.TOTAL_NO_OF_BLOGS') }}">
								</div>
							</div>
							<div class="form-group">
								<label for="background_image" class="col-sm-2 control-label">Read More Text</label>
								<div class="col-sm-10">
									<input type="text" class="form-control upload_image" name="settings[read_more_blog_text]" value="{{ config('constants.READ_MORE_BLOG_TEXT') }}" required>
								</div>
							</div>
							<button type="submit" class="btn btn-primary">Update Settings</button>
							@elseif($settingtype == 'mission')
							<div class="sitesettings">
								<h3>Mission Settings</h3><br />
								<div class="form-group">
									<label for="site_title" class="col-sm-2 control-label">Title</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="settings[homepage_mission_title]" placeholder="Site Title" value="{{ config('constants.HOMEPAGE_MISSION_TITLE') }}">
									</div>
								</div>
								<div class="form-group">
									<label for="background_color" class="col-sm-2 control-label">
										Background Color
										<span style="width: 10px;height: 10px;display: inline-block;background:{{ config('constants.MISSION_BACKGROUND_COLOR') }}"></span>
									</label>
									<div class="col-sm-10">
										<input type="text" id="mission_bg_color" class="form-control upload_image  section_bg_color" name="settings[MISSION_BACKGROUND_COLOR]" value="{{ config('constants.MISSION_BACKGROUND_COLOR') }}">
										<button class="jscolor {valueElement:'chosen-value', onFineChange:'setTextColor(this)'}">
											Pick color
										</button>
									</div>
								</div>
								<div class="form-group">
									<label for="background_image" class="col-sm-2 control-label">
										Mission Image
										<a href="{{ config('constants.MISSION_SIDE_IMAGE') }}" target="_blank" class="fa fa-eye"></a>
									</label>
									<div class="col-sm-10">
										<input type="file" class="form-control upload_image" name="settings[mission_side_image]" value="{{ config('constants.MISSION_SIDE_IMAGE') }}">
									</div>
								</div>
								<div class="form-group">
									<label for="background_image" class="col-sm-2 control-label">
										Background Image
									</label>
									<div class="col-sm-10">
										<input type="file" class="form-control upload_image" name="settings[mission_background_image]" value="{{ config('constants.MISSION_BACKGROUND_IMAGE') }}">
									</div>
								</div>
								<div class="form-group">
									<label for="background_image" class="col-sm-2 control-label">
									</label>
									<div class="col-sm-10">
										@if(!empty(config('constants.MISSION_BACKGROUND_IMAGE')))
											<img src="{{ config('constants.MISSION_BACKGROUND_IMAGE') }}" height="100" width="150">
											<div>
												<button><a href="javascript:void(0)" data-reset_options="MISSION_BACKGROUND_IMAGE" class="text-danger">Delete</a></button>
											</div>
										@endif
									</div>
								</div>
								<div class="form-group">
									<label for="background_image" class="col-sm-2 control-label">
										Content</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="settings[mission_content]" value="{{ config('constants.MISSION_CONTENT') }}">
									</div>
								</div>
								<div class="form-group">
									<label for="background_image" class="col-sm-2 control-label">
										Number Of list</label>
									<div class="col-sm-10">
										<input type="text" class="form-control upload_image" name="settings[total_no_of_list]" value="{{ config('constants.TOTAL_NO_OF_LIST') }}">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<button type="submit" class="btn btn-primary">Update Settings</button>
										<div class="btn btn-primary add_item" style="float:right;">Add Item</div>
									</div>
								</div>
							</div>
							@endif
			</form>
			<form id="add_list_mission_form" enctype="multipart/form-data">
				<div class="show_input_fields hidden">
					<div class="form-group">
						<label for="background_image" class="col-sm-2 control-label">Image URL</label>
						<input type="file" id="mission_item_image" name="image_url" />
					</div>
					<div class="form-group">
						<label for="background_image" class="col-sm-2 control-label">Content</label>
						<input type="text" id="mission_item_content" name="item_content" />
					</div>
					<button type="submit" class="btn btn-primary" id="add_list_itme_mission" data-mission_list_update="#add_list_mission_form">Add Item to List</button>
					<button type="button" class="btn btn-danger" id="close_list_itme_mission">Close</button>
				</div>
			</form>
		</div>
	</div>
	@if($settingtype == 'mission')
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-striped" id="listitem_mission_table">
				<thead>
					<tr>
						<th>#</th>
						<th>Content</th>
						<th>Image</th>
						<th>Action</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	@endif
</div>
<script src="{{ asset('/') }}dist/js/jquery.js"></script>
<script src="{{ asset('/') }}dist/js/jscolor.js"></script>
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
	$(function() {
		$(document).on('submit', '#add_list_mission_form', function(e) {
			e.preventDefault();
			var formData = new FormData(this);
			let mission_list_update = $(this).data('mission_list_update');
			$.ajax({
				url: '{{url("/mission/addlist")}}',
				data: formData,
				type: 'POST',
				dataType: 'json',
				processData: false,
				contentType: false,
				success: function(response) {
					let html = '<div class="alert alert-success">Item Added successfully.</div>';
					$(".mission").empty().html(html);
					$(".mission").removeClass('hidden');
					$('#add_list_mission_form')[0].reset();
				},
				error: function(xhr, status, error) {
					let html = '<div class="alert alert-danger">Failed to Item Added successfully.</div>';
					$(".mission").empty().html(html);
					$(".mission").removeClass('hidden');
				}
			});
		});
		@if($settingtype == 'mission')
		var oTable = $('#listitem_mission_table').DataTable({
			"oLanguage": {
				"sSearch": "",
				"sEmptyTable": "No Record's Found For your Query!",
				'sLengthMenu': '<select class="search-select form-control" id="lx23">' +
					'<option value="10">Show 10</option>' +
					'<option value="20">Show 20</option>' +
					'<option value="50">Show 50</option>' +
					'<option value="-1">Show All</option>' +
					'</select>'
			},
			"bProcessing": false,
			"bServerSide": true,
			'iDisplayLength': 10,
			'sPaginationType': 'full_numbers',
			"bPaginate": true,
			'sErrMode': 'none',
			"sAjaxSource": '{{URL::to("admin/missionListings")}}',
			"fnServerParams": function(aoData) {
				aoData.push({
					"name": "_token",
					"value": 'token_value'
				});
			},
			"aaSorting": [],
			"aoColumns": [{
					mData: 'index',
					sWidth: "40px",
				},
				{
					mData: 'content',
					sWidth: "150px"
				},
				{
					mData: 'image',
					sWidth: "150px"
				},
				{
					mData: 'action',
					sWidth: "150px",
					bSortable: false
				}
			],
			"fnInfoCallback": function(
				oSettings,
				iStart,
				iEnd,
				iMax,
				iTotal,
				sPre
			) {
				iStart = iTotal > 0 ? iStart : 0;
				return "Showing " + iStart + " to " + iEnd + " of " + iTotal + " entries";
			}
		});
		$(document).on('click', '[data-delete_listitem]', function() {
			let itemDeleteIS = $(this).data('delete_listitem');
			$.ajax({
				url: '{{URL::to("mission/missionRemoveItem")}}',
				type: 'post',
				data: {
					itemDeleteIS: itemDeleteIS
				},
				dataType: 'json',
				success: function() {
					let html = '<div class="alert alert-success">Selected Item removed successfully.</div>';
					$(".mission").empty().html(html);
					$(".mission").removeClass('hidden');
					oTable.ajax.reload();
				},
				error: function(xhr, status, error) {
					let html = '<div class="alert alert-danger">Failed to remove Item from list.</div>';
					$(".mission").empty().html(html);
					$(".mission").removeClass('hidden');
				}
			});
		});
		@endif
	});
	$(document).on("click", ".add_item", function() {
		$('.show_input_fields').removeClass('hidden');
	});
	$(document).on("click", "#close_list_itme_mission", function() {
		$('.show_input_fields').addClass('hidden');
	});

	$(document).on('click', '[data-reset_options]', function() {
		if(confirm('Are you sure you want to delete')){
			//return true;
		
		let reset_options = $(this).data('reset_options');
		let field = $(document).find('input[name="'+reset_options+'"]');
		$.ajax({
				url: '{{URL::to("/admin/settings/optionRemove")}}',
				type: 'post',
				data: {
					itemDelete: reset_options
				},
				dataType: 'json',
				success: function() {
					let html = '<div class="alert alert-success">Selected Item removed successfully.</div>';
					$(".mission").empty().html(html);
					$(".mission").removeClass('hidden');
				},
				error: function(xhr, status, error) {
					let html = '<div class="alert alert-danger">Failed to update!.</div>';
					$(".mission").empty().html(html);
					$(".mission").removeClass('hidden');
				}
			});
	}
	});


	function setTextColor(picker) {
		$('.section_bg_color').val('#' + picker.toString());
	}
</script>
@endsection
@extends('backpack::layout')

@section('header')
<section class="content-header">
    <h1>Add Product</h1>
    <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li><a href="{{ backpack_url('pages') }}">Products</a></li>
        <li>Add Product</li>
    </ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">
    
    <div class="row">
        <div class="col-sm-12">
            <form method="post" action='{{backpack_url("product/$cat_id/create/$type")}}' enctype="multipart/form-data">
            <div class="form-group">
                <input type="hidden" value="{{csrf_token()}}" name="_token" />
                <label for="title">Name:</label>
                <input type="text" class="form-control" name="product_title" value ="{{ old('product_title') }}" />
                @if ($errors->has('product_title'))
                    <span class="invalid-feedback text-danger">
                        <strong>{{ $errors->first('product_title') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="description">Description:</label>
                <textarea name="description" class="form-control" id="page-ckeditor">{{ old('description') }}</textarea>
                @if ($errors->has('description'))
                    <span class="invalid-feedback text-danger">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group images"> 
                <label for="default_image">Photo:</label>
                <input type="file" onchange="$('.added-img').remove();" name="image" id="upload" />
                <?php if($errors->has('image')): ?>
                    <span class="invalid-feedback text-danger">
                        <strong><?php echo e($errors->first('image')); ?></strong>
                    </span>
                <?php endif; ?>
            </div>
            <div class="form-group">
                <label for="region">Region:</label>&nbsp;
                <input type="text" class="form-control" name="region" value ="{{ old('region') }}" />
                @if ($errors->has('region'))
                    <span class="invalid-feedback text-danger">
                        <strong>{{ $errors->first('region') }}</strong>
                    </span>
                @endif
            </div>
            <?php if($type == 'winery'){?>
            <div class="form-group winery">
                <label for="varietal">Varietal:</label>&nbsp;
                 <input type="text" class="form-control" name="varietal" value ="{{ old('varietal') }}" />
                @if ($errors->has('varietal'))
                    <span class="invalid-feedback text-danger">
                        <strong>{{ $errors->first('varietal') }}</strong>
                    </span>
                @endif
            </div>
        <?php } ?>
            <div class="form-group">
                <label for="country">Country:</label>&nbsp;
                <select name="country_id" id="country">
                    <option value="">Select</option>
                    @foreach($countries as $country)
                        <option value="{{$country->id}}">{{$country->nicename}}</option>
                    @endforeach
                </select>
                @if ($errors->has('country_id'))
                    <span class="invalid-feedback text-danger">
                        <strong>{{ $errors->first('country_id') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group images"> 
                <label for="default_image">Background Image:</label>
                <input type="file" onchange="$('.added-img').remove();" name="product_background_img" id="upload" />
                <?php if($errors->has('product_background_img')): ?>
                    <span class="invalid-feedback text-danger">
                        <strong><?php echo e($errors->first('product_background_img')); ?></strong>
                    </span>
                <?php endif; ?>
            </div>
            <input type ="hidden" name="added_by" value="{{ Auth::user()->id }}" / >
            <input type ="hidden" name="cat_id" value="{{$cat_id}}" / >
            <button type="submit" class="btn btn-primary">Create</button>
            </form>
        </div>
    </div>

</div>


<script>
    window.onload = function() {
        CKEDITOR.replace( 'page-ckeditor', {
            filebrowserUploadUrl: '{{route("upload",["_token" => csrf_token()])}}'
        });
    };
</script>

@endsection

@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Add Product</h1>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('pages') }}">Products</a></li>
		<li>Add Product</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <a href="{{action('ProductsController@addProduct',[$data['winesandspirits']->id,$data['winesandspirits']->type])}}" class="btn btn-primary">Add Product</a>
        </div>
    </div>

	<br />
		<div class="row">
		<div class="col-sm-12">
        @if(isset($data['products']) && count($data['products'])>0)
        <table class="table table-striped"  id="menu_table">
          <thead>
            <tr>
              <th width="7%"></th>
              <th width="5%">Title</th>
              <th width="5%">Title</th>
              <th width="10%">Descripton</th>
              <th width="5%">Photo</th>
              <th width="10%">Country</th>
              <th width="10%">Region</th>
              <th width="25%">Action</th>
            </tr>
          </thead>
          <tbody id="nav-menu-body">
            @foreach($data['products'] as $productDetails)
                <tr class="dd-item-tr" data-id="{{$productDetails->id}}">
                    <td class="dd-handle-field"><div style="width:40px; text-align:center;"><i class="fa fa-arrows"></i></div></td>
                    <td>{{$productDetails->product_title}}</td>
                    <td>{{$productDetails->wine->type}}</td>
                    <td>{!! str_limit($productDetails->description, 50) !!}</td>
                    <td><img src="{{URL::to('/')}}/uploads/{{ $productDetails->image }}" height="30" width="50"></td>
                    <td>{{$productDetails->country->nicename}}</td>
                    <td>{{$productDetails->region}}</td>
                    <td>
                        <a href="{{action('ProductsController@editProduct',[$productDetails->id,$productDetails->wine->type])}}" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>&nbsp;
                        <a href="{{action('ProductsController@delete',$productDetails->id)}}" onclick="return confirm('Are you sure want to delete?')" class="btn btn-danger delete_page"><i class="fa fa-trash-o"></i>&nbsp;DELETE</a>
                    </td>
                </tr>
            @endforeach
          </tbody>
        </table>
      @else
      <div class="alert alert-info">
        There are no Product added yet.
      </div>
      @endif
		</div>
	</div>

</div >
@endsection
@section('after_scripts')
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('#menu_table').DataTable( {
    "aoColumnDefs" : [
      {
      'bSortable' : false,
      'aTargets' : [ 6, 7 ]
      }
    ]
    });
  $('#menu_table_paginate').addClass('pull-right');
});
$(function() {
  var fieldOrder = [];
  $( "#nav-menu-body" ).sortable({      
    handle: ".dd-handle-field",
    update: function(event, ui) {
        $('.dd-item-tr').each(function(){         
            fieldOrder.push($(this).attr('data-id'));
        });
        $.ajax({
            type:'POST',
            url:'/admin/product',
            data:{'_token': '<?php echo csrf_token() ?>','order':fieldOrder},
            success:function(cost){
                new PNotify({'text':'Product has been re-orders.','type':'success'});
                fieldOrder.length = 0;
            }
        });
    }
  });
});
</script>
@endsection

@extends('backpack::layout')

@section('header')
<section class="content-header">
	<!-- <h1>Add Product</h1> -->
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('pages') }}">Products</a></li>
		<li>Add Product</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
			<!-- <a href='{{url("/admin/product/$id/add-product")}}' class="btn btn-primary">Add Product</a> -->
        </div>
    </div>

	<br />
		<div class="row">
		<div class="col-sm-12">
			
        <table class="table table-striped">
          <thead>
            <tr>
              <th width="5%">Title</th>
              <th width="10%">Descripton</th>
              <th width="5%">Photo</th>
              <th width="10%">Country</th>
              <th width="10%">Region</th>
              <th width="10%">Varietal</th>
            </tr>
          </thead>
          <tbody>
            
            <tr>
              <td><a href='{{url("/admin/showProduct/".$edit->id)}}'>{{$edit->product_title}}</a></td> 
              <td>{!!html_entity_decode($edit->description)!!}</td>
              <td><img src="{{URL::to('/')}}/uploads/{{ $edit->image }}" height="30" width="50"></td>
              <td>{{$edit->country->nicename}}</td>
              <td>{{$edit->region}}</td>
              <td>{{$edit->varietal}}</td>
            </tr>
          </tbody>
        </table>
      <!-- <div class="alert alert-info">
        There are no Product added yet.
      </div> -->
		</div>
	</div>

</div >
@endsection
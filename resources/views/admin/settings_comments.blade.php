@extends('backpack::layout')

@section('header')
<section class="content-header">
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li>Comments Settings</li>
	</ol>
</section>
@endsection
@section('content')
<div class="body">
	<div class="blog_comment_update_messgaes hidden"></div>
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{backpack_url('setting_blog_comment?type=blogs_comments_options')}}" class="form-horizontal" id="blog_comment_settings">
			<input type="hidden" value="{{csrf_token()}}" name="_token" />
			<input type="hidden" value="{{csrf_token()}}" name="_token" />
			<div class="sitesettings">
				<h3>Blog Comments Settings</h3><br/>
				<div class="form-group">
					<label for="site_title" class="col-sm-2 control-label">Default Approve Comments</label>
						<div class="col-sm-10">
							<input type="checkbox" 
							name="settings[default_approve_comments]"
							{{ config('constants.DEFAULT_APPROVE_COMMENTS') == '1' ? 'checked' : '' }}
							value="1">
							check to allow Approve
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Allow Mail Notifications</label>
						<div class="col-sm-10">
							<input type="checkbox" 
							name="settings[comment_notification_allow]"
							{{ config('constants.COMMENT_NOTIFICATION_ALLOW') == '1' ? 'checked' : '' }}
							value="1">
							check to allow
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">
							Notification Email</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" 
							name="settings[comment_notification_email]" 
							value="{{ config('constants.COMMENT_NOTIFICATION_EMAIL') }}">
						</div>
					</div>
					<div class="row">
					    <div class="col-md-12"> 
					        <button type="submit" class="btn btn-primary">Update Settings</button>
					    </div>
				    </div> 
				</div>
			</form>
		</div>
	</div>
</div>
<script src="{{ asset('/') }}dist/js/jquery.js"></script>
<script>
	$(function() {
		$(document).on('submit', '#blog_comment_settings', function(e) {
			e.preventDefault();
			var formData = new FormData(this);
			let mission_list_update = $(this).data('mission_list_update');
			$.ajax({
				url: '{{backpack_url("setting_blog_comment?type=blogs_comments_options&ajax=1")}}',
				data: formData,
				type: 'POST',
				dataType: 'json',
				processData: false,
				contentType: false,
				success: function() {
					let html = '<div class="alert alert-success">Settings are update successfully.</div>';
					$(".blog_comment_update_messgaes").empty().html(html);
					$(".blog_comment_update_messgaes").removeClass('hidden');
				},
				error: function(xhr, status, error) {
					let html = '<div class="alert alert-danger">Failed to update settings.</div>';
					$(".blog_comment_update_messgaes").empty().html(html);
					$(".blog_comment_update_messgaes").removeClass('hidden');
				}
			});
		});
	});
</script>
@endsection

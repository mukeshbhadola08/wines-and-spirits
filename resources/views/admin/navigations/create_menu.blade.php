@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
		Create @if(isset($id)) Sub @endif Navigation Menu
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Navigations  </li>
      </ol>
    </section>
@endsection

@section('content')
	@if ($errors->any())
	<div class="alert alert-danger">
        <ul>
		@foreach($errors->all() as $error)
			<li>{{$error}}</li>
		@endforeach
		</ul>
	</div><br />
	@endif
    <div class="row">
		<div class="col-sm-12">
			<form method="post" class="form-horizontal" action="{{backpack_url('save_menu')}}" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<div class="col-sm-2">
					<label for="title">Navigation Title:</label>
				</div>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="title"/>
				</div>
			</div>	
			
			<div class="form-group">
				<div class="col-sm-2">
				<label for="Page">Choose Page Link: </label>
				</div>
				<div class="col-sm-8">
				<select class="form-control" name="page">
					<option value="homepage">Home Page</option>
					<option value="wineslisting">Wines Listing</option>
					<option value="spiritslisting">Spirits Listing</option>
					<option value="bloglisting">Blogs Listing</option>
					<option value="contactus">Contact Us</option>
					@if(isset($pages) && count($pages)>0)
						<optgroup label="Pages">
						@foreach($pages as $p)
							<option value="-page-{{$p->id}}">{{$p->page_title}}</option>
						@endforeach
						</optgroup>
					@endif
				</select>
				</div>
			</div>
			
			<div class="form-group hidden">
				<div class="col-sm-2">
				<label for="visible">Visible:</label>
				</div>
				<div class="col-sm-8">
				<input type="hidden"  name="visible" value="1" />
				</div>
			</div>
			@if(!isset($id))
				<div class="form-group">
					<div class="col-sm-2">
					<label for="visible">Show In Header:</label>
					</div>
					<div class="col-sm-8">
					<input type="checkbox"  name="show_header" value="1" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-2">
					<label for="visible">Show In Footer:</label>
					</div>
					<div class="col-sm-8">
					<input type="checkbox"  name="show_footer" value="1" />
					</div>
				</div>
			@endif

			<input type="hidden" name="parent" value="{{ $parent = isset($id)?$id:0}}" />
			<input type ="hidden" name="lang_code" value="en" / >
			<input type ="hidden" name="parent_lang_id" value="0" / >
			<button type="submit" class="btn btn-primary">Create</button>
			</form>
		</div>
	</div>
@endsection
@section('after_scripts')
<script>

		 jQuery(document).on('change','.nav_type',function(){
			
			if (this.value == 1) {
				$('#page_form_fields').show();
				$('#blog_form_fields').hide();
			}
			else {
				$('#page_form_fields').hide();
				$('#blog_form_fields').show();
			}
		}); 
		
</script>
@endsection

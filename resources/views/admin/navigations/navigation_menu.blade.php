@extends('backpack::layout')

@section('after_styles')
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
@endsection

@section('header')
    <section class="content-header">
      <h1>
         Navigations
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Navigations  </li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            
				<a href="{{url('/admin/create_menu')}}" class="btn btn-primary">Create Navigation</a>
			
        </div>
    </div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			@if(isset($navigations) && count($navigations)>0)
			<table class="table table-striped" id="menu_table">
				<thead>
				<tr>
					<th></th>
					<th>Title</th>
					<th>Page/News</th>
					<th>Static Page</th>
					<th class="hidden">Visible</th>
					<th>Header</th>
					<th class="hidden">Footer</th>
					<th class="hidden">Dutch</th>
					<th width="30%">Action</th>
				</tr>
				</thead>
				<tbody id="nav-menu-body">
				@foreach($navigations as $nav)
					<tr class="dd-item-tr" data-id="{{$nav->id}}">
						<td class="dd-handle-field"><div style="width:40px; text-align:center;"><i class="fa fa-arrows"></i></div></td>
						<td>{{$nav->title}}</td>
						<td>
							@php
								
								if($nav->type == 1){
									$pageDetails = $nav->page($nav->type)->first();
									$ttitle = $pageDetails->page_title;
									
								} else if($nav->type == 2){
									$blogDetails = $nav->page($nav->type)->first();
									$ttitle = $blogDetails->blog_title;
								} else {
									$ttitle = "N/A";
								}

							@endphp
							{{$ttitle}}
						</td>
						<td>
							@php
								
								if($nav->type == 3){
									if($nav->static_page_name == "eventslisting"){
										$statictitle = "Events Listing Page";
									} else if($nav->static_page_name == "resultslisting"){
										$statictitle = "Results Listing Page";
									} else if($nav->static_page_name == "portfoliolisting"){
										$statictitle = "Portfolio Listing Page";
									} else if($nav->static_page_name == "bloglisting"){
										$statictitle = "Blogs Listing Page";
									} else if($nav->static_page_name == "routinelisting"){
										$statictitle = "Routine listing Page";
									} else if($nav->static_page_name == "wineslisting"){
										$statictitle = "Wines listing Page";
									} else if($nav->static_page_name == "spiritslisting"){
										$statictitle = "Spirits listing Page";
									} else {
										$statictitle = "Home Page";
									}
								} else {
									$statictitle = "N/A";
								}

							@endphp
							{{$statictitle}}
						</td>
						<td class="hidden">@if($nav->is_visible==1) Yes @else No @endif</td>
						<td>@if($nav->show_header==1) Yes @else No @endif</td>
						<td class="hidden">@if($nav->show_footer==1) Yes @else No @endif</td>
						<td class="hidden">
							@php
							 $parentDetails = App\Navigation::where('parent_lang_id',$nav->id)->first();
							@endphp
							@if(empty($parentDetails))
								<a href="{{action('NavigationController@addInSpanish',$nav->id)}}" class="btn btn-info" ><i class="fa fa-language" ></i>&nbsp;Add Dutch</a>
							@else
								<a href="{{action('NavigationController@editNavInLang',$parentDetails->id)}}" class="btn btn-info"><i class="fa fa-language" ></i>&nbsp;Edit Dutch</a>
							@endif
						</td>
						<td><a href="{{action('NavigationController@getSubNav',$nav->id)}}" class="btn btn-info hidden">View Sub-Navigation</a>&nbsp;&nbsp;<a href="{{action('NavigationController@editNav',$nav->id)}}" class="btn btn-primary">Edit</a>&nbsp;&nbsp;<a href="{{action('NavigationController@deleteNav',$nav->id)}}" class="btn btn-danger delete_nav">DELETE</a></td>
					</tr>
				@endforeach
				</tbody>
			</table>
			@else
			 <p>No Navigation is added.</p>
			@endif
		</div>
	</div>
</div >
@endsection
@section('after_scripts')
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('#menu_table').DataTable( {
		"aoColumnDefs" : [
			{
			'bSortable' : false,
			'aTargets' : [ 6, 7 ]
			}
		]
    });
	$('#menu_table_paginate').addClass('pull-right');
});
jQuery(document).on('click','.delete_nav',function(e){
	if(confirm("Are your sure?")){
		return true;
	} else{
		return false;
	}
});
$(function() {
	var fieldOrder = [];
	$( "#nav-menu-body" ).sortable({			
		handle: ".dd-handle-field",
		update: function(event, ui) {
			$('.dd-item-tr').each(function(){					
				fieldOrder.push($(this).attr('data-id'));					
			});
			$.ajax({
			   type:'POST',
			   url:'/admin/navigation_order',
			   data:{'_token': '<?php echo csrf_token() ?>','order':fieldOrder},
			   success:function(cost){
				 new PNotify({'text':'Navigation has been re-orders.','type':'success'});
				 fieldOrder.length = 0;
			   }
			});
		}
	});
});
</script>
@endsection

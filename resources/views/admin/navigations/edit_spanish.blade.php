@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
		Edit Navigation Menu
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Navigations  </li>
      </ol>
    </section>
@endsection

@section('content')
	@if ($errors->any())
	<div class="alert alert-danger">
        <ul>
		@foreach($errors->all() as $error)
			<li>{{$error}}</li>
		@endforeach
		</ul>
	</div><br />
	@endif
    <div class="row">
		<div class="col-sm-12">
			<form method="post" class="form-horizontal" action="{{action('NavigationController@updateNavigation',$navigation->id)}}" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<div class="col-sm-2">
					<label for="title">Navigation Title:</label>
				</div>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="title" value="{{$navigation->title}}"/>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-2">
					<label for="title">Do you want to show:</label>
				</div>
				<div class="col-sm-8">
					<input type="radio" name="type" value="1" class="nav_type" @if($navigation->type == 1) checked @endif  > Page 
					<input type="radio" name="type" value="2" class="nav_type" @if($navigation->type == 2) checked @endif> Blog 
				</div>
			</div>
			<div class="form-group" id="page_form_fields" @if($navigation->type == 2) style="display:none;" @endif >
				<div class="col-sm-2">
				<label for="Page">Page:</label>
				</div>
				<div class="col-sm-8">
				<select class="form-control" name="page" >
					<option value="">Select Page</option>
					 @if(isset($pages) && count($pages)>0)
						@foreach($pages as $p)
							@php
								$selected = '';
								if($navigation->type == 1 && $navigation->reference_type_id == $p->id){
									$selected = 'selected="selected"';
								}
							@endphp
							<option value="{{$p->id}}" {{$selected}} >{{$p->page_title}}</option>
						@endforeach
					 @endif
				</select>
				</div>
			</div>

			<div class="form-group" id="blog_form_fields" @if($navigation->type == 1) style="display:none;" @endif >
				<div class="col-sm-2">
				<label for="Page">Blog:</label>
				</div>
				<div class="col-sm-8">
				<select class="form-control" name="blog" >
					<option value="">Select Blog</option>
					 @if(isset($blogs) && count($blogs)>0)
						@foreach($blogs as $p)
							@php
								$selected = '';
								if($navigation->type == 2 && $navigation->reference_type_id == $p->id){
									$selected = 'selected="selected"';
								}
							@endphp
							<option value="{{$p->id}}" {{$selected}} >{{$p->blog_title}}</option>
						@endforeach
					 @endif
				</select>
				</div>
			</div>
			
			<input type ="hidden" name="visible" value="{{$navigation->is_visible}}" / >
			<input type ="hidden" name="show_header" value="{{$navigation->show_header}}" / >
			<input type ="hidden" name="show_footer" value="{{$navigation->show_footer}}" / >

			<input type="hidden" name="parent" value="{{$navigation->parent_id}}" />
			<button type="submit" class="btn btn-primary">Update</button>
			</form>
		</div>
	</div>
@endsection
@section('after_scripts')
<script>

		 jQuery(document).on('change','.nav_type',function(){
			
			if (this.value == 1) {
				$('#page_form_fields').show();
				$('#blog_form_fields').hide();
			}
			else {
				$('#page_form_fields').hide();
				$('#blog_form_fields').show();
			}
		}); 
		
</script>
@endsection

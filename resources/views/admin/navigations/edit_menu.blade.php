@extends('backpack::layout')



@section('header')

    <section class="content-header">

      <h1>

		Edit Navigation Menu

      </h1>

      <ol class="breadcrumb">

        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>

        <li class="active">Navigations  </li>

      </ol>

    </section>

@endsection



@section('content')

	@if ($errors->any())

	<div class="alert alert-danger">

        <ul>

		@foreach($errors->all() as $error)

			<li>{{$error}}</li>

		@endforeach

		</ul>

	</div><br />

	@endif

    <div class="row">

		<div class="col-sm-12">

			<form method="post" class="form-horizontal" action="{{action('NavigationController@updateNavigation',$navigation->id)}}" enctype="multipart/form-data">

			<div class="form-group">

				<input type="hidden" value="{{csrf_token()}}" name="_token" />

				<div class="col-sm-2">

					<label for="title">Navigation Title:</label>

				</div>

				<div class="col-sm-8">

					<input type="text" class="form-control" name="title" value="{{$navigation->title}}"/>

				</div>

			</div>

			<div class="form-group hidden">

				<div class="col-sm-2">

					<label for="title">Do you want to show:</label>

				</div>

				<div class="col-sm-8">

					 Page 

					<!-- <input type="radio" name="type" value="2" class="nav_type" @if($navigation->type == 2) checked @endif> Blog  -->

				</div>

			</div>

			<input type="hidden" name="type" value="1" class="nav_type" @if($navigation->type == 1) checked @endif  >
			
			<div id="page_form_fields" @if($navigation->type == 2) style="display:none;" @endif >
				<div class="form-group">
				<div class="col-sm-2 ">
				<label for="Page">Choose Page Link: </label>
				</div>
				<div class="col-sm-8">
				<?php
				$static_page_name = $navigation->static_page_name;
				$homepage_selected = $wineslisting_selected = $spiritslisting_selected = $bloglisting_selected = $contactus_selected = '';
				if ($static_page_name == 'homepage') {
					$homepage_selected = 'selected';
				}
				if ($static_page_name == 'wineslisting') {
					$wineslisting_selected = 'selected';
				}
				if ($static_page_name == 'spiritslisting') {
					$spiritslisting_selected = 'selected';
				}
				if ($static_page_name == 'bloglisting') {
					$bloglisting_selected = 'selected';
				}
				if ($static_page_name == 'contactus') {
					$contactus_selected = 'selected';
				}
				?>
				<select class="form-control" name="page" >
					<option value="homepage" <?php echo $homepage_selected; ?>>Home Page</option>
					<option value="wineslisting" <?php echo $wineslisting_selected; ?>>Wines Listing</option>
					<option value="spiritslisting" <?php echo $spiritslisting_selected; ?>>Spirits Listing</option>
					<option value="bloglisting" <?php echo $bloglisting_selected; ?>>Blogs Listing</option>
					<option value="contactus" <?php echo $contactus_selected; ?>>Contact Us</option>
					@if(isset($pages) && count($pages)>0)
						<optgroup label="Pages">
						@foreach($pages as $p)
							<?php
							$option_selected = '';
							if($navigation->reference_type_id == $p->id){
								$option_selected = 'selected';
							}
							?>
							<option <?php echo $option_selected; ?> value="-page-{{$p->id}}">{{$p->page_title}}</option>
						@endforeach
						</optgroup>
					@endif					 
				</select>
				</div>
			</div>
	
			<div class="form-group hidden">
				<div class="col-sm-2">
				<label for="visible">Visible:</label>
				</div>
				<div class="col-sm-8">
					@if($navigation->is_visible == 1)
						<input type="checkbox"  name="visible" value="1" checked="checked" />
					@else
						<input type="checkbox"  name="visible" value="1" />
					@endif
				</div>
			</div>

			@if($navigation->parent_id == 0)
				<div class="form-group">
					<div class="col-sm-2">
					<label for="visible">Show In Header:</label>
					</div>
					<div class="col-sm-8">
						@if($navigation->show_header == 1)
							<input type="checkbox"  name="show_header" value="1" checked="checked" />
						@else
							<input type="checkbox"  name="show_header" value="1" />
						@endif
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-2">
					<label for="visible">Show In Footer:</label>
					</div>
					<div class="col-sm-8">
						@if($navigation->show_footer == 1)
							<input type="checkbox"  name="show_footer" value="1" checked="checked" />
						@else
							<input type="checkbox"  name="show_footer" value="1" />
						@endif
					</div>
				</div>
			@endif

			<input type="hidden" name="parent" value="{{$navigation->parent_id}}" />

			<button type="submit" class="btn btn-primary">Update</button>

			</form>

		</div>

	</div>

@endsection

@section('after_scripts')

<script>



		 jQuery(document).on('change','.nav_type',function(){

			

			if (this.value == 1) {

				$('#page_form_fields').show();

				$('#blog_form_fields').hide();

			}

			else {

				$('#page_form_fields').hide();

				$('#blog_form_fields').show();

			}

		}); 

		

</script>

@endsection


@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <ul class="list-inline nav nav-tabs">
            <li><a href="{{ backpack_url('settings') }}">General Settings</a></li>
            <li><a href="{{ backpack_url('contact') }}">Contact us</a></li>
        </ul>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Contact us</li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <a href="{{url('/admin/contact/add')}}" class="btn btn-primary">Add contact</a>
        </div>
    </div>

	<br />
		<div class="row">
		<div class="col-sm-12">
			@if(isset($data) && count($data)>0)
        <table class="table table-striped" id="example1">
          <thead>
            <tr>
              <th width="5%">Address</th>
              <th width="10%">Contact</th>
              <th width="5%">Email</th>
              <th width="25%">Action</th>
            </tr>
          </thead>
          <tbody>
          
          @foreach($data as $dataDetails)
            <tr>
              <td>{{$dataDetails->address}}</a></td>
              <td>{{$dataDetails->contact}}</td>
              <td>{{$dataDetails->email}}</td>
              <td>
               <a href="{{action('ContactusController@edit',$dataDetails->id)}}" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>&nbsp;
                
                <a href="{{action('ContactusController@delete',$dataDetails->id)}}" onclick="return confirm('Are you sure want to delete?')" class="btn btn-danger delete_page"><i class="fa fa-trash-o"></i>&nbsp;DELETE</a>
                
              </td>
            </tr>
          @endforeach 
          </tbody>
        </table>
      @else
      <div class="alert alert-info">
        There are no Contact added yet.
      </div>
      @endif
		</div>
	</div>

</div >
@endsection
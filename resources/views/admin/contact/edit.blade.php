@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Edit Contact</h1>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('Contact') }}">Contact</a></li>
		<li>Edit Contact</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">
	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{action('ContactusController@update',$id)}}" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="address">Address:</label>
				<input type="text" class="form-control" name="address" value="{{$contact->address}}"/>
			</div>
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="Latitude">Latitude:</label>
				<input type="text" class="form-control" name="latitude" value ="{{ $contact->latitude }}" />
				@if ($errors->has('latitude'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('latitude') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="longitude">Longitude:</label>
				<input type="text" class="form-control" name="longitude" value ="{{ $contact->longitude }}" />
				@if ($errors->has('longitude'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('longitude') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
				<label for="contact">Contact:</label>
				<input type="number" class="form-control" name="contact" value ="{{ old('contact') }}" />
				@if ($errors->has('contact'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('contact') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
				<label for="contact">Contact:</label>
				<input type="number" class="form-control" name="contact" value ="{{$contact->contact}}" />
				@if ($errors->has('contact'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('contact') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" class="form-control" name="email" value ="{{$contact->email}}" />
				@if ($errors->has('email'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
				<label for="visible">Show In Contactus Page:</label>
					@if($contact->show_contact == 1)
						<input type="checkbox"  name="show_contact" value="1" checked="checked" />
					@else
						<input type="checkbox"  name="show_contact" value="1" />
					@endif
			</div>

			<input type ="hidden" name="added_by" value="{{ Auth::user()->id }}" / >
			<button type="submit" class="btn btn-primary" id="add" >Update</button>
			</form>
		</div>
	</div>

</div >

<script>
	CKEDITOR.replace( 'blog-ckeditor' );
</script>


@endsection

@section('after_scripts')
<script>
// $(function () {
// 	var _URL = window.URL || window.webkitURL;
// 	$("#upload").on("change", function () {
// 		 var file, img;
// 		if ((file = this.files[0])) {
// 			if ( (/\.(png|jpeg|jpg|gif)$/i).test(file.name) ) {
// 				img = new Image();
// 				img.onload = function () {
// 					if(this.width<1000 && this.height<686){
// 					   new PNotify({'text':'Image dimension is not correct.','type':'error'}); 
// 					    $('#add_slide').attr('disabled','disabled');
// 					} else {
// 						$('#add_slide').removeAttr('disabled');
// 					}
// 				};
// 				 img.src = _URL.createObjectURL(file); 
// 		  } else {
// 			errors = file.name +" Unsupported Image extension.";  
// 			new PNotify({'text':errors,'type':'error'});
// 				 $('#add_slide').attr('disabled','disabled');
// 		  }
			
// 		}
// 	});
// });
		 
</script>
@endsection

@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Add Contactus</h1>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('history') }}">Contact</a></li>
		<li>Add Contact</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">
	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{backpack_url('contact/store')}}">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="address">Address:</label>
				<input type="text" class="form-control" name="address" value ="{{ old('address') }}" />
				@if ($errors->has('address'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('address') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="Latitude">Latitude:</label>
				<input type="text" class="form-control" name="latitude" value ="{{ old('latitude') }}" />
				@if ($errors->has('latitude'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('latitude') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="longitude">Longitude:</label>
				<input type="text" class="form-control" name="longitude" value ="{{ old('longitude') }}" />
				@if ($errors->has('longitude'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('longitude') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
				<label for="contact">Contact:</label>
				<input type="number" class="form-control" name="contact" value ="{{ old('contact') }}" />
				@if ($errors->has('contact'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('contact') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" class="form-control" name="email" value ="{{ old('email') }}" />
				@if ($errors->has('email'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
				<label for="contact">Show In Contactus Page:</label>
				<input type="checkbox"  name="show_contact" value="1" />
			</div>
			<button type="submit" class="btn btn-primary" id="add_slide" >Create</button>
			</form>
		</div>
	</div>

</div >

@endsection

@section('after_scripts')
@endsection

@extends('backpack::layout')

@section('header')
<section class="content-header">
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li>Settings</li>
	</ol>
</section>
@endsection
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
@section('content')
<div class="body">
	
	<?php
	foreach ($settings as $setting){
		$variable = $setting->option_name;
		$$variable = $setting->option_value;
	}
	?>
	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{backpack_url('settings')}}" enctype="multipart/form-data" class="form-horizontal">
			
			<input type="hidden" value="{{csrf_token()}}" name="_token" />

			<div class="sitesettings">
				<ul class="list-inline nav nav-tabs">
				    <li><a href="{{ backpack_url('settings') }}">General Settings</a></li>
				    <li><a href="{{ backpack_url('contact') }}">Contact us</a></li>
			  	</ul>
			  	@if(\Session::has('success'))
			        <div class="alert alert-success">
			            {{\Session::get('success')}}
			        </div>
			    @endif
				@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				<h3>Site Settings</h3><br/>
				<div class="form-group">
					<label for="site_title" class="col-sm-2 control-label">Company Name</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[site_title]" placeholder="Site Title" value="{{ config('constants.SITE_TITLE') }}">
					</div>
				</div>
				<div class="form-group">
					<label for="admin_email" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[admin_email]" placeholder="Admin Email" value="{{ config('constants.ADMIN_EMAIL') }}">
					</div>
				</div>
				<div class="form-group">
					<label for="phone_no" class="col-sm-2 control-label">Contact</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[phone_no]" placeholder="Phone no." value="{{ config('constants.PHONE_NO') }}">
					</div>
				</div>
				<div class="form-group">
					<label for="phone_no" class="col-sm-2 control-label">Fax</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[phone_no]" placeholder="Phone no." value="{{ config('constants.PHONE_NO') }}">
					</div>
				</div>
				<div class="form-group">
					<label for="address" class="col-sm-2 control-label">Address</label>
					<div class="col-sm-10">
						<textarea class="form-control" value="{{ config('constants.ADDRESS') }}" name="settings[address]">{{ config('constants.ADDRESS') }}</textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="address" class="col-sm-2 control-label">Latitude</label>
					<div class="col-sm-10">
						<input class="form-control" value="{{ config('constants.LATITUDE') }}" name="settings[latitude]" value="{{ config('constants.LATITUDE') }}">
					</div>
				</div>
				<div class="form-group">
					<label for="address" class="col-sm-2 control-label">Longitude</label>
					<div class="col-sm-10">
						<input class="form-control" value="{{ config('constants.LONGITUDE') }}" name="settings[longitude]" value="{{ config('constants.LONGITUDE') }}">
					</div>
				</div>
				<div class="form-group">
					<label for="address" class="col-sm-2 control-label">Google API</label>
					<div class="col-sm-10">
						<textarea class="form-control" value="{{ config('constants.GOOGLE_API') }}" name="settings[google_api]">{{ config('constants.GOOGLE_API') }}</textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="address" class="col-sm-2 control-label">Meta Keywords</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="metasetting" placeholder="Phone no." value="{{ config('constants.metasetting') }}">
					</div>
				</div>
				<div class="form-group">
					<label for="tag_line" class="col-sm-2 control-label">Meta Description</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="settings[tag_line]" placeholder="Tag Line" value="{{ config('constants.TAG_LINE') }}">
					</div>
				</div>
				<div class="form-group">
						<label for="background_image" class="col-sm-2 control-label">Header Logo
							<a target="_blank" href="{{ config('constants.WINES_LOGO') }}" class="fa fa-eye"></a>
						</label>
						<div class="col-sm-10">
							<input type="file" class="form-control" name="settings[wines_logo]" />
						</div>
					</div>
					<div class="form-group">
						<label for="background_image" class="col-sm-2 control-label">Footer Logo
						<a target="_blank" href="{{ config('constants.WINES_FOOTER_LOGO') }}" class="fa fa-eye"></a>
						</label>
						<div class="col-sm-10">
							<input type="file" class="form-control" name="settings[wines_footer_logo]" value="{{ config('constants.WINES_FOOTER_LOGO') }}">
						</div>
					</div>
					<div class="form-group">
						<label for="background_image" class="col-sm-2 control-label">Facebook</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="settings[wines_fb_link]" value="{{ config('constants.WINES_FB_LINK') }}">
						</div>
					</div>
					<div class="form-group">
						<label for="background_image" class="col-sm-2 control-label">INSTAGRAM</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="settings[wines_insta_link]" value="{{ config('constants.WINES_INSTA_LINK') }}">
						</div>
					</div>
					<div class="form-group">
						<label for="background_image" class="col-sm-2 control-label">Twitter</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="settings[wines_twitter_link]" value="{{ config('constants.WINES_TWITTER_LINK') }}">
						</div>
					</div>
					<div class="form-group">
						<label for="background_image" class="col-sm-2 control-label">Youtube</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="settings[wines_youtube_link]" value="{{ config('constants.WINES_YOUTUBE_LINK') }}">
						</div>
					</div>
					<div class="form-group">
						<label for="background_image" class="col-sm-2 control-label">LinkedIn</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="settings[wines_linkedin_link]" value="{{ config('constants.WINES_LINKEDIN_LINK') }}">
						</div>
					</div>
			<button type="submit" class="btn btn-primary">Update Settings</button>
			</form>
		</div>
	</div>

</div>

@endsection

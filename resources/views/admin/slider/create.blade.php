@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Add Slider</h1>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('slider') }}">Slider</a></li>
		<li>Add Slider</li>
	</ol>
		<div class="alert alert-info" style="margin-bottom: 0;">
	    <strong>Note: </strong>
	    Please note that recommended dimension of image is 1200X450 pixels.
	</div>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">

	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{backpack_url('photo/create')}}" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="title">Title:</label>
				<input type="text" class="form-control" name="title" value ="{{ old('title') }}" />
				@if ($errors->has('title'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('title') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
				<label for="description">Description:</label>
				<textarea name="description" class="form-control" id="blog-ckeditor">{{ old('description') }}</textarea>
				@if ($errors->has('description'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('description') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">	
				<label for="image">Image:</label>
				<input type="file"  name="image" id="upload" />
				@if ($errors->has('image'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('image') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group"></div>
			<div class="form-group">
				<input type="checkbox" name = "is_visible" value="1 {{ old('is_visible') ? 'checked' : '' }} "/>&nbsp;
				<label for="is_visible">Show in homepage slider</label>
			</div>						
			<input type ="hidden" name="lang_code" value="en" / >
			<button type="submit" class="btn btn-primary" id="add_slide" >Create</button>
			</form>
		</div>
	</div>

</div >
<script>
	CKEDITOR.replace( 'blog-ckeditor' );
</script>

@endsection

@section('after_scripts')
@endsection

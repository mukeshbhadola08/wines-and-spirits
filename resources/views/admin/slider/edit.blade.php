@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Edit Slider</h1>
	<div class="alert alert-info" style="margin-bottom: 0;">
	    <strong>Note: </strong>
	    Please note that recommended dimension of image is 1200X450 pixels.
	</div>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('slider') }}">Slider</a></li>
		<li>Edit Slider</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">
	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{action('SliderController@update',$id)}}" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="blog_title">Title:</label>
				<input type="text" class="form-control" name="title" value="{{$slide->title}}"/>
			</div>
			<div class="form-group">
			
				<label for="blog_description">Description:</label>
				<textarea name="description" class="form-control"  id="blog-ckeditor">{{$slide->description}}</textarea>
			</div>
			<div class="form-group">	
				<label for="image">Old Image:</label>
				<img class="media-object" src="{{URL::to('/')}}/uploads/slider_images/{{ $slide->image }}" alt="no-img" height="150" >
			</div>
			
			<div class="form-group">	
				<label for="image">New Image:</label>
				<input type="file" class="form-control" name="image" id="upload" value="{{ $slide->image }}" />
				<?php if($errors->has('image')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('image')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			
			@if($slide->lang_code == 'en')	
			<div class="form-group">
				<input type="checkbox" name = "is_visible" value="1" @if($slide->is_visible == 1) checked @endif />&nbsp;
				<label for="is_visible">Show in homepage slider</label>
			</div>
			@else 
				<input type="hidden" name="is_visible" value="1">
			@endif

			<input type ="hidden" name="added_by" value="{{ Auth::user()->id }}" / >
			<button type="submit" class="btn btn-primary" id="add_slide" >Update</button>
			</form>
		</div>
	</div>

</div >

<script>
	CKEDITOR.replace( 'blog-ckeditor' );
</script>


@endsection

@section('after_scripts')
<script>
$(function () {
	var _URL = window.URL || window.webkitURL;
	$("#upload").on("change", function () {
		 var file, img;
		if ((file = this.files[0])) {
			if ( (/\.(png|jpeg|jpg|gif)$/i).test(file.name) ) {
				img = new Image();
				img.onload = function () {
					if(this.width<1000 && this.height<686){
					   new PNotify({'text':'Image dimension is not correct.','type':'error'}); 
					    $('#add_slide').attr('disabled','disabled');
					} else {
						$('#add_slide').removeAttr('disabled');
					}
				};
				 img.src = _URL.createObjectURL(file); 
		  } else {
			errors = file.name +" Unsupported Image extension.";  
			new PNotify({'text':errors,'type':'error'});
				 $('#add_slide').attr('disabled','disabled');
		  }
			
		}
	});
});
		 
</script>
@endsection

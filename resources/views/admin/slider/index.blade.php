@extends('backpack::layout')
@section('after_styles')
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
@endsection
@section('header')
    <section class="content-header">
      <h1>
        Slider
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Slider</li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
    @if(\Session::has('danger'))
        <div class="alert alert-danger">
            {{\Session::get('danger')}}
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12"><a href="{{url('/admin/photo/create')}}" class="btn btn-primary">Add Slider</a></div>
    </div><hr>

	<div class="row">
		<div class="col-sm-12">
			@if(isset($slides) && count($slides)>0)
				<table class="table table-striped" id="example1">
					<thead>
						<tr>
						  <th></th>
						  <th width="25%">Image/Video</th>
						  <th width="20%">Title</th>
						  <th>Description</th>
						  <th width="15%">Is Visible</th>
						  <th width="35%">Action</th>
						</tr>
					</thead>
					<tbody id="nav-menu-body-slider">
						
					@foreach($slides as $slideDetails)
						<tr class="dd-item-slider-tr" data-id="{{$slideDetails->id}}">
							<td width="5%" class="dd-handle-slider-field"><div style="width:40px; text-align:center;"><i class="fa fa-arrows"></i></div></td>
							<td>
								<img class="media-object" src="{{url('/uploads/slider_images/' . $slideDetails->image)}}" alt="no-img" height="100">
							</td>
							<td>{{$slideDetails->title}}</td>
							<td>{!! str_limit($slideDetails->description, 20) !!}</td>
							<td>@if($slideDetails->is_visible) YES @else No @endif</td>
							<td>
								<a href="{{action('SliderController@edit',$slideDetails->id)}}" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>&nbsp;								
								<a href="{{action('SliderController@delete',$slideDetails->id)}}" class="btn btn-danger delete_slide"><i class="fa fa-trash-o"></i>&nbsp;DELETE</a>
								
							</td>
						</tr>
					@endforeach	
					</tbody>
				</table>
			@else
			<div class="alert alert-info">
				There are no slider added yet.
			</div>
			@endif
		</div>
	</div>

</div >
@endsection

@section('after_scripts')
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>		 
jQuery(document).on('click','.delete_slide',function(e){
	if(confirm("Are you sure want to delete this slide?")){
		return true;
	} else{
		return false;
	}
});		

$(function() {
	var fieldOrder = [];
	$( "#nav-menu-body-slider" ).sortable({			
		handle: ".dd-handle-slider-field",
		update: function(event, ui) {
			$('.dd-item-slider-tr').each(function(){					
				fieldOrder.push($(this).attr('data-id'));					
			});
			$.ajax({
			   type:'POST',
			   url:'/admin/media_order',
			   data:{'_token': '<?php echo csrf_token() ?>','order':fieldOrder},
			   success:function(cost){
				 new PNotify({'text':'Media has been re-orders.','type':'success'});
				 fieldOrder.length = 0;
			   }
			});
		}
	});
});

</script>
@endsection

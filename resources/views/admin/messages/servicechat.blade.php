@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Chat Panel</h1>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('blogs') }}">Messages</a></li>
		<li>Message Thread</li>
	</ol>
</section>
@endsection

@section('content')
	<div class="container">
		<div class="people-list" id="people-list">
			<ul class="list">

				<?php 
				$firstthread = array();
				
				foreach($threads as $keythread => $thread){ 
				
					if($keythread == 0){
						$firstthread = $thread;
					}
				?>
				<li class="clearfix">
					<div class="img-area"><img src="<?php echo URL::to('dist/images/chat_avatar_02.jpg');?>" alt="img"/></div>
					<div class="about">
						<div class="name">{{$thread->latestMessage->user->name}} </div>
						<div class="status">
							<i class="fa fa-circle online"></i> {{$thread->latestMessage->created_at->diffForHumans()}}
						</div>
						<p class="people-list-message">{{$thread->latestMessage->body}}</p>
					</div>
					
				</li>
				<?php } 
		
				if(!isset($firstthread->messages)){
					$firstthread = $thread;
				}
				?>
			</ul>
		</div>

		<div class="chat">
			<div class="chat-header clearfix">
				<div class="user-img-chat">
					<img src="<?php echo URL::to('dist/images/chat_avatar_02.jpg');?>" alt="avatar">
				</div>
			
				<div class="chat-about">
					<div class="chat-with"><?php echo $firstthread->subject; ?>&nbsp;&nbsp;<a href=""><i class="fa fa-eye"></i></a></div>
					<?php if(isset($firstthread->messages)){ ?>
					<div class="chat-num-messages">Already <?php echo count($firstthread->messages);?> messages</div>
					<?php } ?>
				</div>
				<!-- <i class="fa fa-star"></i> -->
			</div> <!-- end chat-header -->
		  
			<div class="chat-history">
				<ul>
					<?php 
					if(isset($firstthread->messages)){ 
						foreach($firstthread->messages as $keymessage => $message){ 
					?>

						<?php if($message->user_id != Auth::id()){ ?>
							<li class="clearfix">
							<div class="message-data align-right">
							<span class="message-data-time">{{ $message->created_at->diffForHumans() }}</span> &nbsp; &nbsp;
							<span class="message-data-name">{{$message->user->name}}</span> <i class="fa fa-circle me"></i>

							</div>
							<div class="message other-message float-right">
							{{$message->body}}
							</div>
							</li>
						<?php } else { ?>
							<li>
							<div class="message-data">
							<span class="message-data-name"><i class="fa fa-circle online"></i> {{$message->user->name}}</span>
							<span class="message-data-time">{{ $message->created_at->diffForHumans() }}</span>
							</div>
							<div class="message my-message">
							{{$message->body}}
							</div>
							</li>
						<?php } ?>

					<?php } 
					}?>
				</ul>
			</div> <!-- end chat-history -->
		  
			<div class="chat-message clearfix">
				<form action="{{backpack_url('messages/'.$firstthread->id)}}" method="post">
				{{ csrf_field() }}
					<textarea  name="message" id="message-to-send" placeholder="Type your message" rows="3" required="true"></textarea>								
					<button type="submit">Send</button>
				</form>

			</div> <!-- end chat-message -->

	</div>
@endsection

@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Service Request For - {{ $service->name }} - Request No. <?php echo str_pad($servicerequestdetails->id, 6, "0", STR_PAD_LEFT); ?></h1>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('blogs') }}">Messages</a></li>
		<li>Message Thread</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">
	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<div class="row">
		<div class="col-sm-12">
			
			<form action="{{backpack_url('messages/create')}}" method="post">
				{{ csrf_field() }}
				<div class="col-md-6">
					
					@if($users->count() > 0)

					<input type="hidden" class="form-control" name="subject" placeholder="Subject" value="Service Request For - {{ $service->name }} - Request No. <?php echo str_pad($servicerequestdetails->id, 6, "0", STR_PAD_LEFT); ?>">
				

					<!-- Message Form Input -->
					<div class="form-group">
						<label class="control-label">Message</label>
						<textarea name="message" class="form-control">{{ old('message') }}</textarea>
					</div>
					
					<strong>Agents Providing "{{ $service->name }}"</strong><br/>
					
						<div class="checkbox">
							@foreach($users as $user)
								<label title="{{ $user->name }}"><input type="checkbox" name="recipients[]"
																		value="{{ $user->id }}">{!!$user->name!!}</label>
							@endforeach
						</div>

						<input type="hidden" name="service_request_id" placeholder="service_request_id" value="{{ $requestid }}">

						<!-- Submit Form Input -->
						<div class="form-group">
							<button type="submit" class="btn btn-primary form-control">Submit</button>
						</div>
					@else
					<p>There are no agents who provide this service</p>

					@endif
					

					
				</div>
			</form>
		</div>
	</div>

</div >

@endsection

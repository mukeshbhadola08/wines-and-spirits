@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Edit Property</h1>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('properties') }}">Properties</a></li>
		<li>Edit Property</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">
	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{action('PropertyController@update',$id)}}" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="title"><em>*</em> Title:</label>
				<input type="text" class="form-control" name="title" value="{{$property->title}}"/>
			</div>
			<div class="form-group">
				<label for="description"><em>*</em> Description:</label>
				<textarea name="description" class="form-control" id="property_description"> {{$property->description}}</textarea>
			</div>

			
			
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="description"><em>*</em> Residential Type:</label>
						<select name="residential_type" class="form-control" id="residential_type">
							<option value="1" <?php if ($property->residential_type == 1){ echo 'selected="selected"'; } ?>>Apartments</option>
							<option value="2"  <?php if ($property->residential_type == 2){ echo 'selected="selected'; } ?>>Builder Floor</option>
							<option value="3"  <?php if ($property->residential_type == 3){ echo 'selected="selected'; } ?>>Villa</option>
							<option value="4"  <?php if ($property->residential_type == 4){ echo 'selected="selected'; } ?>>Land</option>
							<option value="5"  <?php if ($property->residential_type == 5){ echo 'selected="selected'; } ?>>Farm House</option>
							<option value="6"  <?php if ($property->residential_type == 6){ echo 'selected="selected'; } ?>>Studio Apartment</option>
							<option value="7"  <?php if ($property->residential_type == 7){ echo 'selected="selected'; } ?>>Serviced Apartment</option>
						</select>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="description"><em>*</em> Sale Type:</label>
						<select name="sale_type" class="form-control" id="sale_type">
							<option value="1" <?php if ($property->sale_type == 1){ echo 'selected="selected"'; } ?>>Resale</option>
							<option value="2" <?php if ($property->sale_type == 2){ echo 'selected="selected"'; } ?>>Rent</option>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="description"><em>*</em> Price($)</label>
						<input type="text" class="form-control required" name="price" value="<?php if(isset($propertymetadata) && isset($propertymetadata['property_price'])){ echo $propertymetadata['property_price']; }?>"/>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="description"><em>*</em> No of Bedrooms:</label>
						<select name="property_bedrooms" class="form-control" id="property_bedrooms">
							<?php for($i=1;$i<10;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if(isset($propertymetadata) && isset($propertymetadata['no_of_bedrooms']) && $propertymetadata['no_of_bedrooms']==$i){ echo "selected='true'"; }?> ><?php echo $i; ?></option>
							<?php } ?>
							<option value="11" <?php if(isset($propertymetadata) && isset($propertymetadata['no_of_bedrooms']) && $propertymetadata['no_of_bedrooms']==11){ echo "selected='true'"; }?> >10+</option>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="description"><em>*</em> No of Bathrooms</label>
						<select name="property_bathrooms" class="form-control" id="property_bathrooms">
							<?php for($i=1;$i<10;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if(isset($propertymetadata) && isset($propertymetadata['no_of_bathrooms']) && $propertymetadata['no_of_bathrooms']==$i){ echo "selected='true'"; }?> ><?php echo $i; ?></option>
							<?php } ?>
							<option value="11" <?php if(isset($propertymetadata) && isset($propertymetadata['no_of_bathrooms']) && $propertymetadata['no_of_bathrooms']==11){ echo "selected='true'"; }?> >10+</option>
						</select>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="description"><em>*</em> No of Garages</label>
						<select name="property_garages" class="form-control" id="property_garages">
							<?php for($i=1;$i<10;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if(isset($propertymetadata) && isset($propertymetadata['no_of_garages']) && $propertymetadata['no_of_garages']==$i){ echo "selected='true'"; }?> ><?php echo $i; ?></option>
							<?php } ?>
							<option value="11" <?php if(isset($propertymetadata) && isset($propertymetadata['no_of_garages']) && $propertymetadata['no_of_garages']==11){ echo "selected='true'"; }?> >10+</option>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="description"><em>*</em> Area</label><br/>
						<input type="text" class="required form-control" id="property_area" name="property_area" style="width:78%;display:inline-block;"  value="<?php if(isset($propertymetadata) && isset($propertymetadata['property_area'])){ echo $propertymetadata['property_area']; }?>"/>

						<select name="property_area_type" class="form-control" id="property_area_type" style="width:20%;display:inline-block;">
							<option value="Sq.Ft." <?php if(isset($propertymetadata) && isset($propertymetadata['area_type']) && $propertymetadata['area_type']=="Sq.Ft."){ echo "selected='true'"; }?> >Sq.Ft.</option>
							<option value="Sq. Yards" <?php if(isset($propertymetadata) && isset($propertymetadata['area_type']) && $propertymetadata['area_type']=="Sq. Yards"){ echo "selected='true'"; }?> >Sq. Yards</option>
							<option value="Sq. Meter" <?php if(isset($propertymetadata) && isset($propertymetadata['area_type']) && $propertymetadata['area_type']=="Sq. Meter"){ echo "selected='true'"; }?> >Sq. Meter</option>
							<option value="Hectares" <?php if(isset($propertymetadata) && isset($propertymetadata['area_type']) && $propertymetadata['area_type']=="Hectares"){ echo "selected='true'"; }?> >Hectares</option>
						</select>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="description"><em>*</em> No of Floors</label>
						<select name="property_floors" class="form-control" id="property_floors">
							<?php for($i=1;$i<10;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if(isset($propertymetadata) && isset($propertymetadata['no_of_floors']) && $propertymetadata['no_of_floors']==$i){ echo "selected='true'"; }?> ><?php echo $i; ?></option>
							<?php } ?>
							<option value="11" <?php if(isset($propertymetadata) && isset($propertymetadata['no_of_floors']) && $propertymetadata['no_of_floors']==11){ echo "selected='true'"; }?> >10+</option>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="address"><em>*</em> Address</label>
						<textarea class="form-control" name="address"><?php if(isset($propertymetadata) && isset($propertymetadata['address'])){ echo $propertymetadata['address']; }?></textarea>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="street_address">Street Address</label>
						<input type="text" class="form-control required" name="street_address" value="<?php if(isset($propertymetadata) && isset($propertymetadata['street_address'])){ echo $propertymetadata['street_address']; }?>"/>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label for="county"><em>*</em> County</label>
						<input type="text" class="form-control required" name="county" value="<?php if(isset($propertymetadata) && isset($propertymetadata['county'])){ echo $propertymetadata['county']; }?>"/>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label for="state"><em>*</em> State</label>
						<input type="text" class="form-control required" name="state" value="<?php if(isset($propertymetadata) && isset($propertymetadata['state'])){ echo $propertymetadata['state']; }?>"/>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label for="zipcode"><em>*</em> Zipcode</label>
						<input type="text" class="form-control required" name="zipcode" value="<?php if(isset($propertymetadata) && isset($propertymetadata['zipcode'])){ echo $propertymetadata['zipcode']; }?>"/>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="contact_no">Contact Number</label>
						<input type="text" class="form-control required" name="contact_no" value="<?php if(isset($propertymetadata) && isset($propertymetadata['contact_no'])){ echo $propertymetadata['contact_no']; }?>"/>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="email_address"><em>*</em> Email Address</label>
						<input type="email" class="form-control required" name="email_address" value="<?php if(isset($propertymetadata) && isset($propertymetadata['email_address'])){ echo $propertymetadata['email_address']; }?>"/>
					</div>
				</div>
				
			</div>

			
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<img class="media-object" src="{{action('PropertyController@getImage',$property->id)}}" alt="no-img" height="150" width="150">
						<label for="image"><em>*</em> Main Image:</label>
						<input type="file" class="form-control" name="main_image" />
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<input type="checkbox" name = "is_active" value="0"  @if($property->is_active == 1) checked @endif />&nbsp;
						<label for="description">Is Active</label>
					</div>
				</div>
			</div>

			<div class="form-group">	
				<label for="image">Other Images:</label>
				<input type="file" class="form-control" name="property_images[]" />
			</div>

			<input type ="hidden" name="user_id" value="{{ Auth::user()->id }}" / >
			<button type="submit" class="btn btn-primary">Create</button>
			</form>
		</div>
	</div>

</div >
<script>
	CKEDITOR.replace( 'property_description' );
</script>

@endsection

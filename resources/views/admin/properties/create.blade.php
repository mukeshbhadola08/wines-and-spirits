@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Add Property</h1>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('properties') }}">Properties</a></li>
		<li>Add Property</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">
	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{backpack_url('properties/create')}}" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="title"><em>*</em> Title:</label>
				<input type="text" class="form-control" name="title" value="{{ old('title') }}"/>
			</div>
			<div class="form-group">
				<label for="description"><em>*</em> Description:</label>
				<textarea name="description" class="form-control" id="property_description">{{ old('description') }}</textarea>
			</div>

			
			
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="description"><em>*</em> Residential Type:</label>
						<select name="residential_type" class="form-control" id="residential_type">
							<option value="1" @if (old('residential_type') == 1){ selected="selected" } @endif>Apartments</option>
							<option value="2">Builder Floor</option>
							<option value="3">Villa</option>
							<option value="4">Land</option>
							<option value="5">Farm House</option>
							<option value="6">Studio Apartment</option>
							<option value="7">Serviced Apartment</option>
						</select>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="description"><em>*</em> Sale Type:</label>
						<select name="sale_type" class="form-control" id="sale_type">
							<option value="1">Resale</option>
							<option value="2">Rent</option>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="description"><em>*</em> Price($)</label>
						<input type="text" class="form-control required" name="price"  value="{{ old('price') }}"/>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="description"><em>*</em> No of Bedrooms:</label>
						<select name="property_bedrooms" class="form-control" id="property_bedrooms">
							<?php for($i=1;$i<10;$i++){ ?>
							<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php } ?>
							<option value="11">10+</option>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="description"><em>*</em> No of Bathrooms</label>
						<select name="property_bathrooms" class="form-control" id="property_bathrooms">
							<?php for($i=1;$i<10;$i++){ ?>
							<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php } ?>
							<option value="11">10+</option>
						</select>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="description"><em>*</em> No of Garages</label>
						<select name="property_garages" class="form-control" id="property_garages">
							<?php for($i=1;$i<10;$i++){ ?>
							<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php } ?>
							<option value="11">10+</option>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="description"><em>*</em> Area</label><br/>
						<input type="text" class="required form-control" id="property_area" name="property_area" style="width:78%;display:inline-block;" value="{{ old('property_area') }}"/>

						<select name="property_area_type" class="form-control" id="property_area_type" style="width:20%;display:inline-block;">
							<option value="Sq.Ft.">Sq.Ft.</option>
							<option value="Sq. Yards">Sq. Yards</option>
							<option value="Sq. Meter">Sq. Meter</option>
							<option value="Hectares">Hectares</option>
						</select>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="description"><em>*</em> No of Floors</label>
						<select name="property_floors" class="form-control" id="property_floors">
							<?php for($i=1;$i<10;$i++){ ?>
							<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php } ?>
							<option value="11">10+</option>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="address"><em>*</em> Address</label>
						<textarea class="form-control" name="address">{{ old('address') }}</textarea>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="street_address">Street Address</label>
						<input type="text" class="form-control required" name="street_address" value="{{ old('street_address') }}"/>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label for="county"><em>*</em> County</label>
						<input type="text" class="form-control required" name="county" value="{{ old('county') }}"/>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label for="state"><em>*</em> State</label>
						<input type="text" class="form-control required" name="state" value="{{ old('state') }}"/>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label for="zipcode"><em>*</em> Zipcode</label>
						<input type="text" class="form-control required" name="zipcode" value="{{ old('zipcode') }}"/>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="contact_no">Contact Number</label>
						<input type="text" class="form-control required" name="contact_no" value="{{ old('contact_no') }}"/>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="email_address"><em>*</em> Email Address</label>
						<input type="email" class="form-control required" name="email_address" value="{{ old('email_address') }}"/>
					</div>
				</div>
				
			</div>


			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">	
						<label for="image"><em>*</em> Main Image:</label>
						<input type="file" class="form-control" name="main_image" />
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<input type="checkbox" name = "is_active" value="0"/>&nbsp;
						<label for="description">Is Active</label>
					</div>
				</div>
			</div>

			<div class="form-group">	
				<label for="image">Other Images:</label>
				<input type="file" class="form-control" name="property_images[]" />
			</div>

			<input type ="hidden" name="user_id" value="{{ Auth::user()->id }}" / >
			<button type="submit" class="btn btn-primary">Create</button>
			</form>
		</div>
	</div>

</div >
<script>
	CKEDITOR.replace( 'property_description' );
</script>

@endsection

@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Properties
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Properties</li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12"><a href="{{url('/admin/properties/create')}}" class="btn btn-primary">Add Property</a></div>
    </div><hr>

	<div class="row">
		<div class="col-sm-12">
			@if(isset($properties) && count($properties)>0)
				<table class="table table-striped">
					<thead>
						<tr>
						  <th width="10%">Image</th>
						  <th width="10%">Title</th>
						  <th width="15%">Residential Type</th>
						  <th width="15%">Sale Type</th>
						  <th width="8%">Is Active</th>
						  <th width="30%">Action</th>
						</tr>
					</thead>
					<tbody>
						
					@foreach($properties as $propDetails)
						<tr>
							<td><img class="media-object" src="{{action('PropertyController@getImage',$propDetails->id)}}" alt="no-img" height="100" width="100"></td>
							
							<td>{{$propDetails->title}}</td>
							<td><?php
							switch($propDetails->residential_type){
								case 1: 
									echo "Apartment";
									break;
								case 2:
									echo "Builder Floor";
									break;
								case 3:
									echo "Villa";
									break;
								case 4:
									echo "Land";
									break;
								case 5:
									echo "Farm House";
									break;
								case 6:
									echo "Studio Apartment";
									break;
								case 7:
									echo "Serviced Apartment";
									break;
								Default:
									echo "Unknown";
									break;
							}
							?></td>
							<td>@if($propDetails->sale_type==1) Sale @else Rent @endif</td>
							<td>@if($propDetails->is_active==1) YES @else No @endif</td>
							
							<td>
								<a href="{{action('PropertyController@view',$propDetails->slug)}}" class="btn btn-success" target="_blank"><i class="fa fa-eye"></i>&nbsp;View</a>&nbsp;
								<a href="{{action('PropertyController@edit',$propDetails->id)}}" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>&nbsp;								
								<a href="{{action('PropertyController@delete',$propDetails->id)}}" class="btn btn-danger"><i class="fa fa-trash-o"></i>&nbsp;DELETE</a>

								@if($propDetails->is_active==0) 
									<a href="{{action('PropertyController@block_unblock_property',$propDetails->id)}}" class="btn btn-success">&nbsp;Unblock</a> 
								@else 
									<a href="{{action('PropertyController@block_unblock_property',$propDetails->id)}}" class="btn btn-danger">&nbsp;&nbsp;Block&nbsp;&nbsp;</a> 
								@endif
								
								
							</td>
						</tr>
					@endforeach	
					</tbody>
				</table>
			@else
			<div class="alert alert-info">
				There are no properties added yet.
			</div>
			@endif
		</div>
	</div>

</div >
@endsection

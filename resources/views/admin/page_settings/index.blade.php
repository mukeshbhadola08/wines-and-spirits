@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Static Page Setting</h1>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('pages') }}">Page Settings</a></li>
		<li>Static Page Setting</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
    <div class="alert alert-info" style="margin-bottom: 0;">
	    <strong>Note: </strong>
	    Please note that recommended dimension of background image should be more than 1200 X 1200 pixels.
	</div>
	
	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{backpack_url('page_settings/store')}}" enctype="multipart/form-data">
			<input type="hidden" value="{{csrf_token()}}" name="_token" />

				<h3>Contact Us Page</h3>
				<div class="form-group">
					<?php
						$contact_us_background_color = '';
						$contact_us_background_image = '';
						if(!empty($pageSettingsDetails[0])){
							$contact_us_background_color = $pageSettingsDetails[0]->background_color;
							$contact_us_background_image = $pageSettingsDetails[0]->background_image;
						}
					?>
					<label for="contact_us_background_image">Background Image:</label>
					<input type="file" class="form-control" onchange="$('.added-img').remove();" name="background_image['contactuspage']" id="contact_us_background_image" />
					@if(!empty($contact_us_background_image))
						<div id="contact_us_background_image_block">
							<br>
							<img class="media-object" src="{{URL::to('/')}}/uploads/static_page_background_image/{{ $contact_us_background_image }}" alt="no-img" height="150"  style="border:2px solid #ccc; padding:5px;" ><br>
							<a href="javascript:void(0)" background-image-id="contact_us_background_image_block" class="btn btn-sm btn-danger delete_background_image" >Delete</a>
						</div>
					@endif
				</div>

				<div class="form-group">	
					<label for="contact_us_background_color">Background Color:</label>
					<input type="text" id="contact_us_background_color" class="form-control contact_us_background_color_class" name="settings[contactuspage]" value="{{ $contact_us_background_color }}">
					<button class="jscolor {valueElement:'chosen-value', onFineChange:'setContactUsTextColor(this)'}">
						Pick color
					</button>
				</div>

				<h3>Wines Listing Page</h3>
				<div class="form-group">
					<?php
						$wine_background_color = '';
						$wine_background_image = '';
						if(!empty($pageSettingsDetails[1])){
							$wine_background_color = $pageSettingsDetails[1]->background_color;
							$wine_background_image = $pageSettingsDetails[1]->background_image;
						}
					?>
					<label for="wine_background_image">Background Image:</label>
					<input type="file" class="form-control" onchange="$('.added-img').remove();" name="background_image['wineslisting']" id="wine_background_image" />
					@if(!empty($wine_background_image))
						<div id="wine_background_image_block">
							<br>
							<img class="media-object" src="{{URL::to('/')}}/uploads/static_page_background_image/{{ $wine_background_image }}" alt="no-img" height="150"  style="border:2px solid #ccc; padding:5px;" ><br>
							<a href="javascript:void(0)" background-image-id="wine_background_image_block" class="btn btn-sm btn-danger delete_background_image" >Delete</a>
						</div>
					@endif
				</div>

				<div class="form-group">	
					<label for="wine_background_color">Background Color:</label>
					<input type="text" id="wine_background_color" class="form-control wine_background_color_class" name="settings[wineslisting]" value="{{ $wine_background_color }}">
					<button class="jscolor {valueElement:'chosen-value', onFineChange:'setWineTextColor(this)'}">
						Pick color
					</button>
				</div>

				<h3>Spirits Listing Page</h3>
				<div class="form-group">
					<?php
						$spirits_background_color = '';
						$spirits_background_image = '';
						if(!empty($pageSettingsDetails[2])){
							$spirits_background_color = $pageSettingsDetails[2]->background_color;
							$spirits_background_image = $pageSettingsDetails[2]->background_image;
						}
					?>
					<label for="spirits_background_image">Background Image:</label>
					<input type="file" class="form-control" onchange="$('.added-img').remove();" name="background_image['spiritslisting']" id="spirits_background_image" />
					@if(!empty($spirits_background_image))
						<div id="spirit_background_image_block">
							<br>
							<img class="media-object" src="{{URL::to('/')}}/uploads/static_page_background_image/{{ $spirits_background_image }}" alt="no-img" height="150"  style="border:2px solid #ccc; padding:5px;" ><br>
							<a href="javascript:void(0)" background-image-id="spirit_background_image_block" class="btn btn-sm btn-danger delete_background_image" >Delete</a>
						</div>
					@endif
				</div>

				<div class="form-group">	
					<label for="spirits_background_color">Background Color:</label>
					<input type="text" id="spirits_background_color" class="form-control spirits_background_color_class" name="settings[spiritslisting]" value="{{ $spirits_background_color }}">
					<button class="jscolor {valueElement:'chosen-value', onFineChange:'setSpiritTextColor(this)'}">
						Pick color
					</button>
				</div>

				<h3>Blogs Listing Page</h3>
				<div class="form-group">
					<?php
						$blogs_background_color = '';
						$blogs_background_image = '';
						if(!empty($pageSettingsDetails[3])){
							$blogs_background_color = $pageSettingsDetails[3]->background_color;
							$blogs_background_image = $pageSettingsDetails[3]->background_image;
						}
					?>	
					<label for="blogs_background_image">Background Image:</label>
					<input type="file" class="form-control" onchange="$('.added-img').remove();" name="background_image['bloglisting']" id="blogs_background_image" />
					@if(!empty($blogs_background_image))
						<div id="blog_background_image_block">
							<br>
							<img class="media-object" src="{{URL::to('/')}}/uploads/static_page_background_image/{{ $blogs_background_image }}" alt="no-img" height="150"  style="border:2px solid #ccc; padding:5px;" ><br>
							<a href="javascript:void(0)" background-image-id="blog_background_image_block" class="btn btn-sm btn-danger delete_background_image" >Delete</a>
						</div>
					@endif
				</div>

				<div class="form-group">	
					<label for="blogs_background_color">Background Color:</label>
					<input type="text" id="blogs_background_color" class="form-control blogs_background_color_class" name="settings[bloglisting]" value="{{ $blogs_background_color }}">
					<button class="jscolor {valueElement:'chosen-value', onFineChange:'setBlogTextColor(this)'}">
						Pick color
					</button>
				</div>

				<h3>Single Blog Page</h3>
				<?php
					$single_blog_background_color = '';
					$single_blog_background_image = '';
					if(!empty($pageSettingsDetails[4])){
						$single_blog_background_color = $pageSettingsDetails[4]->background_color;
						$single_blog_background_image = $pageSettingsDetails[4]->background_image;
					}
				?>
				<div class="form-group">
					<label for="single_blog_background_image">Background Image:</label>
					<input type="file" class="form-control" onchange="$('.added-img').remove();" name="background_image['singleblogpage']" id="single_blog_background_image" />
					@if(!empty($single_blog_background_image))
						<div id="single_blog_background_image_block">
							<br>
							<img class="media-object" src="{{URL::to('/')}}/uploads/static_page_background_image/{{ $single_blog_background_image }}" alt="no-img" height="150"  style="border:2px solid #ccc; padding:5px;" ><br>
							<a href="javascript:void(0)" background-image-id="single_blog_background_image_block" class="btn btn-sm btn-danger delete_background_image" >Delete</a>
						</div>
					@endif
				</div>

				<div class="form-group">	
					<label for="single_blog_background_color">Background Color:</label>
					<input type="text" id="single_blog_background_color" class="form-control single_blog_background_color_class" name="settings[singleblogpage]" value="{{ $single_blog_background_color }}">
					<button class="jscolor {valueElement:'chosen-value', onFineChange:'setSingleBlogTextColor(this)'}">
						Pick color
					</button>
				</div>

				<h3>Product Listing Page</h3>
				<?php
					$productlisting_background_color = '';
					$productlisting_background_image = '';
					if(!empty($pageSettingsDetails[5])){
						$productlisting_background_color = $pageSettingsDetails[5]->background_color;
						$productlisting_background_image = $pageSettingsDetails[5]->background_image;
					}
				?>
				<div class="form-group">
					<label for="productlisting_background_color">Background Image:</label>
					<input type="file" class="form-control" onchange="$('.added-img').remove();" name="background_image['productlisting']" id="productlisting_background_image" />
					@if(!empty($productlisting_background_image))
						<div id="productlisting_background_image_block">
							<br>
							<img class="media-object" src="{{URL::to('/')}}/uploads/static_page_background_image/{{ $productlisting_background_image }}" alt="no-img" height="150"  style="border:2px solid #ccc; padding:5px;" ><br>
							<a href="javascript:void(0)" background-image-id="productlisting_background_image_block" class="btn btn-sm btn-danger delete_background_image" >Delete</a>
						</div>
					@endif
				</div>

				<div class="form-group">	
					<label for="single_blog_background_color">Background Color:</label>
					<input type="text" id="productlisting_background_color" class="form-control productlisting_background_color_class" name="settings[productlisting]" value="{{ $productlisting_background_color }}">
					<button class="jscolor {valueElement:'chosen-value', onFineChange:'setProductListingTextColor(this)'}">
						Pick color
					</button>
				</div>
				
				<button type="submit" class="btn btn-primary">Update</button>
			</form>
		</div>
	</div>

</div>

<script src="{{ asset('/') }}dist/js/jquery.js"></script>
<script src="{{ asset('/') }}dist/js/jscolor.js"></script>
<script>
	window.onload = function() {
		CKEDITOR.replace( 'page-ckeditor', {
			filebrowserUploadUrl: '{{route("upload",["_token" => csrf_token()])}}'
		});
	};

    function setContactUsTextColor(picker) {
		$('.contact_us_background_color_class').val('#' + picker.toString());
	}
	function setWineTextColor(picker) {
		$('.wine_background_color_class').val('#' + picker.toString());
	}
	function setSpiritTextColor(picker) {
		$('.spirits_background_color_class').val('#' + picker.toString());
	}
	function setBlogTextColor(picker) {
		$('.blogs_background_color_class').val('#' + picker.toString());
	}
	function setSingleBlogTextColor(picker) {
		$('.single_blog_background_color_class').val('#' + picker.toString());
	}
	function setProductListingTextColor(picker) {
		$('.productlisting_background_color_class').val('#' + picker.toString());
	}

	$(document).ready(function(){
        $(".delete_background_image").click(function(e){
			var that = this;
			bootbox.confirm("Do you want to remove background image", function(result){ 
				if(result){
					var background_image_id = $(that).attr('background-image-id');
					var site_url = '<?php echo asset('/'); ?>';
					$.ajax({
						type: 'POST',
						url:site_url+'admin/page_settings/remove_background_image',
						dataType: 'json',
						data: {'background_image_id':background_image_id},
						success: function(response) {
							if(response==1){
								$('#'+background_image_id).remove();
							}
						},
						error: function(xhr, status, error){
							let message = 'Failed to delete image';
							if (xhr.status == 400) {
								message = xhr.responseText.replace('["', '');
								message = message.replace('"]', '');
								$(commentmsg).show().empty().html('<div class="text-danger">'+message+'</div>');
							}
							$(commentmsg).show().empty().html('<div class="text-danger">'+message+'</div>');
						}
					});
				}
			});
        });
	});

</script>

@endsection
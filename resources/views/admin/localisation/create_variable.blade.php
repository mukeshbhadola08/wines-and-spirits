@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
		Create Variable 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Manage Localisation  </li>
      </ol>
    </section>
@endsection

@section('content')
	@if ($errors->any())
	<div class="alert alert-danger">
        <ul>
		@foreach($errors->all() as $error)
			<li>{{$error}}</li>
		@endforeach
		</ul>
	</div><br />
	@endif
    <div class="row">
		<div class="col-sm-12">
			<form method="post" class="form-horizontal" action="{{backpack_url('create_variable')}}" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<div class="col-sm-2">
					<label for="var_name">Variable Name:</label>
				</div>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="var_name"/>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-2">
					<label for="var_type">Type:</label>
				</div>
				<div class="col-sm-8">
					<select name="var_type">
						<option value="">Select Type</option>
						<option value="label">Label</option>
						<option value="message">Message</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-2">
					<label for="var_text">Variable Text:</label>
				</div>
				<div class="col-sm-8">
					<textarea class="form-control" name="var_text" rows="3"></textarea>
				</div>
			</div>
			<input type="hidden" name="lang_code" value="en" />
			<button type="submit" class="btn btn-primary">Create</button>
			</form>
		</div>
	</div>
@endsection


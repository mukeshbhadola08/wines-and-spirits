@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
		Edit Variable 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Manage Localisation  </li>
      </ol>
    </section>
@endsection

@section('content')
	@if ($errors->any())
	<div class="alert alert-danger">
        <ul>
		@foreach($errors->all() as $error)
			<li>{{$error}}</li>
		@endforeach
		</ul>
	</div><br />
	@endif
    <div class="row">
		<div class="col-sm-12">
			<form method="post" class="form-horizontal" action="{{action('LanguageController@update',$langVar->id)}}" enctype="multipart/form-data">
			<div class="form-group"> 
				<div class="col-sm-2">
					<input type="hidden" value="{{csrf_token()}}" name="_token" />
					<label for="var_name">Select Language:</label>
				</div>
				<div class="col-sm-8">
					<select name="lang_code" class="form-control" style="pointer-events:none;">
						<option value="">Select Language</option>
						@php
							$allLanguages = App\Language::where('disabled',0)->get();
						@endphp
						@if(isset($allLanguages) && count($allLanguages)>0)
							@foreach($allLanguages as $lang)
								@php
								$sel = $langVar->lang_code;
								if(isset($lang_code)){
									$sel = $lang_code;
								}
								@endphp
								<option value="{{trim($lang->code)}}" @if($lang->code == $sel) selected @endif >{{$lang->language}}</option>
							@endforeach
						@endif
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-2">
					<label for="var_name">Variable Name:</label>
				</div>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="var_name" value="{{$langVar->var_name}}" readonly/>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-2">
					<label for="var_text">Variable Text:</label>
				</div>
				<div class="col-sm-8">
					<textarea class="form-control" name="var_text" rows="3">{{$langVar->value}}</textarea>
				</div>
			</div>
			<input type="hidden" name="var_type" value="{{$langVar->type}}" />
			<button type="submit" class="btn btn-primary">Update</button>
			</form>
		</div>
	</div>
@endsection


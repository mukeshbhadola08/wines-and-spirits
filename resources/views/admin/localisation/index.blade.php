@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Manage Localisation
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Manage Localisation</li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
    <div class="row">
          <!-- <div class="col-sm-4"><a href="{{url('/admin/create_variable')}}" class="btn btn-primary">Add New Variable</a></div> -->  <!-- developer use only -->
		<div class="col-sm-4">
			<form id="filterLanguageForm" method="post" action="{{url('/admin/filtered_records')}}">
				@csrf
				<select class="form-control" name="language" id="langauage_select">
					@php
						$allLanguages = App\Language::where('disabled',0)->get();
						$sel_lang = 'en';
						if(isset($language)){
							$sel_lang = $language;
						}
					@endphp
					@if(isset($allLanguages) && count($allLanguages)>0)
						@foreach($allLanguages as $lang)
							@php
								$selected ='';
								if($lang->code==$sel_lang){ 
									$selected = 'selected="selected"';
								}
							@endphp
							<option value="{{trim($lang->code)}}" {{$selected}} >{{$lang->language}}</option>
						@endforeach
					@endif
				</select>
			</form>
		</div>
    </div>
	<br>
	<div class="row">
		<div class="col-sm-12">
		  @if(isset($langVariables) && count($langVariables)>0)
			<table class="table table-striped" id="language_variable_table">
				<thead>
					<tr>
						<th>Name</th>
						<th>text</th>
						<th>Edit in lanuages</th>
					</tr>
				</thead>
				<tbody>
					@foreach($langVariables as $var)
						<tr>
							<td>{{$var->var_name}}</td>
							<td>{{$var->value}}</td>
							<td><a href="{{action('LanguageController@edit',$var->id)}}" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>
							@if($sel_lang == 'en')
							&nbsp;&nbsp;
							<a href="{{action('LanguageController@editSpan',$var->id,'es')}}" class="btn btn-info"><i class="fa fa-pencil"></i>&nbsp;Edit Dutch</a></td>
							@endif
						</tr>
					@endforeach
				</tbody>
			</table>
		  @else
			<p> Variable is not added yet.</p>
		  @endif
			
		</div>
	</div>

</div >
@endsection
@section('after_scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('#language_variable_table').DataTable( {
        "order": [[ 0, "asc" ]]
    });
	//$('#language_variable_table_filter').hide();
	$('#language_variable_table_paginate').addClass('pull-right');
});		 
jQuery(document).on('change','#langauage_select',function(e){
	$('#filterLanguageForm').submit();
});
</script>
@endsection

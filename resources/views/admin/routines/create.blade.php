@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Add Routine</h1>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('pages') }}">Routines</a></li>
		<li>Add Routine</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">
	
	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{backpack_url('routines/create')}}" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="title">Routine Name:</label>
				<input type="text" class="form-control" name="title" value ="" />
				@if ($errors->has('title'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('title') }}</strong>
					</span>
				@endif
			</div>
			
			<div class="form-group">
			
				<label for="description">Description:</label>
				<textarea name="description" class="form-control"></textarea>
				@if ($errors->has('description'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('description') }}</strong>
					</span>
				@endif
			</div>

		
			<div class="form-group">
			
				<label for="working_on">Working On:</label>
				<textarea name="working_on" class="form-control" ></textarea>
				@if ($errors->has('working_on'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('working_on') }}</strong>
					</span>
				@endif
			</div>

			<div class="form-group">
				<label for="additional_skills">Additional Skills:</label>
				<input type="text" class="form-control" name="additional_skills" value ="" />
			</div>
			<div class="form-group">
				<label for="level">Level:</label>
				<input type="text" class="form-control" name="level" value ="" />
			</div>
			<div class="form-group">	

				<label for="image">Youtube Video Url:</label>

				<input type="url" class="form-control" name="video_url" id="video_url" value="{{ old('video_url') }}" />

			</div>
			<div class="form-group">	

				<label for="image">Video Time:</label>

				<input type="text" class="form-control" name="video_time" id="video_time" value="{{ old('video_time') }}" />

			</div>
			 <div class="form-group images">	
				<label for="image">Change Default Image:</label>
				<input type="file" class="form-control" onchange="$('.added-img').remove();" name="image" id="upload" />
				<?php if($errors->has('image')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('image')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group">	
				<label>OR</label>
			</div>
			<div class="form-group">
				<button type="button" class="btn btn-primary addimage" data-toggle="modal" data-target="#portfolioModal">Add Media</button>
			</div>


			
			<div class="form-group hidden">
				
				<label for="visible">Visible:</label>
				<input type="checkbox"  name="is_visible" value="1" />
			</div>
			<button type="submit" class="btn btn-primary">Create</button>
			</form>
		</div>
	</div>

</div >

<script>
	CKEDITOR.replace( 'page-ckeditor' );
</script>


@endsection

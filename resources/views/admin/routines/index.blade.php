@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Routines
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Routines</li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            
				<a href="{{url('/admin/routines/create')}}" class="btn btn-primary">Add Routine</a>
			
        </div>
    </div>
	<br />
	<div class="row">
		<div class="col-sm-12">
			@if(isset($routines) && count($routines)>0)
				<table class="table table-striped" id="routines_table">
					<thead>
						<tr>
							<th width="5%">Image</th>
						  <th width="20%">Title</th>
						  <th width="10%">Descripton</th>
						  <th width="15%">Working on</th>
						  <th width="15%">Level</th>
						  <th width="15%">Video Url</th>
						  <th width="15%">Video Time</th>
						  <th width="20%">Additional Skills</th>
						</tr>
					</thead>
					<tbody>
						
					@foreach($routines as $routinesDetails)
						
						<tr>
							<td>
								@if($routinesDetails->slider_id)
									<img class="media-object" src="{{App\Http\Controllers\SliderController::getImage($routinesDetails->slider_id)}}" alt="no-img" height="100" width="100">
								@else
									<img class="media-object" src="{{action('RoutinesController@getImage',$routinesDetails->id)}}" alt="no-img" height="100" width="100">
								@endif
							</td>
							<td>{{$routinesDetails->title}}</td>
							<td>{{$routinesDetails->description}}</td>
							<td>{{$routinesDetails->working_on}}</td>
							<td>{{$routinesDetails->level}}</td>
							<td>{{$routinesDetails->video_url}}</td>
							<td>{{$routinesDetails->video_time}}</td>
							<td>{{$routinesDetails->additional_skills}}</td>
							
							<td>
								<a href="{{action('RoutinesController@edit',$routinesDetails->id)}}" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>&nbsp;

								
								<a href="{{action('RoutinesController@delete',$routinesDetails->id)}}" class="btn btn-danger delete_page"><i class="fa fa-trash-o"></i>&nbsp;DELETE</a>
								
							</td>
						</tr>
					@endforeach	
					</tbody>
				</table>
			@else
			<div class="alert alert-info">
				There are no routines added yet.
			</div>
			@endif
		</div>
	</div>

</div >
@endsection

@section('after_scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('#routines_table').DataTable( {
        "order": [[ 1, "asc" ]]
    });
	$('#routines_table_paginate').addClass('pull-right');
});	
jQuery(document).on('click','.delete_page',function(e){
	if(confirm("Are you sure all related information also be deleted?")){
		return true;
	} else{
		return false;
	}
});		
</script>
@endsection

@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Add History</h1>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('history') }}">History</a></li>
		<li>Add History</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">
	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{backpack_url('history/create')}}" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="title">Title:</label>
				<input type="text" class="form-control" name="title" value ="{{ old('title') }}" />
				@if ($errors->has('title'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('title') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
				<label for="description">Description:</label>
				<textarea name="description" class="form-control" id="blog-ckeditor">{{ old('description') }}</textarea>
				@if ($errors->has('description'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('description') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">	
				<label for="image">Image:</label>
				<input type="file" class="form-control" name="image" id="upload" />
				@if ($errors->has('image'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('image') }}</strong>
					</span>
				@endif
			</div>
			<button type="submit" class="btn btn-primary" id="add_slide" >Create</button>
			</form>
		</div>
	</div>

</div >
<script>
	CKEDITOR.replace( 'blog-ckeditor' );
</script>

@endsection

@section('after_scripts')
@endsection

@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         History
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">History</li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <a href="{{url('/admin/history/create')}}" class="btn btn-primary">Add History</a>
        </div>
    </div>

	<br />
		<div class="row">
		<div class="col-sm-12">
			@if(isset($data) && count($data)>0)
        <table class="table table-striped" id="example1">
          <thead>
            <tr>
              <th width="5%">Title</th>
              <th width="10%">Descripton</th>
              <th width="5%">Image</th>
              <th width="25%">Action</th>
            </tr>
          </thead>
          <tbody>
          
          @foreach($data as $dataDetails)
            <tr>
              <td>{{$dataDetails->title}}</a></td>
              <td>{!! str_limit($dataDetails->description, 20) !!}</td>
              <td><img src="{{URL::to('/')}}/uploads/{{ $dataDetails->image }}" height="30" width="50"></td>
              <td>
               <a href="{{action('HistoryController@edit',$dataDetails->id)}}" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>&nbsp;
                
                <a href="{{action('HistoryController@delete',$dataDetails->id)}}" onclick="return confirm('Are you sure want to delete?')" class="btn btn-danger delete_page"><i class="fa fa-trash-o"></i>&nbsp;DELETE</a>
                
              </td>
            </tr>
          @endforeach 
          </tbody>
        </table>
      @else
      <div class="alert alert-info">
        There are no history added yet.
      </div>
      @endif
		</div>
	</div>

</div >
@endsection
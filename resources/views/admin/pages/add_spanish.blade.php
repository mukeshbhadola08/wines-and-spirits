@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Add Dutch Page</h1>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('pages') }}">CMS Pages</a></li>
		<li>Add Page</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif



	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{action('PageController@storeInlang',$id)}}" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="title">Page Title:</label>
				<input type="text" class="form-control" name="page_title" value="{{$page->page_title}}" />
			</div>
			<div class="form-group">
			
				<label for="permalink">Permalinks:</label>
				<input type="text" class="form-control" name="permalink" required  />
			</div>
			<div class="form-group">
			
				<label for="description">Description:</label>
				<textarea name="page_description" class="form-control" id="page-ckeditor">{{$page->page_description}}</textarea>
			</div>

			<div class="form-group">
			
				<label for="description">Keywords</label>
				<textarea name="page_keywords" class="form-control" >{{$page->page_keywords}}</textarea>
			</div>

			<input type="hidden" name="is_visible" value="1">
			<input type ="hidden" name="added_by" value="{{ Auth::user()->id }}" / >
			<input type ="hidden" name="lang_code" value="es" / >
			<input type ="hidden" name="parent" value="{{$id}}" / >
			<button type="submit" class="btn btn-primary">Create</button>
			</form>
		</div>
	</div>

</div >

<script>
	CKEDITOR.replace( 'page-ckeditor' );
</script>


@endsection

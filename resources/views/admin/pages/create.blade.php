@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Add Page</h1>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li><a href="{{ backpack_url('pages') }}">Pages</a></li>
		<li>Add Page</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">
	
	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{backpack_url('pages/create')}}" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="title">Page Title:</label>
				<input type="text" class="form-control" name="page_title" value ="{{ old('page_title') }}" />
				@if ($errors->has('page_title'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('page_title') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
			
				<label for="permalink">Permalinks:</label>
				<input type="text" class="form-control" name="permalink" value ="{{ old('permalink') }}" />
				@if ($errors->has('permalink'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('permalink') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
			
				<label for="description">Description:</label>
				<textarea name="page_description" class="form-control" id="page-ckeditor">{{ old('page_description') }}</textarea>
				@if ($errors->has('page_description'))
					<span class="invalid-feedback text-danger">
						<strong>{{ $errors->first('page_description') }}</strong>
					</span>
				@endif
			</div>

			<div class="form-group">
			
				<label for="description">Keywords</label>
				<textarea name="page_keywords" class="form-control" >{{ old('page_keywords') }}</textarea>
			</div>
			<div class="form-group images">	
				<label for="image">Change Default Image:</label>
				<input type="file" class="form-control" onchange="$('.added-img').remove();" name="image" id="upload" />
				<?php if($errors->has('image')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('image')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group">
				<label for="background_image">Background Image:</label>
				<input type="file" class="form-control" onchange="$('.added-img').remove();" name="background_image" id="background_image" />
				@if(!empty($background_image))
					<div id="background_image_block">
						<br>
						<img class="media-object" src="{{URL::to('/')}}/uploads/static_page_background_image/{{ $background_image }}" alt="no-img" height="150"  style="border:2px solid #ccc; padding:5px;" ><br>
						<a href="javascript:void(0)" background-image-id="background_image" class="btn btn-sm btn-danger delete_background_image" >Delete</a>
					</div>
				@endif
			</div>

			<div class="form-group">	
				<label for="background_color">Background Color:</label>
				<input type="text" id="background_color" class="form-control background_color_class" name="background_color" value="">
				<button class="jscolor {valueElement:'chosen-value', onFineChange:'setBackgroundColor(this)'}">
					Pick color
				</button>
			</div>

			<div class="form-group hidden">
				
				<label for="visible">Visible:</label>
				<input type="checkbox"  name="is_visible" value="1" checked />
			</div>
			<input type ="hidden" name="added_by" value="{{ Auth::user()->id }}" / >
			<input type ="hidden" name="lang_code" value="en" / >
			<input type ="hidden" name="parent" value="0" / >
			<button type="submit" class="btn btn-primary">Create</button>
			</form>
		</div>
	</div>

</div>

<script src="{{ asset('/') }}dist/js/jscolor.js"></script>
<script>
	window.onload = function() {
		CKEDITOR.replace( 'page-ckeditor', {
			filebrowserUploadUrl: '{{route("upload",["_token" => csrf_token()])}}'
		});
	};

	function setBackgroundColor(picker) {
		$('.background_color_class').val('#' + picker.toString());
	}
</script>

@endsection

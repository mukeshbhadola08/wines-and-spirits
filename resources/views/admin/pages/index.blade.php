@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
         Pages
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Pages</li>
      </ol>
    </section>
@endsection


@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            
				<a href="{{url('/admin/pages/create')}}" class="btn btn-primary">Add Page</a>
			
        </div>
    </div>
	<br />
	<div class="row">
		<div class="col-sm-12">
			@if(isset($pages) && count($pages)>0)
				<table class="table table-striped" id="pages_table">
					<thead>
						<tr>
						  <th width="5%">Image</th>
						  <th width="5%">Slug</th>
						  <th width="15%">Title</th>
						  <th class="hidden" width="7%">Visible</th>
						  <th width="15%">Keywords</th>
						  <th class="hidden" width="8%">Dutch</th>
						  <th width="25%">Action</th>
						</tr>
					</thead>
					<tbody>
						
					@foreach($pages as $pageDetails)
						
						<tr>
							<td>
								@if($pageDetails->slider_id)
									<img class="media-object" src="{{App\Http\Controllers\SliderController::getImage($pageDetails->slider_id)}}" alt="no-img" height="100" width="100">
								@else
									<img class="media-object" src="{{action('PageController@getImage',$pageDetails->id)}}" alt="no-img" height="100" width="100">
								@endif
							</td>
							<td>{{$pageDetails->clean_url}}</td>
							<td>{{$pageDetails->page_title}}</td>
							<td class="hidden">@if($pageDetails->is_visible==1) YES @else No @endif</td>
							<td>{{$pageDetails->page_keywords}}</td>
							<td class="hidden">
								@php
								 $parentDetails = App\Page::where('parent_lang_id',$pageDetails->id)->first();
								@endphp
								@if(empty($parentDetails))
									<a href="{{action('PageController@addInSpanish',$pageDetails->id)}}" class="btn btn-info" ><i class="fa fa-language" ></i>&nbsp;Add Dutch</a>
								@else
									<a href="{{action('PageController@edit',$parentDetails->id)}}" class="btn btn-info"><i class="fa fa-language" ></i>&nbsp;Edit Dutch</a>
								@endif
							</td>
							<td>
								<a href="{{action('PageController@view',$pageDetails->clean_url)}}" class="btn btn-success" target="_blank"><i class="fa fa-eye"></i>&nbsp;View</a>&nbsp;
								<a href="{{action('PageController@edit',$pageDetails->id)}}" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Edit</a>&nbsp;

								@if(isset($pageDetails->clean_url) && ($pageDetails->clean_url != "about-us" && $pageDetails->clean_url != "privacy-policy" && $pageDetails->clean_url != "contact-us"))
								<a href="{{action('PageController@delete',$pageDetails->id)}}" class="btn btn-danger delete_page"><i class="fa fa-trash-o"></i>&nbsp;DELETE</a>
								@endif
							</td>
						</tr>
					@endforeach	
					</tbody>
				</table>
			@else
			<div class="alert alert-info">
				There are no pages added yet.
			</div>
			@endif
		</div>
	</div>

</div >
@endsection

@section('after_scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('#pages_table').DataTable( {
        "order": [[ 1, "asc" ]]
    });
	$('#pages_table_paginate').addClass('pull-right');
});	
jQuery(document).on('click','.delete_page',function(e){
	if(confirm("Are you sure all related information also be deleted?")){
		return true;
	} else{
		return false;
	}
});		
</script>
@endsection

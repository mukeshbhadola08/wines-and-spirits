@extends('backpack::layout')

@section('header')
<section class="content-header">
	<h1>Edit Page</h1>
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">
		    {{ config('backpack.base.project_name') }}
		 </a></li>
		<li><a href="{{ backpack_url('pages') }}">Pages</a></li>
		<li>Edit Page</li>
	</ol>
</section>
@endsection

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@section('content')
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif



	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{action('PageController@update',$id)}}" enctype="multipart/form-data">
			<div class="form-group">
				<input type="hidden" value="{{csrf_token()}}" name="_token" />
				<label for="title">Page Title:</label>
				<input type="text" class="form-control" name="page_title" value="{{$page->page_title}}"/>
			</div>
			<div class="form-group">
			
				<label for="description">Description:</label>
				<textarea name="page_description" class="form-control" id="page-ckeditor" >{{$page->page_description}}</textarea>
			</div>

			<div class="form-group">
			
				<label for="description">Keywords</label>
				<textarea name="page_keywords" class="form-control" >{{$page->page_keywords}}</textarea>
			</div>
			<div class="form-group">	
				<label for="image">Image:</label>
				@if($page->slider_id)
					<img class="media-object" src="{{App\Http\Controllers\SliderController::getImage($page->slider_id)}}" alt="no-img" height="150" width="150">
				@else
					<img class="media-object" src="{{action('PageController@getImage',$page->id)}}" alt="no-img" height="150" width="150">
				@endif
			</div>
			<div class="form-group images">	
				<label for="image">Change Image:</label>
				<input type="file" class="form-control" onchange="$('.added-img').remove();" name="image" id="upload" />
				<?php if($errors->has('image')): ?>
					<span class="invalid-feedback text-danger">
						<strong><?php echo e($errors->first('image')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
			<div class="form-group">
				<label for="background_image">Background Image:</label>
				<input type="file" class="form-control" onchange="$('.added-img').remove();" name="background_image" id="background_image" />
				@if(!empty($page->background_image))
					<div id="background_image_block">
						<br>
						<img class="media-object" src="{{URL::to('/')}}/uploads/page_background_image/{{$page->background_image}}" alt="no-img" height="150"  style="border:2px solid #ccc; padding:5px;" ><br>
					</div>
				@endif
			</div>

			<div class="form-group">	
				<label for="background_color">Background Color:</label>
				<input type="text" id="background_color" class="form-control background_color_class" name="background_color" value="{{$page->background_color}}">
				<button class="jscolor {valueElement:'chosen-value', onFineChange:'setBackgroundColor(this)'}">
					Pick color
				</button>
			</div>

			@if($page->lang_code == 'en')
			<div class="form-group hidden">
				
				<label for="visible">Visible:</label>
				<input type="checkbox"  name="is_visible" value="1" checked="checked"  />
			<!-- 	@if($page->is_visible==1)
					<input type="checkbox"  name="is_visible" value="1" checked="checked"  />
				@else
					<input type="checkbox"  name="is_visible" value="1"  />
				@endif -->
			</div>
			@else
			<input type="hidden" name="is_visible" value="1">
			@endif
			<input type ="hidden" name="added_by" value="{{ Auth::user()->id }}" / >
			<button type="submit" class="btn btn-primary">Update</button>
			</form>
		</div>
	</div>

</div>
<script src="{{ asset('/') }}dist/js/jscolor.js"></script>

<script>
	window.onload = function() {
		CKEDITOR.replace( 'page-ckeditor', {
			filebrowserUploadUrl: '{{route("upload",["_token" => csrf_token()])}}'
		});
	};

	function setBackgroundColor(picker) {
		$('.background_color_class').val('#' + picker.toString());
	}
</script>

@endsection

@extends('backpack::layout')

@section('header')
<section class="content-header">
	<ol class="breadcrumb">
		<li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
		<li>Home Page Section</li>
	</ol>
</section>
@endsection

@section('content')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<div class="body">
	@if(\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif
	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	
	<div class="row">
		<div class="col-sm-12">
			<form method="post" action="{{backpack_url('homepagesection')}}" enctype="multipart/form-data" class="form-horizontal">
			
		
			<div class="sitesettings">
				<h3 class="text-center">Home Page Section</h3><br/>
				<div class="form-group"> 
					<div class="col-sm-2">
						<input type="hidden" value="{{csrf_token()}}" name="_token" />
						<label for="var_name">Select Language:</label>
					</div>
					<div class="col-sm-4">
						<select name="lang_code" class="form-control lang_code">
							@php
								$allLanguages = App\Language::where('disabled',0)->get();
							@endphp
							@if(isset($allLanguages) && count($allLanguages)>0)
								@foreach($allLanguages as $lang)
									<option value="{{trim($lang->code)}}" @if($lang->code == $lang_select) selected @endif >{{$lang->language}}</option>
								@endforeach
							@endif
						</select>
					</div>
				</div>
			
				<div class="form-group">
					<label for="site_title" class="col-sm-2 control-label">Main Title</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="main_title" placeholder="Main Title" value="{{ $homepagesection->title }}">
					</div>
				</div>

				<div class="form-group">
					<label for="site_title" class="col-sm-2 control-label">Old Main Image</label>
					<div class="col-sm-10">
						<img class="media-object" src="{{action('HomeController@getImage', 'main_image')}}" alt="no-img" height="150" width="150">
					</div>
				</div>
				<div class="form-group">
					<label for="site_title" class="col-sm-2 control-label">New Main Image</label>
					<div class="col-sm-10">
						<input type="file" class="form-control upload_image" name="main_image" id="main_image">
					</div>
				</div>
			</div>
			<hr style="border:1px solid white;border-style: dotted;">
			
			<?php for($z=1;$z<=4;$z++) { 
			$sectitle = "section".$z."_title";
			$secdescription = "section".$z."_description";
			$secimage = "section".$z."_image";
			?>
			<div class="sitesettings">
				<h4>Section <?php echo $z;?></h4><br/>
				<div class="form-group">
					<label for="site_title" class="col-sm-2 control-label">Title</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="section{{$z}}_title" placeholder="Title" value="{{ $homepagesection->$sectitle }}">
					</div>
				</div>

				<div class="form-group">
					<label for="site_title" class="col-sm-2 control-label">Description</label>
					<div class="col-sm-10">
						<textarea id="secckeditor{{$z}}" class="form-control sec-ckeditor" name="section{{$z}}_description">{{ $homepagesection->$secdescription }}</textarea>
						<script>
							CKEDITOR.replace( 'secckeditor'+{{$z}} );
						</script>
					</div>
				</div>

				<div class="form-group">
					<label for="site_title" class="col-sm-2 control-label">Old Image</label>
					<div class="col-sm-10">
						<img class="media-object" src="{{action('HomeController@getImage', $secimage)}}" alt="no-img" height="55" width="50">
					</div>
				</div>
				<div class="form-group">
					<label for="site_title" class="col-sm-2 control-label">New Image</label>
					<div class="col-sm-10">
						<input type="file" class="form-control upload_image" name="section{{$z}}_image">
					</div>
				</div>
			</div>
			<?php } ?>
			
			<input type="hidden" name="primary_id" value="{{ $homepagesection->id }}">
			<button type="submit" class="btn btn-primary" id="add">Update</button>
			</form>
		</div>
	</div>

</div>
@endsection
@section('after_scripts')
<script>
$(document).on('change','.lang_code',function(){
	var lang_code = $(this).val();
    window.location.href = "/admin/homepagesection/"+lang_code;
});
$(".upload_image").on("change", function () {
	 var file, img;
	if ((file = this.files[0])) {
		if ( (/\.(png|jpeg|bmp|jpg|gif)$/i).test(file.name) ) {
			
			$('#add').removeAttr('disabled');
			
	  } else {
		errors = file.name +" Unsupported Image extension.";  
		new PNotify({'text':errors,'type':'error'});
			 $('#add').attr('disabled','disabled');
	  }
		
	}
});
</script>
@endsection

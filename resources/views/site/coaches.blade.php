@include('site.partials.header')
@php
	global $lang_val;
	global $languageVariables;

@endphp

	<!-- intro -->
	<section id="intro" class="clearfix">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 intro-img order-md-first order-last">
					<img src="{{ asset('/') }}dist/images/intro-img-top.png" alt="" class="img-fluid wow fadeInUp">
				</div>
				<!--@if(config('constants.COACHES_TEXTAREA'))
					<div style="height:250px; padding: 30px;" class="col-sm-6 dark-box">
						 {{ config('constants.COACHES_TEXTAREA') }}
					</div>
				@endif-->
				<div class="col-sm-12 intro-info  wow fadeInDown order-md-last order-first" style="visibility: visible; animation-name: fadeInDown;">
				@if( $coach_title[0] )
					<h2>{{ $coach_title[0] }}</h2>
				@endif
					
						 					
				</div>
				
			</div>

		</div>
	</section>
	<!-- /intro -->
    <style>
        .badge-pill{
            background-color:#ee8e6c;
        }
    </style>
     @php
		$coaches = App\CoachesGrades::where('page_type', '0')->orderBy('current_coach', 'DESC')->get();
		
	@endphp
	<section class="single-page-container section-padding">
		<div class="container coaches ">
				<div class="table-reponsive">
					
				@if(isset($coaches) && count($coaches)>0)
				<table class="table table-dark">
					    <thead class="">
					      <tr>
					        <th width="33%" >Coach Name </th>
					        <th width="37%" >Coach Email</th>
					        <th width="30%" >Year</th>
					      </tr>
					    </thead>
					    <tbody>
						@foreach($coaches as $coach)
					      <tr>
					         
					        <td>{{$coach->coach_name}} @if($coach->current_coach) <span class="badge badge-pill">Current</span> @else  @endif</td>
					        <td>{{$coach->coach_email}}</td>
					        <td>
					        	@php
					        		if($coach->current_coach){
						        		$status = explode(" to ", $coach->coach_status); 
						        		echo $status[0].' to Present';
					        		} else{
					        			echo $coach->coach_status;			
						        	}
							        	
					        	@endphp
					        </td>
					      </tr>
					    @endforeach		
				    </tbody>
				</table>		
				@endif
				</div>
		  </div>
	</section>

@include('site.partials.footer')


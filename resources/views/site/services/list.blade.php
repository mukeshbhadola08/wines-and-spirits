@include('site.partials.header')
	
<section class="service-container">
		<div class="container">
			<div class="sec-title">
				<h1>Our Services</h1>
			</div>
			<div class="latest-outer">
				<div class="row">
				@if(isset($services) && count($services)>0)
					@foreach($services as $serv)
						<div class="col-sm-6">
							<div class="service-area">
								<div class="img-service">
									<img src="{{action('ServiceController@getServiceImage',$serv->id)}}" />
								</div>
								<div class="content-service">
									<h2>{{$serv->name}}</h2>
									<div class="desc-service">
										{{$serv->summery}}
									</div>
								</div>
							</div>
						</div>
					@endforeach
				@else
					<div class="col-sm-12">
						<div class="alert alert-info">
						There are no services yet.
						</div>	
					</div>
				@endif
				</div>
			</div>

		</div>
	</section>	

@include('site.partials.footer')
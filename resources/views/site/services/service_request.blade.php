@include('site.partials.header')
	
<section class="service-container">
		<div class="container">
			<div class="sec-title">
				<h1>Request Services</h1>
			</div>
			<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
			@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
				@foreach($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div><br />
			@endif
			@if(\Session::has('success'))
				<div class="alert alert-success">
					{{\Session::get('success')}}
				</div>
			@endif
			 <div class="row">
				<div class="col-sm-12">
				<form method="post" action="{{url('/save_request')}}" id="service_request_form" enctype="multipart/form-data">
					<div class="form-group">
						<input type="hidden" value="{{csrf_token()}}" name="_token" />
						<label for="name">Name:</label>
						<input type="text" class="form-control" name="name" value="<?php if(isset($userdata['name'])){ echo stripslashes($userdata['name']); }?>"/>
					</div>
					<div class="form-group">
						<label for="email">Email:</label>
						<input type="text" class="form-control email" name="email" value="<?php if(isset($userdata['email'])){ echo $userdata['email']; }?>" />
					</div>
					<div class="form-group">
						<label for="mobile">Mobile:</label>
						<input type="number" class="form-control" name="mobile" value="{{ $mobile = isset($userdata['mobile'])?$userdata['mobile']:''}}"/>
					</div>
					<div class="form-group">
						<label for="address">Address:</label>
						<textarea name="address" class="form-control">{{ $address = isset($userdata['address'])?$userdata['address']:''}}</textarea>
					</div>
					<div class="form-group">
						<label for="service">Service:</label>
						<select name="service" class="form-control" id="sel-service">
							<option value="">Select Service</option>
							@if(isset($services) && count($services)>0)
								@foreach($services as $s)
									<option value="{{$s->id}}">{{$s->name}}</option>
									@php
										$subservice = $s->getSubServices($s->id);
									@endphp
									@if(isset($subservice) && count($subservice)>0)
										@foreach($subservice as $sub)
											<option value="{{$sub->id}}">&nbsp;&nbsp;{{$sub->name}}</option>
										@endforeach
									@endif
								@endforeach
							@endif
						</select>
					</div>
					<div class="form-group">					
						<label for="photograph">Total Cost:</label>&nbsp;&nbsp;<span id="total_cost"></span>			<input type="hidden" name="cost" id="service_cost" />	
					</div>
					<div class="form-group">
						<label for="photograph">Photograph:</label><br/>
						<input type="file" name="photograph" />
					</div>
					<div class="form-group">
						<label for="description">Description:</label>
						<textarea name="description" id="request_description" class="form-control"></textarea>
					</div>
					<div id="customform"></div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary" id="submit_request">Submit</button>
					</div>
				</form>
				</div>
			</div>
		
		</div>
</section>
<!-- script for jquery validation -->
<script src="{{ asset('plugins/jquery-validation-1.17.0/dist/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation-1.17.0/dist/additional-methods.min.js') }}"></script>
<script>
	$(document).ready(function(){
		$('#service_request_form').validate({
			rules: {
				'name': {
					required: true
				},
				'email': {
					required: true,
					email: true
				},
				'mobile':{
					required: true
				},
				'address':{
					required: true
				},
				'service':{
					required: true
				},
				'photograph' : {
					accept: "jpg|gif|bmp|png|jpeg"
				}
			},
			messages: {
				'name': {
					required: 'Name is required'
				},
				'email': {
					required: 'Email is required',
					email: 'Email is not valid'
				},
				'mobile':{
					required: 'Mobile no is required'
				},
				'address':{
					required: 'Address is required'
				},
				'service':{
					required: 'Service is required'
				},
				'photograph' : {
					accept: 'File is not valid'
				}
			}
		});
	});

</script>
<script>
CKEDITOR.replace('request_description');
$(document).on('change','#sel-service',function(){
	var service = $(this).val();
	if(service !=''){
		$.ajax({
		   type:'POST',
		   url:'/getServiceCost',
		   data:{'_token': '<?php echo csrf_token() ?>','service':service},
		   success:function(cost){
			  $("#total_cost").html(cost);
			  $("#service_cost").val(cost);
		   }
		});
		$.ajax({
		   type:'POST',
		   url:'/getRequestForm',
		   data:{'_token': '<?php echo csrf_token() ?>','service':service},
		   success:function(formfield){
			  $("#customform").html(formfield);
		   }
		});
	}
});
</script>


@include('site.partials.footer')
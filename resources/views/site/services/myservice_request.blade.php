@include('site.partials.header')

	<!-- page title -->
	<div class="pagehding-sec">
		<div class="images-overlay"></div>		
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-heading">
						<h1>My Account</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page title -->

	<section class="single-page-container">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-12">				
					<div class="container-white user-tab">
						@include('site.partials.usernav')
					</div>
				</div>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<div class="container-white dashboard-form">
						<div class="dashboard-heading">
							<h4>Service Requests</h4>
						</div>

		
						@if(\Session::has('success'))
						<div class="alert alert-success">
							{{\Session::get('success')}}
						</div>
						@endif
						 <div class="row">
							<div class="col-sm-12">
							@if(isset($service_requests) && count($service_requests)>0)
							<table class="table table-striped">
								<thead>
									<tr>
									  <th>
									   Service Name
									  </th>
									  <th>
										Photograph
									  </th>
									  <th>
										Service Description
									  </th>
									  <th>
										Cost
									  </th>
									  <td>
									  </td>
									</tr>
								</thead>
								<tbody>
									@foreach($service_requests as $sr)
										<tr>
											<td>{{$sr->service->name}}</td>
											<td>							 
											@php
											$metadataDetails = $sr->service_request_data;
											@endphp
											@if(count($metadataDetails)>0)
												@if(!empty($metadataDetails->where('key','photograph')->first()))
												<img src="{{action('ServiceRequestController@getRequestPhotograph',$metadataDetails->where('key','photograph')->first()->id)}}" width="50" /> 
												@endif

											@endif								
											</td>
											<td>
											@php
											$metadataDetails = $sr->service_request_data;
											if(count($metadataDetails)>0){
												if(!empty($metadataDetails->where('key','description')->first())){
													echo($metadataDetails->where('key','description')->first()->value);
												}
											}
											@endphp
											</td>
											<td>{{$sr->total_cost}}</td>
											<td>
												<button type="button" class="btn btn-primary service-metadata" data-id="{{$sr->id}}" data-toggle="modal" data-target="#infoModal"> More Info</button>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
							<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Service More Information</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									  <span aria-hidden="true">&times;</span>
									</button>
								  </div>
								  <div class="modal-body">
									...
								  </div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								  </div>
								</div>
								</div>
							</div>
							@else
							<div class="text-center">There is no request made.</div>
							@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>
<script>
$(document).on('click','.service-metadata',function(){
	var request = $(this).data('id');
	$.ajax({
	   type:'POST',
	   url:'/serviceMetadata',
	   data:{'_token': '<?php echo csrf_token() ?>','service':request},
	   success:function(response){
		  $("#infoModal").find('.modal-body').html(response);
	   }
	});

});
</script>
@include('site.partials.footer')
@include('site.partials.header')
 <!-- Our history -->
  <div class="wrapper-page" style="background-image: url({{ asset('/') }}dist/images/bg-wines.png);"> 
  	<div class="container">
		<!-- heading -->
		<div class="heading-section white-heading">
			<h2>Our History</h2>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Our History</li>
				</ol>
			</nav>
		</div>
		<!-- /heading -->
		<!-- inner wrapper -->
		<div class="inner-wrapper">
			@foreach($history as $his)
			<!-- history box -->
			<div class="history-main">
				<div class="row">
					<div class="col-sm-6 col-md-6 col-12">
						<img src="{{URL::to('/')}}/uploads/{{ $his->image }}" class="img-fluid about-img"/> 
						<!-- <img src="<?php //echo asset('dist/images/history-img.png');?>" class="img-fluid about-img"/>  -->
					</div>
					<div class="col-sm-6 col-md-6 col-12">
						<div class="history-content">
							<div class="heading-history">
								<h3>{{$his->title}}</h3>
								</div>
								<div class="history-content-inner">
									{!! $his->description !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /history box -->
			@endforeach
		</div>
		<!-- /inner wrapper -->
	</div>
  </div>
  <!-- Our history -->
@include('site.partials.footer')
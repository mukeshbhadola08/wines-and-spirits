@include('site.partials.header')
<?php 
//echo "<pre>";print_r(count($services));echo "</pre>";die;
//echo "<pre>";print_r($agent_service_ids);echo "</pre>";die;
/*echo "<pre>";print_r($my_id);echo "</pre>";
echo "<pre>";print_r($user_name);echo "</pre>";die;*/
/*foreach($user_profile as $profile) {
    $my_id=$profile->user_id;
}*/
//die; 
?>	
	<!-- page title -->
	<div class="pagehding-sec">
		<div class="images-overlay"></div>		
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-heading">
					@if ($user_type == 'A')
						<h1>Agent Profile</h1>
					@elseif($user_type == 'C')
						<h1>Customer Profile</h1>
					@elseif($user_type == 'S')
						<h1>User Profile</h1>
					@endif
						<ul>
							<li><a href="/">Home</a></li>
							<li><a href="#">My Profile</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page title -->

	<section class="single-page-container">
		<div class="container">
			<div class="heading find">
				<h2>My Profile</h2>
			</div>
			<div class="row">
				<div class=" col-sm-8 col-xs-12 col-md-offset-2">
					<div class="row justify-content-center">
						@if ($my_id != 0)
							<form action="/update_profile" method="post" enctype="multipart/form-data" id="edit_profile">
								@if ($message = Session::get('success'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>
										<strong>{{ $message }}</strong>
									</div>
								@endif
								@if (count($errors) > 0)
									<div class="alert alert-danger">
										<strong>Whoops!</strong> There were some problems with your input.<br><br>
										<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
										</ul>
									</div>
								@endif
								<div class="form-group row">
									<div class="profile-header-container">
										<div class="profile-header-img">
											<img class="rounded-circle" src="<?php echo asset("storage/avatars/$image_path")?>"
											<!-- badge -->
											<div class="rank-label-container">
												<!-- <span class="label label-default rank-label">{{$my_id}}</span> -->
											</div>
										</div>
									</div>
									@csrf
									<input type="file" class="form-control-file" name="avatar" id="avatarFile" aria-describedby="fileHelp">
									<small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
								</div>
								<div class="form-group row">
		                            <label for="user_name" class="col-md-4 col-form-label text-md-right">User Name</label>

		                            <div class="col-md-6">
		                                <input id="user_name" type="text" class="form-control" name="user_name" value="{{$user_name}}" required>
		                            </div>
	                        	</div>
	                        	<div class="form-group row">
		                            <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

		                            <div class="col-md-6">
		                                <input id="email" type="email" class="form-control" name="email" value="{{$user_email}}" disabled>
		                                <input id="user_email" type="hidden" class="form-control" name="user_email" value="{{$user_email}}">
		                            </div>
	                        	</div>
	                        	<div class="form-group row">
		                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

		                            <div class="col-md-6">
		                                <input id="password" type="password" class="form-control" name="password" value="{{$user_password}}" disabled>
		                                <input id="user_password" type="hidden" class="form-control" name="user_password" value="{{$user_password}}">
		                            </div>

		                            <div class="col-xs-12">
		                            	<a href="javascript:;" class="btn btn-warning btn-detail open_modal" value="{{$my_id}}">Edit Password</a>
		                            </div>
	                        	</div>
	                        	<div class="form-group row">
		                            <label for="mobile_no" class="col-md-4 col-form-label text-md-right">Mobile Number</label>

		                            <div class="col-md-6">
		                                <input id="mobile_no" type="tel" class="form-control" name="mobile_no" value="{{$mobile_no}}" required>
		                            </div>
	                        	</div>
	                        	<div class="form-group row">
		                            <label for="phone_no" class="col-md-4 col-form-label text-md-right">Phone Number</label>

		                            <div class="col-md-6">
		                                <input id="phone_no" type="tel" class="form-control" name="phone_no"  value="{{$phone_no}}" required>
		                            </div>
	                        	</div>
	                        	<div class="form-group row">
		                            <label for="address" class="col-md-4 col-form-label text-md-right">Address</label>

		                            <div class="col-md-6">
		                                <input id="address" type="text" class="form-control" name="address"  value="{{$address}}" required>
		                            </div>
	                        	</div>
	                        	@if(isset($services) && count($services)>0)
	                        	<div class="form-group row">
		                            <label for="services" class="col-md-4 col-form-label text-md-right">Services Offered</label>

		                            <div class="col-md-6">
		                                	
												@foreach($services as $service)
													<?php 
													if(in_array($service->id, $agent_service_ids)){
														$checked = 'checked="checked"' ;
														//echo "hi checked";die;
													}else{
														//echo "hello";die;
														$checked = '' ;
													}
													?>
												<label class="checkbox-inline">
													<input type="checkbox" value="{{$service->id}}" class="service-group" {{$checked}}>
													{{$service->name}}
												</label>
												@endforeach	
											
									</div>
	                        	</div>
	                        	<input id="agent_services" type="hidden" name="agent_services" value="">
	                        	@endif
								<button type="submit" class="btn btn-primary update-profile">Submit</button>
							</form>
							<!--Modal for editing Password-->
							  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						        <div class="modal-dialog">
						           <div class="modal-content">
						             	<div class="modal-header">
							             	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
							                <h4 class="modal-title" id="myModalLabel">Edit Password</h4>
							            </div>
							            <div class="modal-body">
 
								            <form method="POST" id="ChangePassword"  class="form-horizontal" novalidate="">
								            	@csrf
								                <div class="form-group error">				
						                 			<label for="password" class="col-sm-5 control-label">New Password</label>
							                    	<div class="col-sm-9">
								                    	<input id="new_password" type="password" class="form-control" name="password" value="" placeholder="Type new password">
						                                <span class="text-danger">
							                                <strong id="password-error"></strong>
							                            </span>
								                    </div>
							                   	</div>
							                 	<div class="form-group">
								                 	<label for="password_confirmation" class="col-sm-5 control-label">Retype Password</label>
								                    <div class="col-sm-9">
								                    	<input id="new_confirm_password" type="password" name="password_confirmation" class="form-control" placeholder="Retype password">
								                    </div>
								                    <!-- <input id="current_user_name" type="hidden" class="form-control" name="current_user_name" value="{{$user_name}}">
							            			<input id="current_user_email" type="hidden" class="form-control" name="current_user_email" value="{{$user_email}}">
                            						<input id="current_current_password" type="hidden" class="form-control" name="current_password" value="{{$user_password}}"> -->
								                </div>
								            </form>

							            </div>
							            <div class="modal-footer">
							            	<button type="button" class="btn btn-primary" id="save_changes" value="add">Save changes</button>
							            </div>
						        	</div>
						      	</div>
						  	</div>
						  	<!--/Modal for editing Password-->
						</div>
						@else
						
							<div class="alert alert-danger">
								Sorry {{$user_name}}! Your Profile does not exist !
							</div>
						
						@endif
						
					</div>
				</div>
			</div>
	</section>
	<script type="text/javascript">
		//Change password related code// 
	    $(document).on('click','.open_modal',function(){
	       	$('#myModal').modal('show');
	       
	    });
	    
		$('body').on('click', '#save_changes', function(){
	        var registerForm = $("#ChangePassword");
	        var formData = registerForm.serialize();
	        $( '#name-error' ).html( "" );
	        $( '#email-error' ).html( "" );
	        $( '#password-error' ).html( "" );

	        $.ajax({
	            url:'/change_password',
	            type:'POST',
	            data:formData,
	            dataType: 'json',              // let's set the expected response format
	            success:function(data) {
	                if(data.errors) {
	                    /*if(data.errors.name){
	                        $( '#name-error' ).html( data.errors.name[0] );
	                    }
	                    if(data.errors.email){
	                        $( '#email-error' ).html( data.errors.email[0] );
	                    }*/
	                    if(data.errors.password){
	                        $( '#password-error' ).html( data.errors.password[0] );
	                    }
	                    
	                }
	                if(data.success) {
	                	//console.log(data.new_password);
	                	var new_password = data.new_password;
	                    $('#success-msg').removeClass('hide');
	                    setInterval(function(){ 
	                        $('#myModal').modal('hide');
	                        $('#user_password').val(new_password);
	                        $('#success-msg').addClass('hide');
	                    }, 3000);
	                }
	            },
	        });
    	});
    	//---Change password related code----// 

    	//Agent services related code//
		$(document).on('click','.update-profile',function(){
			var values = $('input:checkbox:checked.service-group').map(function () {
  				return this.value;
			}).get();
			//alert(values);
			$('#agent_services').val(values);
			//return false;
			$("#edit_profile").submit();
		});
		//-----Agent services related code-----//
    	
	</script>

@include('site.partials.footer')
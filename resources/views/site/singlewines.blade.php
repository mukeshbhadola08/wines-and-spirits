@include('site.partials.header')
	<?php 
	$type = (!empty($wine->type) && $wine->type=='winery')?'winery':'distillery';
	$category_link = (!empty($wine->type) && $wine->type=='winery')?'wines':'spirits';  
	?>
  <!-- Single page -->
	<?php 
		if($type=='winery'){
	  		if(!empty($wine->background_image)){
		?>  	
	  			<div class="wrapper-page" style="background-image: url(<?php echo URL::to('/'); ?>/uploads/background_img/{{ $wine->background_image }});">
	<?php } else { ?>
			<div class="wrapper-page" style="background-image: url(<?php echo config('constants.SINGLEPAGE_WINE_BACKGROUND_IMAGE'); ?>)">
	<?php } }?>

	<?php 
		if($type=='distillery'){
  			if(!empty($wine->background_image)){
		?> 
			<div class="wrapper-page" style="background-image: url(<?php echo URL::to('/'); ?>/uploads/background_img/{{ $wine->background_image }});">
	<?php }else { ?>
			<div class="wrapper-page" style="background-image: url(<?php echo config('constants.SINGLEPAGE_DISTELLERY_BACKGROUND_IMAGE'); ?>)">
	<?php } } ?>

  	<div class="container">
		<!-- heading -->
		<div class="heading-section white-heading">
			<h2>{{ucwords($type)}}</h2>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
					<li class="breadcrumb-item"><a href="{{url('/'.$category_link)}}">{{$type}}</a></li>
					<li class="breadcrumb-item active" aria-current="page">{{ucwords($wine->cat_title)}}</li>
				</ol>
			</nav>
		</div>
		<!-- /heading -->
		<!-- inner wrapper -->
		<div class="inner-wrapper">
			<div class="wrapper-singlePage">
				<div class="row">
					<div class="col-sm-5">
						<img src="{{URL::to('/')}}/uploads/{{ $wine->logo }}" class="img-fluid">
					</div>
					<div class="col-sm-7">
						<div class="single-productRight">
							<!-- <h3>Product name here</h3> -->
							<h3>{{ucwords($wine->cat_title)}}</h3>
							<ul>
								<li>Region:<span> {{$wine->region}} </span></li>
								<li>Country: <span> {{$wine->country->nicename}}</span></li>
								@if(!empty($wine->pdf_file))
									<li>Catalog: <span><a href="{{URL::to('/')}}/uploads/{{ $wine->pdf_file }}" target="_blank" class="pdf-Link"><img src="{{URL::to('/')}}/dist/images/pdf.svg" width="30"/></a></span></li>
								@endif
								<li>Number of products: <span><a href="{{URL::to('/')}}/singleWines/{{$wine->id}}" class="btn btn-outline-wine">{{$wine->products->count()}}</a></span></li>
							</ul>
						</div>
						<!-- desc -->
						<div class="descItem-detail">
							<p>
								{!!html_entity_decode($wine->description)!!}
							</p>
						</div>
						<!-- desc -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /Single page -->
@include('site.partials.footer')

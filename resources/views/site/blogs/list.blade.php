@include('site.partials.header')
@php
	global $lang_val;
	global $languageVariables;
	$title = App\Navigation::where('static_page_name','bloglisting')->pluck('title')[0];
@endphp
	
	<!--heading-img-->
	<!--<div class="heading-img">
		<div class="heading heading-bg-img">
			<h2> {{$title}} </h2>
		</div>
	</div>-->
	<!--/heading-img-->
	<section id="intro" class="clearfix">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 intro-img order-md-first order-last">
					<img src="{{ asset('/') }}dist/images/intro-img-top.png" alt="" class="img-fluid wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
				</div>
				<div class="col-sm-12 intro-info  wow fadeInDown order-md-last order-first" style="visibility: visible; animation-name: fadeInDown;" >
					<h2 class="blog-tp">{{$title}}</h2>
					<!-- {{$languageVariables['label']['lbl_all_blogs']}} -->
				</div>
			</div>

		</div>
	</section>

	<section class="single-page-container section-padding">
		<div class="container">
			@if(isset($blogs) && count($blogs)>0)
				<div class="row">
				
					@foreach($blogs as $bg)
						@php
							$bg_title = $bg->blog_title;
							$bg_short_description = $bg->short_description;
							$bg_clean_url = $bg->clean_url;
							$bg_id = $bg->id;
							if($lang_val !='en'){
								$bgDetails = App\Blog::where([['parent_lang_id','=',$bg->id],['lang_code','=',$lang_val]])->first();
								if(!empty($bgDetails)){
									$bg_title = $bgDetails->blog_title;
									$bg_short_description = $bgDetails->short_description;
									$bg_clean_url = $bgDetails->clean_url;
									$bg_id = $bgDetails->id;
								}
							 }

							if($bg->slider_id): 
								$url  = App\Http\Controllers\SliderController::getImage($bg->slider_id);
							else: 
								$url = action('BlogController@getImage',$bg->id);
							endif;
						@endphp

						<div class="col-sm-6 col-xs-12 news-list-outer">
							
							<div class="wrapper" style="background-image: url({{$url}})" onclick="window.location='{{URL::to('/news').'/'.$bg_clean_url}}';">
								<div class="date">
									<span class="day">{{date('d',strtotime($bg->created_at))}}</span>
									<span class="month">{{date('M',strtotime($bg->created_at))}}</span>
									<span class="year">{{date('Y',strtotime($bg->created_at))}}</span>
								</div>
								<div class="data">
									<div class="content">
										<!-- <span class="author">{{date('H:i:s',strtotime($bg->created_at))}}</span> -->
										<h3 class="title"><a href="#">{{$bg_title}}</a></h3>
										<p class="text">
											<?php echo substr(html_entity_decode($bg_short_description), 0,145)."..."; ?>
										</p>
									</div>
								</div>
							</div>
							<!-- <div class="thumbnail thumb-blog">
								
								<div class="caption">
									<h4 title="{{$bg_title}}">{{$bg_title}}</h4>
									<p class="blog-time" ><i class="fa fa-calendar" aria-hidden="true"></i> {{date('jS M, Y H:i:s',strtotime($bg->created_at))}} </p>
									<div class="row">
										<div class="col-sm-12 col-xs-12">
											<p class="blog-text"><?php 
													echo substr(html_entity_decode($bg_short_description), 0,145)."..."; 
												?>  
											</p>
											<a href="" class="btn btn-blog pull-right">{{$languageVariables['label']['lbl_read_more']}} </a>
										</div>
									</div>
								</div>
							</div> -->
						</div>
					@endforeach
					
				</div>
				<div class="pull-right">{{ $blogs->links() }}</div>
			@endif

		</div>
	</section>

@include('site.partials.footer')
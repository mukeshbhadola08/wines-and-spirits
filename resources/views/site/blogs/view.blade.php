@include('site.partials.header')
@php
global $languageVariables;
global $lang_val;

$parent_child = App\Blog::where(['parent_lang_id'=>$blog->id,'is_visible'=>1])->first();
$array_ids[] = $blog->id;
$current_blog_id = Crypt::encrypt($blog->id);
$array_ids[] = $blog->parent_lang_id;
if($parent_child) {
$array_ids[] = $parent_child->id;
}
$title = ucfirst($blog->blog_title);
$blogCommenturl = URL::to("/blogs/addcomment/adduser");
if (auth()->guest()) :
    $blogCommenturl = URL::to("/blogs/addcomment/addguest");
endif
@endphp
<?php 
$background_image = $singleBlogPageSettings['background_image']; 
$background_color = $singleBlogPageSettings['background_color']; 
?>
<!-- Single page -->
<div class="wrapper-page" style='background-color: {{$background_color}}; background-image: url({{ asset("/") }}uploads/static_page_background_image/{{$background_image}});'> 
		<div class="container">
			<!-- heading -->
			<div class="heading-section white-heading">
				<h2>{{$title}}</h2>
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{URL::to('/')}}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{URL::to('/blogs')}}">Blogs</a></li>
						<li class="breadcrumb-item">{{$blog->blog_title}}</li>
					</ol>
				</nav>
			</div>
			<!-- /heading -->
			<!-- inner wrapper -->
			<div class="inner-wrapper blogs-listing">
				<div class="row">
					<div class="col-sm-9">
						<div class="wrapper-singlePage">
							<div class="blog-img">
								<img class="" src="{{action('BlogController@getImage',$blog->id)}}" alt="">
							</div>
							<div class="singleBlog-desc ">
								<div class="date-show singleBlog-date">
									<h4>{{date('d', strtotime($blog->created_at))}}</h4>
									<div class="month">
										<span> {{date('M', strtotime($blog->created_at))}}</span>
									</div>
								</div>
								<p>
								<?php echo html_entity_decode($blog->blog_description); ?>
								</p>
							</div>							
						</div>
						<!-- comment -->
						<div class="commentSection">
                        @php
                            $subClass = '';
							$comments = App\BlogComments::where('status',1)->where('comment_id', 0)->whereIn('blog_id', $array_ids)->get();
							@endphp
                            @if(isset($comments) && count($comments) > 0)
                            <div class="comment-title">Comments</div>
                            @foreach($comments as $comment)
                            <div class="comment-box">
								<div class="comment-content">
									<div class="comment-img">
										<img src="/dist/images/porfile-img.png" alt="porfile-img">
									</div>
									<div class="comment-name">
										{{$comment->user->name??$comment->guest_name}}
									</div>
								</div>
								<div class="comment-text">
									{{$comment->comment}}
								</div>
								<div class="comment-relay" data-add_replay="#replay_{{$comment->id}}">
								  <i class="fa fa-reply" aria-hidden="true"></i> Reply
								</div>
								<div class="comment-Date text-right">
								  <i class="fa fa-clock-o" aria-hidden="true"></i> {{date('F d , Y ,h.i A', strtotime($comment->created_at))}}
                                </div>
                                <div class="leaveComment-area d-none" id="replay_{{$comment->id}}" style="margin-top:0">
								<form class="comment-form" id="add_comments_reply_{{$comment->id}}" style="margin-top:0">
									<div class="row">
                                    <div class="col-sm-12 col-12 form-group commentmsg_{{$comment->id}}">
									</div>
                                    @if (auth()->guest())
										<div class="col-sm-6 col-12">
											<div class="form-group">
												<input type="text" name="name" class="form-control"  placeholder="Name">
											</div>
										</div>
										<div class="col-sm-6 col-12">
											<div class="form-group">
												<input type="text" name="email" class="form-control"  placeholder="Email Id">
											</div>
                                        </div>
                                        @endif
										<div class="col-sm-12 mt-3">
											<textarea placeholder="Comment" name="comment" style="height:100px; resize: none;" class="form-control comment_{{$comment->id}}" ></textarea>
                                            <input type="hidden" name="blog_id" class="blog_id_{{$comment->id}}" id="blog_id" value="{{$current_blog_id}}" />
                                            <input type="hidden" name="parent_blog_id" class="parent_blog_id" id="parent_blog_id" value="{{Crypt::encrypt($comment->id)}}" />
                                            <input type="hidden" name="_token" class="_token" id="_token" value="{{csrf_token()}}" />
                                            <input type="hidden" name="addcomment" class="addcomment" id="addcomment" value="1" />
											<input type="button" class="btn btn-comment mt-3 submitcomment" data-blogto=".blog_id_{{$comment->id}}" data-commentbox=".comment_{{$comment->id}}" data-messagebox=".commentmsg_{{$comment->id}}" data-formid="#add_comments_reply_{{$comment->id}}" data-submit_url="{{$blogCommenturl}}" value="Post Comment">
										</div>
									</div>
								</form>
							</div>
							</div>
                                @if(count($comment->children) > 0)
                                @foreach($comment->children as $relays)
                                <?php $name = $relays->user_id > 0 ? $relays->user->name : $relays->guest_name; ?>
                                <div class="comment-box ml-5">
									<div class="comment-content">
										<div class="comment-img">
											<img src="/dist/images/porfile-img.png" alt="porfile-img">
										</div>
										<div class="comment-name">
											{{$name}}
										</div>
									</div>
									<div class="comment-text">
										{{$relays->comment}}
									</div>
									<div class="comment-relay">
									</div>
									<div class="comment-Date text-right">
									<i class="fa fa-clock-o" aria-hidden="true"></i> {{date('F d , Y ,h.i A', strtotime($relays->created_at))}}
									</div>
								</div>
                            @endforeach
							@endif
							@endforeach
							@endif

							<div class="leaveComment-area">
								<div class="comment-title">
									Leave a comment
                                </div>
								<form class="comment-form" id="add_blog_comments">
									<div class="row">
                                    <div class="col-sm-12 col-12 form-group commentmsg">
									</div>
                                    @if (auth()->guest())
										<div class="col-sm-6 col-12">
											<div class="form-group">
												<input type="text" name="name" class="form-control"  placeholder="Name">
											</div>
										</div>
										<div class="col-sm-6 col-12">
											<div class="form-group">
												<input type="text" name="email" class="form-control"  placeholder="Email Id">
											</div>
                                        </div>
                                        @endif
										<div class="col-sm-12 mt-3">
											<textarea placeholder="Comment" name="comment" style="height:100px; resize: none;" class="form-control comment" ></textarea>
                                            <input type="hidden" name="blog_id" class="blog_id" id="blog_id" value="{{$current_blog_id}}" />
                                            <input type="hidden" name="_token" class="_token" id="_token" value="{{csrf_token()}}" />
                                            <input type="hidden" name="addcomment" class="addcomment" id="addcomment" value="1" />
											<input type="button" class="btn btn-comment mt-3 submitcomment" data-blogto=".blog_id" data-commentbox=".comment" data-messagebox=".commentmsg" data-formid="#add_blog_comments" data-submit_url="{{$blogCommenturl}}" value="Post Comment">
										</div>
									</div>
								</form>
							</div>
						</div>
						<!-- /comment -->
					</div>
					<div class="col-sm-3">
						<div class="relatedBlog">
							<div class="list-heading">
								<h3>Latest Blogs</h3>
							</div>
							@if(isset($allBlogs) && count($allBlogs)>0)
							@foreach($allBlogs as $bg)
							@php
							$bg_title = $bg->blog_title;
							$bg_short_description = $bg->short_description;
							$bg_clean_url = $bg->clean_url;
							$bg_id = $bg->id;
							if($lang_val !='en'){
							$bgDetails = App\Blog::where([['parent_lang_id','=',$bg->id],['lang_code','=',$lang_val]])->first();
							if(!empty($bgDetails)){
								$bg_short_description = $bgDetails->short_description;
								$bg_clean_url = $bgDetails->clean_url;
								$bg_id = $bgDetails->id;
							}
							}
							$url = action('BlogController@getImage',$bg->id);
							@endphp
							<!-- related blog 1 -->
							<div class="relatedBlog-inner">
								<div class="blog-list-right" onclick="window.location='{{URL::to('/blogs').'/'.$bg_clean_url}}';">
									<div class="relatedBlog-img">
										<img src="{{$url}}" alt="">
									</div>
									<div class="relatedBlog-info">
										<p>{{$bg_title}}</p><!-- <p>{{$bg_short_description}}</p> -->
										<span> {{date('jS M Y',strtotime($bg->created_at))}}</span>
									</div>
								</div>
							</div>
							@endforeach
							@endif
							<!-- /related blog 1 -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@include('site.partials.footer')
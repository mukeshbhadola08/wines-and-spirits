<style>


@media (max-width: 768px) {
	#intro .container .row .dark-box {
	    top: 80px !important;
	 }
	#intro .intro-info h2 {
    	    font-size: 25px !important;
		    margin-bottom: 30px !important;
		    bottom: -85px !important;
		    right: -20px !important;
	}
}

@media (max-width: 992px) {
	/*#intro .container .row .dark-box {
	    top: 80px !important;
	 }
	#intro .intro-info h2 {
    	bottom: -105px !important;
	}*/
}
</style>






@include('site.partials.header')
@php
	global $lang_val;
	global $languageVariables;

@endphp

	<!-- intro -->
	<section id="intro" class="clearfix">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 intro-img order-md-first order-last">
					<img src="{{ asset('/') }}dist/images/intro-img-top.png" alt="" class="img-fluid wow fadeInUp">
				</div>
				<div class="col-sm-12 intro-info  wow fadeInDown order-md-last order-first" style="visibility: visible; animation-name: fadeInDown;">
					<h2>Grades</h2>		
				</div>
			</div>

		</div>
	</section>
	<!-- /intro -->

	<section class="single-page-container section-padding">
		<div class="container coaches">
			@if( $grades_textarea[0] )
				<div  class="top-tag-text">
					 {{ $grades_textarea[0] }}
				</div>
			@endif
			<!-- @if( $grades_textarea[0] )
				<div>
					<p>{{$grades_textarea[0]}}</p>
				</div>
				@endif -->
				@if(isset($grades) && count($grades)>0)
				<div class="table-responsive">
					<table class="table table-dark">
							<thead class="">
							  <tr>
								<th>School Name</th>
								<th>School grade</th>
								<th width="90">School Year</th>
							  </tr>
							</thead>
							<tbody>
							@foreach($grades as $grade)
							  <tr>
								<td>{{$grade->school_name}}</td>
								<td>{{$grade->school_grade}}</td>
								<td>{{$grade->school_year}}</td>
							  </tr>
							@endforeach		
						</tbody>
					</table>
				</div>
				@else
					<div class="alert alert-danger" role="alert">No Grades Added!!		</div>
				@endif
		  </div>
	</section>

<style>
iframe img{max-width: 100%;}
</style>

@include('site.partials.footer')


@include('site.partials.header')
@php
	global $lang_val;
	global $languageVariables;

@endphp

	<!-- intro -->
	<section id="intro" class="clearfix">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 intro-img order-md-first order-last">
					<img src="{{ asset('/') }}dist/images/intro-img-top.png" alt="" class="img-fluid wow fadeInUp">
				</div>
				<div class="col-sm-6 intro-info  wow fadeInDown order-md-last order-first">
					<h2>{{ config('constants.ACHIEVEMENTS_TITLE') }}</h2>
				</div>
			</div>

		</div>
	</section>
	<!-- /intro -->

	<section class="single-page-container section-padding">
		<div class="container routine">
				
				@if(isset($alleventswithresults) && count($alleventswithresults)>0)
				<table class="">
					    <thead class="">
					      <tr>
					        <th>Events</th>
					        <th>Results</th>
					      </tr>
					    </thead>
					    <tbody>
						@foreach($alleventswithresults as $event)
					      <tr>
					        <td>{{stripslashes( $event->title)}}</td>
					        <td>{{stripslashes( $event->results)}}</td>
					      </tr>
					    @endforeach		
				    </tbody>
				</table>		
				@endif
		  </div>
	</section>

<style>
iframe img{max-width: 100%;}
</style>

@include('site.partials.footer')


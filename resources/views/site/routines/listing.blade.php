<style>
	 i.fa-play:hover .routine-video-watch{
	  	 display: block !important;
	  }
</style>

@include('site.partials.header')
@php
	global $lang_val;
	global $languageVariables;

	$title = App\Navigation::where('static_page_name','routinelisting')->pluck('title')[0];
@endphp

	<!-- intro -->
	<section id="intro" class="clearfix">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 intro-img order-md-first order-last">
					<img src="{{ asset('/') }}dist/images/intro-img-top.png" alt="" class="img-fluid wow fadeInUp">
				</div>
				<div class="col-sm-12 intro-info  wow fadeInDown order-md-last order-first" style="visibility: visible; animation-name: fadeInDown;">
						<h2>{{$title}}  </h2>
			</div>

		</div>
	</section>
	<!-- /intro -->

	<section class="single-page-container section-padding">
		<div class="container routine">
			<!-- <p>Myra&#39;s videos are on her&nbsp;<a href="https://www.youtube.com/channel/UCmjR3rVbqpmVeGIKIAKR2Tg" target="_blank">OFFICIAL YOUTUBE PAGE</a>. To watch her routine videos, please click on the &quot;Watch Video&quot; link. We hope you enjoy watching Myra&nbsp;in action!&nbsp;
						&nbsp;</p> -->
						<br>
				<div class="row">
					
				@if(isset($routines) && count($routines)>0)
					@foreach($routines as $routine)
					<div class="col-sm-6">
				      <div class="card">
				        <div class="card-header" style="text-align: center;">
					         <p class="text-left pull-left" style="width:45%; margin:0;padding-top:10px;"> {{$routine->title}} </p>

							@if($routine->slider_id)
								<img alt="" class="pull-left" style="max-width: 50px;padding-top:8px;" src="{{App\Http\Controllers\SliderController::getImage($routine->slider_id)}}">
							@else
								<img alt="" class="pull-left" style="max-width: 45px;padding-top:8px;" src="{{action('RoutinesController@getImage',$routine->id)}}">
							@endif
							<div style="position: relative;">
								@if($routine->video_url && $routine->video_time)	
										
									@endif
									@php
										$video_key  = App\News::youtube_id_from_url($routine->video_url);
									@endphp
									@if($video_key)	
									<a class="fancybox-media2 playIcon-routine"  href="http://www.youtube.com/embed/{{$video_key}}?enablejsapi=1">
										<i class="fa fa-youtube-play"></i>
									</a>
									@endif
									<!-- <div class="routine-video-watch" style="display: none; position: absolute;top: -15px;right: -16px;">
										@if($routine->video_url && $routine->video_time)	
											<span href="{{$routine->video_time}}" style="margin-right: 10px;    font-size: 14px;">{{$routine->video_time}}</span>
										@endif
										@php
											$video_key  = App\News::youtube_id_from_url($routine->video_url);
										@endphp
										@if($video_key)	
											<a style="font-size: 14px;" href="http://www.youtube.com/embed/{{$video_key}}" target="_blank">Watch Video</a>
										@endif
									</div> -->
								
							</div>
						</div>
				        <div class="card-body">
				          <blockquote class="blockquote mb-0">
						  <div class="body-routine" style="padding: 0;">

				            <p>{{$routine->description}} </p>
							@if($routine->additional_skills)
							 <p><strong>Additional Skills:</strong> {{$routine->additional_skills}}</p>
							@endif

				            <footer class="blockquote-footer">
							
							<strong>Working On:</strong> {{$routine->working_on}}
							
							
							
							</footer>
							
							</div>
				          </blockquote>
				          
				        </div>
				      </div>        
				      </div>        
				      @endforeach				
				@endif
			</div>
				<div class="row">
					<div class="col-sm-12 routine-text">
						 <?php  echo config('constants.ROUTINESTEXTAREA'); ?>
					</div>
				</div>
			</div>
	</section>

@include('site.partials.footer')

<script src="https://www.youtube.com/player_api"></script>
<script type="text/javascript">
function onYouTubePlayerAPIReady() {
	    $(document).ready(function () {
	        $('.fancybox-media2').fancybox({
				helpers: {
	                media: {
	                    youtube: {
	                        params: {
	                            autoplay: 1,
	                            rel: 0,
	                            // controls: 0,
	                            showinfo: 0,
	                            autohide: 1,
	                            mute:1
	                        }
	                    }
	                },
	            },
				
				hash : false,  
				slideShow: {
					autoStart: false,
					speed: 3000
				},
				afterShow: function () {
				var id = jQuery(document).find(".fancybox-slide--current").find('.fancybox-iframe').attr('id');
				var player = new YT.Player(id, {
				    events: {
				        onReady: onPlayerReady,
				        onStateChange: onPlayerStateChange
				    }
				});
				},
				
			  	share: {
				  url: function(instance, item) {
					return (
					  (!instance.currentHash && !(item.type === "inline" || item.type === "html") ? item.origSrc || item.src : false) || window.location
					);
				  },				  
				}
					

			});
	    });
	}
		
	function onPlayerReady(event) {
	    event.target.playVideo();
	}

	function onPlayerStateChange(event) {
	    if (event.data === 0) {
	        jQuery.fancybox.close();
	    }
	}
	
</script>
@include('site.partials.header')
	
	<!-- page title -->
	<div class="pagehding-sec">
		<div class="images-overlay"></div>		
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-heading">
						<h1>Invoice <?php echo str_pad($invoiceid, 6,"0",STR_PAD_LEFT); ?></h1>
						<ul>
							<li><a href="/">Home</a></li>
							<li><a href="#">About us</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page title -->

	<section class="single-page-container">
		<div class="container invoice-container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-xs-12">
					<div class="invoice-title">
						<h2>Invoice</h2><h3 class="pull-right">Order #<?php echo str_pad($invoiceid, 6,"0",STR_PAD_LEFT); ?></h3>
					</div>
					<hr>
					<div class="row">
						<div class="col-sm-6 col-md-6 col-xs-6">
							<address>
							<strong>Billed To:</strong><br>
							<?php echo nl2br($userdetails->name); ?><br/>
								<?php echo nl2br($servicerequestdetails->address); ?><br>
								<strong>Invoice Status:</strong><br>
								<span class="<?php if($invoice->is_paid == 1){ echo "text-success"; } else { echo "text-danger"; } ?>"><?php if($invoice->is_paid == 1){ echo "Paid"; } else { echo "Pending"; } ?></span><br>
							</address>
						</div>
						<div class="col-sm-6 col-md-6 col-xs-6 text-right">
							<address>
								<strong>Payment Method:</strong><br>
								Paypal<br>
								<?php echo nl2br($userdetails->email); ?><br/>
								<strong>Order Date:</strong><br>
								<?php echo date("F,j Y", strtotime($invoice->created_at)); ?><br><br>
							</address>
						</div>
					</div>
					
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><strong>Order summary</strong></h3>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-condensed">
									<thead>
										<tr>
											<td><strong>Description</strong></td>
											<td class="text-right"><strong>Amount</strong></td>
										</tr>
									</thead>
									<tbody>
										<!-- foreach ($order->lineItems as $line) or some such thing here -->
										<tr>
											<td>Payment for service request - <strong>{{$servicedetails->name}}</strong></td>
											<td class="text-right">$<?php echo $invoice->price; ?></td>
										</tr>
									
										<tr>
											<td class="no-line text-left"><strong>Total</strong></td>
											<td class="no-line text-right">$<?php echo $invoice->price; ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</section>

@include('site.partials.footer')
@include('site.partials.header')

	<!-- page title -->
	<div class="pagehding-sec">
		<div class="images-overlay"></div>		
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-heading">
						<h1>My Account</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page title -->

	<section class="single-page-container">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-12">				
					<div class="container-white user-tab">
						@include('site.partials.usernav')
					</div>
				</div>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<div class="container-white dashboard-form">
						<div class="dashboard-heading">
							<h4>Transactions</h4>
						</div>

						 <div class="row">
							<div class="col-sm-12">
								@if(isset($invoices) && count($invoices)>0)
								<table class="table">
									<thead>
									<tr>
										<th>Invoice ID</th>
										<th>Service Name</th>
										<th>Transactions ID</th>
										<th>Full Name</th>
										<th>Email</th>
										<th>Date</th>
										<th>Price</td>
									</tr>
									</thead>
									<tbody>
										@foreach($invoices as $trans)
										<tr>
											<td>
												<a href="/invoices/{{$trans->id}}"><?php echo str_pad($trans->id, 6, "0", STR_PAD_LEFT); ?></a>
											</td>
											<td>
											 @php
												$serviceRequest = $trans->ServiceRequest;
												if(!empty($serviceRequest)){
													$serviceDetails = App\Services::find($serviceRequest->service_id);
													echo  $serviceDetails->name;
												}
											 @endphp
											</td>
											<td>
											{{$trans->paypal_txn_id}}
											</td>
											<td>
											{{$name =isset($trans->user->name)?$trans->user->name:''}}
											</td>
											<td>
											{{$email =isset($trans->user->email)?$trans->user->email:''}}
											</td>
											<td>
											{{ date('d-m-Y H:i', strtotime($trans->created_at))}}
											</td>
											<td>
											${{$trans->price}}
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								@else
								<p>There is no transaction history.</p>
								@endif
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>
@include('site.partials.footer')
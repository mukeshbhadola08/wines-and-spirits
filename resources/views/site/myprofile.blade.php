@include('site.partials.header')
@php
global $languageVariables;
@endphp

	<!--heading-img-->
	<div class="heading-img">
		<div class="heading heading-bg-img">
			<h2> {{$languageVariables['label']['lbl_user_profile']}} </h2>
		</div>
	</div>
	<!--/heading-img-->
	<!--container-->
		<div class="container ">
			<div class="section-padding">
			@if (\Session::has('success'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>{{ \Session::get('success') }}</strong>
				</div>
			@endif
			<div id="success-msg" style="display:none;">
				<div class="alert alert-success alert-block">
					<strong>{{$languageVariables['message']['msg_password_changed']}}</strong>
				</div>
			</div>
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> {{$languageVariables['message']['msg_some_error_happen']}} <br><br>
					<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
					</ul>
				</div>
			@endif

			<form action="{{url('/update_profile')}}" method="post" enctype="multipart/form-data" id="edit_profile">
				@csrf
				 <div class="row">
					<div class="col-sm-4 col-xs-12">
						<div class="user-profile-outer">
							<div class="user-img">
								<img src="{{action('UserController@getImage',$id)}}" alt="profile image" id="user-image" class=" img-responsive ">
							</div>
							<div class="user-name">
								<p>{{$user_name}}</p>
							</div>
							<div class="text-center"><div class="upload-image btn btn-upload">
								<span><i class="fa fa-upload" aria-hidden="true"></i> {{$languageVariables['label']['lbl_upload_image']}}</span>
								<input type="file" class="upload" name="avatar" id="avatarFile">
							</div></div>
						</div>
					</div>
					<div class="col-sm-8 col-xs-12">
						<div class="profile-heading">
							<h3>{{$languageVariables['label']['lbl_edit_profile']}}</h3>
						</div>
						<div class="row">
							<div class="col-sm-3 col-xs-12">
							  <label class="label-text" for="pwd" > {{$languageVariables['label']['lbl_full_name']}} </label>
							</div>
							<div class="col-sm-9 col-xs-12">
								<div class="form-group">
									<div class="input-group ">
										<input id="user_name" type="text" class="form-control" name="user_name" value="{{old('user_name',$user_name)}}" placeholder="{{$languageVariables['label']['lbl_full_name']}}" required>
										<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user" aria-hidden="true"></i></span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3 col-xs-12">
								<label class="label-text" for="pwd" > {{$languageVariables['label']['lbl_email_address']}} </label>
							</div>
							<div class="col-sm-9 col-xs-12">
								<div class="form-group">
									<div class="input-group ">
										<input id="email" type="email" class="form-control" name="email" value="{{$user_email}}" placeholder="{{$languageVariables['label']['lbl_email_address']}}" disabled>
				                         <input id="user_email" type="hidden" class="form-control" name="user_email" value="{{$user_email}}">
										<span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope massage-icon" aria-hidden="true"></i></span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3 col-xs-12">
								<label class="label-text" for="pwd" > {{$languageVariables['label']['lbl_password']}} </label>
							</div>
							<div class="col-sm-9 col-xs-12">
								<div class="form-group">
									<div class="input-group ">
										<input id="password" type="password" class="form-control" name="password" value="{{$user_password}}" placeholder="{{$languageVariables['label']['lbl_password']}}" disabled>
										<input id="user_password" type="hidden" class="form-control" name="user_password" value="{{$user_password}}">
										<span class="input-group-addon profile-addon" ><i class="fa fa-key" aria-hidden="true" style="font-size:12px;"></i></span>
									</div>
								</div>
								<div class="form-group">
									<a href="javascript:;" class="btn btn-detail open_modal" style="margin-top: 10px;background-color: #ffaa15;color: #fcf9f5;" value="{{$id}}"> {{$languageVariables['label']['lbl_change_password']}}</a>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-sm-3 col-xs-12">
							  <label class="label-text" for="pwd" > {{$languageVariables['label']['lbl_address']}} </label>
							</div>
							<div class="col-sm-9 col-xs-12">
								<div class="form-group">
									<div class="input-group ">
										<input id="address" type="text" class="form-control" name="address"  value="{{old('address',$address)}}" Placeholder="{{$languageVariables['label']['lbl_address']}}" required>
										<span class="input-group-addon" id="basic-addon1"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
									</div>
								</div>
							</div>
							<div class="col-sm-3 col-xs-12">
							  <label class="label-text" for="pwd" > {{$languageVariables['label']['lbl_phone_no']}} </label>
							</div>
							<div class="col-sm-9 col-xs-12">
								<div class="form-group">
									<div class="input-group ">
										 <input id="phone_no" type="tel" class="form-control" name="phone_no"  value="{{old('phone_no',$phone_no)}}" Placeholder="{{$languageVariables['label']['lbl_phone_no']}}" >
										<span class="input-group-addon" id="basic-addon1"><i class="fa fa-phone " aria-hidden="true"></i></span>
									</div>
								</div>
							</div>
							
							<div class="col-sm-3 col-xs-12">
							  <label class="label-text" for="pwd" > {{$languageVariables['label']['lbl_mobile_no']}} </label>
							</div>
							<div class="col-sm-9 col-xs-12">
								<div class="form-group">
									<div class="input-group ">
										<input id="mobile_no" type="tel" class="form-control" name="mobile_no" value="{{old('mobile_no',$mobile_no)}}" Placeholder="{{$languageVariables['label']['lbl_mobile_no']}}" >
										<span class="input-group-addon" id="basic-addon1"><i class="fa fa-phone-square" aria-hidden="true"></i></span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-9 col-sm-offset-3 col-xs-12">
							<div class="form-group">
								<button type="submit" class="btn btn-small" > {{$languageVariables['label']['lbl_submit']}}  </button>
							</div>
						</div>
						
						
					</div>
					
				</div> 
				</form>

				<!--Modal for editing Password-->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
					   <div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
								<h4 class="modal-title" id="myModalLabel">{{$languageVariables['label']['lbl_change_password']}}</h4>
							</div>
							<div class="modal-body">
								<div class="dashboard dashboard-change-form container-white">
									<div id="password-error" class="text-danger"></div><br/>
									<form method="POST" action="{{url('/change_password')}}" id="ChangePassword"  class="form-horizontal" >
										@csrf
										
									<div class="row">
										<div class="col-sm-3 col-xs-12">
										  <label class="label-text" for="pwd" > {{$languageVariables['label']['lbl_new_password']}} </label>
										</div>
										<div class="col-sm-9 col-xs-12">
											<div class="form-group">
												<div class="input-group ">
													<input id="new_password" type="password" class="form-control" name="password" value="" placeholder="{{$languageVariables['label']['lbl_new_password']}}">
													<span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock" aria-hidden="true"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-3 col-xs-12">
										  <label class="label-text" for="pwd" > {{$languageVariables['label']['lbl_confirm_password']}} </label>
										</div>
										<div class="col-sm-9 col-xs-12">
											<div class="form-group">
												<div class="input-group ">
													<input id="new_confirm_password" type="password" name="password_confirmation" class="form-control" placeholder="{{$languageVariables['label']['lbl_confirm_password']}}">
													<span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock" aria-hidden="true"></i></span>
												</div>
											</div>
										</div>
									</div>
									</form>
								</div>

							</div>
							<div class="modal-footer">
								<div class="form-group">
									<button type="submit" class="btn btn-small" id="save_changes"> {{$languageVariables['label']['lbl_change_password']}}  </button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--/Modal for editing Password-->
			</div>
		</div>
		<!--container-->

	
<script type="text/javascript">
		//Change profile pic related code//
		function readURL(input) {
		    var url = input.value;
		    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
		    if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#user-image').attr('src', e.target.result);//Shows uploaded pic
		        }

		        reader.readAsDataURL(input.files[0]);
		    }else{
		         $('#img').attr('src', 'dist/images/no-image-slide.png');
		    }
		}


		$("#avatarFile").change(function(){ 
		        readURL(this);
		    });
		//----Change profile pic related code------//
		//Change password related code// 
	    $(document).on('click','.open_modal',function(){
	       	$('#myModal').modal('show');
	       
	    });
	    
		$('body').on('click', '#save_changes', function(){

			
	        var registerForm = $("#ChangePassword");
	        var formData = registerForm.serialize();
	        $( '#password-error' ).html( "" );

	        $.ajax({
	            url:'/change_password',
	            type:'POST',
	            data:formData,
	            dataType: 'json',              // let's set the expected response format
	            success:function(data) {
	                if(data.errors) {
	                    /*if(data.errors.name){
	                        $( '#name-error' ).html( data.errors.name[0] );
	                    }
	                    if(data.errors.email){
	                        $( '#email-error' ).html( data.errors.email[0] );
	                    }*/
	                    if(data.errors.password){
	                        $( '#password-error' ).html( data.errors.password[0] );
	                    }
	                    
	                }
	                if(data.success) {
	                	//console.log(data.new_password);
						 var new_password = data.new_password;
	                    $('#success-msg').show();
						$('#user_password').val(new_password);
	                    setInterval(function(){ 	                        
	                        
	                        $('#success-msg').hide();
							
	                    }, 5000);
						$('#myModal').modal('hide');
	                }
	            },
	        });
    	});
    	//---Change password related code----// 

		$('#myModal').on('hidden.bs.modal', function (e) {
		   $('#new_password').val('');
		   $('#new_confirm_password').val('');
		});

    	//Agent services related code//
		$(document).on('click','.update-profile',function(){
			var values = $('input:checkbox:checked.service-group').map(function () {
  				return this.value;
			}).get();
			//alert(values);
			$('#agent_services').val(values);
			//return false;
			$("#edit_profile").submit();
		});
		//-----Agent services related code-----//
    	
</script>

@include('site.partials.footer')
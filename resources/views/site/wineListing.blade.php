@include('site.partials.header')
  <!-- / header -->
  <!-- Our history -->
  <?php 
  $background_image = $winePageSettings['background_image']; 
  $background_color = $winePageSettings['background_color']; 
  ?>
  <div class="wrapper-page" style='background-color: {{$background_color}}; background-image: url({{ asset("/") }}uploads/static_page_background_image/{{$background_image}});'>
  	<div class="container">
		<!-- heading -->
		<div class="heading-section white-heading">
			<h2>Winery</h2>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Winery</li>
				</ol>
			</nav>
		</div>
		<!-- /heading -->
		<!-- inner wrapper -->
		<div class="inner-wrapper">
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-9">
					@if(!empty($search))
						<div class="defect-arrow">
							<strong>{{$search}}</strong>
							<a class="defect-filter-tag removesearch" href="javascript:void();" id="search_text">×</a>
						</div>
					@endif
					@if(!empty($country))
						<div class="defect-arrow">
							Country: <strong>
							<?php 
								$selectedCountriesArr = [];
								foreach($countries as $countryObj){
									if(!empty($country) && in_array($countryObj->id, $country)){
										$selectedCountriesArr[] = $countryObj->nicename;
									}
								}
								echo implode(', ', $selectedCountriesArr);
							?></strong>
							<a class="defect-filter-tag removesearch" href="javascript:void;" id="country_search">×</a>
						</div>
					@endif
					@if(!empty($varietal))
						<div class="defect-arrow">
							Varietal: <strong><?php echo implode(', ', $varietal); ?></strong>
							<a class="defect-filter-tag removesearch" href="javascript:void;" id="varietal_search">×</a>
						</div>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					<div class="cat-shorting-left">
						<div class="heading-shorting-top">
							<h4>Search here</h4><br>
							<form method="post">
							<div class="form-group">
								<label style="padding-left:7px;" class="pull-left" for="search_box"><small>Search Winery :</small></label>
								<input type="text" placeholder="Title / Description / Region" class="col-sm-11 form-control" style="display:inline !important;" value="{{$search}}" id="search_box" name="search">
							</div>
							<div class="form-group">
								<label style="padding-left:7px;" class="pull-left" for="country"><small>Country :</small></label>
								<select name="country" id="country" class="form-control col-sm-11 select_2" multiple>
									@foreach($countries as $countryObj)
									<?php
									$selected = '';
									if(!empty($country) && in_array($countryObj->id, $country)){
										$selected = 'selected';
									}
									?>
									<option value='{{$countryObj->id}}' {{$selected}}>{{$countryObj->nicename}}</option>	
									@endforeach
								</select>	
							</div>
							<div class="form-group">
								<label style="padding-left:7px;" class="pull-left" for="varietal"><small>Varietal :</small></label>
								<select name="varietal" id="varietal" class="form-control col-sm-11 select_2" multiple>
									@foreach($products as $productObj)
									<?php
									$selected='';
									if(!empty($varietal) && in_array($productObj->varietal, $varietal)){
										$selected = 'selected';
									}
									?>
									<option value='{{$productObj->varietal}}' {{$selected}}>{{$productObj->varietal}}</option>	
									@endforeach
								</select>	
							</div>
								<button class="search_btn btn btn-md btn-primary" type="button">Search</button>
							</form>
						</div>
					</div>
				</div>

				<div class="col-sm-9">
					@if(count($data)>0)
						@foreach($data as $wines)
						<!-- cat item 1 -->
						<div class="cat-item-wrapper">

							<div class="row">

								<div class="col-sm-4">
									<div class="wine-image">
										<!-- <img src="{{ asset('/') }}/dist/images/wine-single-img.png" class="img-fluid"> -->
										<img src="{{URL::to('/')}}/uploads/{{ $wines->logo }}" class="img-fluid">
									</div>
								</div>
								<div class="col-sm-8">
									<div class="cat-item-content">
										<div class="heading-item">
											<!-- <h3>Viña Zorzal Wines</h3> -->
											<h3><a href="{{URL::to('/')}}/wine/{{$wines->id}}" >{{$wines->cat_title}}</a></h3>
											<div class="info-item">
												<span><img src="{{ URL::to('/')}}/flags/{{strtolower($wines->country->code)}}.png" alt="" width="24"> &nbsp; {{$wines->country->nicename}}</span>
												<!-- <span class="city">Navarra </span> -->
												<span class="city">{{$wines->region}}</span>
											</div>
										</div>
										<div class="cat-item-text">
											{!!html_entity_decode($wines->description)!!}
											<!-- Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen  -->
										</div>
										<a href="{{URL::to('/')}}/singleWines/{{$wines->id}}" class="btn btn-outline-wine">Our Portfolio &nbsp; <span class="badge badge-warning" style="font-size:14px">{{$wines->products->count()}}</span> </a>
									</div>
								</div>
							</div>
						</div>
						@endforeach
						<?php echo $data->links(); ?>
					@else
						<div class="text-center alert alert-danger" style="font-size:24px !important;">No result found</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
<style>
	.defect-arrow:first-child {
		margin-left: 0px;
	}
	.defect-arrow {
		position: relative;
		background: #327B00;
		margin-bottom: 5px;
		border: 0px solid #133000;
		height: 30px;
		min-width: 40px;
		padding-left: 8px;
		padding-right: 5px;
		line-height: 28px;
		float: left;
		margin-left: 15px;
		color: #fff;
		margin-right: 15px;
	}
	a.defect-filter-tag {
		color: #fff;
		padding-left: 7px;
	}
	.defect-arrow:after, .defect-arrow:before {
		left: 100%;
		top: 50%;
		border: solid transparent;
		content: " ";
		height: 0;
		width: 0;
		position: absolute;
		pointer-events: none;
	}
	.defect-arrow:before {
		border-color: rgba(19, 48, 0, 0);
		border-left-color: #133000;
		border-width: 16px;
		margin-top: -16px;
	}
</style>
<script>
  $(document).ready(function() {
	$("#country").select2({
		placeholder: "Select Countries",
		allowClear: true
	});
	$("#varietal").select2({
		placeholder: "Select Varietals",
		allowClear: true
	});
    $(".search_btn").click(function(e) {
		e.preventDefault();
      	var site_url = '<?php echo asset(''); ?>';
	  	var countryArr = $("#country").val();
		var varietalArr = $("#varietal").val();
      	var value = $("#search_box").val();

      	var redirect_url = site_url+'wines?search='+value;
		if(countryArr.length!=0){
			redirect_url+='&country='+countryArr;
		}
		if(varietalArr.length!=0){
			redirect_url+='&varietal='+varietalArr;
		}
      	window.location.href = redirect_url;
    });

	///
	$(document).on('click','.removesearch',function(){
		var id = $(this).attr('id');

		if (id=="country_search") {
			$("#country").select2("val", "");
			$('#country').val(null).trigger('change');
		} else if (id=="varietal_search") {
			$("#varietal").select2("val", "");
			$('#varietal').val(null).trigger('change');
		} else {
			$('#search_box').val('');
		}

		$('.search_btn').trigger('click');
	});
  });
</script>
@include('site.partials.footer')

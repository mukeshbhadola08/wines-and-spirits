@include('site.partials.header')
	<?php
	$type = (!empty($wineOrSpirit->type) && $wineOrSpirit->type=='winery')?'winery':'distillery'; 
	$category_link = (!empty($wineOrSpirit->type) && $wineOrSpirit->type=='winery')?'wines':'spirits'; 
	?>
  <!-- Single page -->
  <?php 
  $background_image = $productPageSettings['background_image']; 
  $background_color = $productPageSettings['background_color']; 
  ?>
  <div class="wrapper-page" style='background-color: {{$background_color}}; background-image: url({{ asset("/") }}uploads/static_page_background_image/{{$background_image}});'>
 <!--  	<div class="wrapper-page" style="background-image: url({{ asset('/') }}dist/images/bg-single-wine.png);"> -->
  	<div class="container">
		<!-- heading -->
		<div class="heading-section white-heading">
			<h2>{{ucwords($type)}}</h2>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
					<li class="breadcrumb-item"><a href="{{url('/'.$category_link)}}">{{$type}}</a></li>
					<li class="breadcrumb-item active" aria-current="page">{{$wineOrSpirit->cat_title}} Listings</li>
				</ol>
			</nav>
		</div>
		<!-- /heading -->
		<!-- inner wrapper -->
		@if(count($showProduct)>0)
			@foreach($showProduct as $product)
				<div class="inner-wrapper">
					<div class="wrapper-singlePage product-list">
						<div class="row">
							<div class="col-sm-5">
								<img src="{{URL::to('/')}}/uploads/{{ $product->image }}" class="img-fluid">
							</div>
							<div class="col-sm-7">
								<div class="single-productRight">
									<h3><a href="{{URL::to('/')}}/product/{{$product->id}}">{{$product->product_title}}</a></h3>
									<ul>
										<li>Region:<span> {{$product->region}}</span></li>
										<?php if($type=='winery'){ ?>
											<li>Varietal: <span> {{$product->varietal}} </span></li>
										<?php } ?>
									</ul>
								</div>
								<!-- desc -->
								<div class="descItem-detail">
										{!!html_entity_decode($product->description)!!}
								</div>
								<!-- desc -->
								<div class="text-right">
									<a href="{{URL::to('/')}}/product/{{$product->id}}" class="btn btn-outline-wine">Read More</a>
								</div> 
							</div>
						</div>
					</div>
				</div>
			@endforeach
			<?php echo $showProduct->links(); ?>
		@else
			<div class="text-center alert alert-danger" style="font-size:24px !important;">No products found</div>
		@endif
	</div>
</div>
<!-- /Single page -->
@include('site.partials.footer')

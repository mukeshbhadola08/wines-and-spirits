@include('site.partials.header')


@section('content')
    <div class="col-md-6">
        <h1>{{ $thread->subject }}</h1>
        @each('site.messenger.partials.messages', $thread->messages, 'message')

        @include('site.messenger.partials.form-message')
    </div>
@include('site.partials.footer')


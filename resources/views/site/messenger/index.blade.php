@include('site.partials.header')
<!-- page title -->
<div class="pagehding-sec">
		<div class="images-overlay"></div>		
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-heading">
						<h1>Chats</h1>
					</div>
				</div>
			</div>
		</div>
</div>
<!-- /page title -->

<section class="single-page-container">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-12">				
					<div class="container-white user-tab">
						 <ul class="nav nab-bar nav-icon">
							<li>
								<a href="#">
									<i class="fa fa-user" aria-hidden="true"></i>&nbsp; Profile
								</a>
							</li>
							<li class="active">
								<a href="#">
									<i class="fa fa-comment" aria-hidden="true"></i>&nbsp; Chat
								</a>
							</li>						
							<li>
								<a href="#">
									<i class="fa fa-handshake-o" aria-hidden="true"></i>&nbsp; Services
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa fa-exchange" aria-hidden="true"></i>&nbsp; Transactions
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<div class="container-white dashboard-form">
						<div class="dashboard-heading">
							<h4>Chat Panel</h4>
						</div>

						<div class="people-list" id="people-list">
							<ul class="list">

								<?php 
								$firstthread = array();
								
								foreach($threads as $keythread => $thread){ 
								
									if($keythreadactive == 0 && $keythread == 0){
										$firstthread = $thread;
									} else if($thread->id == $keythreadactive) {
										$firstthread = $thread;
									}
								?>
								<li class="clearfix chat_list" data-urltogo="<?php echo URL::to('messages/list/'.$thread->id);?>">
									<div class="img-area"><img src="<?php echo URL::to('dist/images/chat_avatar_02.jpg');?>" alt="img"/></div>
									<div class="about">
										<div class="name">{{$thread->latestMessage->user->name}} </div>
										<div class="status">
											<i class="fa fa-circle online"></i> {{$thread->latestMessage->created_at->diffForHumans()}}
										</div>
										<p class="people-list-message">{{$thread->latestMessage->body}}</p>
									</div>
								</li>
								<?php } 
						
								if(!isset($firstthread->messages)){
									$firstthread = $thread;
								}
								?>
							</ul>
						</div>

						<div class="chat">
							<div class="chat-header clearfix">
								<div class="user-img-chat">
									<img src="<?php echo URL::to('dist/images/chat_avatar_02.jpg');?>" alt="avatar">
								</div>
							
								<div class="chat-about">
									<div class="chat-with"><?php echo $firstthread->subject; ?>&nbsp;&nbsp;<a href=""><i class="fa fa-eye"></i></a></div>
									<?php if(isset($firstthread->messages)){ ?>
									<div class="chat-num-messages">Already <?php echo count($firstthread->messages);?> messages</div>
									<?php } ?>
								</div>
								<!-- <i class="fa fa-star"></i> -->
							</div> <!-- end chat-header -->
						  
							<div class="chat-history">
								<ul>
									<?php 
									if(isset($firstthread->messages)){ 
										foreach($firstthread->messages as $keymessage => $message){ 
									?>

										<?php if($message->user_id != Auth::id()){ ?>
											<li class="clearfix" >
											<div class="message-data align-right">
											<span class="message-data-time">{{ $message->created_at->diffForHumans() }}</span> &nbsp; &nbsp;
											<span class="message-data-name">{{$message->user->name}}</span> <i class="fa fa-circle me"></i>

											</div>
											<div class="message other-message float-right">
											{{$message->body}}
											</div>
											</li>
										<?php } else { ?>
											<li>
											<div class="message-data">
											<span class="message-data-name"><i class="fa fa-circle online"></i> {{$message->user->name}}</span>
											<span class="message-data-time">{{ $message->created_at->diffForHumans() }}</span>
											</div>
											<div class="message my-message">
											{{$message->body}}
											</div>
											</li>
										<?php } ?>

									<?php } 
									}?>
								</ul>
							</div> <!-- end chat-history -->
						  
							<div class="chat-message clearfix">
								<form action="{{backpack_url('messages/'.$firstthread->id)}}" method="post">
								{{ csrf_field() }}
									<textarea  name="message" id="message-to-send" placeholder="Type your message" rows="3" required="true"></textarea>								
									<button type="submit">Send</button>
								</form>

							</div> <!-- end chat-message -->
					
						</div>

					</div>
				</div>
			</div>
</section>
@include('site.partials.footer')

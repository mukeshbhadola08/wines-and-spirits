@include('site.partials.header')
<!-- full-image-slider-section-->

  <section class="full-image-slider">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		<div class="container">
			<ol class="carousel-indicators">
				@foreach($slides as $key => $slideDetails)
				<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				@endforeach
			</ol>
		</div>

		<div class="carousel-inner">
			@foreach($slides as $key => $slideDetails)
			<div class="carousel-item @if($key == 0) active @endif">
			  	<img class="d-block w-100" src="{{URL::to('/')}}/uploads/slider_images/{{ $slideDetails->image }}" alt="First slide">
			  <div class="container">
				<div class="carousel-caption d-none d-md-block">
				  <h5>{{$slideDetails->title}}</h5>
				  <p>{!!html_entity_decode($slideDetails->description)!!}</p>
				</div>
			  </div>
			</div>
			@endforeach
		</div>

		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<img src="<?php echo asset('dist/images/left-arrow.svg'); ?>" alt="arrow-prev" class="arrow-img" />
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			<img src="<?php echo asset('dist/images/right-arrow.svg'); ?>" class="arrow-img" />
			<span class="sr-only">Next </span>
		</a>
	</div>
</section>

<!-- / Home Slider -->

 <!-- Our mission -->
<section class="about-section" style="<?php echo $mission_color; ?>">
	<div class="about-container">
		<div class="row">
			<div class="col-sm-6 about-bg-img about-bg-responsive"
			style="background-image:url(<?php echo $mission_side_image->option_value; ?>);"></div>
			<div class="col-sm-6">
				<div class="about-us-content">
				   <div class="heading-section">
					<h2>{{$config_mission_title->option_value}}</h2>
				   </div>
				   <div class="outer-content">
				   	<p>{{$config_mission_content->option_value}}</p>
				  </div>
				  @foreach($mission as $missionval)
				  <div class="mission-list">
					<ul>
						<li>
							<img src="{{$missionval->image }}">
							<p>{{$missionval->description}}</p>
						</li>
					</ul>
				  </div>
				  @endforeach
				</div>
			</div>
	   </div>
	</div>
</section>
<!-- / Our misssion -->

<!-- Our history -->
<section class="history-section" style="<?php echo $history_color_img; ?>;">
	<div class="container">
		<div class="heading-section white-heading">
			<!-- <h2>Our History</h2> -->
			<h2>{{config('constants.HOMEPAGE_HISTORY_TITLE')}}</h2>
		</div>
		<!-- slider -->
		<div class="owl-carousel owl-theme site-owl">
			@foreach($history as $his)
			<div class="item">
				<div class="row">
					<div class="col-sm-6 col-md-6 col-12">
						<img src="{{URL::to('/')}}/uploads/{{ $his->image }}" alt="history-img" class="img-fluid about-img"/>
					</div>
					<div class="col-sm-6 col-md-6 col-12">
						<div class="history-content">
							<div class="heading-history">
								<h3>{{$his->title}} </h3>
							</div>
							<div class="history-content-inner">
								<p>{!! $his->description !!}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
        </div>
		<!-- slider -->
	</div>
</section>
<!-- /Our history -->

<!-- Wines -->
<section class="wines-section" style="<?php echo $wines_color_img; ?>">
	<div class="container">
		<div class="heading-section">
			<h2>{{ config('constants.HOMEPAGE_WINES_TITLE') }}</h2>
		</div>

		<!-- slider -->
		<div class="owl-carousel owl-theme site-owl">
			@foreach($productlist as $wines)
			<div class="item">
				<div class="row">
					<div class="col-sm-6 col-md-6 col-12">
						<div class="wines-content">
							
							<h2><a href="{{URL::to('/')}}/product/{{$wines->id}}">{{$wines->product_title }}</a></h2>
							
							<h4><a href="{{url('/wine/'.$wines->wine->id)}}">{{$wines->wine->cat_title}}</a>, {{$wines->country->nicename}}</h4>
							<div class="content-area-wine">
								{!! str_limit($wines->description, 500) !!}
							</div>
							<a href="{{URL::to('/')}}/product/{{$wines->id}}" class="btn btn-success">{{$config_readtext_wines->option_value}}</a>
						</div>
					</div>
					<div class="col-sm-6 col-md-6 col-12">
						<div class="icon-circle-outer">
							<div class="icon-circle">
								<img src="{{ asset('/') }}dist/images/white-icon-grape.svg"  alt="wine-logo">
							</div>
						</div>
						<div class="wines-image">
							<img src="{{URL::to('/')}}/uploads/{{ $wines->image }}" alt="wine-logo">
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
		<!-- /slider -->
	</div>
</section>
<!-- /Wines -->

<!-- Blogs -->
<section class="history-section" style="<?php echo $blog_color_image;?>">
	<div class="container">
		<div class="heading-section white-heading">
			<h2>{{ config('constants.HOMEPAGE_BLOG_TITLE') }}</h2>
		</div>
		<!-- row -->
		<div class="row">
			@foreach($allrecentblogs as $blogs)
			<!-- blog 1 -->
            <div class="col-sm-6 mb-3">
				<div class="latestBlog">
					<div class="row no-gutters">
						<div class="col-md-5 img-blog-home" style="background-image: url({{action('BlogController@getImage',$blogs->id)}})">
						</div>
						<div class="col-md-7">
							<div class="blog-inner">
								<div class="date">
									<h4>{{$blogs->created_at->format('d')}}</h4>
									<div class="month">
										<span>{{$blogs->created_at->format('M')}}</span>
									</div>
								</div>
								<h3>{{$blogs->blog_title}}</h3>
								<div class="blog-desc">
									{!! $blogs->blog_description !!}
								</div>
								<a href="{{URL::to('/')}}/blogs/{{$blogs->clean_url}}" class="btn btn-outline-wine mt-2">{{$blog_readtext->option_value}}</a>
								<!-- <a href="{{URL::to('/')}}/singleBlog/{{$blogs->id}}" class="btn btn-outline-wine mt-2">Read More</a> -->
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
		<!-- /row -->
	</div>
</section>
<!-- /Blogs -->
@include('site.partials.footer')

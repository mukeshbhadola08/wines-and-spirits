@include('site.partials.header')
<!-- Single page -->
	<?php 
  	$background_image = $blogPageSettings['background_image']; 
  	$background_color = $blogPageSettings['background_color']; 
  	?>
	<div class="wrapper-page" style='background-color: {{$background_color}}; background-image: url({{ asset("/") }}uploads/static_page_background_image/{{$background_image}});'> 
		<div class="container">
			<!-- heading -->
			<div class="heading-section white-heading">
				<h2>Blogs</h2>
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{URL::to('/')}}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{URL::to('/blog')}}">Blogs</a></li>
					</ol>
				</nav>
			</div>
			<!-- /heading -->
			<!-- inner wrapper -->
			<div class="inner-wrapper blogs-listing">
				<div class="row">
					<!-- blog list -->
					@foreach($blogs as $blog)
					<div class="col-sm-4">
						<div class="card-content">
							<div class="card-img" style="max-height:180px;overflow:hidden;">
								<img src="{{URL::to('/')}}/blogs/image/{{$blog->id}}"><!-- <img src="{{ asset('/') }}/dist/images/blogs-img-1.png" alt=""> -->
							</div>
							<div class="card-desc">
								<!-- <h3>New Wine</h3> -->
								<h3>{{$blog->blog_title}}</h3>
								<div class="blogs-info">
									<span class="comment-view"><i class="fa fa-commenting" aria-hidden="true"></i>&nbsp;
									{{count($blog->comments)}} Comments</span>
									<span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;<?php  echo date('M'); ?>&nbsp;<?php  echo date('d,'); ?>&nbsp;<?php  echo date('Y'); ?></span><!-- May 20, 2019 -->
								</div>
								<div class="desc-blog">
									<p>{!! str_limit($blog->blog_description, 150) !!}</p>
								</div>
								<div class="text-right">
									<a href="{{URL::to('/blogs/'.$blog->clean_url)}}" class="btn btn-outline-wine">Read More</a>
								</div>  
							</div>
						</div>
					</div>
					@endforeach
				</div>
				
			</div>
		</div>
	</div>
@include('site.partials.footer')
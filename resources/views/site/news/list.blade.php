@include('site.partials.header')
@php
	global $lang_val;
	global $languageVariables;
	$title = App\Navigation::where('static_page_name','eventslisting')->pluck('title')[0];
@endphp
	<!-- intro -->
	<section id="intro" class="clearfix">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 intro-img order-md-first order-last">
					<img src="{{ asset('/') }}dist/images/intro-img-top.png" alt="" class="img-fluid wow fadeInUp">
				</div>
				<div class="col-sm-12 intro-info  wow fadeInDown order-md-last order-first">
					<h2>{{$title}}</h2>
					<!-- <h2><br>Events {{$languageVariables['label']['lbl_all_events']}}</h2> -->
				</div>
			</div>

		</div>
	</section>
	<!-- /intro -->
	<section class="inner-page-section Events-page">
		<div class="container">

			<div class="row row-flex">
				@if(isset($allNews) && count($allNews)>0)
					@foreach($allNews as $news)
						@php
							$nw_title = $news->title;
							$nw_summery = $news->summery;
							$nw_permalink = $news->permalink;
							$nw_videourl = $news->video_url;
						
						@endphp


						<!-- event list 1 -->
						<div class="col-sm-6 wow fadeInDown">
							<div class="media" onclick="window.location='{{action('NewsController@view',$nw_permalink)}}';">
								<div class="mr-3 date-area">{{date('d',strtotime($news->event_date))}}<span>{{date('M Y',strtotime($news->event_date))}}</span></div>
								<div class="media-body">
									<h5 class="mt-0 mb-1">
										{{$nw_title}}
									</h5>
									<div class="location-events"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp; {{$news->event_venue}}</div>
									<div class="desc-event-small">
										<?php 
											echo substr(html_entity_decode($nw_summery), 0, 180)."..."; 
										?>
									</div>
								</div>
							</div>						
						</div>
						<!-- /event list 1 -->
						
						<!-- <div class="col-sm-4 col-xs-12">
							<div class="thumbnail outer-part">
								<div class="img-inner">
									@if($news->slider_id)
										<img class="media-object" src="{{action('SliderController@getImage',$news->slider_id)}}" alt="no-img" style="width:100%">
									@else
										<img class="media-object" src="{{action('NewsController@getImage',$news->id)}}" alt="no-img" style="width:100%" >
									@endif
								</div>
								<div class="caption-text ">
									<h3> {{$nw_title}} </h3>
									<p class="blog-time" ><i class="fa fa-calendar" aria-hidden="true"></i> {{date('jS M, Y',strtotime($news->event_date))}} </p>
									<p class="blog-time"><i class="fa fa-map-marker" aria-hidden="true"></i> {{$news->event_venue}} </p>
									<div class="row">
										<div class="col-sm-7">
											<div style="height: 42px;overflow: hidden;"><?php 
												echo substr(html_entity_decode($nw_summery), 0,58)."..."; 
												?>
											</div>
										</div>
										<div class="col-sm-5 text-right">
											<a class="btn btn-read btn-xs-read" href ="{{action('NewsController@view',$nw_permalink)}}">Read More</a>
										</div>
									</div>
								</div>
							</div>
						</div> -->
					@endforeach
				<div class="row">
					<div class="col-sm-12">
						<div class="pull-right">{{ $allNews->links() }}</div>
					</div>
				</div>
				@endif
			</div>

		</div>
	</section>
<script>
var myAudio = new Audio('');
$(document).on('click','.audio-play',function(e){
	e.preventDefault();
	var obj = $(this);
	var audio = $(this).parent().closest('.media-margin').find('.audio-content').attr('src');
	myAudio.src = audio;	
	myAudio.play();
	obj.hide();
	obj.next().show();
		
});
$(document).on('click','.audio-pause',function(e){
	e.preventDefault();
	var obj = $(this);
	var audio = $(this).parent().closest('.media-margin').find('.audio-content').attr('src');
	 myAudio.src = audio;
	myAudio.pause();
	obj.hide();
	obj.prev().show();
});
</script>

@include('site.partials.footer')
@include('site.partials.header')
	<!-- intro -->
	<section id="intro" class="clearfix">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 intro-img order-md-first order-last">
					
					@if($news->slider_id)
						<img  alt="" class="img-fluid wow fadeInUp" src="{{App\Http\Controllers\SliderController::getImage($news->slider_id)}}">
					@else
						<img  alt="" class="img-fluid wow fadeInUp" src="{{action('NewsController@getImage',$news->id)}}">
					@endif
					<!-- <img src="{{ asset('/') }}dist/images/intro-img-top.png" alt="" class="img-fluid wow fadeInUp"> -->
				</div>
				<div class="col-sm-12 intro-info  wow fadeInDown order-md-last order-first">
					<h2>Events</h2>
				</div>
			</div>

		</div>
	</section>
	<!-- /intro -->

		<section class="inner-page-section Events-page">
			<div class="container">
				<div class="heading-page with-left-date">
					<div class="show-date-circle">
						{{date('d',strtotime($news->event_date))}}<div>{{date('M Y',strtotime($news->event_date))}}</div>
					</div>
					<h1>{{$news->title}}</h1>
				</div>
				<div class="content-page-area">

					@php
						$videoId = App\News::youtube_id_from_url($news->video_url);
					@endphp
					<div class="row justify-content-md-center">
						<div class="col col-sm-6">
							<div class="embed-responsive embed-responsive-4by3">
								<iframe type="text/html"  
										height="280"
										frameborder="0"
										src="https://www.youtube.com/embed/{{$videoId}}?rel=0" class="embed-responsive-item">
								</iframe>
							</div>
						</div>
						<div class="col col-sm-6 text-center">
							@if($news->slider_id)
								<img  alt="" class="img-fluid" src="{{App\Http\Controllers\SliderController::getImage($news->slider_id)}}">
							@else
								<img  alt="" class="img-fluid" src="{{action('NewsController@getImage',$news->id)}}">
							@endif
						</div>
					</div>
					@if($news->video_url !='')


						@else
						@if($news->file_name !='')
							@php
								$exe = pathinfo($news->file_name, PATHINFO_EXTENSION);
								$image_types = array('jpeg','bmp','png','gif','jpg');
								$audio_types = array('m4a','mp3','wav','ogg');
							@endphp
							<!-- <div class="text-center">
								<img src="{{action('NewsController@getImage',$news->id)}}" alt="{{$news->title}}" class="img-fluid" >
							</div> -->
						@endif
					@endif


					<div class="page-desc-area">
						<?php echo html_entity_decode($news->description); ?>
					</div>
					<div class="addthis_inline_share_toolbox_m4v9"></div>
				</div>
			</div>
		</section>

	
		
	
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b926030a100c5c3"></script>

           <!-- Go to www.addthis.com/dashboard to customize your tools -->
                
<script>
var myAudio = new Audio('');
$(document).on('click','.audio-play',function(e){
	e.preventDefault();
	var obj = $(this);
	var audio = $(this).parent().closest('.media-margin').find('.audio-content').attr('src');
	myAudio.src = audio;	
	myAudio.play();
	obj.hide();
	obj.next().show();
		
});
$(document).on('click','.audio-pause',function(e){
	e.preventDefault();
	var obj = $(this);
	var audio = $(this).parent().closest('.media-margin').find('.audio-content').attr('src');
	 myAudio.src = audio;
	myAudio.pause();
	obj.hide();
	obj.prev().show();
});
</script>

@include('site.partials.footer')
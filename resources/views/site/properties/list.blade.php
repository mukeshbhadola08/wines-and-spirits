@include('site.partials.header')
	
	<!-- page title -->
	<div class="pagehding-sec">
		<div class="images-overlay"></div>		
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-heading">
						<h1>All Blogs</h1>
						<ul>
							<li><a href="/">Home</a></li>
							<li><a href="#">Blogs</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page title -->

	<section class="single-page-container">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
				@if(isset($blogs) && count($blogs)>0)
					@foreach($blogs as $blogDetails)
					<div class="media">
						<div class="media-left">
							<a href="{{action('BlogController@view',$blogDetails->clean_url)}}"><img class="media-object" src="{{action('BlogController@getImage',$blogDetails->id)}}" alt="no-img" height="200" width="200"></a>
						</div>
						<div class="media-body">
							<h4 class="media-heading">{{$blogDetails->blog_title}}</h4>
							<?php echo html_entity_decode($blogDetails->short_description); ?>
						</div>
					</div>
					@endforeach	
				@else
				<div class="alert alert-info">
				There are no blogs yet.
				</div>
				@endif
				</div>
			</div>

		</div>
	</section>

@include('site.partials.footer')
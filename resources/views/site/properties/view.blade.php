@include('site.partials.header')
	
	<!-- page title -->
	<div class="pagehding-sec">
		<div class="images-overlay"></div>		
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-heading">
						<h1>{{$property->title}}</h1>
						<ul>
							<li><a href="/">Home</a></li>
							<li><a href="#">About us</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page title -->

	<section class="single-page-container">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
				<img class="media-object" src="{{action('PropertyController@getImage',$property->id)}}" alt="no-img" width="100%"><br/>
				<?php echo html_entity_decode($property->description); ?>
				</div>
			</div>
		</div>
	</section>

@include('site.partials.footer')
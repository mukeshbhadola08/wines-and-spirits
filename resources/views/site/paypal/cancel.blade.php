@include('site.partials.header')
	
	<!-- page title -->
	<div class="pagehding-sec">
		<div class="images-overlay"></div>		
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-heading">
						<h1>Transaction Cancelled</h1>
						<ul>
							<li><a href="/">Home</a></li>
							<li><a href="#">My Transactions</a></li>
							<li><a href="#">Cancel Transaction</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page title -->

	<section class="single-page-container">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<p>You have cancelled your last transacton.</p>
				</div>
			</div>
		</div>
	</section>

@include('site.partials.footer')
@include('site.partials.header')
<!-- full-image-slider-section-->
<?php 
    $background_image = $contactPageSettings['background_image']; 
	$background_color = $contactPageSettings['background_color'];  
  ?>
<!-- Single page -->
<div class="wrapper-page" style='background-color: {{$background_color}}; background-image: url({{ asset("/") }}uploads/static_page_background_image/{{$background_image}});'> 
	<div class="container">
		<!-- heading -->
		<div class="heading-section white-heading">
			<h2>Contact Us</h2>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Contact Us</li>
				</ol>
			</nav>
		</div>
		<!-- /heading -->
		<!-- inner wrapper -->
		<div class="inner-wrapper">
			<!-- top -->
			<div class="wrapper-singlePage">
				<div class="row contact-info-page">
					<div class="col-sm-4">
					  <div class="address-inner">
						<div class="address-icon">
						  <i class="fa fa-map-marker" aria-hidden="true"></i>
						</div>
						<div class="heading">
						  Address
						</div>
						<p>
						{{ $configSettings['ADDRESS'] }}
						<a href="https://maps.google.com/?q={{ config('constants.LATITUDE') }},{{ config('constants.LONGITUDE') }}" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
						</p>
						@foreach($get_contactus as $contactus)
							<p>
								{{ $contactus->address}}
								<a href="https://maps.google.com/?q={{ $contactus->latitude }},{{ $contactus->longitude }}" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
							</p>
						@endforeach
					  </div>
					</div>
					<div class="col-sm-4">
					  <div class="address-inner">
						<div class="address-icon">
						  <i class="fa fa-phone" aria-hidden="true"></i>
						</div>
						<div class="heading">
						  Contact
						</div>
						<p>
						{{ $configSettings['PHONE_NO'] }}
						</p>
						@foreach($get_contactus as $contactus)
							<p>
								{{ $contactus->contact}}
							</p>
						@endforeach
					  </div>
					</div>
					<div class="col-sm-4">
					  <div class="address-inner">
						<div class="address-icon">
						  <i class="fa fa-envelope" aria-hidden="true"></i>
						</div>
						<div class="heading">
						  Join Our Mailing list
						</div>
						<p>
						{{ $configSettings['ADMIN_EMAIL'] }}
						</p>
						@foreach($get_contactus as $contactus)
							<p>
								{{ $contactus->email}}
							</p>
						@endforeach
					  </div>
					</div>
				</div>
			</div>
			<!-- /top -->
			<!-- form -->
			<form action="{{url('contactus/send')}}" method="post">
			<input type="hidden" name="_token" class="_token" id="_token" value="{{csrf_token()}}" />
				<div class="wrapper-singlePage contact-form">
					<div class="form-heading text-center">
						Get in touch
					</div>
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					@if ($message = Session::get('success'))
					<div class="alert alert-success alert-block">
						<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>{{ $message }}</strong>
					</div>
					@endif
					<div class="row">
						<div class="col-sm-6 col-12">
							<div class="form-group ">
								<input type="text" class="form-control"  placeholder="Name" name="name">
							</div>
						</div>
						<div class="col-sm-6 col-12">
							<div class="form-group ">
								<input type="text" class="form-control"  placeholder="Email Id " name="email">
							</div>
						</div>
						<div class="col-sm-12 col-12">
							<div class="form-group ">
								<input type="text" class="form-control"  placeholder="Subject" name="subject">
							</div>
							<div class="form-group ">
								<textarea placeholder="Message" style="height:100px; resize: none;" class="form-control" name="message" > </textarea>
							</div>
						</div>

						<div class="form-group{{ $errors->has('captcha') ? ' has-error' : '' }}">
	                      	<label for="password" class="col-md-12 control-label">Captcha</label>
	                      	<div class="col-md-12">
	                          	<div class="captcha">
		                          	<span>{!! captcha_img() !!}</span>
		                          	<button type="button" class="btn btn-success btn-refresh"><i class="fa fa-refresh"></i></button>
	                          	</div>
	                          	<input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
	                      	</div>
	                  	</div>


						<div class="col-sm-12 col-12">
							<div class="text-center">
								<button class="btn btn-outline-wine">Send Now</button>
							</div>
						</div>

					</div>
				</div>
			</form>
			<!-- /form -->
		</div>
		<!-- /inner wrapper -->
	</div>
</div>

<script type="text/javascript">
	$(".btn-refresh").click(function(){
	  	$.ajax({
	     	type:'GET',
	     	url:'/refresh_captcha',
	     	success:function(data){
	        	$(".captcha span").html(data.captcha);
	     	}
	  	});
	});
</script>

<!-- Single page -->
@include('site.partials.footer')

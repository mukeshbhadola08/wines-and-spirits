@include('site.partials.header')
<?php 
$background_image = $page->background_image; 
$background_color = $page->background_color; 
?>
<!-- Single page -->
<div class="wrapper-page" style='background-color: {{$background_color}}; background-image: url({{ asset("/") }}uploads/page_background_image/{{$background_image}});'> 
	<div class="container">
		<!-- heading -->
		<div class="heading-section white-heading">
			<h2>{{$page->page_title}}</h2>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">{{$page->page_title}}</li>
				</ol>
			</nav>
		</div>
		<!-- /heading -->
		<!-- inner wrapper -->
		<div class="inner-wrapper">
			<!-- top -->
			<div class="wrapper-singlePage">
			<div class="intro-img">
					<img  alt="" class="img-fluid wow fadeInUp" src="{{action('PageController@getImage',$page->id)}}">
				</div>
				<div class="about-content"><?php echo $page->page_description; ?></div>
			</div>
			<!-- /top -->
		</div>
		<!-- /inner wrapper -->
	</div>
</div>
<!-- Single page -->

@include('site.partials.footer')
@include('site.partials.header')
<?php
$type = (!empty($product->wine->type) && $product->wine->type=='winery')?'winery':'distillery';
$category_link = (!empty($product->wine->type) && $product->wine->type=='winery')?'wines':'spirits'; 
?>
	<?php 
		if($type=='winery'){
	  		if(!empty($product->product_background_img)){
		?>  	
	  			<div class="wrapper-page" style="background-image: url(<?php echo URL::to('/'); ?>/uploads/background_img/{{ $product->product_background_img }});">
	<?php } else { ?>
			<div class="wrapper-page" style="background-image: url(<?php echo config('constants.SINGLEPAGE_PRODUCT_BACKGROUND_IMAGE'); ?>)">
	<?php } }?>

	<?php 
		if($type=='distillery'){
  			if(!empty($product->product_background_img)){
		?> 
			<div class="wrapper-page" style="background-image: url(<?php echo URL::to('/'); ?>/uploads/background_img/{{ $product->product_background_img }});">
	<?php }else { ?>
			<div class="wrapper-page" style="background-image: url(<?php echo config('constants.SINGLEPAGE_PRODUCT_BACKGROUND_IMAGE'); ?>)">
	<?php } } ?>

  	<div class="container">
		<!-- heading -->
		<div class="heading-section white-heading">
			<h2>{{ucwords($type)}}</h2>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page"><a href="{{url('/'.$category_link)}}">{{$type}}</a></li>
					<li class="breadcrumb-item active" aria-current="page"><a href="{{url('/wine/'.$product->wine->id)}}">{{$product->wine->cat_title}}</a></li>
					<li class="breadcrumb-item active" aria-current="page">{{$product->product_title}}</li>
				</ol>
			</nav>
		</div>
		<!-- /heading -->
		<!-- inner wrapper -->
		<div class="inner-wrapper">
			<div class="wrapper-singlePage">
				<div class="row">
					<div class="col-sm-5">
						<img src="{{URL::to('/')}}/uploads/{{ $product->image }}" class="img-fluid">
					</div>
					<div class="col-sm-7">
						<div class="single-productRight">
							<h3>{{$product->product_title}}</h3>
							<ul>
								<li>Region:<span> {{$product->region}}</span></li>
								<?php if($type=='winery'){ ?>
									<li>Varietal: <span> {{$product->varietal}} </span></li>
								<?php } ?>
							</ul>
						</div>
						<!-- desc -->
						<div class="descItem-detail">
							<p>
								{!!html_entity_decode($product->description)!!}
							</p>
						</div>
						<!-- desc -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /Single page -->
@include('site.partials.footer')

@include('site.partials.header')
@php
	global $lang_val;
	global $languageVariables;
	$title = App\Navigation::where('static_page_name','portfoliolisting')->pluck('title')[0];
@endphp

	<!-- intro -->
	<section id="intro" class="clearfix">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 intro-img order-md-first order-last">
					<img src="{{ asset('/') }}dist/images/intro-img-top.png" alt="" class="img-fluid wow fadeInUp">
				</div>
				<div class="col-sm-12 intro-info  wow fadeInDown order-md-last order-first">
					<h2>
						@if(isset($_GET['type'])) {{$_GET['type']}} @endif
					</h2>
				</div>
			</div>

		</div>
	</section>
	<!-- /intro -->

<style type="text/css">
	@media (max-width: 600px){
		#portfolio-main{
		
	    -moz-column-count: 2 !important;
	    column-count: 2 !important;
	  
		}
	}
	#portfolio-main{
		 margin: 1rem 0;
    -webkit-column-count: 3; 
    -moz-column-count: 3;
    column-count: 3;
    -webkit-column-gap: 1rem;
    -moz-column-gap: 1rem;
    column-gap: 1rem;
    -webkit-column-width: 33.33333333333333%;
    -moz-column-width: 33.33333333333333%;
    column-width: 33.33333333333333%;
	}
	#portfolio-main-video{
		 margin: 1rem 0;
    -webkit-column-count: 4; 
    -moz-column-count: 4;
    column-count: 4;
    -webkit-column-gap: 1rem;
    -moz-column-gap: 1rem;
    column-gap: 1rem;
   /*-webkit-column-width: 33.33333333333333%;
    -moz-column-width: 33.33333333333333%;
    column-width: 33.33333333333333%;*/
    -webkit-column-width: 25%;
    -moz-column-width: 25%;
    column-width: 25%;
	}
	.tile { 
    -webkit-transform: scale(0);
    transform: scale(0);
    -webkit-transition: all 350ms ease;
    transition: all 350ms ease;

}
.tile:hover { 

}

.scale-anm {
  transform: scale(1);
}



p{ 
  padding:10px; 
  border-bottom: 1px #ccc dotted; 
  text-decoration: none; 
  font-family: lato; 
  text-transform:uppercase; 
  font-size: 12px; 
  color: #333; 
  display:block; 
  float:left;
}

p:hover { 
  cursor:pointer; 
  background: #333; 
  color:#eee; }

.tile img {
    max-width: 100%;
    width: 100%;
    height: auto;
    margin-bottom: 1rem;
  
}
.tile .col-4{
	width:100%;
	float:left;
	overflow:hidden;
	padding-right:15px;
}

.btn {
    font-family: Lato;
    font-size: 1rem;
    font-weight: normal;
    text-decoration: none;
    cursor: pointer;
    display: inline-block;
    line-height: normal;
    padding: .5rem 1rem;
    margin: 0;
    height: auto;
    border: 1px solid;
    vertical-align: middle;
    -webkit-appearance: none;
    color: #555;
    background-color: rgba(0, 0, 0, 0);
}

.btn:hover {
  text-decoration: none;
}

.btn:focus {
  outline: none;
  border-color: var(--darken-2);
  box-shadow: 0 0 0 3px var(--darken-3);
}

::-moz-focus-inner {
  border: 0;
  padding: 0;
}
.parent-div{
	position: absolute;
	top:0;
	left :0;
	right:0;
	bottom: 0;
	z-index: 9999;

}

/*.fancybox-slide{

	max-width: 70% !important;
	position: absolute; !important;
	margin:auto;

}*/

@media (max-width: 758px){
		#portfolio-main-video{
		
	    -moz-column-count: 1 !important;
	    column-count: 1 !important;
	    padding-bottom:2px;
	  
		}
	}
</style>
	<section class="single-page-container section-padding">
		<div class="container">
			<!-- tabs start -->
			<div class="media-main-outer">
				
				@if(isset($_GET['type']) && $_GET['type']== "Photos")
				<div id="portfolio-main">
				
						@if(isset($photos) && count($photos)>0)							
						
							@foreach($photos as $photo)
								@php
									$photo_title = $photo->title;
									$photo_id = $photo->id;
									$video_key  = App\Http\Controllers\SliderController::YoutubeID($photo_id);
									
								@endphp
									
										@if(!$video_key)
											
											<div class="tile scale-anm all">
												<a class="fancybox-media" href="{{url('/public/storage/slider_images/' . $photo->image)}}" data-fancybox="gallery" data-caption="{{$photo_title}}" data-fancybox-play="">
													<img src="{{url('/public/storage/slider_images/' . $photo->image)}}" alt="{{$photo_title}}" class="blog-sm-img img-responsive" style="width: 100%">
												</a>
											</div>
										@endif
									
								@endforeach				
							@endif

					</div>
				</div>
					
					@elseif(isset($_GET['type'])  && $_GET['type']== "Videos" )
						<div id="portfolio-main-video" >
							@if(isset($videos) && count($videos)>0)							
							
								@foreach($videos as $video)
									@php
										$video_title = $video->title;
										$video_id = $video->id;
										$video_key  = App\Http\Controllers\SliderController::YoutubeID($video_id);
										
									@endphp
									
										
											@if($video_key)
												 <div class="tile scale-anm all " style="position: relative;">
												 	<a class="fancybox-media" data-fancybox="video" data-type="iframe" data-src="https://www.youtube.com/embed/{{$video_key}}?enablejsapi=1" href="javascript:;">
																<div class="parent-div"></div>
															<iframe href="javascript:;" src="https://www.youtube.com/embed/{{$video_key}}?enablejsapi=1" style="width:100%; margin-bottom:20px;height:25%;"></iframe></a>
													
												</div>
												
											@endif
										
									@endforeach				
								@endif
						
						</div>

				</div>
				

				@endif

			
			
		</div>
	</section>
	
  
<style>
iframe img{max-width: 100%;}
</style>
<script>
	$("#list_media").change(function(){
		var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?'+this.value;
			window.location.href = newurl;
	});
	
</script>
@include('site.partials.footer')
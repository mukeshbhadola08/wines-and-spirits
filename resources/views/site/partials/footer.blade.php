<!--==========================
    Footer
  ============================-->
  <!--footer-->
  <?php
use App\Http\Controllers\SiteController;
list($footerlogo, $configwinefb, $configinsta, $configtwiiter, $configyoutube, $configlinkedin) = SiteController::wineFooter();

?>
  <footer >
  <div class="footer-bg">
    <div class="container">
      <div class="row">
        <div class="col-sm-5 col-md-5 col-12">
          <h3>Essential Details</h3>
          <div class="footer-nav navbar-nav ml-auto">
            <ul>
              <!--  <ul class="navbar-nav ml-auto"> -->
            @php
                $navigations = App\Navigation::where([['is_visible','=',1],['show_footer','=',1],['parent_id','=',0],['lang_code','=','en']])->orderBy('nav_order', 'asc')->get();
              @endphp

              @if(isset($navigations) && count($navigations)>0)
                @foreach($navigations as $nav)
                  @php

                    $nav_title = $nav->title;
                    if($nav->type == 1){
                      $pageDetails = $nav->page($nav->type)->first();
                      $slug = $pageDetails->clean_url;
                      $pageurl = '/'.$slug;

                    } else if($nav->type == 2){
                      $blogDetails = $nav->page($nav->type)->first();
                      $slug = $blogDetails->clean_url;
                      $pageurl = '/blogs/'.$slug;
                    } else {
                      if($nav->static_page_name == "wineslisting"){
                        $pageurl = "/wines";
                      } else if($nav->static_page_name == "spiritslisting"){
                        $pageurl = "/spirits";
                      } else if($nav->static_page_name == "historylisting"){
                        $pageurl = "/history";
                      } else if($nav->static_page_name == "bloglisting"){
                        $pageurl = "/blog";

                      } else if($nav->static_page_name == "bloglisting"){
                        $pageurl = "/blogs";
                      } else if($nav->static_page_name == "routinelisting"){
                        $pageurl = "/routine/list";
                      } else if($nav->static_page_name == "contactus") {
                        $pageurl = "/contactus";
                      } else {
                        $pageurl = "/";
                      }
                    }


                  @endphp

                @php
                $page_url = $pageurl;
                $pageurl = strtok($pageurl,'?');
                $page_slug = trim($pageurl, '/');
                $active = Request::path() == $page_slug ? 'active': '';

                if($page_slug == 'home'){
                    $page_url = '/';
                }

                if(Request::path() == $pageurl){
                  $active = 'active';

                }
                @endphp
            <li class="{{ $active }} nav-item"><a href="{{url($page_url)}}" class="nav-link">{{$nav_title}}</a></li>
             @endforeach
        @endif
            </ul>
          </div>
        </div>
        <div class="col-sm-2 col-md-2 col-12">
          <div class="footer-logo mr-auto text-center">
            <img src="{{$footerlogo}}" alt="footer-logo">
          </div>
        </div>
        <div class="col-sm-5 col-md-5 col-12">
          <h3> Stay In Touch</h3>

          <div class="footer-social-links">
            <a href="{{$configwinefb}}" title="Facebook" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
            <a href="{{$configinsta}}" title="Instagram" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            <a href="{{$configtwiiter}}" title="Twitter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            <a href="{{$configyoutube}}" title="YouTube" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
            <a href="{{$configlinkedin}}" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="copyright text-center">
    <p>Design By IDEA Foundation</p>
  </div>
</footer>
<!-- / footer -->
  <div class='scrolltop'>
      <div class='scroll icon'><i class="fa fa-4x fa-angle-up"></i></div>
  </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('/') }}dist/js/jquery.js"></script>
    <script src="{{ asset('/') }}dist/js/bootstrap.min.js" ></script>
    <script src="{{ asset('/') }}dist/js/main.js"></script>
    <script src="{{ asset('/') }}dist/owlcarousel/owl.carousel.min.js"></script>
    <link href="{{ asset('/') }}dist/css/select2.min.css" rel="stylesheet">
    <script src="{{ asset('/') }}dist/js/select2.min.js" ></script>
    <script>
      $(document).ready(function(){
        $(".advanceSearch").click(function(e){
          var site_url = '<?php echo asset('/'); ?>';
          window.location.href = site_url+'search';
        });

        $('#search').on("click",(function(e){
        $(".form-group").addClass("sb-search-open");
          e.stopPropagation()
        }));
         $(document).on("click", function(e) {
          if ($(e.target).is("#search") === false && $(".form-control").val().length == 0) {
            $(".form-group").removeClass("sb-search-open");
          }
        });
        $(".form-control-submit").click(function(e){
            if($(".form-control").val().length != 0){
              e.preventDefault();
              var site_url = '<?php echo asset('/'); ?>';
              var value = $(".form-control").val();
              window.location.href = site_url+'search?search='+value;
            }
        })
      })

       $(document).ready(function() {
              $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                navText: ['<img src="{{ asset("/") }}dist/images/left-arrow-slider.svg" style="width:25px;"/>','<img src="{{ asset("/") }}dist/images/right-arrow-slider.svg" style="width:25px;"/>'] ,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
                    nav: true
                  },
                  600: {
                    items: 1,
                    nav: false
                  },
                  1000: {
                    items: 1,
                    nav: true,
                    loop: false,
                    margin: 20
                  }
                }
              })
            })

            $(window).scroll(function() {
      if ($(this).scrollTop() > 50 ) {
          $('.scrolltop:hidden').stop(true, true).fadeIn();
      } else {
          $('.scrolltop').stop(true, true).fadeOut();
      }
  });
  $(function(){
    $(".scroll").click(function(){
      $("html,body").animate({
        scrollTop:$(".thetop").offset().top
      }, '1000');
      return false })
  });
  $(".comment").on("keydown",function() {
      if($.trim($(this).val()).length > 5) {
        $(this).removeClass("border border-danger");
      }
  });
$(document).on('click', '.submitcomment', function() {
    let commentmsg = $(this).data('messagebox');
    let commentbox = $(this).data('commentbox');
    let comment_blog = $(this).data('blogto');
        $(commentmsg).hide();
        let formId = $(this).data('formid');
        let submit_url = $(this).data('submit_url');
        let formData = $(formId).serialize();
		var comment = $.trim($(commentbox).val());
        var blog_id = $(comment_blog).val();
		if (comment) {
			$(commentbox).removeClass("border border-danger");
			$.ajax({
                type: 'POST',
                url:submit_url,
                dataType: 'json',
				data: formData,
				success: function(response) {
                    $(commentmsg).show();
                    $(commentmsg).show().empty().html('<div class="text-success">'+response.message+'</div>');
                    $(commentbox).val('');
                },
                error: function(xhr, status, error){
                    let message = 'Failed to add comment';
                    if (xhr.status == 400) {
                        message = xhr.responseText.replace('["', '');
                        message = message.replace('"]', '');
                        $(commentmsg).show().empty().html('<div class="text-danger">'+message+'</div>');
                    }
                    $(commentmsg).show().empty().html('<div class="text-danger">'+message+'</div>');
                }
			});
		} else {
            console.log(commentbox);
            $(commentbox).addClass("border border-danger");
            $(commentmsg).show().empty().html('<div class="text-danger">Invalid comment data!</div>');
		}
  });
  $(document).on('click', '[data-add_replay]', function(){
    let replayBox = $(this).data('add_replay');
    if ($(replayBox).hasClass('d-none')) {
      $(replayBox).removeClass('d-none')
    } else {
      $(replayBox).addClass('d-none')
    }


  });
</script>
</body>
</html>

<!DOCTYPE html>
<html>
<head>

	<meta charset="UTF-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="description" content="">

    <meta name="author" content="">

    <link rel="icon" href="favicon.ico">

	<title>{{ config('constants.SITE_TITLE') }} <?php if(isset($title)){ echo " - ". $title; } ?></title>


	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="{{ asset('/') }}dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="{{ asset('/') }}dist/css/style.css" rel="stylesheet">

	<link rel="stylesheet" href="{{ asset('/') }}dist/fontawesome/css/font-awesome.css">
	<link rel="stylesheet" href="{{ asset('/') }}dist/owlcarousel/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="{{ asset('/') }}dist/owlcarousel/assets/owl.carousel.min.css">

	<script charset="utf-8" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script charset="utf-8" src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

</head>
<?php
use App\Http\Controllers\SiteController;
global $lang_val;
list($logo) = SiteController::wineLogo();
// active class for language
$activeClasssp = "";
$activeClassen = "";
if($lang_val=="es") {
	$activeClasssp = "active";
} else {
	$activeClassen = "active";
}
// active class for language
?>
 <body>

	<!-- header-->
  <div class='thetop'></div>
  <header class="header">
    <nav class="navbar navbar-expand-lg custom-navbar">
      <div class="container">
        <a href="{{url('/')}}"> <img class="navbar-brand" src="{{ $logo}}" id="logo_custom"></a>
        <button class="navbar-toggler navbar-toggler-right custom-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse navbar-expand-lg" id="collapsibleNavbar">
          <ul class="navbar-nav ml-auto">
            @php
				$navigations = App\Navigation::where([['is_visible','=',1],['show_header','=',1],['parent_id','=',0],['lang_code','=','en']])->orderBy('nav_order', 'asc')->get();
			@endphp
			@if(isset($navigations) && count($navigations)>0)
				@foreach($navigations as $nav)
					@php

						$nav_title = $nav->title;
						if($nav->type == 1){
							$pageDetails = $nav->page($nav->type)->first();
							$slug = $pageDetails->clean_url;
							$pageurl = '/'.$slug;

						} else if($nav->type == 2){
							$blogDetails = $nav->page($nav->type)->first();
							$slug = $blogDetails->clean_url;
							$pageurl = '/blogs/'.$slug;
						} else {
							if($nav->static_page_name == "wineslisting"){
								$pageurl = "/wines";
							} else if($nav->static_page_name == "historylisting"){
								$pageurl = "/history";
							} else if($nav->static_page_name == "spiritslisting"){
								$pageurl = "/spirits";
							} else if($nav->static_page_name == "bloglisting"){
								$pageurl = "/blog";
							} else if($nav->static_page_name == "routinelisting"){
								$pageurl = "/routine/list";
							} else if($nav->static_page_name == "contactus"){
								$pageurl = "/contactus";
							} else {
								$pageurl = "/";
							}
						}

						if($lang_val !='en'){
							$navDetails = App\Navigation::where([['parent_lang_id','=',$nav->id],['lang_code','=',$lang_val]])->first();

							if(!empty($navDetails)){
								$nav_title = $navDetails->title;
								if($navDetails->type == 1){
									$pageDetails = $navDetails->page($navDetails->type)->first();
									$slug = $pageDetails->clean_url;
									$pageurl = '/'.$slug;

								} else {
									$blogDetails = $navDetails->page($navDetails->type)->first();
									$slug = $blogDetails->clean_url;
									$pageurl = '/blogs/'.$slug;
								}
							}
						}



					@endphp

						@php
						$page_url = $pageurl;
						$pageurl = strtok($pageurl,'?');
						$page_slug = trim($pageurl, '/');
						$active = Request::path() == $page_slug ? 'active': '';

						if($page_slug == 'home'){
							$page_url = '/';
						}
						if(Request::path() == $pageurl){
							$active = 'active';

						}
						@endphp
						<li class="{{ $active }} nav-item">
							<a href="{{url($page_url)}}" class="nav-link"> {{$nav_title}}  </a>
				        </li>
				@endforeach
			@endif
          </ul>
           <div class="navbar-right">
            <form class="search-form" role="search">
              <div class="form-group pull-right" id="search">
                <input type="text" class="form-control" placeholder="Search Wine & Spirit">				
                <button type="submit" class="form-control form-control-submit">Submit</button>
                <span class="search-label"><i class="fa fa-search" aria-hidden="true"></i></span>
				<small class="advanceSearch">Advance search</small>
              </div>
			  
            </form>
          </div>
        </div>
      </div>
    </nav>
  </header>
  <!-- / header -->
<!--header-->

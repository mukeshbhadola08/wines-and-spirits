<ul class="nav nab-bar nav-icon">
	<li class="active">
		<a href="#">
			<i class="fa fa-user" aria-hidden="true"></i>&nbsp; Profile
		</a>
	</li>
	<?php if($user_type == "A" || $user_type == "S"){ ?>
	<li>
		<a href="/messages/list">
			<i class="fa fa-comment" aria-hidden="true"></i>&nbsp; Chat
		</a>
	</li>
	<?php } ?>

	<li>
		<a href="/my_request">
			<i class="fa fa-handshake-o" aria-hidden="true"></i>&nbsp; My Services Request
		</a>
	</li>

	<li>
		<a href="/invoices">
			<i class="fa fa-money" aria-hidden="true"></i>&nbsp; Transactions
		</a>
	</li>
</ul>
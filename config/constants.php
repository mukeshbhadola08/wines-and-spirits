<?php
return [
	'SITE_TITLE'			=> 'Property Manager',
	'LANGUAGE'				=> 'es',  //Dutch
	'ADDRESS'				=> '#1041, Disha Arcade, MDC sector 4',
	'CITY'					=> 'Panchkula',
	'STATE'					=> 'Haryana',
	'ZIPCODE'				=> '134114',
	'ADMIN_NAME'			=> 'Administrator',
	'ADMIN_EMAIL'			=> 'admin@admin.cim',
	'PHONE_NO'				=> 'xxx-xxxx-xxx',
	'YOUTUBE_URL'			=> 'https://www.youtube.com/',
	'FACEBOOK_URL'			=> 'https://facebook.com',
	'TWITTER_URL'			=> 'https://twitter.com',
	'INSTAGRAM_URL'			=> 'https://instagram.com',
	'LINKEDIN_URL'			=> 'https://linkedin.com',
	'GOOGLEPLUS_URL'		=> 'https://googleplus.com',
	'PAYPAL_BUSINESSEMAIL'	=> 'Administrator',
	'PAYPAL_APIUSERNAME'	=> 'Administrator',
	'PAYPAL_APIPASSWORD'	=> 'Administrator',
	'PAYPAL_MODE'			=>	1	// 1 Sandbox Mode, 0 Live Mode
];
?>

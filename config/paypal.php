<?php
/**
 * PayPal Setting & API Credentials
 * Created by Raza Mehdi <srmk@outlook.com>.
 */

return [
    'mode'    => 'sandbox', // Can only be 'sandbox' Or 'live'. If empty or invalid, 'live' will be used.
    'sandbox' => [
        'username'    => "baljinderaus_api1.ideafoundation.in",
        'password'    => "PHU6DHA3WYD6RE4F",
        'secret'      => "AFcWxV21C7fd0v3bYYYRCpSSRl31AKw8oN-U3jmElP0COvbHg5jDy09o",
        'certificate' => "",
        'app_id'      => 'APP-80W284485P519543T', // Used for testing Adaptive Payments API in sandbox mode
    ],
    'live' => [
        'username'    => "baljinderaus_api1.ideafoundation.in",
        'password'    => "PHU6DHA3WYD6RE4F",
        'secret'      => "AFcWxV21C7fd0v3bYYYRCpSSRl31AKw8oN-U3jmElP0COvbHg5jDy09o",
        'certificate' => "",
        'app_id'      => 'APP-80W284485P519543T', // Used for testing Adaptive Payments API in sandbox mode
    ],

    'payment_action' => 'Sale', // Can only be 'Sale', 'Authorization' or 'Order'
    'currency'       => 'USD',
    'notify_url'     => '', // Change this accordingly for your application.
    'locale'         => '', // force gateway language  i.e. it_IT, es_ES, en_US ... (for express checkout only)
    'validate_ssl'   => false, // Validate SSL when creating api client.
	'invoice_prefix' => "PROP"
];
